#include "psxtoasm.hpp"
#include <ctype.h>
#include <new.h>


ostream & operator << (ostream &s, Token *t )
{ 
	// s << '[' << t->Leading << '|' << t->Val << ']'; 
	s << t->Leading << t->Val; 
	return(s);
}
Tokenizer::Tokenizer()
{
	buff = NULL;
	close();
}

Errcode Tokenizer::Open(char *path)
{

	if(buff)
		return(Failure);


	if((buff = (char *)operator new(TBUFSIZE)) == NULL)
		return(Failure);


	get_token = get_cpp_token;
	fin.clear();
	fin.open(path);


	if(!fin)
	{
		close();
		return(Failure);
	}

	return(Success);
}
void Tokenizer::close()
{
	if(buff != NULL)
	{
		delete buff;
		fin.close();
	}
	buff = NULL;
	bend = NULL;
	bp = NULL;
	starting = TRUE;
	get_token = get_no_token;
	ReleaseToken(&tok);  		// release all tokens
}

Token *Tokenizer::get_no_token()
{
	return(NULL);
}
Token *Tokenizer::get_quote_token()
{
int c;

	tok.Val.val = bp;
	tok.type = tok.tquote;
	if(starting)
		++bp;

	for(;;)
	{
		if(bp >= bend)  // continued on subsequent line
		{
			get_token = get_quote_token;
			tok.flags |= tok.PARTIAL;
			break;
		}
		c = *bp++;
		if(c == '\\') // pass through char after a '\'
		{
			++bp;
			continue;
		}
		if(c == '"') // terminal '"'
		{
			get_token = get_cpp_token;
			break;
		}
	}
	tok.Val.len = bp - tok.Val.val;
	return(&tok);
}
Token *Tokenizer::get_cpp_token()
{
int c;

	tok.Leading.val = bp; // beginning of leading whitespace
	tok.type = tok.tword; // default type;

	for(;;)
	{
		if(bp >= bend)
		{
			tok.type = tok.tnewline;
			break;
		}
		c = *bp;
		if(isgraph(c))
			break;
		++bp;
	}
	tok.Leading.len = bp - tok.Leading.val; // set length


	tok.Val.val = bp;     // beginning of token

	while(bp < bend)
	{
	  	switch(c)
		{	 
			case '!':
			case '%':
			case '^':
			case '&':
			case '*':
			case '(':
			case ')':
			case '-':
			case '+':
			case '=':
			case '{':
			case '}':
			case '|':
			case '~':
			case '[':
			case ']':
			case ':':
			case '<':
			case '>':
			case '?':
			case ',':
			case '.':
			case '\\':
				if(bp != tok.Val.val) // these delimit prior strings 
					goto tok_done;
				goto operator_tok;
			case '$':
				if(bp != tok.Val.val) // these delimit prior strings 
					goto tok_done;
				tok.type = tok.tdollar;
				goto add_char;
			case '"':
				if(bp != tok.Val.val) // this delimits prior strings 
					goto tok_done;
				starting = TRUE;
				return(get_quote_token());
			case ';':
			{
				if(bp != tok.Val.val)
					goto tok_done;
				bp = bend; 
				tok.type = tok.tcomment; 
				goto tok_done;
			}
			default:
				if((!isgraph(c)))
					goto tok_done;
			add_char:
				c = *++bp;
				continue;
			operator_tok:
				tok.type = tok.toperator;
				++bp;
				goto tok_done;
		}
	}
tok_done:
	tok.Val.len = bp - tok.Val.val;
	return(&tok);

}

Token *Tokenizer::NextToken()
{
	if(bp >= bend) // no line, read next in
	{
		bend = bp = buff;
		if(!(fin.getline(bp,TBUFSIZE,'\n')))
			return(NULL);
		bend += strlen(bp);
		*bend++ = '\n';
		*bend = 0;
	}
	tok.Leading.len = 0;
	tok.flags = 0;
	(this->*get_token)();
	starting = FALSE;
	return(&tok);
}
Errcode Tokenizer::ReleaseToken(Token *t)
{
	if(!t)
		return(Failure);

	t->type = t->tnull;
	return(Success);
}
Token *Tokenizer::NextToken(Token *t)
{
	ReleaseToken(t);
	return(NextToken());
}
