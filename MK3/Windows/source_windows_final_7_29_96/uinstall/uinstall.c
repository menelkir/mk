/******************************************************************/
#include <windows.h>
#include <shlobj.h>
#include <shlguid.h>

extern void WINAPI DeleteExecutable(DWORD dwExitCode, BOOL fRemoveDir);

/********************************************************************\
|					RemoveFiles()
\********************************************************************/
 BOOL RemoveGroup(void)
 {
	WIN32_FIND_DATA _sFile;
	LPWIN32_FIND_DATA sFile;
 	HANDLE  hFile;
	char szDel[MAX_PATH];
	char szUninst[MAX_PATH];
	char szUnFile[60];
	char szFind[MAX_PATH];
	char szWinDir[MAX_PATH];
    DWORD ret;

	sFile = &_sFile;
	GetWindowsDirectory( szWinDir, MAX_PATH );
	strcat(szWinDir,"\\start menu\\programs\\Mortal Kombat III\\");
	strcpy(szFind,szWinDir);
	strcpy(szDel,szWinDir);
	strcat(szFind,"*.*");
	hFile=FindFirstFile(szFind,sFile);
	if(hFile==INVALID_HANDLE_VALUE)
	   return TRUE;
    strcat(szDel,sFile->cFileName);
	DeleteFile(szDel);
	while(FindNextFile(hFile,sFile) )
	{
		strcpy(szDel,  szWinDir);
		strcat(szDel,sFile->cFileName);
		if(!DeleteFile(szDel))
		{

		   ret=GetLastError();
		   if(ret==ERROR_ACCESS_DENIED)
		   {
			  char szBuff[300];
			  strcpy(szBuff,szDel);
			  strcat(szBuff,". Access denied.");
			  MessageBox(NULL,szBuff,"Uninstaller",0);
			  return FALSE;
		   }
		}
	}
	FindClose(hFile);
	RemoveDirectory(szWinDir);
	
	SHChangeNotify(SHCNE_RMDIR, SHCNF_PATH, szFind, 0);

	GlobalFree(sFile);
	return TRUE;
 }
int PASCAL WinMain(HANDLE hInstance, HANDLE hPrevInstance, LPSTR szCmdLineArgs, int nCmdShow)
{
	BOOL bretval;
	DWORD error;
	STARTUPINFO startinfo;
	PROCESS_INFORMATION pi;
	HKEY hKey;
	DWORD type,size;
	char *ptr;
	char path[MAX_PATH];
	char pathexe[MAX_PATH];
	char pathcfg[MAX_PATH];
	char pathunistall[MAX_PATH];
	char pathdoc[MAX_PATH];
	WIN32_FIND_DATA finddata;

	error = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						  "SOFTWARE\\Mortal Kombat III",                             
                           0,
						   KEY_ALL_ACCESS	,
                           &hKey);

    if (error == ERROR_SUCCESS)
	{
		size  = MAX_PATH;
		error = RegQueryValueEx(hKey, 
								"path", 
								NULL,
								&type, 
								path, 
								&size);

		RegDeleteKey(hKey, "" );

	    RegCloseKey(hKey);
	}
	else
	{
		GetCurrentDirectory(MAX_PATH,path);
	}

	// Kill any trailling stuff 
	for( ptr = path + (strlen(path)-1); ptr >= path ; *ptr = '\0', ptr--)
	{
		if ( *ptr == ' ' )
			continue;
		if ( *ptr == '\t' )
			continue;
		if ( *ptr == '\\' )
			continue;

		break;
	}

	error = RegOpenKeyEx (HKEY_LOCAL_MACHINE,
							"SOFTWARE\\Microsoft\\windows\\currentVersion\\uninstall",                             
                            0,
							KEY_ALL_ACCESS	,
                            &hKey);
	if (error == ERROR_SUCCESS)
    {
		RegDeleteKey(hKey,"Mortal Kombat III");
		RegCloseKey(hKey);
    }


	// Kill the files 
	strcpy( pathexe, path );
	strcpy( pathcfg, path );
	strcpy( pathunistall, path );
	strcpy( pathdoc, path );
	strcat( pathexe,"\\mk3w.exe" );
	strcat( pathcfg,"\\mk3w.cfg" );
	strcat( pathunistall,"\\uinstall.exe" );
	strcat( pathdoc,"\\mk3w95.doc" );
    SetFileAttributes( pathexe, FILE_ATTRIBUTE_NORMAL );
    SetFileAttributes( pathcfg, FILE_ATTRIBUTE_NORMAL );
    SetFileAttributes( pathunistall, FILE_ATTRIBUTE_NORMAL );
    SetFileAttributes( pathdoc, FILE_ATTRIBUTE_NORMAL );
	DeleteFile( pathexe );
	DeleteFile( pathcfg );
	DeleteFile( pathdoc );

	// Remove program group 
	RemoveGroup();

	// This should be interesting I'm deleting my self 
	DeleteExecutable(0, TRUE);
//	DeleteFile( pathunistall );
//	RemoveDirectory(path);

	return 0;
}


