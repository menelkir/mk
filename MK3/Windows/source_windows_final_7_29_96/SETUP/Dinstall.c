/*==========================================================================
 *
 *  Copyright (C) 1995 Microsoft Corporation.  All Rights Reserved.
 *
 *  File:       dinstall.c
 *  Content:    Game SDK sample setup program
 ***************************************************************************/
#include <windows.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <io.h>
#include <direct.h>
#include <assert.h>
#include <shlobj.h>
#include<shlguid.h>
#include "ddraw.h"
#include "dsetup.h"
#include "dinstall.h"

HWND hwndApp;
/*
 * list of files that will be copied from the source directory to
 * to the directory the game is created in
 */
static char* filename [] =
{
    "mk3w.EXE",
	"uinstall.exe",
	"mk3w95.doc"
};

/*
 * default directory to install game in
 */
static char gameDirectory [] = "C:\\MORTAL KOMBAT III\\";

/*
 * title of game installation
 */
static char title [] = "Mortal Kombat III Setup";
HPALETTE      hpalApp;
extern CHAR           achFileName[];
short xStartBmp=40;
short yStartBmp=40;

/*
 * prototypes
 */
BOOL FAR PASCAL masterDlgProc( HWND hdlg,DWORD message,DWORD wparam,DWORD lparam );
static void     getWorkingDirectory( HANDLE hinst,char* working_directory,DWORD working_directory_len );
static void     appendSlash( char* sz );
static void     stripSlash( char* sz );

void DrawBmp(HDC hdc,HBITMAP hBmp,short xStart,short yStart);

BOOL SetStartMenu(void);
BOOL AddShortcut(void);
BOOL LoadDrectXPal(LPSTR pFileName);
HANDLE ReadPal(LPSTR pName);

extern CreateLink(LPCSTR, LPSTR, LPSTR);

DWORD DetermineVersion(void);
BOOL AddGroup(void);
BOOL CreateFolder(LPSTR lpszFolder) ;
/*
 * globals
 */
static HANDLE   hinst;
static HGLOBAL  mk3;
static HGLOBAL  sonya;
static HDC      hdcMK3;
static HDC      hdcSonya;
static BITMAP   bmpMK3;
static BITMAP   bmpSonya;
char     workingDirectory[MAX_PATH];
static char     windowsDirectory[MAX_PATH];
static char     windowsSystemDirectory[MAX_PATH];
char     workingGameDirectory[MAX_PATH];
static char		uninstallpath[MAX_PATH];

/*
 * support functions
 */

/*
 * retrieves the path from which this program was launched.
 */
static void getWorkingDirectory( HANDLE hinst, char* working_directory, DWORD working_directory_len )
{
    char*       root_path_end;
    int         root_path_len;

    root_path_len = GetModuleFileName( hinst, workingDirectory, MAX_PATH );
    root_path_end = NULL;

    /*
     * remove the executable name from the module path
     */
    if( root_path_len != 0 )
    {
        root_path_len--;
        root_path_end = &workingDirectory[root_path_len];
    }

    while( ( root_path_end != NULL ) && ( root_path_len != 0 ) )
    {
        char    c;

        c = *root_path_end;
        if( ( c == '\\' ) || ( c == ':' ) )
        {
            root_path_end++;            /* keep the \ or : */
            *root_path_end = 0;
            break;
        }
        root_path_end--;
        root_path_len--;
    }
} /* getWorkingDirectory */

/*
 * appends a single slash to the end of the string
 */
static void appendSlash(LPSTR sz1)
{
    LPSTR sz2 = sz1;

    while (*sz2)
    {
        sz1 = sz2;
        sz2 = AnsiNext(sz1);
    }

    if (sz2 != sz1 + 1 || *sz1 != '\\')
    {
        *sz2++ = '\\';
        *sz2 = '\0';
    }
} /* appendSlash */

/*
 * removes slashes from the end of a string
 */
static void stripSlash(LPSTR sz1)
{
    LPSTR sz2 = sz1;

    while (*sz2)
    {
        sz1 = sz2;
        sz2 = AnsiNext(sz1);
    }

    if (sz2 == sz1 + 1
       && *sz1 == '\\')
    {
        *sz1 = '\0';
    }
} /* stripSlash */

/*
 * draws the specified bit onto the setup dialog
 */

/*
 * dlg proc for wizard dialog box, the setup is controlled from here.
 */
BOOL FAR PASCAL masterDlgProc(HWND hDlg,DWORD dwMessage,DWORD wParam,DWORD lParam)
{
    int         result;
    int         system_restart;
    PAINTSTRUCT paint_struct;
    static int  current_dialog;
    static HWND hwndDirections1;
    static HWND hwndDirections2;
    static HWND hwndEditText;
    static HWND hwndEdit;
    static HWND hwndNext;
    static HWND hwndReboot1;
    static HWND hwndReboot2;
    static HWND hwndHelp;
    static HWND hwndBack;
    static int  busy;
    static int  sonya_bitmap;
	HDC hdc;

    switch(dwMessage)
    {
    case WM_INITDIALOG:
        hwndApp=hDlg;
	//	mk3 = LoadBitmap( hinst, "mk3" );
    //  sonya = LoadBitmap( hinst, "sonya" );
        busy = 0;
        sonya_bitmap = 0;
        current_dialog = 0;
        SendMessage( hDlg, WM_SETTEXT, 0, (LONG)(void*)title );
        hwndDirections1 = GetDlgItem( hDlg, IDC_DIRECTIONS1 );
        hwndDirections2 = GetDlgItem( hDlg, IDC_DIRECTIONS2 );
        hwndEditText = GetDlgItem( hDlg, IDC_EDITTEXT );
        hwndEdit = GetDlgItem( hDlg, IDC_EDIT );
        hwndNext = GetDlgItem( hDlg, IDOK );
        hwndReboot1 = GetDlgItem( hDlg, IDC_REBOOT1 );
        hwndReboot2 = GetDlgItem( hDlg, IDC_REBOOT2 );
        hwndBack = GetDlgItem( hDlg, IDC_B );
        hwndHelp = GetDlgItem( hDlg, IDC_H );
        EnableWindow( hwndBack, 0 );
        EnableWindow( hwndHelp, 0 );

        /*
         * workingGameDirectory is where we will install the game
         * gameDirectory is the default location for installation
         */
        lstrcpy( workingGameDirectory, gameDirectory );
        stripSlash( workingGameDirectory );
        /*
         * limit the size of the input of this text field to the length of a path
         * put the default directory to install the game into in it
         * select the whole thing to make it easy for people to replace it
         * set the focus to it
         */
        SendDlgItemMessage( hDlg, IDC_EDIT, EM_LIMITTEXT, MAX_PATH, 0L);
        SetDlgItemText( hDlg, IDC_EDIT, (LPSTR)workingGameDirectory );
        SendDlgItemMessage( hDlg, IDC_EDIT, EM_SETSEL, 0, MAKELONG(256, 256) );
        SetFocus( hwndEdit );
        /*
         * return 0 here indicating we have set the focus for the dialog box
         * and it doesn't need to help us
         */
        return 0;

    case WM_PAINT:
        {
            hdc=BeginPaint( hDlg, &paint_struct );
			InitDIB (hDlg);
			AppPaint(hDlg,hdc,0,0);//xStartBmp,yStartBmp);
            EndPaint( hDlg, &paint_struct );
        }
        break;

	case WM_SETCURSOR:

		if ( busy > 0 )
		{
			SetCursor(IDC_WAIT);
			return TRUE;
		}

    case WM_COMMAND:
        switch(wParam)
        {
        case IDOK:
            if( busy > 0 )
            {
                /*
                 * busy bit keeps us from taking input while we are off doing
                 * things that can create other dialog boxes and end up causing
                 * us to be reentered.
                 */
                break;
            }
            else if( current_dialog == 0 )
            {
                int     a;

				SetCursor(IDC_WAIT);
                busy++;
                GetWindowText( GetDlgItem( hDlg,IDC_EDIT ),workingGameDirectory,MAX_PATH );
                stripSlash( workingGameDirectory );
                appendSlash( workingGameDirectory );

                system_restart = TRUE;
          		SetStartMenu();
                CreateDirectory( workingGameDirectory, NULL );
                for( a = 0; a < sizeof( filename )/sizeof( char* ); a++ )
                {
                    char    src[MAX_PATH];
                    char    dest[MAX_PATH];
            
                    lstrcpy( src, workingDirectory );
                    lstrcat( src, filename[a] );
                    lstrcpy( dest, workingGameDirectory );
                    lstrcat( dest, filename[a] );

					result = 0;
                    while( CopyFile( src, dest, 0 ) == 0 )
                    {
                        char    errorTemplate[] = "Setup Failure: %s could not be copied.";
                        char    errorText[MAX_PATH+sizeof(errorTemplate)];
    
                        wsprintf( errorText, errorTemplate, src );
                        result = MessageBox( hDlg, errorText, title, MB_RETRYCANCEL );
                        #ifdef DEBUG
                            wsprintf( errorText, errorTemplate, dest );
                            MessageBox( hDlg, errorText, title, 0 );
                        #endif
                        if( result == IDCANCEL )
                        {
                            result = -1;
                            break;
                        }
                    }

                    if( result >= 0 )
                    {
                        SetFileAttributes( dest, FILE_ATTRIBUTE_NORMAL );
                    }

					// Set the registry 
                    if( result >= 0 )
					{
						DWORD error;
						HKEY hKey;
						DWORD dwDisposition;
					
					    error = RegCreateKeyEx( HKEY_LOCAL_MACHINE, 
												"\\SOFTWARE\\Mortal Kombat III",                             
												0,
												"MK3", 
												REG_OPTION_NON_VOLATILE,
												KEY_ALL_ACCESS,
												NULL,
												&hKey, 
												&dwDisposition
						                        );
					
						error = RegSetValueEx(	hKey, 
												"path", 
												0, 
												REG_SZ,
					                            (BYTE *)&workingGameDirectory,
					                            strlen(workingGameDirectory)
					                           );

	
						RegFlushKey(hKey);        
						RegCloseKey(hKey);

						strcpy(uninstallpath, workingGameDirectory);
						strcat(uninstallpath,"uinstall.exe");

					    error = RegCreateKeyEx( HKEY_LOCAL_MACHINE, 
												"SOFTWARE\\Microsoft\\windows\\currentVersion\\uninstall\\Mortal Kombat III",                             
												0,
												"MK3", 
												REG_OPTION_NON_VOLATILE,
												KEY_ALL_ACCESS,
												NULL,
												&hKey, 
												&dwDisposition
						                        );

						error = RegSetValueEx(	hKey, 
												"DisplayName", 
												0, 
												REG_SZ,
					                            (BYTE *)"Mortal Kombat III",
					                            strlen("Mortal Kombat III")
					                           );
						error = RegSetValueEx(	hKey, 
												"UninstallString", 
												0, 
												REG_SZ,
					                            (BYTE *)uninstallpath,
					                            strlen(uninstallpath)
					                           );
						RegFlushKey(hKey);        
						RegCloseKey(hKey);
					}
                }


				// Setup Direct X 
                if( result >= 0 )
				{
	                result = DirectXSetup( hDlg, workingDirectory, DSETUP_DIRECTX );
//	                result = DirectXSetup( hDlg, workingDirectory, windowsDirectory, windowsSystemDirectory, DSETUP_DIRECTX );
		            if( result < 0 )
			        {
				        if( result != DSETUPERR_BADWINDOWSVERSION )
					    {
						    MessageBox( hDlg, "DirectX failed to install.", title, 0 );
						}
					}
				}

                if( result >= 0 )
                {
                        MessageBox( hDlg, "Mortal Kombat III has been successfully installed.", title, 0 );
                        current_dialog++;																	
        
                        /*
                         * hide current controls
                         */
                        ShowWindow( hwndDirections1, SW_HIDE );
                        ShowWindow( hwndDirections2, SW_HIDE );
                        ShowWindow( hwndEdit, SW_HIDE );
                        ShowWindow( hwndEditText, SW_HIDE );
        
                        if( system_restart )
                        {
                            /*
                             * show new dialogs
                             */
							FreeDib();
							xStartBmp=400;
							yStartBmp=40;
							strcpy(achFileName,"sonya.bmp");
							InvalidateRect(hDlg,NULL,FALSE);
                            ShowWindow( hwndReboot1, SW_SHOW );
                            ShowWindow( hwndReboot2, SW_SHOW );
                            SendMessage( hwndNext, WM_SETTEXT, 0, (LONG)"&Reboot" );
                            sonya_bitmap++;
                        }
                        else
                        {
                            busy--;
                            EndDialog( hDlg, 0 );
                            break;
                        }
                }

                if( result < 0 )
                {
                    busy--;
                    EndDialog( hDlg, result );
                    break;
                }
            }
            else
            {
                /*
                 * restart windows, kill apps that aren't responding, reboot
                 */
                ExitWindowsEx( EWX_REBOOT, 0 );
            }
            busy--;
            break;

        case IDCANCEL:
            if( busy <= 0 )
            {
                /*
                 * only allow cancel if we aren't doing anything else
                 */
                EndDialog( hDlg, -1 );
            }
            break;

        }
    }
    return 0;
}

/* **************************************************************** */
int PASCAL WinMain(HANDLE hInstance, HANDLE hPrevInstance, LPSTR szCmdLineArgs,
        int nCmdShow)
{
    hinst = hInstance;

	{
	#define VER_PLATFORM_WIN32_WINDOWS 1
	OSVERSIONINFO version;
	version.dwOSVersionInfoSize = sizeof(version);
	GetVersionEx(&version);
	if ( version.dwPlatformId != VER_PLATFORM_WIN32_WINDOWS && version.dwMajorVersion < 4 )
	{
		MessageBox( NULL, "The Windows version of Mortal Kombat III can only run under Win95","MKIII ERROR",MB_OK | MB_ICONEXCLAMATION	);
		return(0);
	}
	}

    /*
     * retrieve the path to the directory that this program
     * was launched from, that will become the workingDirectory
     */
    getWorkingDirectory( hInstance, workingDirectory, MAX_PATH );

    /*
     * where is windows?
     */
    GetWindowsDirectory( windowsDirectory, MAX_PATH );
    stripSlash( windowsDirectory );
    appendSlash( windowsDirectory );
    GetSystemDirectory( windowsSystemDirectory, MAX_PATH );
    stripSlash( windowsSystemDirectory );
    appendSlash( windowsSystemDirectory );

    /*
     * check if there is enough space to install the game
     * NOTE: there is always enough space at the moment :-)
     */
    if( 1 )
    {
        /*
         * load Mortal Kombat III & Sonya bitmaps
         * display MK3, save Sonya for reboot screen!
         */

       // LoadDrectXPal("DirectX.pal");
     //   mk3 = LoadBitmap( hinst, "mk3" );
     //   sonya = LoadBitmap( hinst, "sonya" );
        #ifdef DEBUG
            if( !mk3 )
            {
                MessageBox( 0, "no mk3.bmp", 0, 0 );
            }
            if( !sonya )
            {
                MessageBox( 0, "no sonya", 0, 0 );
            }
        #endif

        /*
         * do the setup thing, it is all one big dialog box that you show
         * and hide things from depending on the screen
         * we just sign on, ask where to install, and install
         */
		 CoInitialize(NULL);
		 DialogBox( hInstance, "DLG_MASTER", NULL, masterDlgProc );
		 CoUninitialize();


        /*
         * clean up our MK3 and Sonya bitmap hdc's and object stuff
         */
        if( hdcMK3 )
        {
            DeleteDC( hdcMK3 );
        }
        if( hdcSonya )
        {
            DeleteDC( hdcSonya );
        }

        if( mk3 )
        {
            DeleteObject( mk3 );
            mk3 = NULL;
        }
        if( sonya )
        {
            DeleteObject( sonya );
            sonya = NULL;
        }
    }
    return 0;
} /* WinMain */

 /***********************************************************************\
|						SetStartMenu
\***********************************************************************/
BOOL SetStartMenu(void)
{

 DWORD       g_dwPlatform;
	// Find out what OS we're running on in case we have to call any platform
    // specific code.
    g_dwPlatform = DetermineVersion();

	AddGroup();
	AddShortcut();
 return TRUE;
}
 /************************************************************************\
 |					AddShortcut()
 \************************************************************************/
 BOOL AddShortcut(void)
 {
	 char szPath[MAX_PATH];
	 char szSourcePath[MAX_PATH];

	strcpy(szPath,windowsDirectory);
	strcat(szPath,"start menu\\programs\\Mortal Kombat III\\MK3W.exe.lnk");
	strcpy(szSourcePath,workingGameDirectory);
	strcat(szSourcePath,"MK3W.exe");	 

	 CreateLink(szSourcePath,szPath,0);

	 return TRUE;
 }


/**************************************************************************

   DetermineVersion()

**************************************************************************/

DWORD DetermineVersion(void)
{
OSVERSIONINFO os;

GetVersionEx(&os);
return (os.dwPlatformId);
}
/******************************************************************\
|					AddGroup()
\******************************************************************/
BOOL AddGroup(void)
{

 char szPath[MAX_PATH];

 strcpy(szPath,windowsDirectory);
 strcat(szPath,"start menu\\programs\\Mortal Kombat III");
 //create the folder
CreateFolder(szPath);
return TRUE;
}
/**************************************************************************

   CreateFolder()

**************************************************************************/

BOOL CreateFolder(LPSTR lpszFolder) 
{ 
BOOL i;
//create the folder
i=CreateDirectory(lpszFolder, NULL);

//notify the shell that you made a change
SHChangeNotify(SHCNE_MKDIR, SHCNF_PATH, lpszFolder, 0);

return TRUE;
}
