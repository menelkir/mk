# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

!IF "$(CFG)" == ""
CFG=Setup - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to Setup - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Setup - Win32 Release" && "$(CFG)" != "Setup - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "Setup.mak" CFG="Setup - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Setup - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Setup - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "Setup - Win32 Debug"
MTL=mktyplib.exe
RSC=rc.exe
CPP=cl.exe

!IF  "$(CFG)" == "Setup - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "WinRel"
# PROP BASE Intermediate_Dir "WinRel"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "WinRel"
# PROP Intermediate_Dir "WinRel"
OUTDIR=.\WinRel
INTDIR=.\WinRel

ALL : "$(OUTDIR)\Setup.exe" "$(OUTDIR)\Setup.bsc"

CLEAN : 
	-@erase ".\WinRel\Setup.bsc"
	-@erase ".\WinRel\SHOWDIB.SBR"
	-@erase ".\WinRel\DIB.SBR"
	-@erase ".\WinRel\link.sbr"
	-@erase ".\WinRel\DINSTALL.SBR"
	-@erase ".\WinRel\Setup.exe"
	-@erase ".\WinRel\DINSTALL.OBJ"
	-@erase ".\WinRel\SHOWDIB.OBJ"
	-@erase ".\WinRel\DIB.OBJ"
	-@erase ".\WinRel\link.obj"
	-@erase ".\WinRel\DINSTALL.res"
	-@erase ".\WinRel\Setup.map"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /W3 /GX /O2 /I "..\emulator\inc" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /c
CPP_PROJ=/nologo /ML /W3 /GX /O2 /I "..\emulator\inc" /D "WIN32" /D "NDEBUG" /D\
 "_WINDOWS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/Setup.pch" /YX /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\WinRel/
CPP_SBRS=.\WinRel/
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
RSC_PROJ=/l 0x409 /fo"$(INTDIR)/DINSTALL.res" /d "NDEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Setup.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/SHOWDIB.SBR" \
	"$(INTDIR)/DIB.SBR" \
	"$(INTDIR)/link.sbr" \
	"$(INTDIR)/DINSTALL.SBR"

"$(OUTDIR)\Setup.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 ..\lib\ddraw.lib ole32.lib dsetup.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib uuid.lib /nologo /subsystem:windows /map /machine:I386
LINK32_FLAGS=..\lib\ddraw.lib ole32.lib dsetup.lib kernel32.lib user32.lib\
 gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib uuid.lib /nologo\
 /subsystem:windows /incremental:no /pdb:"$(OUTDIR)/Setup.pdb"\
 /map:"$(INTDIR)/Setup.map" /machine:I386 /def:".\DINSTALL.DEF"\
 /out:"$(OUTDIR)/Setup.exe" 
DEF_FILE= \
	".\DINSTALL.DEF"
LINK32_OBJS= \
	"$(INTDIR)/DINSTALL.OBJ" \
	"$(INTDIR)/SHOWDIB.OBJ" \
	"$(INTDIR)/DIB.OBJ" \
	"$(INTDIR)/link.obj" \
	"$(INTDIR)/DINSTALL.res"

"$(OUTDIR)\Setup.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Setup - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "."
# PROP BASE Intermediate_Dir "."
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "."
# PROP Intermediate_Dir "."
OUTDIR=.\.
INTDIR=.\.

ALL : "$(OUTDIR)\Setup.exe" "$(OUTDIR)\Setup.bsc"

CLEAN : 
	-@erase ".\vc40.pdb"
	-@erase ".\vc40.idb"
	-@erase ".\Setup.bsc"
	-@erase ".\DINSTALL.SBR"
	-@erase ".\link.sbr"
	-@erase ".\SHOWDIB.SBR"
	-@erase ".\DIB.SBR"
	-@erase ".\Setup.exe"
	-@erase ".\DINSTALL.OBJ"
	-@erase ".\link.obj"
	-@erase ".\SHOWDIB.OBJ"
	-@erase ".\DIB.OBJ"
	-@erase ".\DINSTALL.res"
	-@erase ".\Setup.ilk"
	-@erase ".\Setup.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /W3 /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "..\emulator\inc" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /c
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "..\emulator\inc" /D "WIN32" /D\
 "_DEBUG" /D "_WINDOWS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/Setup.pch" /YX\
 /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\./
CPP_SBRS=.\./
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
RSC_PROJ=/l 0x409 /fo"$(INTDIR)/DINSTALL.res" /d "_DEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Setup.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/DINSTALL.SBR" \
	"$(INTDIR)/link.sbr" \
	"$(INTDIR)/SHOWDIB.SBR" \
	"$(INTDIR)/DIB.SBR"

"$(OUTDIR)\Setup.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 ..\lib\ddraw.lib ole32.lib dsetup.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib uuid.lib /nologo /subsystem:windows /debug /machine:I386
LINK32_FLAGS=..\lib\ddraw.lib ole32.lib dsetup.lib kernel32.lib user32.lib\
 gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib uuid.lib /nologo\
 /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)/Setup.pdb" /debug\
 /machine:I386 /def:".\DINSTALL.DEF" /out:"$(OUTDIR)/Setup.exe" 
DEF_FILE= \
	".\DINSTALL.DEF"
LINK32_OBJS= \
	"$(INTDIR)/DINSTALL.OBJ" \
	"$(INTDIR)/link.obj" \
	"$(INTDIR)/SHOWDIB.OBJ" \
	"$(INTDIR)/DIB.OBJ" \
	"$(INTDIR)/DINSTALL.res"

"$(OUTDIR)\Setup.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "Setup - Win32 Release"
# Name "Setup - Win32 Debug"

!IF  "$(CFG)" == "Setup - Win32 Release"

!ELSEIF  "$(CFG)" == "Setup - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\DINSTALL.C
DEP_CPP_DINST=\
	".\..\emulator\inc\ddraw.h"\
	".\dsetup.h"\
	".\dinstall.h"\
	

"$(INTDIR)\DINSTALL.OBJ" : $(SOURCE) $(DEP_CPP_DINST) "$(INTDIR)"

"$(INTDIR)\DINSTALL.SBR" : $(SOURCE) $(DEP_CPP_DINST) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\DINSTALL.RC
DEP_RSC_DINSTA=\
	".\setup.ico"\
	".\dinstall.h"\
	

"$(INTDIR)\DINSTALL.res" : $(SOURCE) $(DEP_RSC_DINSTA) "$(INTDIR)"
   $(RSC) $(RSC_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\DINSTALL.DEF

!IF  "$(CFG)" == "Setup - Win32 Release"

!ELSEIF  "$(CFG)" == "Setup - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\link.cpp

"$(INTDIR)\link.obj" : $(SOURCE) "$(INTDIR)"

"$(INTDIR)\link.sbr" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\SHOWDIB.C
DEP_CPP_SHOWD=\
	".\SHOWDIB.H"\
	

"$(INTDIR)\SHOWDIB.OBJ" : $(SOURCE) $(DEP_CPP_SHOWD) "$(INTDIR)"

"$(INTDIR)\SHOWDIB.SBR" : $(SOURCE) $(DEP_CPP_SHOWD) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\DIB.C
DEP_CPP_DIB_C=\
	".\SHOWDIB.H"\
	

"$(INTDIR)\DIB.OBJ" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"

"$(INTDIR)\DIB.SBR" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"


# End Source File
# End Target
# End Project
################################################################################
