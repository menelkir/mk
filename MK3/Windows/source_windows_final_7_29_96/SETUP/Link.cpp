  	 #define STRICT
#include <windows.h>
#include <windowsx.h>
#include <shlobj.h>


  extern "C" {

HRESULT CreateLink(LPCSTR, LPSTR, LPSTR);
 extern char workingDirectory[MAX_PATH];
 extern char workingGameDirectory[];
 }
/**************************************************************************

   CreateLink()

   uses the shell's IShellLink and IPersistFile interfaces to create and 
   store a shortcut to the specified object. 
 
   Returns the result of calling the member functions of the interfaces. 
 
   lpszPathObj - address of a buffer containing the path of the object 

   lpszPathLink - address of a buffer containing the path where the shell 
      link is to be stored 

   lpszDesc - address of a buffer containing the description of the shell 
      link 
 
**************************************************************************/

HRESULT CreateLink(  LPCSTR lpszSource, 
                     LPSTR lpszTarget, 
                     LPSTR lpszDesc) 
{ 
HRESULT hres; 
IShellLink* pShellLink; 
//char szWorkingDir[]="c:\\bubsy";

//CoInitialize must be called before this
// Get a pointer to the IShellLink interface. 
hres = CoCreateInstance(   CLSID_ShellLink, 
                           NULL, 
                           CLSCTX_INPROC_SERVER, 
                           IID_IShellLink, 
                           (LPVOID*)&pShellLink); 

if (SUCCEEDED(hres)) 
   { 
   IPersistFile* pPersistFile; 
   // Set the path to the shortcut target, and add the description. 
   pShellLink->SetPath(lpszSource); 
   pShellLink->SetDescription(lpszDesc); 
   pShellLink->SetWorkingDirectory(workingGameDirectory);
//   pShellLink->SetWorkingDirectory(szWorkingDir);
   // Query IShellLink for the IPersistFile interface for saving the 
   // shortcut in persistent storage. 
   hres = pShellLink->QueryInterface(IID_IPersistFile, (LPVOID*)&pPersistFile); 

   if (SUCCEEDED(hres)) 
      { 
      WCHAR wsz[MAX_PATH]; 

      // Ensure that the string is ANSI. 
      MultiByteToWideChar( CP_ACP, 
                           0, 
                           lpszTarget, 
                           -1, 
                           wsz, 
                           MAX_PATH); 

      // Save the link by calling IPersistFile::Save. 
      hres = pPersistFile->Save(wsz, TRUE); 
 
     // if(FAILED(hres)) 
     //    ErrorHandler();

      pPersistFile->Release(); 
      } 

   pShellLink->Release(); 
   } 

return hres; 
}

