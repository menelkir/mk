Sequence: 0 Name: GS_DEATH_GIRL
 PATCH: #: 82 Name: pat_pbdeath 
  MAP: #: 82 Name: map_pbdeath 
  VAG: #: 40 Name: vag_pbdeath File: pbdeath.vag
   VAG SIZE: 19664
   VAG RATE: 12500
   VAG POS: 346112
Sequence: 1 Name: GS_DEATH_VP
 PATCH: #: 67 Name: pat_vpdeath 
  MAP: #: 67 Name: map_vpdeath 
  VAG: #: 51 Name: vag_vpdeath File: vpdeath.vag
   VAG SIZE: 22656
   VAG RATE: 11000
   VAG POS: 440320
Sequence: 2 Name: GS_DEATH_DF
 PATCH: #: 199 Name: pat_dfdeath 
  MAP: #: 199 Name: map_dfdeath 
  VAG: #: 62 Name: vag_dfdeath File: dfdeath.vag
   VAG SIZE: 13872
   VAG RATE: 12500
   VAG POS: 542720
Sequence: 3 Name: GS_DEATH_EB
 PATCH: #: 45 Name: pat_jhdeath 
  MAP: #: 45 Name: map_jhdeath 
  VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
   VAG SIZE: 16000
   VAG RATE: 12000
   VAG POS: 739328
Sequence: 4 Name: GS_DEATH_JH
 PATCH: #: 216 Name: pat_ebldeath 
  MAP: #: 216 Name: map_ebldeath 
  VAG: #: 74 Name: vag_ebldeath File: ebldeath.vag
   VAG SIZE: 19952
   VAG RATE: 12500
   VAG POS: 634880
Sequence: 5 Name: GS_DEATH_ROBO
 PATCH: #: 45 Name: pat_jhdeath 
  MAP: #: 45 Name: map_jhdeath 
  VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
   VAG SIZE: 16000
   VAG RATE: 12000
   VAG POS: 739328
Sequence: 6 Name: GS_DEATH_KANG
 PATCH: #: 29 Name: pat_lkdeath 
  MAP: #: 29 Name: map_lkdeath 
  VAG: #: 122 Name: vag_lkdeath File: lkdeath.vag
   VAG SIZE: 22192
   VAG RATE: 11000
   VAG POS: 1005568
Sequence: 7 Name: GS_DEATH_SG
 PATCH: #: 240 Name: pat_amdeath 
  MAP: #: 240 Name: map_amdeath 
  VAG: #: 231 Name: vag_amdeath File: amdeath.vag
   VAG SIZE: 26272
   VAG RATE: 15624
   VAG POS: 2115584
Sequence: 8 Name: GS_DEATH_SK
 PATCH: #: 260 Name: pat_skwast2 
  MAP: #: 260 Name: map_skwast2 
  VAG: #: 251 Name: vag_skwast2 File: skwast2.vag
   VAG SIZE: 10128
   VAG RATE: 15624
   VAG POS: 2289664
Sequence: 9 Name: GS_SHOOK_GIRL
 PATCH: #: 92 Name: pat_pbwast1 
  MAP: #: 92 Name: map_pbwast1 
  VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
   VAG SIZE: 9520
   VAG RATE: 15624
   VAG POS: 430080
Sequence: 10 Name: GS_SHOOK_VP
 PATCH: #: 68 Name: pat_vpnotbut 
  MAP: #: 68 Name: map_vpnotbut 
  VAG: #: 52 Name: vag_vpnotbut File: vpnotbut.vag
   VAG SIZE: 17536
   VAG RATE: 11000
   VAG POS: 464896
Sequence: 11 Name: GS_SHOOK_DF
 PATCH: #: 68 Name: pat_vpnotbut 
  MAP: #: 68 Name: map_vpnotbut 
  VAG: #: 52 Name: vag_vpnotbut File: vpnotbut.vag
   VAG SIZE: 17536
   VAG RATE: 11000
   VAG POS: 464896
Sequence: 12 Name: GS_SHOOK_EB
 PATCH: #: 97 Name: pat_ebshook 
  MAP: #: 97 Name: map_ebshook 
  VAG: #: 75 Name: vag_ebshook File: ebshook.vag
   VAG SIZE: 12560
   VAG RATE: 12500
   VAG POS: 655360
Sequence: 13 Name: GS_SHOOK_JH
 PATCH: #: 97 Name: pat_ebshook 
  MAP: #: 97 Name: map_ebshook 
  VAG: #: 75 Name: vag_ebshook File: ebshook.vag
   VAG SIZE: 12560
   VAG RATE: 12500
   VAG POS: 655360
Sequence: 14 Name: GS_SHOOK_ROBO
 PATCH: #: 46 Name: pat_robtalk5 
  MAP: #: 46 Name: map_robtalk5 
  VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
   VAG SIZE: 10064
   VAG RATE: 15624
   VAG POS: 942080
Sequence: 15 Name: GS_SHOOK_KANG
 PATCH: #: 30 Name: pat_lkrun 
  MAP: #: 30 Name: map_lkrun 
  VAG: #: 123 Name: vag_lkrun File: lkrun.vag
   VAG SIZE: 10592
   VAG RATE: 12500
   VAG POS: 1028096
Sequence: 16 Name: GS_SHOOK_SG
 PATCH: #: 257 Name: pat_amwast1 
  MAP: #: 257 Name: map_amwast1 
  VAG: #: 248 Name: vag_amwast1 File: amwast1.vag
   VAG SIZE: 12256
   VAG RATE: 15624
   VAG POS: 2254848
Sequence: 17 Name: GS_SHOOK_SK
 PATCH: #: 259 Name: pat_skwast1 
  MAP: #: 259 Name: map_skwast1 
  VAG: #: 250 Name: vag_skwast1 File: skwast1.vag
   VAG SIZE: 9040
   VAG RATE: 15624
   VAG POS: 2279424
Sequence: 18 Name: GS_RUN_GIRL
 PATCH: #: 83 Name: pat_pbrun 
  MAP: #: 83 Name: map_pbrun 
  VAG: #: 41 Name: vag_pbrun File: pbrun.vag
   VAG SIZE: 17536
   VAG RATE: 15624
   VAG POS: 366592
Sequence: 19 Name: GS_RUN_VP
 PATCH: #: 69 Name: pat_vprun 
  MAP: #: 69 Name: map_vprun 
  VAG: #: 53 Name: vag_vprun File: vprun.vag
   VAG SIZE: 11568
   VAG RATE: 15624
   VAG POS: 483328
Sequence: 20 Name: GS_RUN_DF
 PATCH: #: 200 Name: pat_dfrun 
  MAP: #: 200 Name: map_dfrun 
  VAG: #: 63 Name: vag_dfrun File: dfrun.vag
   VAG SIZE: 14656
   VAG RATE: 15624
   VAG POS: 557056
Sequence: 21 Name: GS_RUN_EB
 PATCH: #: 217 Name: pat_eblrun 
  MAP: #: 217 Name: map_eblrun 
  VAG: #: 76 Name: vag_eblrun File: eblrun.vag
   VAG SIZE: 11648
   VAG RATE: 15624
   VAG POS: 669696
Sequence: 22 Name: GS_RUN_JH
 PATCH: #: 98 Name: pat_jhrun 
  MAP: #: 98 Name: map_jhrun 
  VAG: #: 88 Name: vag_jhrun File: jhrun.vag
   VAG SIZE: 9328
   VAG RATE: 12500
   VAG POS: 755712
Sequence: 23 Name: GS_RUN_ROBO
 PATCH: #: 46 Name: pat_robtalk5 
  MAP: #: 46 Name: map_robtalk5 
  VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
   VAG SIZE: 10064
   VAG RATE: 15624
   VAG POS: 942080
Sequence: 24 Name: GS_RUN_KANG
 PATCH: #: 30 Name: pat_lkrun 
  MAP: #: 30 Name: map_lkrun 
  VAG: #: 123 Name: vag_lkrun File: lkrun.vag
   VAG SIZE: 10592
   VAG RATE: 12500
   VAG POS: 1028096
Sequence: 25 Name: GS_RUN_SG
 PATCH: #: 241 Name: pat_amrun 
  MAP: #: 241 Name: map_amrun 
  VAG: #: 232 Name: vag_amrun File: amrun.vag
   VAG SIZE: 10720
   VAG RATE: 15624
   VAG POS: 2142208
Sequence: 26 Name: GS_RUN_SK
 PATCH: #: 241 Name: pat_amrun 
  MAP: #: 241 Name: map_amrun 
  VAG: #: 232 Name: vag_amrun File: amrun.vag
   VAG SIZE: 10720
   VAG RATE: 15624
   VAG POS: 2142208
Sequence: 27 Name: GS_TV_GIRL
 PATCH: #: 84 Name: pat_pbtrip 
  MAP: #: 84 Name: map_pbtrip 
  VAG: #: 42 Name: vag_pbtrip File: pbtrip.vag
   VAG SIZE: 3552
   VAG RATE: 15624
   VAG POS: 385024
Sequence: 28 Name: GS_TV_VP
 PATCH: #: 70 Name: pat_vpgrab 
  MAP: #: 70 Name: map_vpgrab 
  VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
   VAG SIZE: 2864
   VAG RATE: 15624
   VAG POS: 495616
Sequence: 29 Name: GS_TV_DF
 PATCH: #: 47 Name: pat_dftrip 
  MAP: #: 47 Name: map_dftrip 
  VAG: #: 64 Name: vag_dftrip File: dftrip.vag
   VAG SIZE: 4784
   VAG RATE: 15624
   VAG POS: 573440
Sequence: 30 Name: GS_TV_EB
 PATCH: #: 218 Name: pat_ebtrip 
  MAP: #: 218 Name: map_ebtrip 
  VAG: #: 77 Name: vag_ebtrip File: ebtrip.vag
   VAG SIZE: 3072
   VAG RATE: 15624
   VAG POS: 681984
Sequence: 31 Name: GS_TV_JH
 PATCH: #: 99 Name: pat_jhtrip 
  MAP: #: 99 Name: map_jhtrip 
  VAG: #: 89 Name: vag_jhtrip File: jhtrip.vag
   VAG SIZE: 2912
   VAG RATE: 12500
   VAG POS: 765952
Sequence: 32 Name: GS_TV_KANG
 PATCH: #: 31 Name: pat_lktrip 
  MAP: #: 31 Name: map_lktrip 
  VAG: #: 124 Name: vag_lktrip File: lktrip.vag
   VAG SIZE: 4368
   VAG RATE: 15624
   VAG POS: 1040384
Sequence: 33 Name: GS_TV_SG
 PATCH: #: 242 Name: pat_amtrip 
  MAP: #: 242 Name: map_amtrip 
  VAG: #: 233 Name: vag_amtrip File: amtrip.vag
   VAG SIZE: 3552
   VAG RATE: 15624
   VAG POS: 2154496
Sequence: 34 Name: GS_TV_SK
 PATCH: #: 246 Name: pat_skreact1 
  MAP: #: 246 Name: map_skreact1 
  VAG: #: 237 Name: vag_skreact1 File: skreact1.vag
   VAG SIZE: 4368
   VAG RATE: 15624
   VAG POS: 2174976
Sequence: 35 Name: GS_FHV_GIRL1
 PATCH: #: 85 Name: pat_pbface1 
  MAP: #: 85 Name: map_pbface1 
  VAG: #: 43 Name: vag_pbface1 File: pbface1.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 389120
Sequence: 36 Name: GS_FHV_GIRL2
 PATCH: #: 86 Name: pat_pbface2 
  MAP: #: 86 Name: map_pbface2 
  VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 395264
Sequence: 37 Name: GS_FHV_GIRL3
 PATCH: #: 86 Name: pat_pbface2 
  MAP: #: 86 Name: map_pbface2 
  VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 395264
Sequence: 38 Name: GS_FHV_VP1
 PATCH: #: 71 Name: pat_vpface1 
  MAP: #: 71 Name: map_vpface1 
  VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 499712
Sequence: 39 Name: GS_FHV_VP2
 PATCH: #: 71 Name: pat_vpface1 
  MAP: #: 71 Name: map_vpface1 
  VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 499712
Sequence: 40 Name: GS_FHV_VP3
 PATCH: #: 72 Name: pat_vpface2 
  MAP: #: 72 Name: map_vpface2 
  VAG: #: 56 Name: vag_vpface2 File: vpface2.vag
   VAG SIZE: 5200
   VAG RATE: 15624
   VAG POS: 505856
Sequence: 41 Name: GS_FHV_DF1
 PATCH: #: 201 Name: pat_dfface1 
  MAP: #: 201 Name: map_dfface1 
  VAG: #: 65 Name: vag_dfface1 File: dfface1.vag
   VAG SIZE: 4304
   VAG RATE: 15624
   VAG POS: 579584
Sequence: 42 Name: GS_FHV_DF2
 PATCH: #: 201 Name: pat_dfface1 
  MAP: #: 201 Name: map_dfface1 
  VAG: #: 65 Name: vag_dfface1 File: dfface1.vag
   VAG SIZE: 4304
   VAG RATE: 15624
   VAG POS: 579584
Sequence: 43 Name: GS_FHV_DF3
 PATCH: #: 202 Name: pat_dfface2 
  MAP: #: 202 Name: map_dfface2 
  VAG: #: 66 Name: vag_dfface2 File: dfface2.vag
   VAG SIZE: 4992
   VAG RATE: 15624
   VAG POS: 585728
Sequence: 44 Name: GS_FHV_EB1
 PATCH: #: 219 Name: pat_eblface1 
  MAP: #: 219 Name: map_eblface1 
  VAG: #: 78 Name: vag_eblface1 File: eblface1.vag
   VAG SIZE: 2464
   VAG RATE: 15624
   VAG POS: 686080
Sequence: 45 Name: GS_FHV_EB2
 PATCH: #: 192 Name: pat_eblface2 
  MAP: #: 192 Name: map_eblface2 
  VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
   VAG SIZE: 4512
   VAG RATE: 15624
   VAG POS: 690176
Sequence: 46 Name: GS_FHV_EB3
 PATCH: #: 192 Name: pat_eblface2 
  MAP: #: 192 Name: map_eblface2 
  VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
   VAG SIZE: 4512
   VAG RATE: 15624
   VAG POS: 690176
Sequence: 47 Name: GS_FHV_JH1
 PATCH: #: 142 Name: pat_jhface1 
  MAP: #: 142 Name: map_jhface1 
  VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
   VAG SIZE: 4992
   VAG RATE: 15624
   VAG POS: 770048
Sequence: 48 Name: GS_FHV_JH2
 PATCH: #: 142 Name: pat_jhface1 
  MAP: #: 142 Name: map_jhface1 
  VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
   VAG SIZE: 4992
   VAG RATE: 15624
   VAG POS: 770048
Sequence: 49 Name: GS_FHV_JH3
 PATCH: #: 142 Name: pat_jhface1 
  MAP: #: 142 Name: map_jhface1 
  VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
   VAG SIZE: 4992
   VAG RATE: 15624
   VAG POS: 770048
Sequence: 50 Name: GS_FHV_ROBO1
 PATCH: #: 48 Name: pat_robshk2 
  MAP: #: 48 Name: map_robshk2 
  VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 952320
Sequence: 51 Name: GS_FHV_ROBO2
 PATCH: #: 48 Name: pat_robshk2 
  MAP: #: 48 Name: map_robshk2 
  VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 952320
Sequence: 52 Name: GS_FHV_ROBO3
 PATCH: #: 64 Name: pat_robshk3 
  MAP: #: 64 Name: map_robshk3 
  VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
   VAG SIZE: 6704
   VAG RATE: 15624
   VAG POS: 915456
Sequence: 53 Name: GS_FHV_KANG1
 PATCH: #: 32 Name: pat_lkface1 
  MAP: #: 32 Name: map_lkface1 
  VAG: #: 125 Name: vag_lkface1 File: lkface1.vag
   VAG SIZE: 4656
   VAG RATE: 15624
   VAG POS: 1046528
Sequence: 54 Name: GS_FHV_KANG2
 PATCH: #: 32 Name: pat_lkface1 
  MAP: #: 32 Name: map_lkface1 
  VAG: #: 125 Name: vag_lkface1 File: lkface1.vag
   VAG SIZE: 4656
   VAG RATE: 15624
   VAG POS: 1046528
Sequence: 55 Name: GS_FHV_KANG3
 PATCH: #: 33 Name: pat_lkface2 
  MAP: #: 33 Name: map_lkface2 
  VAG: #: 126 Name: vag_lkface2 File: lkface2.vag
   VAG SIZE: 3216
   VAG RATE: 15624
   VAG POS: 1052672
Sequence: 56 Name: GS_FHV_SG1
 PATCH: #: 244 Name: pat_amface1 
  MAP: #: 244 Name: map_amface1 
  VAG: #: 235 Name: vag_amface1 File: amface1.vag
   VAG SIZE: 4176
   VAG RATE: 15624
   VAG POS: 2162688
Sequence: 57 Name: GS_FHV_SG2
 PATCH: #: 243 Name: pat_ambody2 
  MAP: #: 243 Name: map_ambody2 
  VAG: #: 234 Name: vag_ambody2 File: ambody2.vag
   VAG SIZE: 2800
   VAG RATE: 15624
   VAG POS: 2158592
Sequence: 58 Name: GS_FHV_SG3
 PATCH: #: 245 Name: pat_amface2 
  MAP: #: 245 Name: map_amface2 
  VAG: #: 236 Name: vag_amface2 File: amface2.vag
   VAG SIZE: 5472
   VAG RATE: 15624
   VAG POS: 2168832
Sequence: 59 Name: GS_FHV_SK1
 PATCH: #: 246 Name: pat_skreact1 
  MAP: #: 246 Name: map_skreact1 
  VAG: #: 237 Name: vag_skreact1 File: skreact1.vag
   VAG SIZE: 4368
   VAG RATE: 15624
   VAG POS: 2174976
Sequence: 60 Name: GS_FHV_SK2
 PATCH: #: 247 Name: pat_skreact2 
  MAP: #: 247 Name: map_skreact2 
  VAG: #: 238 Name: vag_skreact2 File: skreact2.vag
   VAG SIZE: 5136
   VAG RATE: 15624
   VAG POS: 2181120
Sequence: 61 Name: GS_FHV_SK3
 PATCH: #: 248 Name: pat_skreact3 
  MAP: #: 248 Name: map_skreact3 
  VAG: #: 239 Name: vag_skreact3 File: skreact3.vag
   VAG SIZE: 6368
   VAG RATE: 15624
   VAG POS: 2187264
Sequence: 62 Name: GS_ATK_GIRL1
 PATCH: #: 87 Name: pat_pbatt1 
  MAP: #: 87 Name: map_pbatt1 
  VAG: #: 45 Name: vag_pbatt1 File: pbatt1.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 401408
Sequence: 63 Name: GS_ATK_GIRL2
 PATCH: #: 88 Name: pat_pbatt2 
  MAP: #: 88 Name: map_pbatt2 
  VAG: #: 46 Name: vag_pbatt2 File: pbatt2.vag
   VAG SIZE: 3696
   VAG RATE: 15624
   VAG POS: 407552
Sequence: 64 Name: GS_ATK_VP1
 PATCH: #: 73 Name: pat_vpatt1 
  MAP: #: 73 Name: map_vpatt1 
  VAG: #: 57 Name: vag_vpatt1 File: vpatt1.vag
   VAG SIZE: 3008
   VAG RATE: 15624
   VAG POS: 512000
Sequence: 65 Name: GS_ATK_VP2
 PATCH: #: 74 Name: pat_vpatt2 
  MAP: #: 74 Name: map_vpatt2 
  VAG: #: 58 Name: vag_vpatt2 File: vpatt2.vag
   VAG SIZE: 3136
   VAG RATE: 15624
   VAG POS: 516096
Sequence: 66 Name: GS_ATK_DF1
 PATCH: #: 49 Name: pat_dfatt1 
  MAP: #: 49 Name: map_dfatt1 
  VAG: #: 67 Name: vag_dfatt1 File: dfatt1.vag
   VAG SIZE: 3968
   VAG RATE: 15624
   VAG POS: 591872
Sequence: 67 Name: GS_ATK_DF2
 PATCH: #: 50 Name: pat_dfatt2 
  MAP: #: 50 Name: map_dfatt2 
  VAG: #: 68 Name: vag_dfatt2 File: dfatt2.vag
   VAG SIZE: 3216
   VAG RATE: 15624
   VAG POS: 595968
Sequence: 68 Name: GS_ATK_EB1
 PATCH: #: 220 Name: pat_eblatt1 
  MAP: #: 220 Name: map_eblatt1 
  VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
   VAG SIZE: 3008
   VAG RATE: 15624
   VAG POS: 696320
Sequence: 69 Name: GS_ATK_EB2
 PATCH: #: 220 Name: pat_eblatt1 
  MAP: #: 220 Name: map_eblatt1 
  VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
   VAG SIZE: 3008
   VAG RATE: 15624
   VAG POS: 696320
Sequence: 70 Name: GS_ATK_JH1
 PATCH: #: 100 Name: pat_jhatt1 
  MAP: #: 100 Name: map_jhatt1 
  VAG: #: 91 Name: vag_jhatt1 File: jhatt1.vag
   VAG SIZE: 2080
   VAG RATE: 12500
   VAG POS: 776192
Sequence: 71 Name: GS_ATK_JH2
 PATCH: #: 143 Name: pat_jhatt2 
  MAP: #: 143 Name: map_jhatt2 
  VAG: #: 92 Name: vag_jhatt2 File: jhatt2.vag
   VAG SIZE: 3280
   VAG RATE: 15624
   VAG POS: 780288
Sequence: 72 Name: GS_ATK_KANG1
 PATCH: #: 34 Name: pat_lkatt1 
  MAP: #: 34 Name: map_lkatt1 
  VAG: #: 127 Name: vag_lkatt1 File: lkatt1.vag
   VAG SIZE: 2736
   VAG RATE: 15624
   VAG POS: 1056768
Sequence: 73 Name: GS_ATK_KANG2
 PATCH: #: 35 Name: pat_lkatt2 
  MAP: #: 35 Name: map_lkatt2 
  VAG: #: 128 Name: vag_lkatt2 File: lkatt2.vag
   VAG SIZE: 3824
   VAG RATE: 15624
   VAG POS: 1060864
Sequence: 74 Name: GS_ATK_SG1
 PATCH: #: 249 Name: pat_amatt1 
  MAP: #: 249 Name: map_amatt1 
  VAG: #: 240 Name: vag_amatt1 File: amatt1.vag
   VAG SIZE: 3408
   VAG RATE: 15624
   VAG POS: 2195456
Sequence: 75 Name: GS_ATK_SG2
 PATCH: #: 250 Name: pat_amatt2 
  MAP: #: 250 Name: map_amatt2 
  VAG: #: 241 Name: vag_amatt2 File: amatt2.vag
   VAG SIZE: 3616
   VAG RATE: 15624
   VAG POS: 2199552
Sequence: 76 Name: GS_ATK_SK1
 PATCH: #: 252 Name: pat_skatt1 
  MAP: #: 252 Name: map_skatt1 
  VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
   VAG SIZE: 5744
   VAG RATE: 15624
   VAG POS: 2207744
Sequence: 77 Name: GS_ATK_SK2
 PATCH: #: 253 Name: pat_skatt2 
  MAP: #: 253 Name: map_skatt2 
  VAG: #: 244 Name: vag_skatt2 File: skatt2.vag
   VAG SIZE: 6160
   VAG RATE: 15624
   VAG POS: 2213888
Sequence: 78 Name: GS_JUMP_GIRL
 PATCH: #: 89 Name: pat_pbjump 
  MAP: #: 89 Name: map_pbjump 
  VAG: #: 47 Name: vag_pbjump File: pbjump.vag
   VAG SIZE: 3552
   VAG RATE: 15624
   VAG POS: 411648
Sequence: 79 Name: GS_JUMP_VP
 PATCH: #: 75 Name: pat_vpjump 
  MAP: #: 75 Name: map_vpjump 
  VAG: #: 59 Name: vag_vpjump File: vpjump.vag
   VAG SIZE: 3216
   VAG RATE: 15624
   VAG POS: 520192
Sequence: 80 Name: GS_JUMP_DF
 PATCH: #: 51 Name: pat_dfjump 
  MAP: #: 51 Name: map_dfjump 
  VAG: #: 69 Name: vag_dfjump File: dfjump.vag
   VAG SIZE: 2048
   VAG RATE: 15624
   VAG POS: 600064
Sequence: 81 Name: GS_JUMP_EB
 PATCH: #: 222 Name: pat_ebljump 
  MAP: #: 222 Name: map_ebljump 
  VAG: #: 82 Name: vag_ebljump File: ebljump.vag
   VAG SIZE: 2320
   VAG RATE: 15624
   VAG POS: 704512
Sequence: 82 Name: GS_JUMP_JH
 PATCH: #: 101 Name: pat_jhjump 
  MAP: #: 101 Name: map_jhjump 
  VAG: #: 93 Name: vag_jhjump File: jhjump.vag
   VAG SIZE: 2032
   VAG RATE: 12500
   VAG POS: 784384
Sequence: 83 Name: GS_JUMP_KANG
 PATCH: #: 36 Name: pat_lkjump 
  MAP: #: 36 Name: map_lkjump 
  VAG: #: 129 Name: vag_lkjump File: lkjump.vag
   VAG SIZE: 3280
   VAG RATE: 15624
   VAG POS: 1064960
Sequence: 84 Name: GS_JUMP_SG
 PATCH: #: 251 Name: pat_amjump 
  MAP: #: 251 Name: map_amjump 
  VAG: #: 242 Name: vag_amjump File: amjump.vag
   VAG SIZE: 3280
   VAG RATE: 15624
   VAG POS: 2203648
Sequence: 85 Name: GS_JUMP_SK
 PATCH: #: 253 Name: pat_skatt2 
  MAP: #: 253 Name: map_skatt2 
  VAG: #: 244 Name: vag_skatt2 File: skatt2.vag
   VAG SIZE: 6160
   VAG RATE: 15624
   VAG POS: 2213888
Sequence: 86 Name: GS_GRAB_GIRL
 PATCH: #: 90 Name: pat_pbgrab 
  MAP: #: 90 Name: map_pbgrab 
  VAG: #: 48 Name: vag_pbgrab File: pbgrab.vag
   VAG SIZE: 3280
   VAG RATE: 15624
   VAG POS: 415744
Sequence: 87 Name: GS_GRAB_VP
 PATCH: #: 70 Name: pat_vpgrab 
  MAP: #: 70 Name: map_vpgrab 
  VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
   VAG SIZE: 2864
   VAG RATE: 15624
   VAG POS: 495616
Sequence: 88 Name: GS_GRAB_DF
 PATCH: #: 203 Name: pat_dfgrab 
  MAP: #: 203 Name: map_dfgrab 
  VAG: #: 70 Name: vag_dfgrab File: dfgrab.vag
   VAG SIZE: 2800
   VAG RATE: 15624
   VAG POS: 602112
Sequence: 89 Name: GS_GRAB_EB
 PATCH: #: 223 Name: pat_eblgrab 
  MAP: #: 223 Name: map_eblgrab 
  VAG: #: 83 Name: vag_eblgrab File: eblgrab.vag
   VAG SIZE: 3216
   VAG RATE: 15624
   VAG POS: 708608
Sequence: 90 Name: GS_GRAB_JH
 PATCH: #: 102 Name: pat_jhgrab 
  MAP: #: 102 Name: map_jhgrab 
  VAG: #: 94 Name: vag_jhgrab File: jhgrab.vag
   VAG SIZE: 2416
   VAG RATE: 12500
   VAG POS: 786432
Sequence: 91 Name: GS_GRAB_ROBO
 PATCH: #: 52 Name: pat_robtalk2 
  MAP: #: 52 Name: map_robtalk2 
  VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
   VAG SIZE: 5952
   VAG RATE: 15624
   VAG POS: 827392
Sequence: 92 Name: GS_GRAB_KANG
 PATCH: #: 37 Name: pat_lkgrab 
  MAP: #: 37 Name: map_lkgrab 
  VAG: #: 130 Name: vag_lkgrab File: lkgrab.vag
   VAG SIZE: 1904
   VAG RATE: 15624
   VAG POS: 1069056
Sequence: 93 Name: GS_GRAB_SG
 PATCH: #: 254 Name: pat_amgrab 
  MAP: #: 254 Name: map_amgrab 
  VAG: #: 245 Name: vag_amgrab File: amgrab.vag
   VAG SIZE: 3696
   VAG RATE: 15624
   VAG POS: 2222080
Sequence: 94 Name: GS_GRAB_SK
 PATCH: #: 252 Name: pat_skatt1 
  MAP: #: 252 Name: map_skatt1 
  VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
   VAG SIZE: 5744
   VAG RATE: 15624
   VAG POS: 2207744
Sequence: 95 Name: GS_SLAM_GIRL
 PATCH: #: 91 Name: pat_pbthrow 
  MAP: #: 91 Name: map_pbthrow 
  VAG: #: 49 Name: vag_pbthrow File: pbthrow.vag
   VAG SIZE: 9936
   VAG RATE: 15624
   VAG POS: 419840
Sequence: 96 Name: GS_SLAM_VP
 PATCH: #: 76 Name: pat_vpthrow 
  MAP: #: 76 Name: map_vpthrow 
  VAG: #: 60 Name: vag_vpthrow File: vpthrow.vag
   VAG SIZE: 8560
   VAG RATE: 15624
   VAG POS: 524288
Sequence: 97 Name: GS_SLAM_DF
 PATCH: #: 204 Name: pat_dfthrow 
  MAP: #: 204 Name: map_dfthrow 
  VAG: #: 71 Name: vag_dfthrow File: dfthrow.vag
   VAG SIZE: 7328
   VAG RATE: 15624
   VAG POS: 606208
Sequence: 98 Name: GS_SLAM_EB
 PATCH: #: 224 Name: pat_eblthrow 
  MAP: #: 224 Name: map_eblthrow 
  VAG: #: 84 Name: vag_eblthrow File: eblthrow.vag
   VAG SIZE: 7600
   VAG RATE: 15624
   VAG POS: 712704
Sequence: 99 Name: GS_SLAM_JH
 PATCH: #: 103 Name: pat_jhthrow 
  MAP: #: 103 Name: map_jhthrow 
  VAG: #: 95 Name: vag_jhthrow File: jhthrow.vag
   VAG SIZE: 5488
   VAG RATE: 12500
   VAG POS: 790528
Sequence: 100 Name: GS_SLAM_ROBO
 PATCH: #: 65 Name: pat_robtalk1 
  MAP: #: 65 Name: map_robtalk1 
  VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 923648
Sequence: 101 Name: GS_SLAM_KANG
 PATCH: #: 38 Name: pat_lkthrow 
  MAP: #: 38 Name: map_lkthrow 
  VAG: #: 131 Name: vag_lkthrow File: lkthrow.vag
   VAG SIZE: 6640
   VAG RATE: 15624
   VAG POS: 1071104
Sequence: 102 Name: GS_SLAM_SG
 PATCH: #: 255 Name: pat_amthrow 
  MAP: #: 255 Name: map_amthrow 
  VAG: #: 246 Name: vag_amthrow File: amthrow.vag
   VAG SIZE: 8688
   VAG RATE: 15624
   VAG POS: 2226176
Sequence: 103 Name: GS_SLAM_SK
 PATCH: #: 252 Name: pat_skatt1 
  MAP: #: 252 Name: map_skatt1 
  VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
   VAG SIZE: 5744
   VAG RATE: 15624
   VAG POS: 2207744
Sequence: 104 Name: GS_WST_GIRL1
 PATCH: #: 92 Name: pat_pbwast1 
  MAP: #: 92 Name: map_pbwast1 
  VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
   VAG SIZE: 9520
   VAG RATE: 15624
   VAG POS: 430080
Sequence: 105 Name: GS_WST_GIRL2
 PATCH: #: 92 Name: pat_pbwast1 
  MAP: #: 92 Name: map_pbwast1 
  VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
   VAG SIZE: 9520
   VAG RATE: 15624
   VAG POS: 430080
Sequence: 106 Name: GS_WST_VP1
 PATCH: #: 77 Name: pat_vpwast2 
  MAP: #: 77 Name: map_vpwast2 
  VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
   VAG SIZE: 7728
   VAG RATE: 12500
   VAG POS: 534528
Sequence: 107 Name: GS_WST_VP2
 PATCH: #: 77 Name: pat_vpwast2 
  MAP: #: 77 Name: map_vpwast2 
  VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
   VAG SIZE: 7728
   VAG RATE: 12500
   VAG POS: 534528
Sequence: 108 Name: GS_WST_DF1
 PATCH: #: 205 Name: pat_dfwast2 
  MAP: #: 205 Name: map_dfwast2 
  VAG: #: 72 Name: vag_dfwast2 File: dfwast2.vag
   VAG SIZE: 8816
   VAG RATE: 15624
   VAG POS: 614400
Sequence: 109 Name: GS_WST_DF2
 PATCH: #: 205 Name: pat_dfwast2 
  MAP: #: 205 Name: map_dfwast2 
  VAG: #: 72 Name: vag_dfwast2 File: dfwast2.vag
   VAG SIZE: 8816
   VAG RATE: 15624
   VAG POS: 614400
Sequence: 110 Name: GS_WST_EB1
 PATCH: #: 225 Name: pat_eblwast1 
  MAP: #: 225 Name: map_eblwast1 
  VAG: #: 85 Name: vag_eblwast1 File: eblwast1.vag
   VAG SIZE: 10128
   VAG RATE: 15624
   VAG POS: 720896
Sequence: 111 Name: GS_WST_EB2
 PATCH: #: 226 Name: pat_eblwast2 
  MAP: #: 226 Name: map_eblwast2 
  VAG: #: 86 Name: vag_eblwast2 File: eblwast2.vag
   VAG SIZE: 7568
   VAG RATE: 12500
   VAG POS: 731136
Sequence: 112 Name: GS_WST_JH1
 PATCH: #: 144 Name: pat_jhwast1 
  MAP: #: 144 Name: map_jhwast1 
  VAG: #: 96 Name: vag_jhwast1 File: jhwast1.vag
   VAG SIZE: 13424
   VAG RATE: 15624
   VAG POS: 796672
Sequence: 113 Name: GS_WST_JH2
 PATCH: #: 104 Name: pat_jhwast2 
  MAP: #: 104 Name: map_jhwast2 
  VAG: #: 97 Name: vag_jhwast2 File: jhwast2.vag
   VAG SIZE: 9552
   VAG RATE: 12500
   VAG POS: 811008
Sequence: 114 Name: GS_WST_ROBO1
 PATCH: #: 65 Name: pat_robtalk1 
  MAP: #: 65 Name: map_robtalk1 
  VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 923648
Sequence: 115 Name: GS_WST_ROBO2
 PATCH: #: 54 Name: pat_robtalk4 
  MAP: #: 54 Name: map_robtalk4 
  VAG: #: 101 Name: vag_robtalk4 File: robtalk4.vag
   VAG SIZE: 8896
   VAG RATE: 15624
   VAG POS: 841728
Sequence: 116 Name: GS_WST_KANG1
 PATCH: #: 39 Name: pat_lkwast1 
  MAP: #: 39 Name: map_lkwast1 
  VAG: #: 132 Name: vag_lkwast1 File: lkwast1.vag
   VAG SIZE: 14592
   VAG RATE: 11000
   VAG POS: 1079296
Sequence: 117 Name: GS_WST_KANG2
 PATCH: #: 256 Name: pat_lkwast2 
  MAP: #: 256 Name: map_lkwast2 
  VAG: #: 247 Name: vag_lkwast2 File: lkwast2.vag
   VAG SIZE: 17328
   VAG RATE: 15624
   VAG POS: 2236416
Sequence: 118 Name: GS_WST_SG1
 PATCH: #: 257 Name: pat_amwast1 
  MAP: #: 257 Name: map_amwast1 
  VAG: #: 248 Name: vag_amwast1 File: amwast1.vag
   VAG SIZE: 12256
   VAG RATE: 15624
   VAG POS: 2254848
Sequence: 119 Name: GS_WST_SG2
 PATCH: #: 258 Name: pat_amwast2 
  MAP: #: 258 Name: map_amwast2 
  VAG: #: 249 Name: vag_amwast2 File: amwast2.vag
   VAG SIZE: 11984
   VAG RATE: 15624
   VAG POS: 2267136
Sequence: 120 Name: GS_WST_SK1
 PATCH: #: 259 Name: pat_skwast1 
  MAP: #: 259 Name: map_skwast1 
  VAG: #: 250 Name: vag_skwast1 File: skwast1.vag
   VAG SIZE: 9040
   VAG RATE: 15624
   VAG POS: 2279424
Sequence: 121 Name: GS_WST_SK2
 PATCH: #: 260 Name: pat_skwast2 
  MAP: #: 260 Name: map_skwast2 
  VAG: #: 251 Name: vag_skwast2 File: skwast2.vag
   VAG SIZE: 10128
   VAG RATE: 15624
   VAG POS: 2289664
Sequence: 122 Name: RSND_EBOOM1
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 123 Name: RSND_EBOOM2
 PATCH: #: 58 Name: pat_robxplod 
  MAP: #: 58 Name: map_robxplod 
  VAG: #: 105 Name: vag_robxplod File: robxplod.vag
   VAG SIZE: 14656
   VAG RATE: 15624
   VAG POS: 870400
Sequence: 124 Name: RSND_EBOOM3
 PATCH: #: 145 Name: pat_jaxcann 
  MAP: #: 145 Name: map_jaxcann 
  VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
   VAG SIZE: 12880
   VAG RATE: 15624
   VAG POS: 1347584
Sequence: 125 Name: RSND_SPLISH1
 PATCH: #: 261 Name: pat_splish1 
  MAP: #: 261 Name: map_splish1 
  VAG: #: 252 Name: vag_splish1 File: splish1.vag
   VAG SIZE: 2352
   VAG RATE: 12500
   VAG POS: 2299904
Sequence: 126 Name: RSND_SPLISH2
 PATCH: #: 262 Name: pat_splish2 
  MAP: #: 262 Name: map_splish2 
  VAG: #: 253 Name: vag_splish2 File: splish2.vag
   VAG SIZE: 1920
   VAG RATE: 12500
   VAG POS: 2304000
Sequence: 127 Name: RSND_SPLISH3
 PATCH: #: 262 Name: pat_splish2 
  MAP: #: 262 Name: map_splish2 
  VAG: #: 253 Name: vag_splish2 File: splish2.vag
   VAG SIZE: 1920
   VAG RATE: 12500
   VAG POS: 2304000
Sequence: 128 Name: RSND_STAB1
 PATCH: #: 263 Name: pat_stab1 
  MAP: #: 263 Name: map_stab1 
  VAG: #: 254 Name: vag_stab1 File: stab1.vag
   VAG SIZE: 3296
   VAG RATE: 12500
   VAG POS: 2306048
Sequence: 129 Name: RSND_STAB2
 PATCH: #: 264 Name: pat_bigstab1 
  MAP: #: 264 Name: map_bigstab1 
  VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
   VAG SIZE: 3344
   VAG RATE: 12500
   VAG POS: 2310144
Sequence: 130 Name: RSND_STAB3
 PATCH: #: 263 Name: pat_stab1 
  MAP: #: 263 Name: map_stab1 
  VAG: #: 254 Name: vag_stab1 File: stab1.vag
   VAG SIZE: 3296
   VAG RATE: 12500
   VAG POS: 2306048
Sequence: 131 Name: RSND_STAB4
 PATCH: #: 264 Name: pat_bigstab1 
  MAP: #: 264 Name: map_bigstab1 
  VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
   VAG SIZE: 3344
   VAG RATE: 12500
   VAG POS: 2310144
Sequence: 132 Name: RSND_FS1
 PATCH: #: 119 Name: pat_foot1 
  MAP: #: 119 Name: map_foot1 
  VAG: #: 24 Name: vag_foot1 File: foot1.vag
   VAG SIZE: 1536
   VAG RATE: 12500
   VAG POS: 247808
Sequence: 133 Name: RSND_FS2
 PATCH: #: 120 Name: pat_foot2 
  MAP: #: 120 Name: map_foot2 
  VAG: #: 25 Name: vag_foot2 File: foot2.vag
   VAG SIZE: 1808
   VAG RATE: 12500
   VAG POS: 249856
Sequence: 134 Name: RSND_FS3
 PATCH: #: 20 Name: pat_foot3 
  MAP: #: 20 Name: map_foot3 
  VAG: #: 15 Name: vag_foot3 File: foot3.vag
   VAG SIZE: 1696
   VAG RATE: 12500
   VAG POS: 161792
Sequence: 135 Name: RSND_FS4
 PATCH: #: 121 Name: pat_foot4 
  MAP: #: 121 Name: map_foot4 
  VAG: #: 26 Name: vag_foot4 File: foot4.vag
   VAG SIZE: 1040
   VAG RATE: 12500
   VAG POS: 251904
Sequence: 136 Name: RSND_BB1
 PATCH: #: 122 Name: pat_gudblock 
  MAP: #: 122 Name: map_gudblock 
  VAG: #: 27 Name: vag_gudblock File: gudblock.vag
   VAG SIZE: 4336
   VAG RATE: 12500
   VAG POS: 253952
Sequence: 137 Name: RSND_BB2
 PATCH: #: 122 Name: pat_gudblock 
  MAP: #: 122 Name: map_gudblock 
  VAG: #: 27 Name: vag_gudblock File: gudblock.vag
   VAG SIZE: 4336
   VAG RATE: 12500
   VAG POS: 253952
Sequence: 138 Name: RSND_SB1
 PATCH: #: 123 Name: pat_block1 
  MAP: #: 123 Name: map_block1 
  VAG: #: 28 Name: vag_block1 File: block1.vag
   VAG SIZE: 1968
   VAG RATE: 12500
   VAG POS: 260096
Sequence: 139 Name: RSND_SB2
 PATCH: #: 123 Name: pat_block1 
  MAP: #: 123 Name: map_block1 
  VAG: #: 28 Name: vag_block1 File: block1.vag
   VAG SIZE: 1968
   VAG RATE: 12500
   VAG POS: 260096
Sequence: 140 Name: RSND_SM1
 PATCH: #: 124 Name: pat_gudhit1 
  MAP: #: 124 Name: map_gudhit1 
  VAG: #: 29 Name: vag_gudhit1 File: gudhit1.vag
   VAG SIZE: 2256
   VAG RATE: 12500
   VAG POS: 262144
Sequence: 141 Name: RSND_SM2
 PATCH: #: 124 Name: pat_gudhit1 
  MAP: #: 124 Name: map_gudhit1 
  VAG: #: 29 Name: vag_gudhit1 File: gudhit1.vag
   VAG SIZE: 2256
   VAG RATE: 12500
   VAG POS: 262144
Sequence: 142 Name: RSND_MSM1
 PATCH: #: 125 Name: pat_newbig1 
  MAP: #: 125 Name: map_newbig1 
  VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
   VAG SIZE: 4384
   VAG RATE: 12500
   VAG POS: 266240
Sequence: 143 Name: RSND_MSM2
 PATCH: #: 126 Name: pat_newbig3 
  MAP: #: 126 Name: map_newbig3 
  VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
   VAG SIZE: 4384
   VAG RATE: 12500
   VAG POS: 266240
Sequence: 144 Name: RSND_MSM3
 PATCH: #: 126 Name: pat_newbig3 
  MAP: #: 126 Name: map_newbig3 
  VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
   VAG SIZE: 4384
   VAG RATE: 12500
   VAG POS: 266240
Sequence: 145 Name: RSND_MSM4
 PATCH: #: 127 Name: pat_newbig4 
  MAP: #: 127 Name: map_newbig4 
  VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
   VAG SIZE: 4384
   VAG RATE: 12500
   VAG POS: 266240
Sequence: 146 Name: RSND_KLANG1
 PATCH: #: 288 Name: pat_mhit1 
  MAP: #: 288 Name: map_mhit1 
  VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
   VAG SIZE: 3184
   VAG RATE: 12500
   VAG POS: 2527232
Sequence: 147 Name: RSND_KLANG2
 PATCH: #: 289 Name: pat_mhit2 
  MAP: #: 289 Name: map_mhit2 
  VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
   VAG SIZE: 3568
   VAG RATE: 12500
   VAG POS: 2531328
Sequence: 148 Name: RSND_BSM1
 PATCH: #: 128 Name: pat_roldbig1 
  MAP: #: 128 Name: map_roldbig1 
  VAG: #: 31 Name: vag_roldbig1 File: roldbig1.vag
   VAG SIZE: 5424
   VAG RATE: 12500
   VAG POS: 272384
Sequence: 149 Name: RSND_BSM2
 PATCH: #: 129 Name: pat_roldbig2 
  MAP: #: 129 Name: map_roldbig2 
  VAG: #: 31 Name: vag_roldbig1 File: roldbig1.vag
   VAG SIZE: 5424
   VAG RATE: 12500
   VAG POS: 272384
Sequence: 150 Name: RSND_ROCK1
 PATCH: #: 130 Name: pat_rock1 
  MAP: #: 130 Name: map_rock1 
  VAG: #: 32 Name: vag_rock1 File: rock1.vag
   VAG SIZE: 2528
   VAG RATE: 12500
   VAG POS: 278528
Sequence: 151 Name: RSND_ROCK2
 PATCH: #: 131 Name: pat_rock2 
  MAP: #: 131 Name: map_rock2 
  VAG: #: 33 Name: vag_rock2 File: rock2.vag
   VAG SIZE: 1808
   VAG RATE: 12500
   VAG POS: 282624
Sequence: 152 Name: RSND_ROCK3
 PATCH: #: 131 Name: pat_rock2 
  MAP: #: 131 Name: map_rock2 
  VAG: #: 33 Name: vag_rock2 File: rock2.vag
   VAG SIZE: 1808
   VAG RATE: 12500
   VAG POS: 282624
Sequence: 153 Name: RSND_ROCK4
 PATCH: #: 132 Name: pat_rocks3 
  MAP: #: 132 Name: map_rocks3 
  VAG: #: 34 Name: vag_rocks3 File: rocks3.vag
   VAG SIZE: 2912
   VAG RATE: 12500
   VAG POS: 284672
Sequence: 154 Name: RSND_ROCK5
 PATCH: #: 132 Name: pat_rocks3 
  MAP: #: 132 Name: map_rocks3 
  VAG: #: 34 Name: vag_rocks3 File: rocks3.vag
   VAG SIZE: 2912
   VAG RATE: 12500
   VAG POS: 284672
Sequence: 155 Name: RSND_BHT1
 PATCH: #: 133 Name: pat_body1 
  MAP: #: 133 Name: map_body1 
  VAG: #: 160 Name: vag_body1 File: body1.vag
   VAG SIZE: 2912
   VAG RATE: 12500
   VAG POS: 1329152
Sequence: 156 Name: RSND_BHT2
 PATCH: #: 134 Name: pat_body2 
  MAP: #: 134 Name: map_body2 
  VAG: #: 161 Name: vag_body2 File: body2.vag
   VAG SIZE: 2800
   VAG RATE: 12500
   VAG POS: 1333248
Sequence: 157 Name: RSND_GND1
 PATCH: #: 135 Name: pat_gudfall2 
  MAP: #: 135 Name: map_gudfall2 
  VAG: #: 162 Name: vag_gudfall2 File: gudfall2.vag
   VAG SIZE: 2960
   VAG RATE: 12500
   VAG POS: 1337344
Sequence: 158 Name: RSND_GND2
 PATCH: #: 135 Name: pat_gudfall2 
  MAP: #: 135 Name: map_gudfall2 
  VAG: #: 162 Name: vag_gudfall2 File: gudfall2.vag
   VAG SIZE: 2960
   VAG RATE: 12500
   VAG POS: 1337344
Sequence: 159 Name: RSND_GND3
 PATCH: #: 136 Name: pat_gudfall4 
  MAP: #: 136 Name: map_gudfall4 
  VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
   VAG SIZE: 4608
   VAG RATE: 12500
   VAG POS: 1341440
Sequence: 160 Name: RSND_GND4
 PATCH: #: 136 Name: pat_gudfall4 
  MAP: #: 136 Name: map_gudfall4 
  VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
   VAG SIZE: 4608
   VAG RATE: 12500
   VAG POS: 1341440
Sequence: 161 Name: RSND_WHSH1
 PATCH: #: 12 Name: pat_whoosh1 
  MAP: #: 12 Name: map_whoosh1 
  VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
   VAG SIZE: 2192
   VAG RATE: 12500
   VAG POS: 92160
Sequence: 162 Name: RSND_WHSH2
 PATCH: #: 137 Name: pat_whoosh2 
  MAP: #: 137 Name: map_whoosh2 
  VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
   VAG SIZE: 2688
   VAG RATE: 12500
   VAG POS: 96256
Sequence: 163 Name: RSND_WHSH3
 PATCH: #: 13 Name: pat_whoosh3 
  MAP: #: 13 Name: map_whoosh3 
  VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
   VAG SIZE: 2688
   VAG RATE: 12500
   VAG POS: 96256
Sequence: 164 Name: RSND_WHSH4
 PATCH: #: 138 Name: pat_bwhoosh2 
  MAP: #: 138 Name: map_bwhoosh2 
  VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
   VAG SIZE: 2688
   VAG RATE: 12500
   VAG POS: 96256
Sequence: 165 Name: RSND_BWHSH1
 PATCH: #: 139 Name: pat_bwhoosh1 
  MAP: #: 139 Name: map_bwhoosh1 
  VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
   VAG SIZE: 2192
   VAG RATE: 12500
   VAG POS: 92160
Sequence: 166 Name: RSND_BWHSH2
 PATCH: #: 140 Name: pat_bwhoosh3 
  MAP: #: 140 Name: map_bwhoosh3 
  VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
   VAG SIZE: 2192
   VAG RATE: 12500
   VAG POS: 92160
Sequence: 167 Name: ST_KANO_AX
 PATCH: #: 109 Name: pat_kanoax 
  MAP: #: 109 Name: map_kanoax 
  VAG: #: 150 Name: vag_kanoax File: kanoax.vag
   VAG SIZE: 6960
   VAG RATE: 12500
   VAG POS: 1241088
Sequence: 168 Name: ST_KANO_SKNIFE
 PATCH: #: 206 Name: pat_kanoknif 
  MAP: #: 206 Name: map_kanoknif 
  VAG: #: 215 Name: vag_kanoknif File: kanoknif.vag
   VAG SIZE: 11568
   VAG RATE: 15624
   VAG POS: 1961984
Sequence: 169 Name: ST_KANO_SKNIFE_HIT
 PATCH: #: 207 Name: pat_newstab2 
  MAP: #: 207 Name: map_newstab2 
  VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
   VAG SIZE: 4096
   VAG RATE: 15624
   VAG POS: 1974272
Sequence: 170 Name: ST_KANO_SKNIFE_BLK
 PATCH: #: 207 Name: pat_newstab2 
  MAP: #: 207 Name: map_newstab2 
  VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
   VAG SIZE: 4096
   VAG RATE: 15624
   VAG POS: 1974272
Sequence: 171 Name: ST_KANO_BALL
 PATCH: #: 208 Name: pat_kanoball 
  MAP: #: 208 Name: map_kanoball 
  VAG: #: 217 Name: vag_kanoball File: kanoball.vag
   VAG SIZE: 10128
   VAG RATE: 15624
   VAG POS: 1978368
Sequence: 172 Name: FT_KANO_LASER
 PATCH: #: 209 Name: pat_storb 
  MAP: #: 209 Name: map_storb 
  VAG: #: 218 Name: vag_storb File: storb.vag
   VAG SIZE: 9184
   VAG RATE: 15624
   VAG POS: 1988608
Sequence: 173 Name: FT_KANO_LASER_BURN
 PATCH: #: 210 Name: pat_kanlasr1 
  MAP: #: 210 Name: map_kanlasr1 
  VAG: #: 219 Name: vag_kanlasr1 File: kanlasr1.vag
   VAG SIZE: 12528
   VAG RATE: 15624
   VAG POS: 1998848
Sequence: 174 Name: FT_KANO_SKELETON_RIP
 PATCH: #: 110 Name: pat_crunch2 
  MAP: #: 110 Name: map_crunch2 
  VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
   VAG SIZE: 4656
   VAG RATE: 12500
   VAG POS: 1249280
Sequence: 175 Name: FT_KANO_SKIN_FALL
 PATCH: #: 136 Name: pat_gudfall4 
  MAP: #: 136 Name: map_gudfall4 
  VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
   VAG SIZE: 4608
   VAG RATE: 12500
   VAG POS: 1341440
Sequence: 176 Name: ST_SONYA_RINGS
 PATCH: #: 147 Name: pat_sonforce 
  MAP: #: 147 Name: map_sonforce 
  VAG: #: 166 Name: vag_sonforce File: sonforce.vag
   VAG SIZE: 9856
   VAG RATE: 15624
   VAG POS: 1374208
Sequence: 177 Name: ST_SONYA_RINGS_HIT
 PATCH: #: 95 Name: pat_forcehit 
  MAP: #: 95 Name: map_forcehit 
  VAG: #: 145 Name: vag_forcehit File: forcehit.vag
   VAG SIZE: 8080
   VAG RATE: 15624
   VAG POS: 1200128
Sequence: 178 Name: ST_SONYA_RINGS_BLK
 PATCH: #: 95 Name: pat_forcehit 
  MAP: #: 95 Name: map_forcehit 
  VAG: #: 145 Name: vag_forcehit File: forcehit.vag
   VAG SIZE: 8080
   VAG RATE: 15624
   VAG POS: 1200128
Sequence: 179 Name: ST_SONYA_BIKE
 PATCH: #: 233 Name: pat_sonbike 
  MAP: #: 233 Name: map_sonbike 
  VAG: #: 229 Name: vag_sonbike File: sonbike.vag
   VAG SIZE: 7392
   VAG RATE: 15624
   VAG POS: 2101248
Sequence: 180 Name: ST_SONYA_FLY
 PATCH: #: 42 Name: pat_sonyafly 
  MAP: #: 42 Name: map_sonyafly 
  VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
   VAG SIZE: 8288
   VAG RATE: 15624
   VAG POS: 1144832
Sequence: 181 Name: ST_SONYA_BIKE_VOICE
 PATCH: #: 4 Name: pat_pbcycle 
  MAP: #: 4 Name: map_pbcycle 
  VAG: #: 35 Name: vag_pbcycle File: pbcycle.vag
   VAG SIZE: 12192
   VAG RATE: 15624
   VAG POS: 288768
Sequence: 182 Name: FT_SONYA_BLOW_KISS
 PATCH: #: 26 Name: pat_mfirebal 
  MAP: #: 26 Name: map_mfirebal 
  VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
   VAG SIZE: 11296
   VAG RATE: 15624
   VAG POS: 204800
Sequence: 183 Name: FT_SONYA_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 184 Name: ST_JAX_CANNON
 PATCH: #: 145 Name: pat_jaxcann 
  MAP: #: 145 Name: map_jaxcann 
  VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
   VAG SIZE: 12880
   VAG RATE: 15624
   VAG POS: 1347584
Sequence: 185 Name: ST_JAX_ZAP_HIT
 PATCH: #: 149 Name: pat_rockhit 
  MAP: #: 149 Name: map_rockhit 
  VAG: #: 168 Name: vag_rockhit File: rockhit.vag
   VAG SIZE: 7600
   VAG RATE: 15624
   VAG POS: 1392640
Sequence: 186 Name: ST_JAX_ZAP_BLK
 PATCH: #: 149 Name: pat_rockhit 
  MAP: #: 149 Name: map_rockhit 
  VAG: #: 168 Name: vag_rockhit File: rockhit.vag
   VAG SIZE: 7600
   VAG RATE: 15624
   VAG POS: 1392640
Sequence: 187 Name: ST_JAX_BLURR
 PATCH: #: 150 Name: pat_jaxrush 
  MAP: #: 150 Name: map_jaxrush 
  VAG: #: 169 Name: vag_jaxrush File: jaxrush.vag
   VAG SIZE: 7056
   VAG RATE: 15624
   VAG POS: 1400832
Sequence: 188 Name: ST_JAX_GROUND
 PATCH: #: 151 Name: pat_jaxhitg 
  MAP: #: 151 Name: map_jaxhitg 
  VAG: #: 170 Name: vag_jaxhitg File: jaxhitg.vag
   VAG SIZE: 11920
   VAG RATE: 15624
   VAG POS: 1409024
Sequence: 189 Name: ST_JAX_SLAM
 PATCH: #: 94 Name: pat_bigbslam 
  MAP: #: 94 Name: map_bigbslam 
  VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
   VAG SIZE: 8416
   VAG RATE: 15624
   VAG POS: 1189888
Sequence: 190 Name: ST_JAX_GOTCHA
 PATCH: #: 266 Name: pat_jaxgotch 
  MAP: #: 266 Name: map_jaxgotch 
  VAG: #: 257 Name: vag_jaxgotch File: jaxgotch.vag
   VAG SIZE: 5952
   VAG RATE: 15624
   VAG POS: 2320384
Sequence: 191 Name: ST_JAX_BREAK
 PATCH: #: 110 Name: pat_crunch2 
  MAP: #: 110 Name: map_crunch2 
  VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
   VAG SIZE: 4656
   VAG RATE: 12500
   VAG POS: 1249280
Sequence: 192 Name: TS_KLANG1
 PATCH: #: 288 Name: pat_mhit1 
  MAP: #: 288 Name: map_mhit1 
  VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
   VAG SIZE: 3184
   VAG RATE: 12500
   VAG POS: 2527232
Sequence: 193 Name: FT_JAX_ARM_MORPH
 PATCH: #: 148 Name: pat_stmorph 
  MAP: #: 148 Name: map_stmorph 
  VAG: #: 167 Name: vag_stmorph File: stmorph.vag
   VAG SIZE: 7072
   VAG RATE: 15624
   VAG POS: 1384448
Sequence: 194 Name: FT_JAX_CRUNCH1
 PATCH: #: 267 Name: pat_crunch1 
  MAP: #: 267 Name: map_crunch1 
  VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
   VAG SIZE: 4224
   VAG RATE: 12500
   VAG POS: 2326528
Sequence: 195 Name: FT_JAX_CRUNCH2
 PATCH: #: 110 Name: pat_crunch2 
  MAP: #: 110 Name: map_crunch2 
  VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
   VAG SIZE: 4656
   VAG RATE: 12500
   VAG POS: 1249280
Sequence: 196 Name: FT_JAX_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 197 Name: ST_SZ_ICE_UP
 PATCH: #: 193 Name: pat_sziceup 
  MAP: #: 193 Name: map_sziceup 
  VAG: #: 209 Name: vag_sziceup File: sziceup.vag
   VAG SIZE: 11856
   VAG RATE: 15624
   VAG POS: 1900544
Sequence: 198 Name: ST_SZ_ICE_DOWN
 PATCH: #: 194 Name: pat_szicedwn 
  MAP: #: 194 Name: map_szicedwn 
  VAG: #: 210 Name: vag_szicedwn File: szicedwn.vag
   VAG SIZE: 8352
   VAG RATE: 15624
   VAG POS: 1912832
Sequence: 199 Name: ST_SZ_ICE_HIT
 PATCH: #: 195 Name: pat_icehit 
  MAP: #: 195 Name: map_icehit 
  VAG: #: 211 Name: vag_icehit File: icehit.vag
   VAG SIZE: 6368
   VAG RATE: 15624
   VAG POS: 1923072
Sequence: 200 Name: ST_SZ_ICE_BLOCK
 PATCH: #: 195 Name: pat_icehit 
  MAP: #: 195 Name: map_icehit 
  VAG: #: 211 Name: vag_icehit File: icehit.vag
   VAG SIZE: 6368
   VAG RATE: 15624
   VAG POS: 1923072
Sequence: 201 Name: ST_SZ_ICE_HIT2
 PATCH: #: 196 Name: pat_subzturd 
  MAP: #: 196 Name: map_subzturd 
  VAG: #: 212 Name: vag_subzturd File: subzturd.vag
   VAG SIZE: 6656
   VAG RATE: 15624
   VAG POS: 1931264
Sequence: 202 Name: ST_SZ_ICE_DECOY
 PATCH: #: 197 Name: pat_szifrz 
  MAP: #: 197 Name: map_szifrz 
  VAG: #: 213 Name: vag_szifrz File: szifrz.vag
   VAG SIZE: 10272
   VAG RATE: 15624
   VAG POS: 1939456
Sequence: 203 Name: ST_SZ_SLIDE
 PATCH: #: 198 Name: pat_szslide 
  MAP: #: 198 Name: map_szslide 
  VAG: #: 214 Name: vag_szslide File: szslide.vag
   VAG SIZE: 10064
   VAG RATE: 15624
   VAG POS: 1951744
Sequence: 204 Name: FT_SZ_BLOW
 PATCH: #: 26 Name: pat_mfirebal 
  MAP: #: 26 Name: map_mfirebal 
  VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
   VAG SIZE: 11296
   VAG RATE: 15624
   VAG POS: 204800
Sequence: 205 Name: FT_SZ_FROZE
 PATCH: #: 271 Name: pat_robpres2 
  MAP: #: 271 Name: map_robpres2 
  VAG: #: 262 Name: vag_robpres2 File: robpres2.vag
   VAG SIZE: 9728
   VAG RATE: 15624
   VAG POS: 2363392
Sequence: 206 Name: ST_SWAT_BOMB_EXP
 PATCH: #: 58 Name: pat_robxplod 
  MAP: #: 58 Name: map_robxplod 
  VAG: #: 105 Name: vag_robxplod File: robxplod.vag
   VAG SIZE: 14656
   VAG RATE: 15624
   VAG POS: 870400
Sequence: 207 Name: ST_SWAT_BOMB_THROW
 PATCH: #: 231 Name: pat_strkgren 
  MAP: #: 231 Name: map_strkgren 
  VAG: #: 227 Name: vag_strkgren File: strkgren.vag
   VAG SIZE: 7264
   VAG RATE: 15624
   VAG POS: 2084864
Sequence: 208 Name: ST_SWAT_ZOOM
 PATCH: #: 232 Name: pat_strklung 
  MAP: #: 232 Name: map_strklung 
  VAG: #: 228 Name: vag_strklung File: strklung.vag
   VAG SIZE: 7088
   VAG RATE: 15624
   VAG POS: 2093056
Sequence: 209 Name: ST_SWAT_STICK
 PATCH: #: 233 Name: pat_sonbike 
  MAP: #: 233 Name: map_sonbike 
  VAG: #: 229 Name: vag_sonbike File: sonbike.vag
   VAG SIZE: 7392
   VAG RATE: 15624
   VAG POS: 2101248
Sequence: 210 Name: ST_SWAT_ZOOM_THROW
 PATCH: #: 234 Name: pat_strkthro 
  MAP: #: 234 Name: map_strkthro 
  VAG: #: 230 Name: vag_strkthro File: strkthro.vag
   VAG SIZE: 4640
   VAG RATE: 15624
   VAG POS: 2109440
Sequence: 211 Name: ST_SWAT_BANG
 PATCH: #: 123 Name: pat_block1 
  MAP: #: 123 Name: map_block1 
  VAG: #: 28 Name: vag_block1 File: block1.vag
   VAG SIZE: 1968
   VAG RATE: 12500
   VAG POS: 260096
Sequence: 212 Name: FT_SWAT_TASER
 PATCH: #: 25 Name: pat_kanlasrl 
  MAP: #: 25 Name: map_kanlasrl 
  VAG: #: 20 Name: vag_kanlasrl File: kanlasrl.vag
   VAG SIZE: 6224
   VAG RATE: 15624
   VAG POS: 196608
Sequence: 213 Name: FT_SWAT_TASER_FIRE
 PATCH: #: 61 Name: pat_robxpld2 
  MAP: #: 61 Name: map_robxpld2 
  VAG: #: 108 Name: vag_robxpld2 File: robxpld2.vag
   VAG SIZE: 7728
   VAG RATE: 15624
   VAG POS: 894976
Sequence: 214 Name: FT_SWAT_WHISTLE
 PATCH: #: 268 Name: pat_whistle 
  MAP: #: 268 Name: map_whistle 
  VAG: #: 259 Name: vag_whistle File: whistle.vag
   VAG SIZE: 8016
   VAG RATE: 15624
   VAG POS: 2332672
Sequence: 215 Name: FT_SWAT_WAGON
 PATCH: #: 313 Name: pat_wagon 
  MAP: #: 313 Name: map_wagon 
  VAG: #: 304 Name: vag_wagon File: wagon.vag
   VAG SIZE: 16432
   VAG RATE: 22050
   VAG POS: 2902016
Sequence: 216 Name: ST_IND_ARROW_APPEAR
 PATCH: #: 105 Name: pat_bappear 
  MAP: #: 105 Name: map_bappear 
  VAG: #: 146 Name: vag_bappear File: bappear.vag
   VAG SIZE: 5600
   VAG RATE: 12500
   VAG POS: 1208320
Sequence: 217 Name: ST_IND_ARROW_FIRE
 PATCH: #: 106 Name: pat_barrow2 
  MAP: #: 106 Name: map_barrow2 
  VAG: #: 147 Name: vag_barrow2 File: barrow2.vag
   VAG SIZE: 10864
   VAG RATE: 12500
   VAG POS: 1214464
Sequence: 218 Name: ST_IND_BURN_AXE
 PATCH: #: 107 Name: pat_fireloop 
  MAP: #: 107 Name: map_fireloop 
  VAG: #: 148 Name: vag_fireloop File: fireloop.vag
   VAG SIZE: 9984
   VAG RATE: 12500
   VAG POS: 1226752
Sequence: 219 Name: ST_IND_ARROW_HIT
 PATCH: #: 108 Name: pat_arrowhit 
  MAP: #: 108 Name: map_arrowhit 
  VAG: #: 149 Name: vag_arrowhit File: arrowhit.vag
   VAG SIZE: 3792
   VAG RATE: 12500
   VAG POS: 1236992
Sequence: 220 Name: ST_IND_ARROW_BLK
 PATCH: #: 108 Name: pat_arrowhit 
  MAP: #: 108 Name: map_arrowhit 
  VAG: #: 149 Name: vag_arrowhit File: arrowhit.vag
   VAG SIZE: 3792
   VAG RATE: 12500
   VAG POS: 1236992
Sequence: 221 Name: ST_IND_AXE_UP
 PATCH: #: 109 Name: pat_kanoax 
  MAP: #: 109 Name: map_kanoax 
  VAG: #: 150 Name: vag_kanoax File: kanoax.vag
   VAG SIZE: 6960
   VAG RATE: 12500
   VAG POS: 1241088
Sequence: 222 Name: ST_IND_KNEE_BREAK
 PATCH: #: 110 Name: pat_crunch2 
  MAP: #: 110 Name: map_crunch2 
  VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
   VAG SIZE: 4656
   VAG RATE: 12500
   VAG POS: 1249280
Sequence: 223 Name: ST_IND_REFLECT
 PATCH: #: 111 Name: pat_salrflc2 
  MAP: #: 111 Name: map_salrflc2 
  VAG: #: 152 Name: vag_salrflc2 File: salrflc2.vag
   VAG SIZE: 5872
   VAG RATE: 12500
   VAG POS: 1255424
Sequence: 224 Name: ST_IND_SHADOW_SHLD
 PATCH: #: 112 Name: pat_salrush 
  MAP: #: 112 Name: map_salrush 
  VAG: #: 153 Name: vag_salrush File: salrush.vag
   VAG SIZE: 5376
   VAG RATE: 12500
   VAG POS: 1261568
Sequence: 225 Name: ST_IND_AXE_HIT
 PATCH: #: 113 Name: pat_faxhit 
  MAP: #: 113 Name: map_faxhit 
  VAG: #: 154 Name: vag_faxhit File: faxhit.vag
   VAG SIZE: 8016
   VAG RATE: 12500
   VAG POS: 1267712
Sequence: 226 Name: FT_IND_LIGHT_START
 PATCH: #: 114 Name: pat_litestrt 
  MAP: #: 114 Name: map_litestrt 
  VAG: #: 155 Name: vag_litestrt File: litestrt.vag
   VAG SIZE: 4832
   VAG RATE: 12500
   VAG POS: 1275904
Sequence: 227 Name: FT_IND_LIGHT_LOOP
 PATCH: #: 115 Name: pat_liteloop 
  MAP: #: 115 Name: map_liteloop 
  VAG: #: 156 Name: vag_liteloop File: liteloop.vag
   VAG SIZE: 9552
   VAG RATE: 12500
   VAG POS: 1282048
Sequence: 228 Name: FT_IND_LIGHT_END
 PATCH: #: 116 Name: pat_liteend 
  MAP: #: 116 Name: map_liteend 
  VAG: #: 157 Name: vag_liteend File: liteend.vag
   VAG SIZE: 6192
   VAG RATE: 12500
   VAG POS: 1292288
Sequence: 229 Name: FT_IND_LIGHT_HIT_AXE
 PATCH: #: 117 Name: pat_strtboom 
  MAP: #: 117 Name: map_strtboom 
  VAG: #: 158 Name: vag_strtboom File: strtboom.vag
   VAG SIZE: 13488
   VAG RATE: 12500
   VAG POS: 1300480
Sequence: 230 Name: FT_IND_LIGHT_HIT_HIM
 PATCH: #: 118 Name: pat_strtbum2 
  MAP: #: 118 Name: map_strtbum2 
  VAG: #: 159 Name: vag_strtbum2 File: strtbum2.vag
   VAG SIZE: 13104
   VAG RATE: 12500
   VAG POS: 1314816
Sequence: 231 Name: ST_LIA_HAIR
 PATCH: #: 5 Name: pat_hairthrw 
  MAP: #: 5 Name: map_hairthrw 
  VAG: #: 36 Name: vag_hairthrw File: hairthrw.vag
   VAG SIZE: 11376
   VAG RATE: 15624
   VAG POS: 301056
Sequence: 232 Name: ST_LIA_SCREAM
 PATCH: #: 6 Name: pat_scrmtrp2 
  MAP: #: 6 Name: map_scrmtrp2 
  VAG: #: 37 Name: vag_scrmtrp2 File: scrmtrp2.vag
   VAG SIZE: 16176
   VAG RATE: 15624
   VAG POS: 313344
Sequence: 233 Name: ST_LIA_FIREBALL
 PATCH: #: 26 Name: pat_mfirebal 
  MAP: #: 26 Name: map_mfirebal 
  VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
   VAG SIZE: 11296
   VAG RATE: 15624
   VAG POS: 204800
Sequence: 234 Name: ST_LIA_FIREBALL_HIT
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 235 Name: ST_LIA_FIREBALL_BLOCK
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 236 Name: ST_LIA_FLY_UP
 PATCH: #: 7 Name: pat_flying1 
  MAP: #: 7 Name: map_flying1 
  VAG: #: 39 Name: vag_flying1 File: flying1.vag
   VAG SIZE: 8352
   VAG RATE: 15624
   VAG POS: 335872
Sequence: 237 Name: ST_LIA_FLY_ACROSS
 PATCH: #: 7 Name: pat_flying1 
  MAP: #: 7 Name: map_flying1 
  VAG: #: 39 Name: vag_flying1 File: flying1.vag
   VAG SIZE: 8352
   VAG RATE: 15624
   VAG POS: 335872
Sequence: 238 Name: FT_LIA_HAIR
 PATCH: #: 284 Name: pat_whirl1 
  MAP: #: 284 Name: map_whirl1 
  VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
   VAG SIZE: 14448
   VAG RATE: 15624
   VAG POS: 2486272
Sequence: 239 Name: FT_LIA_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 240 Name: FT_LIA_TWFUN
 PATCH: #: 269 Name: pat_pbfun 
  MAP: #: 269 Name: map_pbfun 
  VAG: #: 260 Name: vag_pbfun File: pbfun.vag
   VAG SIZE: 11296
   VAG RATE: 12500
   VAG POS: 2340864
Sequence: 241 Name: ST_ROBO_ARM_OUT
 PATCH: #: 235 Name: pat_roboout 
  MAP: #: 235 Name: map_roboout 
  VAG: #: 117 Name: vag_roboout File: roboout.vag
   VAG SIZE: 3776
   VAG RATE: 15624
   VAG POS: 968704
Sequence: 242 Name: ST_ROBO_ARM_THROW
 PATCH: #: 236 Name: pat_robothrw 
  MAP: #: 236 Name: map_robothrw 
  VAG: #: 118 Name: vag_robothrw File: robothrw.vag
   VAG SIZE: 6432
   VAG RATE: 15624
   VAG POS: 972800
Sequence: 243 Name: ST_ROBO_ARM_IN
 PATCH: #: 237 Name: pat_roboin 
  MAP: #: 237 Name: map_roboin 
  VAG: #: 119 Name: vag_roboin File: roboin.vag
   VAG SIZE: 2736
   VAG RATE: 15624
   VAG POS: 980992
Sequence: 244 Name: ST_ROBO_FAST_ROCKET
 PATCH: #: 238 Name: pat_robrock1 
  MAP: #: 238 Name: map_robrock1 
  VAG: #: 120 Name: vag_robrock1 File: robrock1.vag
   VAG SIZE: 12336
   VAG RATE: 15624
   VAG POS: 985088
Sequence: 245 Name: ST_ROBO_CRUISE_ROCKET
 PATCH: #: 239 Name: pat_robrock2 
  MAP: #: 239 Name: map_robrock2 
  VAG: #: 121 Name: vag_robrock2 File: robrock2.vag
   VAG SIZE: 5472
   VAG RATE: 15624
   VAG POS: 999424
Sequence: 246 Name: ST_ROBO_NET
 PATCH: #: 55 Name: pat_netthrow 
  MAP: #: 55 Name: map_netthrow 
  VAG: #: 102 Name: vag_netthrow File: netthrow.vag
   VAG SIZE: 9520
   VAG RATE: 15624
   VAG POS: 851968
Sequence: 247 Name: ST_ROBO_ZAP_HIT
 PATCH: #: 149 Name: pat_rockhit 
  MAP: #: 149 Name: map_rockhit 
  VAG: #: 168 Name: vag_rockhit File: rockhit.vag
   VAG SIZE: 7600
   VAG RATE: 15624
   VAG POS: 1392640
Sequence: 248 Name: ST_ROBO_DROP
 PATCH: #: 96 Name: pat_robtele 
  MAP: #: 96 Name: map_robtele 
  VAG: #: 116 Name: vag_robtele File: robtele.vag
   VAG SIZE: 10080
   VAG RATE: 15624
   VAG POS: 958464
Sequence: 249 Name: ST_ROBO_CLANG1
 PATCH: #: 56 Name: pat_robball1 
  MAP: #: 56 Name: map_robball1 
  VAG: #: 103 Name: vag_robball1 File: robball1.vag
   VAG SIZE: 3408
   VAG RATE: 15624
   VAG POS: 862208
Sequence: 250 Name: ST_ROBO_CLANG2
 PATCH: #: 57 Name: pat_robball2 
  MAP: #: 57 Name: map_robball2 
  VAG: #: 104 Name: vag_robball2 File: robball2.vag
   VAG SIZE: 3344
   VAG RATE: 15624
   VAG POS: 866304
Sequence: 251 Name: ST_ROBO_BALL_BOOM
 PATCH: #: 58 Name: pat_robxplod 
  MAP: #: 58 Name: map_robxplod 
  VAG: #: 105 Name: vag_robxplod File: robxplod.vag
   VAG SIZE: 14656
   VAG RATE: 15624
   VAG POS: 870400
Sequence: 252 Name: ST_ROBO_CHEST_OPEN
 PATCH: #: 59 Name: pat_robclik 
  MAP: #: 59 Name: map_robclik 
  VAG: #: 106 Name: vag_robclik File: robclik.vag
   VAG SIZE: 4240
   VAG RATE: 15624
   VAG POS: 886784
Sequence: 253 Name: ST_ROBO_TARGET
 PATCH: #: 60 Name: pat_robowarn 
  MAP: #: 60 Name: map_robowarn 
  VAG: #: 107 Name: vag_robowarn File: robowarn.vag
   VAG SIZE: 1904
   VAG RATE: 15624
   VAG POS: 892928
Sequence: 254 Name: ST_ROBO_EXPLODE
 PATCH: #: 61 Name: pat_robxpld2 
  MAP: #: 61 Name: map_robxpld2 
  VAG: #: 108 Name: vag_robxpld2 File: robxpld2.vag
   VAG SIZE: 7728
   VAG RATE: 15624
   VAG POS: 894976
Sequence: 255 Name: ST_ROBO_IMPLODE
 PATCH: #: 62 Name: pat_robimpld 
  MAP: #: 62 Name: map_robimpld 
  VAG: #: 109 Name: vag_robimpld File: robimpld.vag
   VAG SIZE: 4512
   VAG RATE: 15624
   VAG POS: 903168
Sequence: 256 Name: ST_ROBO_SPARK1
 PATCH: #: 63 Name: pat_robshk1 
  MAP: #: 63 Name: map_robshk1 
  VAG: #: 110 Name: vag_robshk1 File: robshk1.vag
   VAG SIZE: 4848
   VAG RATE: 15624
   VAG POS: 909312
Sequence: 257 Name: ST_ROBO_SPARK2
 PATCH: #: 48 Name: pat_robshk2 
  MAP: #: 48 Name: map_robshk2 
  VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
   VAG SIZE: 4720
   VAG RATE: 15624
   VAG POS: 952320
Sequence: 258 Name: ST_ROBO_SPARK3
 PATCH: #: 64 Name: pat_robshk3 
  MAP: #: 64 Name: map_robshk3 
  VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
   VAG SIZE: 6704
   VAG RATE: 15624
   VAG POS: 915456
Sequence: 259 Name: ST_ROBO_SPEECH1
 PATCH: #: 65 Name: pat_robtalk1 
  MAP: #: 65 Name: map_robtalk1 
  VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 923648
Sequence: 260 Name: ST_ROBO_SPEECH2
 PATCH: #: 52 Name: pat_robtalk2 
  MAP: #: 52 Name: map_robtalk2 
  VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
   VAG SIZE: 5952
   VAG RATE: 15624
   VAG POS: 827392
Sequence: 261 Name: ST_ROBO_SPEECH3
 PATCH: #: 65 Name: pat_robtalk1 
  MAP: #: 65 Name: map_robtalk1 
  VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 923648
Sequence: 262 Name: ST_ROBO_SPEECH4
 PATCH: #: 66 Name: pat_netblk 
  MAP: #: 66 Name: map_netblk 
  VAG: #: 113 Name: vag_netblk File: netblk.vag
   VAG SIZE: 6016
   VAG RATE: 15624
   VAG POS: 935936
Sequence: 263 Name: ST_ROBO_SPEAR
 PATCH: #: 265 Name: pat_bigstab2 
  MAP: #: 265 Name: map_bigstab2 
  VAG: #: 256 Name: vag_bigstab2 File: bigstab2.vag
   VAG SIZE: 6032
   VAG RATE: 12500
   VAG POS: 2314240
Sequence: 264 Name: ST_ROBO_CRSH_SMOOSH2
 PATCH: #: 273 Name: pat_scorthr1 
  MAP: #: 273 Name: map_scorthr1 
  VAG: #: 264 Name: vag_scorthr1 File: scorthr1.vag
   VAG SIZE: 15696
   VAG RATE: 15624
   VAG POS: 2383872
Sequence: 265 Name: FT_ROBO_CRSH_OUT
 PATCH: #: 270 Name: pat_robpres1 
  MAP: #: 270 Name: map_robpres1 
  VAG: #: 261 Name: vag_robpres1 File: robpres1.vag
   VAG SIZE: 8400
   VAG RATE: 12500
   VAG POS: 2353152
Sequence: 266 Name: FT_ROBO_CRSH_DOWN
 PATCH: #: 271 Name: pat_robpres2 
  MAP: #: 271 Name: map_robpres2 
  VAG: #: 262 Name: vag_robpres2 File: robpres2.vag
   VAG SIZE: 9728
   VAG RATE: 15624
   VAG POS: 2363392
Sequence: 267 Name: FT_ROBO_CRSH_SMOOSH1
 PATCH: #: 272 Name: pat_bodsmsh1 
  MAP: #: 272 Name: map_bodsmsh1 
  VAG: #: 263 Name: vag_bodsmsh1 File: bodsmsh1.vag
   VAG SIZE: 9248
   VAG RATE: 15624
   VAG POS: 2373632
Sequence: 268 Name: FT_ROBO_CRSH_SMOOSH2
 PATCH: #: 273 Name: pat_scorthr1 
  MAP: #: 273 Name: map_scorthr1 
  VAG: #: 264 Name: vag_scorthr1 File: scorthr1.vag
   VAG SIZE: 15696
   VAG RATE: 15624
   VAG POS: 2383872
Sequence: 269 Name: FT_ROBO_BEEP
 PATCH: #: 274 Name: pat_beep1 
  MAP: #: 274 Name: map_beep1 
  VAG: #: 265 Name: vag_beep1 File: beep1.vag
   VAG SIZE: 2048
   VAG RATE: 15624
   VAG POS: 2400256
Sequence: 270 Name: FT_ROBO_SELF_DESTRUCT
 PATCH: #: 275 Name: pat_compfx1 
  MAP: #: 275 Name: map_compfx1 
  VAG: #: 266 Name: vag_compfx1 File: compfx1.vag
   VAG SIZE: 11920
   VAG RATE: 15624
   VAG POS: 2402304
Sequence: 271 Name: FT_ROBO_DESTRUCT_LAFF
 PATCH: #: 276 Name: pat_roblaff 
  MAP: #: 276 Name: map_roblaff 
  VAG: #: 267 Name: vag_roblaff File: roblaff.vag
   VAG SIZE: 5520
   VAG RATE: 15624
   VAG POS: 2414592
Sequence: 272 Name: FT_ROBO_WHIRL
 PATCH: #: 284 Name: pat_whirl1 
  MAP: #: 284 Name: map_whirl1 
  VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
   VAG SIZE: 14448
   VAG RATE: 15624
   VAG POS: 2486272
Sequence: 273 Name: FT_ROBO_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 274 Name: FT_ROBO_BAT
 PATCH: #: 277 Name: pat_bird 
  MAP: #: 277 Name: map_bird 
  VAG: #: 268 Name: vag_bird File: bird.vag
   VAG SIZE: 11024
   VAG RATE: 15624
   VAG POS: 2420736
Sequence: 275 Name: FT_ROBO_SLIDE
 PATCH: #: 278 Name: pat_slide 
  MAP: #: 278 Name: map_slide 
  VAG: #: 269 Name: vag_slide File: slide.vag
   VAG SIZE: 5232
   VAG RATE: 15624
   VAG POS: 2433024
Sequence: 276 Name: FT_ROBO_HORN
 PATCH: #: 279 Name: pat_horn 
  MAP: #: 279 Name: map_horn 
  VAG: #: 270 Name: vag_horn File: horn.vag
   VAG SIZE: 10000
   VAG RATE: 15624
   VAG POS: 2439168
Sequence: 277 Name: ST_LAO_ANGLE
 PATCH: #: 227 Name: pat_lkflykck 
  MAP: #: 227 Name: map_lkflykck 
  VAG: #: 136 Name: vag_lkflykck File: lkflykck.vag
   VAG SIZE: 5264
   VAG RATE: 15624
   VAG POS: 1130496
Sequence: 278 Name: ST_LAO_HAT_THROW
 PATCH: #: 228 Name: pat_kungthrw 
  MAP: #: 228 Name: map_kungthrw 
  VAG: #: 225 Name: vag_kungthrw File: kungthrw.vag
   VAG SIZE: 12368
   VAG RATE: 15624
   VAG POS: 2064384
Sequence: 279 Name: ST_LAO_HAT_HIT
 PATCH: #: 229 Name: pat_sawhit 
  MAP: #: 229 Name: map_sawhit 
  VAG: #: 226 Name: vag_sawhit File: sawhit.vag
   VAG SIZE: 6016
   VAG RATE: 15624
   VAG POS: 2078720
Sequence: 280 Name: ST_LAO_HAT_BLK
 PATCH: #: 229 Name: pat_sawhit 
  MAP: #: 229 Name: map_sawhit 
  VAG: #: 226 Name: vag_sawhit File: sawhit.vag
   VAG SIZE: 6016
   VAG RATE: 15624
   VAG POS: 2078720
Sequence: 281 Name: ST_LAO_TELE
 PATCH: #: 96 Name: pat_robtele 
  MAP: #: 96 Name: map_robtele 
  VAG: #: 116 Name: vag_robtele File: robtele.vag
   VAG SIZE: 10080
   VAG RATE: 15624
   VAG POS: 958464
Sequence: 282 Name: ST_LAO_THROW
 PATCH: #: 230 Name: pat_dfklthro 
  MAP: #: 230 Name: map_dfklthro 
  VAG: #: 73 Name: vag_dfklthro File: dfklthro.vag
   VAG SIZE: 9168
   VAG RATE: 15624
   VAG POS: 624640
Sequence: 283 Name: FT_LAO_TORNADO
 PATCH: #: 284 Name: pat_whirl1 
  MAP: #: 284 Name: map_whirl1 
  VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
   VAG SIZE: 14448
   VAG RATE: 15624
   VAG POS: 2486272
Sequence: 284 Name: FT_LAO_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 285 Name: FT_LAO_TORNADO_HIT
 PATCH: #: 229 Name: pat_sawhit 
  MAP: #: 229 Name: map_sawhit 
  VAG: #: 226 Name: vag_sawhit File: sawhit.vag
   VAG SIZE: 6016
   VAG RATE: 15624
   VAG POS: 2078720
Sequence: 286 Name: ST_TUSK_ZAP
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 287 Name: ST_TUSK_CANNON
 PATCH: #: 79 Name: pat_cabalgun 
  MAP: #: 79 Name: map_cabalgun 
  VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
   VAG SIZE: 13568
   VAG RATE: 15624
   VAG POS: 1161216
Sequence: 288 Name: ST_TUSK_BLURR
 PATCH: #: 217 Name: pat_eblrun 
  MAP: #: 217 Name: map_eblrun 
  VAG: #: 76 Name: vag_eblrun File: eblrun.vag
   VAG SIZE: 11648
   VAG RATE: 15624
   VAG POS: 669696
Sequence: 289 Name: ST_TUSK_FLIP1
 PATCH: #: 80 Name: pat_hicwhsh 
  MAP: #: 80 Name: map_hicwhsh 
  VAG: #: 141 Name: vag_hicwhsh File: hicwhsh.vag
   VAG SIZE: 1968
   VAG RATE: 15624
   VAG POS: 1175552
Sequence: 290 Name: ST_TUSK_FLIP2
 PATCH: #: 81 Name: pat_locwhsh 
  MAP: #: 81 Name: map_locwhsh 
  VAG: #: 142 Name: vag_locwhsh File: locwhsh.vag
   VAG SIZE: 2352
   VAG RATE: 12500
   VAG POS: 1177600
Sequence: 291 Name: ST_TUSK_SAW
 PATCH: #: 229 Name: pat_sawhit 
  MAP: #: 229 Name: map_sawhit 
  VAG: #: 226 Name: vag_sawhit File: sawhit.vag
   VAG SIZE: 6016
   VAG RATE: 15624
   VAG POS: 2078720
Sequence: 292 Name: ST_TUSK_REFLECT
 PATCH: #: 111 Name: pat_salrflc2 
  MAP: #: 111 Name: map_salrflc2 
  VAG: #: 152 Name: vag_salrflc2 File: salrflc2.vag
   VAG SIZE: 5872
   VAG RATE: 12500
   VAG POS: 1255424
Sequence: 293 Name: FT_TUSK_BUBBLE
 PATCH: #: 281 Name: pat_bubble1 
  MAP: #: 281 Name: map_bubble1 
  VAG: #: 272 Name: vag_bubble1 File: bubble1.vag
   VAG SIZE: 3216
   VAG RATE: 15624
   VAG POS: 2461696
Sequence: 294 Name: FT_TUSK_GROW
 PATCH: #: 282 Name: pat_headgrow 
  MAP: #: 282 Name: map_headgrow 
  VAG: #: 273 Name: vag_headgrow File: headgrow.vag
   VAG SIZE: 5856
   VAG RATE: 15624
   VAG POS: 2465792
Sequence: 295 Name: FT_TUSK_HEAD_SCREAM
 PATCH: #: 283 Name: pat_cblscrm 
  MAP: #: 283 Name: map_cblscrm 
  VAG: #: 274 Name: vag_cblscrm File: cblscrm.vag
   VAG SIZE: 12736
   VAG RATE: 15624
   VAG POS: 2471936
Sequence: 296 Name: FT_TUSK_BURN
 PATCH: #: 107 Name: pat_fireloop 
  MAP: #: 107 Name: map_fireloop 
  VAG: #: 148 Name: vag_fireloop File: fireloop.vag
   VAG SIZE: 9984
   VAG RATE: 12500
   VAG POS: 1226752
Sequence: 297 Name: ST_SG_TELE
 PATCH: #: 93 Name: pat_shevtele 
  MAP: #: 93 Name: map_shevtele 
  VAG: #: 143 Name: vag_shevtele File: shevtele.vag
   VAG SIZE: 6704
   VAG RATE: 15624
   VAG POS: 1181696
Sequence: 298 Name: ST_SG_POUNCE
 PATCH: #: 94 Name: pat_bigbslam 
  MAP: #: 94 Name: map_bigbslam 
  VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
   VAG SIZE: 8416
   VAG RATE: 15624
   VAG POS: 1189888
Sequence: 299 Name: ST_SG_ZAP_HIT
 PATCH: #: 95 Name: pat_forcehit 
  MAP: #: 95 Name: map_forcehit 
  VAG: #: 145 Name: vag_forcehit File: forcehit.vag
   VAG SIZE: 8080
   VAG RATE: 15624
   VAG POS: 1200128
Sequence: 300 Name: ST_SG_ZAP
 PATCH: #: 42 Name: pat_sonyafly 
  MAP: #: 42 Name: map_sonyafly 
  VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
   VAG SIZE: 8288
   VAG RATE: 15624
   VAG POS: 1144832
Sequence: 301 Name: ST_SG_ZAP_START
 PATCH: #: 96 Name: pat_robtele 
  MAP: #: 96 Name: map_robtele 
  VAG: #: 116 Name: vag_robtele File: robtele.vag
   VAG SIZE: 10080
   VAG RATE: 15624
   VAG POS: 958464
Sequence: 302 Name: FT_SG_KLANG
 PATCH: #: 289 Name: pat_mhit2 
  MAP: #: 289 Name: map_mhit2 
  VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
   VAG SIZE: 3568
   VAG RATE: 12500
   VAG POS: 2531328
Sequence: 303 Name: FT_SG_SPIN
 PATCH: #: 284 Name: pat_whirl1 
  MAP: #: 284 Name: map_whirl1 
  VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
   VAG SIZE: 14448
   VAG RATE: 15624
   VAG POS: 2486272
Sequence: 304 Name: ST_ST_CANNON
 PATCH: #: 145 Name: pat_jaxcann 
  MAP: #: 145 Name: map_jaxcann 
  VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
   VAG SIZE: 12880
   VAG RATE: 15624
   VAG POS: 1347584
Sequence: 305 Name: ST_ST_SUMMON
 PATCH: #: 146 Name: pat_stfireup 
  MAP: #: 146 Name: map_stfireup 
  VAG: #: 165 Name: vag_stfireup File: stfireup.vag
   VAG SIZE: 11072
   VAG RATE: 15624
   VAG POS: 1361920
Sequence: 306 Name: ST_ST_FIRE_HIT
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 307 Name: ST_ST_SKULL
 PATCH: #: 147 Name: pat_sonforce 
  MAP: #: 147 Name: map_sonforce 
  VAG: #: 166 Name: vag_sonforce File: sonforce.vag
   VAG SIZE: 9856
   VAG RATE: 15624
   VAG POS: 1374208
Sequence: 308 Name: ST_ST_MORPH
 PATCH: #: 148 Name: pat_stmorph 
  MAP: #: 148 Name: map_stmorph 
  VAG: #: 167 Name: vag_stmorph File: stmorph.vag
   VAG SIZE: 7072
   VAG RATE: 15624
   VAG POS: 1384448
Sequence: 309 Name: ST_ST_FLIP
 PATCH: #: 136 Name: pat_gudfall4 
  MAP: #: 136 Name: map_gudfall4 
  VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
   VAG SIZE: 4608
   VAG RATE: 12500
   VAG POS: 1341440
Sequence: 310 Name: ST_ST_FIRE_BLK
 PATCH: #: 78 Name: pat_fballhit 
  MAP: #: 78 Name: map_fballhit 
  VAG: #: 38 Name: vag_fballhit File: fballhit.vag
   VAG SIZE: 5408
   VAG RATE: 15624
   VAG POS: 329728
Sequence: 311 Name: FT_ST_SWORD
 PATCH: #: 285 Name: pat_swords 
  MAP: #: 285 Name: map_swords 
  VAG: #: 276 Name: vag_swords File: swords.vag
   VAG SIZE: 9792
   VAG RATE: 15624
   VAG POS: 2502656
Sequence: 312 Name: FT_ST_CLOUD
 PATCH: #: 286 Name: pat_stcloud1 
  MAP: #: 286 Name: map_stcloud1 
  VAG: #: 277 Name: vag_stcloud1 File: stcloud1.vag
   VAG SIZE: 3888
   VAG RATE: 15624
   VAG POS: 2512896
Sequence: 313 Name: FT_ST_BONES
 PATCH: #: 28 Name: pat_bones 
  MAP: #: 28 Name: map_bones 
  VAG: #: 23 Name: vag_bones File: bones.vag
   VAG SIZE: 4784
   VAG RATE: 15624
   VAG POS: 241664
Sequence: 314 Name: ST_LK_FIRE
 PATCH: #: 40 Name: pat_lkfball 
  MAP: #: 40 Name: map_lkfball 
  VAG: #: 133 Name: vag_lkfball File: lkfball.vag
   VAG SIZE: 8768
   VAG RATE: 15624
   VAG POS: 1095680
Sequence: 315 Name: ST_LK_FIRE_HIT
 PATCH: #: 41 Name: pat_axhit 
  MAP: #: 41 Name: map_axhit 
  VAG: #: 137 Name: vag_axhit File: axhit.vag
   VAG SIZE: 7536
   VAG RATE: 15624
   VAG POS: 1136640
Sequence: 316 Name: ST_LK_SUPER_KICK
 PATCH: #: 42 Name: pat_sonyafly 
  MAP: #: 42 Name: map_sonyafly 
  VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
   VAG SIZE: 8288
   VAG RATE: 15624
   VAG POS: 1144832
Sequence: 317 Name: ST_LK_FIRE_BLK
 PATCH: #: 41 Name: pat_axhit 
  MAP: #: 41 Name: map_axhit 
  VAG: #: 137 Name: vag_axhit File: axhit.vag
   VAG SIZE: 7536
   VAG RATE: 15624
   VAG POS: 1136640
Sequence: 318 Name: ST_LK_BIKE
 PATCH: #: 43 Name: pat_lkcycle 
  MAP: #: 43 Name: map_lkcycle 
  VAG: #: 134 Name: vag_lkcycle File: lkcycle.vag
   VAG SIZE: 11024
   VAG RATE: 12500
   VAG POS: 1105920
Sequence: 319 Name: ST_LK_FLY_YELL
 PATCH: #: 44 Name: pat_lkflyink 
  MAP: #: 44 Name: map_lkflyink 
  VAG: #: 135 Name: vag_lkflyink File: lkflyink.vag
   VAG SIZE: 12240
   VAG RATE: 12500
   VAG POS: 1118208
Sequence: 320 Name: FT_LK_FLAME_MORPH
 PATCH: #: 287 Name: pat_flmorph 
  MAP: #: 287 Name: map_flmorph 
  VAG: #: 278 Name: vag_flmorph File: flmorph.vag
   VAG SIZE: 9312
   VAG RATE: 15624
   VAG POS: 2516992
Sequence: 321 Name: FT_LK_RUN
 PATCH: #: 30 Name: pat_lkrun 
  MAP: #: 30 Name: map_lkrun 
  VAG: #: 123 Name: vag_lkrun File: lkrun.vag
   VAG SIZE: 10592
   VAG RATE: 12500
   VAG POS: 1028096
Sequence: 322 Name: FT_LK_SLAM
 PATCH: #: 94 Name: pat_bigbslam 
  MAP: #: 94 Name: map_bigbslam 
  VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
   VAG SIZE: 8416
   VAG RATE: 15624
   VAG POS: 1189888
Sequence: 323 Name: ST_MOT_EFF1
 PATCH: #: 211 Name: pat_centaur1 
  MAP: #: 211 Name: map_centaur1 
  VAG: #: 220 Name: vag_centaur1 File: centaur1.vag
   VAG SIZE: 10640
   VAG RATE: 12500
   VAG POS: 2013184
Sequence: 324 Name: ST_MOT_EFF2
 PATCH: #: 212 Name: pat_centaur2 
  MAP: #: 212 Name: map_centaur2 
  VAG: #: 221 Name: vag_centaur2 File: centaur2.vag
   VAG SIZE: 12784
   VAG RATE: 12500
   VAG POS: 2025472
Sequence: 325 Name: ST_MOT_EFF3
 PATCH: #: 213 Name: pat_hboss1 
  MAP: #: 213 Name: map_hboss1 
  VAG: #: 222 Name: vag_hboss1 File: hboss1.vag
   VAG SIZE: 8192
   VAG RATE: 12500
   VAG POS: 2039808
Sequence: 326 Name: ST_MOT_HIT1
 PATCH: #: 214 Name: pat_hhit1 
  MAP: #: 214 Name: map_hhit1 
  VAG: #: 223 Name: vag_hhit1 File: hhit1.vag
   VAG SIZE: 4672
   VAG RATE: 12500
   VAG POS: 2048000
Sequence: 327 Name: ST_MOT_HIT2
 PATCH: #: 215 Name: pat_hhit2 
  MAP: #: 215 Name: map_hhit2 
  VAG: #: 224 Name: vag_hhit2 File: hhit2.vag
   VAG SIZE: 8400
   VAG RATE: 12500
   VAG POS: 2054144
Sequence: 328 Name: ST_MOT_FIRE
 PATCH: #: 79 Name: pat_cabalgun 
  MAP: #: 79 Name: map_cabalgun 
  VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
   VAG SIZE: 13568
   VAG RATE: 15624
   VAG POS: 1161216
Sequence: 329 Name: ST_SK_SHOULDER
 PATCH: #: 112 Name: pat_salrush 
  MAP: #: 112 Name: map_salrush 
  VAG: #: 153 Name: vag_salrush File: salrush.vag
   VAG SIZE: 5376
   VAG RATE: 12500
   VAG POS: 1261568
Sequence: 330 Name: ST_SK_PHOTON
 PATCH: #: 79 Name: pat_cabalgun 
  MAP: #: 79 Name: map_cabalgun 
  VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
   VAG SIZE: 13568
   VAG RATE: 15624
   VAG POS: 1161216
Sequence: 331 Name: ST_SK_KLANG1
 PATCH: #: 288 Name: pat_mhit1 
  MAP: #: 288 Name: map_mhit1 
  VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
   VAG SIZE: 3184
   VAG RATE: 12500
   VAG POS: 2527232
Sequence: 332 Name: ST_SK_KLANG2
 PATCH: #: 289 Name: pat_mhit2 
  MAP: #: 289 Name: map_mhit2 
  VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
   VAG SIZE: 3568
   VAG RATE: 12500
   VAG POS: 2531328
Sequence: 333 Name: ST_SK_LAUGH1
 PATCH: #: 185 Name: pat_sklaff2 
  MAP: #: 185 Name: map_sklaff2 
  VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
   VAG SIZE: 14544
   VAG RATE: 11000
   VAG POS: 1818624
Sequence: 334 Name: ST_SK_LAUGH2
 PATCH: #: 185 Name: pat_sklaff2 
  MAP: #: 185 Name: map_sklaff2 
  VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
   VAG SIZE: 14544
   VAG RATE: 11000
   VAG POS: 1818624
Sequence: 335 Name: ST_SK_LAUGH3
 PATCH: #: 186 Name: pat_sklaff3 
  MAP: #: 186 Name: map_sklaff3 
  VAG: #: 205 Name: vag_sklaff3 File: sklaff3.vag
   VAG SIZE: 17248
   VAG RATE: 11000
   VAG POS: 1835008
Sequence: 336 Name: ST_SK_PATHETIC
 PATCH: #: 290 Name: pat_skpathet 
  MAP: #: 290 Name: map_skpathet 
  VAG: #: 281 Name: vag_skpathet File: skpathet.vag
   VAG SIZE: 20016
   VAG RATE: 15624
   VAG POS: 2535424
Sequence: 337 Name: ST_SK_NEVER_WIN
 PATCH: #: 190 Name: pat_sknvrwin 
  MAP: #: 190 Name: map_sknvrwin 
  VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
   VAG SIZE: 18112
   VAG RATE: 15624
   VAG POS: 1865728
Sequence: 338 Name: ST_SK_STILL_TRYING
 PATCH: #: 291 Name: pat_sktrying 
  MAP: #: 291 Name: map_sktrying 
  VAG: #: 282 Name: vag_sktrying File: sktrying.vag
   VAG SIZE: 19200
   VAG RATE: 15624
   VAG POS: 2555904
Sequence: 339 Name: ST_SK_DONT_LAUGH
 PATCH: #: 292 Name: pat_skdntlaf 
  MAP: #: 292 Name: map_skdntlaf 
  VAG: #: 283 Name: vag_skdntlaf File: skdntlaf.vag
   VAG SIZE: 14176
   VAG RATE: 15624
   VAG POS: 2576384
Sequence: 340 Name: TS_DF_TOASTY
 PATCH: #: 22 Name: pat_dftosty 
  MAP: #: 22 Name: map_dftosty 
  VAG: #: 17 Name: vag_dftosty File: dftosty.vag
   VAG SIZE: 4992
   VAG RATE: 12500
   VAG POS: 174080
Sequence: 341 Name: TS_CROWD_OOO
 PATCH: #: 14 Name: pat_oooh 
  MAP: #: 14 Name: map_oooh 
  VAG: #: 8 Name: vag_oooh File: oooh.vag
   VAG SIZE: 14320
   VAG RATE: 15624
   VAG POS: 100352
Sequence: 342 Name: TS_SK_SUBERB
 PATCH: #: 182 Name: pat_sksuperb 
  MAP: #: 182 Name: map_sksuperb 
  VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
   VAG SIZE: 7488
   VAG RATE: 11000
   VAG POS: 1783808
Sequence: 343 Name: TS_SK_WELL_DONE
 PATCH: #: 183 Name: pat_skweldon 
  MAP: #: 183 Name: map_skweldon 
  VAG: #: 202 Name: vag_skweldon File: skweldon.vag
   VAG SIZE: 7632
   VAG RATE: 11000
   VAG POS: 1792000
Sequence: 344 Name: TS_SK_FIN_HIM
 PATCH: #: 152 Name: pat_skfinhim 
  MAP: #: 152 Name: map_skfinhim 
  VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
   VAG SIZE: 15488
   VAG RATE: 15624
   VAG POS: 1421312
Sequence: 345 Name: TS_SK_FIN_HER
 PATCH: #: 153 Name: pat_skfinher 
  MAP: #: 153 Name: map_skfinher 
  VAG: #: 172 Name: vag_skfinher File: skfinher.vag
   VAG SIZE: 14864
   VAG RATE: 15624
   VAG POS: 1437696
Sequence: 346 Name: TS_RNDHOUSE
 PATCH: #: 8 Name: pat_rhouse 
  MAP: #: 8 Name: map_rhouse 
  VAG: #: 2 Name: vag_rhouse File: rhouse.vag
   VAG SIZE: 4784
   VAG RATE: 15624
   VAG POS: 28672
Sequence: 347 Name: TS_CLOCK_TICK
 PATCH: #: 19 Name: pat_timeout1 
  MAP: #: 19 Name: map_timeout1 
  VAG: #: 14 Name: vag_timeout1 File: timeout1.vag
   VAG SIZE: 1904
   VAG RATE: 15624
   VAG POS: 159744
Sequence: 348 Name: TS_EXP_ST1
 PATCH: #: 117 Name: pat_strtboom 
  MAP: #: 117 Name: map_strtboom 
  VAG: #: 158 Name: vag_strtboom File: strtboom.vag
   VAG SIZE: 13488
   VAG RATE: 12500
   VAG POS: 1300480
Sequence: 349 Name: TS_EXP_ST2
 PATCH: #: 117 Name: pat_strtboom 
  MAP: #: 117 Name: map_strtboom 
  VAG: #: 158 Name: vag_strtboom File: strtboom.vag
   VAG SIZE: 13488
   VAG RATE: 12500
   VAG POS: 1300480
Sequence: 350 Name: TS_ROCK_MOVE
 PATCH: #: 0 Name: pat_srock 
  MAP: #: 1 Name: map_srock 
  VAG: #: 0 Name: vag_srock File: srock.vag
   VAG SIZE: 18272
   VAG RATE: 44100
   VAG POS: 0
Sequence: 351 Name: TS_DRAGON_SPIN
 PATCH: #: 1 Name: pat_swhoosh 
  MAP: #: 2 Name: map_swhoosh 
  VAG: #: 1 Name: vag_swhoosh File: swhoosh.vag
   VAG SIZE: 8912
   VAG RATE: 22050
   VAG POS: 18432
Sequence: 352 Name: TS_CHURCH_BELL
 PATCH: #: 3 Name: pat_bell 
  MAP: #: 3 Name: map_bell 
  VAG: #: 9 Name: vag_bell File: bell.vag
   VAG SIZE: 22480
   VAG RATE: 15624
   VAG POS: 114688
Sequence: 353 Name: TS_BIGDOOR_CLOSE
 PATCH: #: 9 Name: pat_doorslid 
  MAP: #: 9 Name: map_doorslid 
  VAG: #: 3 Name: vag_doorslid File: doorslid.vag
   VAG SIZE: 12336
   VAG RATE: 15624
   VAG POS: 34816
Sequence: 354 Name: TS_BIGDOOR_SLAM
 PATCH: #: 10 Name: pat_newdoor 
  MAP: #: 10 Name: map_newdoor 
  VAG: #: 4 Name: vag_newdoor File: newdoor.vag
   VAG SIZE: 13088
   VAG RATE: 15624
   VAG POS: 49152
Sequence: 355 Name: TS_P1_CURS
 PATCH: #: 15 Name: pat_ncurs1 
  MAP: #: 15 Name: map_ncurs1 
  VAG: #: 10 Name: vag_ncurs1 File: ncurs1.vag
   VAG SIZE: 1872
   VAG RATE: 15624
   VAG POS: 137216
Sequence: 356 Name: TS_P2_CURS
 PATCH: #: 16 Name: pat_ncurs2 
  MAP: #: 16 Name: map_ncurs2 
  VAG: #: 11 Name: vag_ncurs2 File: ncurs2.vag
   VAG SIZE: 1872
   VAG RATE: 15624
   VAG POS: 139264
Sequence: 357 Name: TS_P1_PICKED
 PATCH: #: 17 Name: pat_picked1 
  MAP: #: 17 Name: map_picked1 
  VAG: #: 12 Name: vag_picked1 File: picked1.vag
   VAG SIZE: 8352
   VAG RATE: 15624
   VAG POS: 141312
Sequence: 358 Name: TS_P2_PICKED
 PATCH: #: 18 Name: pat_picked2 
  MAP: #: 18 Name: map_picked2 
  VAG: #: 13 Name: vag_picked2 File: picked2.vag
   VAG SIZE: 7936
   VAG RATE: 15624
   VAG POS: 151552
Sequence: 359 Name: TS_SK_KANO
 PATCH: #: 154 Name: pat_skkano 
  MAP: #: 154 Name: map_skkano 
  VAG: #: 173 Name: vag_skkano File: skkano.vag
   VAG SIZE: 8896
   VAG RATE: 15624
   VAG POS: 1454080
Sequence: 360 Name: TS_SK_SONYA
 PATCH: #: 155 Name: pat_sksonya 
  MAP: #: 155 Name: map_sksonya 
  VAG: #: 174 Name: vag_sksonya File: sksonya.vag
   VAG SIZE: 18976
   VAG RATE: 31250
   VAG POS: 1464320
Sequence: 361 Name: TS_SK_JAX
 PATCH: #: 156 Name: pat_skjax 
  MAP: #: 156 Name: map_skjax 
  VAG: #: 175 Name: vag_skjax File: skjax.vag
   VAG SIZE: 9648
   VAG RATE: 15624
   VAG POS: 1484800
Sequence: 362 Name: TS_SK_NIGHTWOLF
 PATCH: #: 157 Name: pat_sknitwlf 
  MAP: #: 157 Name: map_sknitwlf 
  VAG: #: 176 Name: vag_sknitwlf File: sknitwlf.vag
   VAG SIZE: 12960
   VAG RATE: 15624
   VAG POS: 1495040
Sequence: 363 Name: TS_SK_SUBZERO
 PATCH: #: 158 Name: pat_sksubz 
  MAP: #: 158 Name: map_sksubz 
  VAG: #: 177 Name: vag_sksubz File: sksubz.vag
   VAG SIZE: 10688
   VAG RATE: 15624
   VAG POS: 1509376
Sequence: 364 Name: TS_SK_STRYKER
 PATCH: #: 159 Name: pat_skstrkr2 
  MAP: #: 159 Name: map_skstrkr2 
  VAG: #: 178 Name: vag_skstrkr2 File: skstrkr2.vag
   VAG SIZE: 8688
   VAG RATE: 15624
   VAG POS: 1521664
Sequence: 365 Name: TS_SK_SINDEL
 PATCH: #: 160 Name: pat_sksindel 
  MAP: #: 160 Name: map_sksindel 
  VAG: #: 179 Name: vag_sksindel File: sksindel.vag
   VAG SIZE: 10480
   VAG RATE: 15624
   VAG POS: 1531904
Sequence: 366 Name: TS_SK_SEKTOR
 PATCH: #: 161 Name: pat_sksector 
  MAP: #: 161 Name: map_sksector 
  VAG: #: 180 Name: vag_sksector File: sksector.vag
   VAG SIZE: 10912
   VAG RATE: 15624
   VAG POS: 1544192
Sequence: 367 Name: TS_SK_CYRAX
 PATCH: #: 162 Name: pat_skcyrax 
  MAP: #: 162 Name: map_skcyrax 
  VAG: #: 181 Name: vag_skcyrax File: skcyrax.vag
   VAG SIZE: 11168
   VAG RATE: 15624
   VAG POS: 1556480
Sequence: 368 Name: TS_SK_LAO
 PATCH: #: 163 Name: pat_skknglao 
  MAP: #: 163 Name: map_skknglao 
  VAG: #: 182 Name: vag_skknglao File: skknglao.vag
   VAG SIZE: 10736
   VAG RATE: 15624
   VAG POS: 1568768
Sequence: 369 Name: TS_SK_KABAL
 PATCH: #: 164 Name: pat_skcabal 
  MAP: #: 164 Name: map_skcabal 
  VAG: #: 183 Name: vag_skcabal File: skcabal.vag
   VAG SIZE: 9984
   VAG RATE: 15624
   VAG POS: 1581056
Sequence: 370 Name: TS_SK_SHEEVA
 PATCH: #: 165 Name: pat_sksheeva 
  MAP: #: 165 Name: map_sksheeva 
  VAG: #: 184 Name: vag_sksheeva File: sksheeva.vag
   VAG SIZE: 8560
   VAG RATE: 15624
   VAG POS: 1591296
Sequence: 371 Name: TS_SK_ST
 PATCH: #: 166 Name: pat_skshang 
  MAP: #: 166 Name: map_skshang 
  VAG: #: 185 Name: vag_skshang File: skshang.vag
   VAG SIZE: 12256
   VAG RATE: 15624
   VAG POS: 1601536
Sequence: 372 Name: TS_SK_LKANG
 PATCH: #: 167 Name: pat_skliukng 
  MAP: #: 167 Name: map_skliukng 
  VAG: #: 186 Name: vag_skliukng File: skliukng.vag
   VAG SIZE: 10688
   VAG RATE: 15624
   VAG POS: 1613824
Sequence: 373 Name: TS_SK_MOTARO
 PATCH: #: 168 Name: pat_skmotaro 
  MAP: #: 168 Name: map_skmotaro 
  VAG: #: 187 Name: vag_skmotaro File: skmotaro.vag
   VAG SIZE: 10736
   VAG RATE: 15624
   VAG POS: 1626112
Sequence: 374 Name: TS_SK_SMOKE
 PATCH: #: 169 Name: pat_sksmoke 
  MAP: #: 169 Name: map_sksmoke 
  VAG: #: 188 Name: vag_sksmoke File: sksmoke.vag
   VAG SIZE: 8288
   VAG RATE: 15624
   VAG POS: 1638400
Sequence: 375 Name: TS_MAP_ZOOM
 PATCH: #: 21 Name: pat_mrtlwhsh 
  MAP: #: 21 Name: map_mrtlwhsh 
  VAG: #: 16 Name: vag_mrtlwhsh File: mrtlwhsh.vag
   VAG SIZE: 10208
   VAG RATE: 15624
   VAG POS: 163840
Sequence: 376 Name: TS_SK_EXCELLENT
 PATCH: #: 189 Name: pat_skexelnt 
  MAP: #: 189 Name: map_skexelnt 
  VAG: #: 206 Name: vag_skexelnt File: skexelnt.vag
   VAG SIZE: 11136
   VAG RATE: 15624
   VAG POS: 1853440
Sequence: 377 Name: TS_SK_NEVER_WIN
 PATCH: #: 190 Name: pat_sknvrwin 
  MAP: #: 190 Name: map_sknvrwin 
  VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
   VAG SIZE: 18112
   VAG RATE: 15624
   VAG POS: 1865728
Sequence: 378 Name: TS_SK_OUTSTANDING
 PATCH: #: 191 Name: pat_skoutstd 
  MAP: #: 191 Name: map_skoutstd 
  VAG: #: 208 Name: vag_skoutstd File: skoutstd.vag
   VAG SIZE: 14752
   VAG RATE: 15624
   VAG POS: 1884160
Sequence: 379 Name: TS_COIN_SND
 PATCH: #: 191 Name: pat_skoutstd 
  MAP: #: 191 Name: map_skoutstd 
  VAG: #: 208 Name: vag_skoutstd File: skoutstd.vag
   VAG SIZE: 14752
   VAG RATE: 15624
   VAG POS: 1884160
Sequence: 380 Name: TS_DF_CRISPY
 PATCH: #: 24 Name: pat_dfcrispy 
  MAP: #: 24 Name: map_dfcrispy 
  VAG: #: 19 Name: vag_dfcrispy File: dfcrispy.vag
   VAG SIZE: 7808
   VAG RATE: 15624
   VAG POS: 188416
Sequence: 381 Name: TS_SHOCK
 PATCH: #: 25 Name: pat_kanlasrl 
  MAP: #: 25 Name: map_kanlasrl 
  VAG: #: 20 Name: vag_kanlasrl File: kanlasrl.vag
   VAG SIZE: 6224
   VAG RATE: 15624
   VAG POS: 196608
Sequence: 382 Name: TS_FLAME
 PATCH: #: 26 Name: pat_mfirebal 
  MAP: #: 26 Name: map_mfirebal 
  VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
   VAG SIZE: 11296
   VAG RATE: 15624
   VAG POS: 204800
Sequence: 383 Name: TS_BURNING
 PATCH: #: 27 Name: pat_ignite 
  MAP: #: 27 Name: map_ignite 
  VAG: #: 22 Name: vag_ignite File: ignite.vag
   VAG SIZE: 23776
   VAG RATE: 15624
   VAG POS: 217088
Sequence: 384 Name: TS_BONES
 PATCH: #: 28 Name: pat_bones 
  MAP: #: 28 Name: map_bones 
  VAG: #: 23 Name: vag_bones File: bones.vag
   VAG SIZE: 4784
   VAG RATE: 15624
   VAG POS: 241664
Sequence: 385 Name: TS_SK_ITS_OFFICIAL
 PATCH: #: 310 Name: pat_skyousuk 
  MAP: #: 310 Name: map_skyousuk 
  VAG: #: 301 Name: vag_skyousuk File: skyousuk.vag
   VAG SIZE: 12704
   VAG RATE: 15624
   VAG POS: 2844672
Sequence: 386 Name: TS_DF_FROSTY
 PATCH: #: 23 Name: pat_dfrosty 
  MAP: #: 23 Name: map_dfrosty 
  VAG: #: 18 Name: vag_dfrosty File: dfrosty.vag
   VAG SIZE: 7056
   VAG RATE: 15624
   VAG POS: 180224
Sequence: 387 Name: TS_SK_FLAWLESS
 PATCH: #: 170 Name: pat_skflawls 
  MAP: #: 170 Name: map_skflawls 
  VAG: #: 189 Name: vag_skflawls File: skflawls.vag
   VAG SIZE: 14736
   VAG RATE: 15624
   VAG POS: 1648640
Sequence: 388 Name: TS_SK_FATALITY
 PATCH: #: 171 Name: pat_skfatal 
  MAP: #: 171 Name: map_skfatal 
  VAG: #: 190 Name: vag_skfatal File: skfatal.vag
   VAG SIZE: 13008
   VAG RATE: 15624
   VAG POS: 1665024
Sequence: 389 Name: TS_SK_ANIMALITY
 PATCH: #: 172 Name: pat_skanimal 
  MAP: #: 172 Name: map_skanimal 
  VAG: #: 191 Name: vag_skanimal File: skanimal.vag
   VAG SIZE: 10208
   VAG RATE: 15624
   VAG POS: 1679360
Sequence: 390 Name: TS_SK_BABALITY
 PATCH: #: 173 Name: pat_skbabal 
  MAP: #: 173 Name: map_skbabal 
  VAG: #: 192 Name: vag_skbabal File: skbabal.vag
   VAG SIZE: 10416
   VAG RATE: 15624
   VAG POS: 1689600
Sequence: 391 Name: TS_SK_CRISPY
 PATCH: #: 174 Name: pat_skcrispy 
  MAP: #: 174 Name: map_skcrispy 
  VAG: #: 193 Name: vag_skcrispy File: skcrispy.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 1701888
Sequence: 392 Name: TS_SK_FROSTY
 PATCH: #: 175 Name: pat_skfrost 
  MAP: #: 175 Name: map_skfrost 
  VAG: #: 194 Name: vag_skfrost File: skfrost.vag
   VAG SIZE: 10896
   VAG RATE: 15624
   VAG POS: 1714176
Sequence: 393 Name: TS_SK_FRIEND
 PATCH: #: 176 Name: pat_skfrend1 
  MAP: #: 176 Name: map_skfrend1 
  VAG: #: 195 Name: vag_skfrend1 File: skfrend1.vag
   VAG SIZE: 9312
   VAG RATE: 15624
   VAG POS: 1726464
Sequence: 394 Name: TS_SK_FRIEND1
 PATCH: #: 177 Name: pat_skfrend2 
  MAP: #: 177 Name: map_skfrend2 
  VAG: #: 196 Name: vag_skfrend2 File: skfrend2.vag
   VAG SIZE: 8080
   VAG RATE: 15624
   VAG POS: 1736704
Sequence: 395 Name: TS_SK_AGAIN
 PATCH: #: 178 Name: pat_skagain 
  MAP: #: 178 Name: map_skagain 
  VAG: #: 197 Name: vag_skagain File: skagain.vag
   VAG SIZE: 8416
   VAG RATE: 15624
   VAG POS: 1744896
Sequence: 396 Name: TS_SK_TOASTYH
 PATCH: #: 179 Name: pat_sktoasth 
  MAP: #: 179 Name: map_sktoasth 
  VAG: #: 198 Name: vag_sktoasth File: sktoasth.vag
   VAG SIZE: 6576
   VAG RATE: 11000
   VAG POS: 1755136
Sequence: 397 Name: TS_SK_TOASTYL
 PATCH: #: 180 Name: pat_sktoastl 
  MAP: #: 180 Name: map_sktoastl 
  VAG: #: 199 Name: vag_sktoastl File: sktoastl.vag
   VAG SIZE: 7680
   VAG RATE: 11000
   VAG POS: 1763328
Sequence: 398 Name: TS_SK_OMAW
 PATCH: #: 181 Name: pat_skohmaw 
  MAP: #: 181 Name: map_skohmaw 
  VAG: #: 200 Name: vag_skohmaw File: skohmaw.vag
   VAG SIZE: 10816
   VAG RATE: 15624
   VAG POS: 1771520
Sequence: 399 Name: TS_SK_LAUGH1
 PATCH: #: 187 Name: pat_sklaff4 
  MAP: #: 187 Name: map_sklaff4 
  VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
   VAG SIZE: 14544
   VAG RATE: 11000
   VAG POS: 1818624
Sequence: 400 Name: TS_SK_LAUGH2
 PATCH: #: 185 Name: pat_sklaff2 
  MAP: #: 185 Name: map_sklaff2 
  VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
   VAG SIZE: 14544
   VAG RATE: 11000
   VAG POS: 1818624
Sequence: 401 Name: TS_SK_LAUGH3
 PATCH: #: 188 Name: pat_sklaff5 
  MAP: #: 188 Name: map_sklaff5 
  VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
   VAG SIZE: 14544
   VAG RATE: 11000
   VAG POS: 1818624
Sequence: 402 Name: TS_DANGER
 PATCH: #: 309 Name: pat_danger 
  MAP: #: 309 Name: map_danger 
  VAG: #: 300 Name: vag_danger File: danger.vag
   VAG SIZE: 8768
   VAG RATE: 15624
   VAG POS: 2834432
Sequence: 403 Name: TS_WIND
 PATCH: #: 11 Name: pat_windloop 
  MAP: #: 11 Name: map_windloop 
  VAG: #: 5 Name: vag_windloop File: windloop.vag
   VAG SIZE: 28496
   VAG RATE: 12500
   VAG POS: 63488
Sequence: 404 Name: TS_WHOOSH1
 PATCH: #: 12 Name: pat_whoosh1 
  MAP: #: 12 Name: map_whoosh1 
  VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
   VAG SIZE: 2192
   VAG RATE: 12500
   VAG POS: 92160
Sequence: 405 Name: TS_WHOOSH3
 PATCH: #: 13 Name: pat_whoosh3 
  MAP: #: 13 Name: map_whoosh3 
  VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
   VAG SIZE: 2688
   VAG RATE: 12500
   VAG POS: 96256
Sequence: 406 Name: TS_PIT_MACH1
 PATCH: #: 293 Name: pat_machmix 
  MAP: #: 293 Name: map_machmix 
  VAG: #: 284 Name: vag_machmix File: machmix.vag
   VAG SIZE: 3344
   VAG RATE: 15624
   VAG POS: 2590720
Sequence: 407 Name: TS_PIT_MACH2
 PATCH: #: 272 Name: pat_bodsmsh1 
  MAP: #: 272 Name: map_bodsmsh1 
  VAG: #: 263 Name: vag_bodsmsh1 File: bodsmsh1.vag
   VAG SIZE: 9248
   VAG RATE: 15624
   VAG POS: 2373632
Sequence: 408 Name: TS_CRUNCH1
 PATCH: #: 267 Name: pat_crunch1 
  MAP: #: 267 Name: map_crunch1 
  VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
   VAG SIZE: 4224
   VAG RATE: 12500
   VAG POS: 2326528
Sequence: 409 Name: TS_CRUNCH2
 PATCH: #: 294 Name: pat_kneebrk 
  MAP: #: 294 Name: map_kneebrk 
  VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
   VAG SIZE: 5472
   VAG RATE: 15624
   VAG POS: 2594816
Sequence: 410 Name: TS_ANIM_SIZZLE
 PATCH: #: 295 Name: pat_hissmix 
  MAP: #: 295 Name: map_hissmix 
  VAG: #: 286 Name: vag_hissmix File: hissmix.vag
   VAG SIZE: 13696
   VAG RATE: 15624
   VAG POS: 2600960
Sequence: 411 Name: TS_ANIM_SIZZLE1
 PATCH: #: 296 Name: pat_maul 
  MAP: #: 296 Name: map_maul 
  VAG: #: 287 Name: vag_maul File: maul.vag
   VAG SIZE: 7728
   VAG RATE: 15624
   VAG POS: 2615296
Sequence: 412 Name: TS_ANIM_MORPH
 PATCH: #: 148 Name: pat_stmorph 
  MAP: #: 148 Name: map_stmorph 
  VAG: #: 167 Name: vag_stmorph File: stmorph.vag
   VAG SIZE: 7072
   VAG RATE: 15624
   VAG POS: 1384448
Sequence: 413 Name: TS_ANIM_BONUS
 PATCH: #: 296 Name: pat_maul 
  MAP: #: 296 Name: map_maul 
  VAG: #: 287 Name: vag_maul File: maul.vag
   VAG SIZE: 7728
   VAG RATE: 15624
   VAG POS: 2615296
Sequence: 414 Name: TS_ANIM_ROAR
 PATCH: #: 311 Name: pat_anmlroar 
  MAP: #: 311 Name: map_anmlroar 
  VAG: #: 302 Name: vag_anmlroar File: anmlroar.vag
   VAG SIZE: 12848
   VAG RATE: 15624
   VAG POS: 2859008
Sequence: 415 Name: TS_SK_CHOOSE
 PATCH: #: 297 Name: pat_skdestin 
  MAP: #: 297 Name: map_skdestin 
  VAG: #: 288 Name: vag_skdestin File: skdestin.vag
   VAG SIZE: 17952
   VAG RATE: 15624
   VAG POS: 2623488
Sequence: 416 Name: TS_PLAY_BUY
 PATCH: #: 298 Name: pat_cooltend 
  MAP: #: 298 Name: map_cooltend 
  VAG: #: 289 Name: vag_cooltend File: cooltend.vag
   VAG SIZE: 23168
   VAG RATE: 15624
   VAG POS: 2641920
Sequence: 417 Name: TS_MUSIC_MERCY
 PATCH: #: 299 Name: pat_mercyhit 
  MAP: #: 299 Name: map_mercyhit 
  VAG: #: 290 Name: vag_mercyhit File: mercyhit.vag
   VAG SIZE: 19248
   VAG RATE: 15624
   VAG POS: 2666496
Sequence: 418 Name: TS_NASTY_GOO
 PATCH: #: 300 Name: pat_spinerip 
  MAP: #: 300 Name: map_spinerip 
  VAG: #: 291 Name: vag_spinerip File: spinerip.vag
   VAG SIZE: 10000
   VAG RATE: 15624
   VAG POS: 2686976
Sequence: 419 Name: TS_FLAWLESS_TONE
 PATCH: #: 301 Name: pat_attrct2a 
  MAP: #: 301 Name: map_attrct2a 
  VAG: #: 292 Name: vag_attrct2a File: attrct2a.vag
   VAG SIZE: 20832
   VAG RATE: 15624
   VAG POS: 2697216
Sequence: 420 Name: TS_SK_LAST_HIT
 PATCH: #: 128 Name: pat_roldbig1 
  MAP: #: 128 Name: map_roldbig1 
  VAG: #: 31 Name: vag_roldbig1 File: roldbig1.vag
   VAG SIZE: 5424
   VAG RATE: 12500
   VAG POS: 272384
Sequence: 421 Name: TS_HGAME_FIRE
 PATCH: #: 79 Name: pat_cabalgun 
  MAP: #: 79 Name: map_cabalgun 
  VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
   VAG SIZE: 13568
   VAG RATE: 15624
   VAG POS: 1161216
Sequence: 422 Name: TS_SUB_APPROACH
 PATCH: #: 302 Name: pat_subway4 
  MAP: #: 302 Name: map_subway4 
  VAG: #: 293 Name: vag_subway4 File: subway4.vag
   VAG SIZE: 8976
   VAG RATE: 15624
   VAG POS: 2719744
Sequence: 423 Name: TS_SUB_STEADY
 PATCH: #: 303 Name: pat_subway2 
  MAP: #: 303 Name: map_subway2 
  VAG: #: 294 Name: vag_subway2 File: subway2.vag
   VAG SIZE: 8080
   VAG RATE: 15624
   VAG POS: 2729984
Sequence: 424 Name: TS_SUB_GOING
 PATCH: #: 304 Name: pat_subway3 
  MAP: #: 304 Name: map_subway3 
  VAG: #: 295 Name: vag_subway3 File: subway3.vag
   VAG SIZE: 10336
   VAG RATE: 15624
   VAG POS: 2738176
Sequence: 425 Name: TS_FINAL_DEATH
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 426 Name: TS_SK_DEATH
 PATCH: #: 305 Name: pat_skdiemix 
  MAP: #: 305 Name: map_skdiemix 
  VAG: #: 296 Name: vag_skdiemix File: skdiemix.vag
   VAG SIZE: 37424
   VAG RATE: 15624
   VAG POS: 2750464
Sequence: 427 Name: TS_DEATH_QUAKE
 PATCH: #: 280 Name: pat_quake1 
  MAP: #: 280 Name: map_quake1 
  VAG: #: 271 Name: vag_quake1 File: quake1.vag
   VAG SIZE: 11504
   VAG RATE: 15624
   VAG POS: 2449408
Sequence: 428 Name: TS_DOG
 PATCH: #: 306 Name: pat_dogyip 
  MAP: #: 306 Name: map_dogyip 
  VAG: #: 297 Name: vag_dogyip File: dogyip.vag
   VAG SIZE: 3616
   VAG RATE: 15624
   VAG POS: 2789376
Sequence: 429 Name: TS_DOG_OUCH
 PATCH: #: 307 Name: pat_dogwhine 
  MAP: #: 307 Name: map_dogwhine 
  VAG: #: 298 Name: vag_dogwhine File: dogwhine.vag
   VAG SIZE: 13568
   VAG RATE: 15624
   VAG POS: 2793472
Sequence: 430 Name: TS_BABY_POOF
 PATCH: #: 58 Name: pat_robxplod 
  MAP: #: 58 Name: map_robxplod 
  VAG: #: 105 Name: vag_robxplod File: robxplod.vag
   VAG SIZE: 14656
   VAG RATE: 15624
   VAG POS: 870400
Sequence: 431 Name: TS_BABY_CRY
 PATCH: #: 308 Name: pat_baby1 
  MAP: #: 308 Name: map_baby1 
  VAG: #: 299 Name: vag_baby1 File: baby1.vag
   VAG SIZE: 26336
   VAG RATE: 15624
   VAG POS: 2807808
Sequence: 432 Name: TS_WINS_TONE
 PATCH: #: 312 Name: pat_winstone 
  MAP: #: 312 Name: map_winstone 
  VAG: #: 303 Name: vag_winstone File: fatal2.vag
   VAG SIZE: 27408
   VAG RATE: 15624
   VAG POS: 2873344
Sequence: 433 Name: TS_ONEPLAY_END
 PATCH: #: 314 Name: pat_onep 
  MAP: #: 314 Name: map_onep 
  VAG: #: 305 Name: vag_onep File: onep.vag
   VAG SIZE: 71152
   VAG RATE: 44100
   VAG POS: 2920448
Sequence: 434 Name: TS_FATAL_START
 PATCH: #: 315 Name: pat_fstart 
  MAP: #: 315 Name: map_fstart 
  VAG: #: 306 Name: vag_fstart File: fatal12.vag
   VAG SIZE: 21344
   VAG RATE: 12500
   VAG POS: 2992128
Sequence: 435 Name: TUNE_GAMEOVER
 PATCH: #: 317 Name: pat_greenl 
  MAP: #: 317 Name: map_greenl 
  VAG: #: 308 Name: vag_greenl File: greenl.vag
   VAG SIZE: 255184
   VAG RATE: 44100
   VAG POS: 3221504
Sequence: 436 Name: TUNE_SELECT
 PATCH: #: 316 Name: pat_selectl 
  MAP: #: 316 Name: map_selectl 
  VAG: #: 307 Name: vag_selectl File: selectl.vag
   VAG SIZE: 205088
   VAG RATE: 44100
   VAG POS: 3014656
Sequence: 437 Name: TUNE_VERSUS
 PATCH: #: 318 Name: pat_mrtlsans 
  MAP: #: 318 Name: map_mrtlsans 
  VAG: #: 309 Name: vag_mrtlsans File: mrtlsans.vag
   VAG SIZE: 76896
   VAG RATE: 44100
   VAG POS: 3477504
Sequence: 438 Name: TUNE_END_TRAIN
 PATCH: #: 319 Name: pat_traine1 
  MAP: #: 319 Name: map_traine1 
  VAG: #: 310 Name: vag_traine1 File: finish\traine1.vag
   VAG SIZE: 17824
   VAG RATE: 15624
   VAG POS: 3555328
Sequence: 439 Name: TUNE_END_STREET
 PATCH: #: 320 Name: pat_oldend 
  MAP: #: 320 Name: map_oldend 
  VAG: #: 311 Name: vag_oldend File: finish\oldend.vag
   VAG SIZE: 11984
   VAG RATE: 15624
   VAG POS: 3573760
Sequence: 440 Name: TUNE_END_BANK
 PATCH: #: 321 Name: pat_slowend 
  MAP: #: 321 Name: map_slowend 
  VAG: #: 312 Name: vag_slowend File: finish\slowend.vag
   VAG SIZE: 16656
   VAG RATE: 15624
   VAG POS: 3586048
Sequence: 441 Name: TUNE_END_ROOF
 PATCH: #: 322 Name: pat_coolend 
  MAP: #: 322 Name: map_coolend 
  VAG: #: 313 Name: vag_coolend File: finish\cooltend.vag
   VAG SIZE: 15344
   VAG RATE: 15624
   VAG POS: 3604480
Sequence: 442 Name: TUNE_END_BRIDGE
 PATCH: #: 323 Name: pat_wackend 
  MAP: #: 323 Name: map_wackend 
  VAG: #: 314 Name: vag_wackend File: finish\wackend.vag
   VAG SIZE: 9856
   VAG RATE: 15624
   VAG POS: 3620864
Sequence: 443 Name: TUNE_END_SOUL
 PATCH: #: 324 Name: pat_tonguend 
  MAP: #: 324 Name: map_tonguend 
  VAG: #: 315 Name: vag_tonguend File: finish\tonguend.vag
   VAG SIZE: 16144
   VAG RATE: 15624
   VAG POS: 3631104
Sequence: 444 Name: TUNE_END_CHURCH
 PATCH: #: 325 Name: pat_chrchend 
  MAP: #: 325 Name: map_chrchend 
  VAG: #: 316 Name: vag_chrchend File: finish\chrchend.vag
   VAG SIZE: 19248
   VAG RATE: 15624
   VAG POS: 3647488
Sequence: 445 Name: TUNE_END_GRAVE
 PATCH: #: 326 Name: pat_gravend 
  MAP: #: 326 Name: map_gravend 
  VAG: #: 317 Name: vag_gravend File: finish\gravend.vag
   VAG SIZE: 15120
   VAG RATE: 15624
   VAG POS: 3667968
Sequence: 446 Name: TUNE_END_PIT
 PATCH: #: 327 Name: pat_pitend 
  MAP: #: 327 Name: map_pitend 
  VAG: #: 318 Name: vag_pitend File: finish\pitend.vag
   VAG SIZE: 11984
   VAG RATE: 15624
   VAG POS: 3684352
Sequence: 447 Name: TUNE_END_FRENZY
 PATCH: #: 328 Name: pat_frenzye 
  MAP: #: 328 Name: map_frenzye 
  VAG: #: 319 Name: vag_frenzye File: finish\frenzye.vag
   VAG SIZE: 15056
   VAG RATE: 15624
   VAG POS: 3696640

LOADLIST: Name: SONYA_FIGHT_SNDS File: sonya.lcd
 SEQUENCE: #: 0 Name: GS_DEATH_GIRL 
  PATCH: #: 82 Name: pat_pbdeath 
   MAP: #: 82 Name: map_pbdeath 
    ROOT: 81 FINE: 105
   VAG: #: 40 Name: vag_pbdeath File: pbdeath.vag
    VAG SIZE: 19664
    VAG RATE: 12500
    VAG POS: 346112
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 9 Name: GS_SHOOK_GIRL 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
    VAG LCD FILEPOS: 21712 mod: 1232
 SEQUENCE: #: 18 Name: GS_RUN_GIRL 
  PATCH: #: 83 Name: pat_pbrun 
   MAP: #: 83 Name: map_pbrun 
    ROOT: 77 FINE: 123
   VAG: #: 41 Name: vag_pbrun File: pbrun.vag
    VAG SIZE: 17536
    VAG RATE: 15624
    VAG POS: 366592
    VAG LCD FILEPOS: 31232 mod: 512
 SEQUENCE: #: 27 Name: GS_TV_GIRL 
  PATCH: #: 84 Name: pat_pbtrip 
   MAP: #: 84 Name: map_pbtrip 
    ROOT: 77 FINE: 123
   VAG: #: 42 Name: vag_pbtrip File: pbtrip.vag
    VAG SIZE: 3552
    VAG RATE: 15624
    VAG POS: 385024
    VAG LCD FILEPOS: 48768 mod: 1664
 SEQUENCE: #: 35 Name: GS_FHV_GIRL1 
  PATCH: #: 85 Name: pat_pbface1 
   MAP: #: 85 Name: map_pbface1 
    ROOT: 77 FINE: 123
   VAG: #: 43 Name: vag_pbface1 File: pbface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 389120
    VAG LCD FILEPOS: 52320 mod: 1120
 SEQUENCE: #: 36 Name: GS_FHV_GIRL2 
  PATCH: #: 86 Name: pat_pbface2 
   MAP: #: 86 Name: map_pbface2 
    ROOT: 77 FINE: 123
   VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 395264
    VAG LCD FILEPOS: 57040 mod: 1744
 SEQUENCE: #: 37 Name: GS_FHV_GIRL3 
  PATCH: #: 86 Name: pat_pbface2 
   MAP: #: 86 Name: map_pbface2 
    ROOT: 77 FINE: 123
   VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 395264
 SEQUENCE: #: 62 Name: GS_ATK_GIRL1 
  PATCH: #: 87 Name: pat_pbatt1 
   MAP: #: 87 Name: map_pbatt1 
    ROOT: 77 FINE: 123
   VAG: #: 45 Name: vag_pbatt1 File: pbatt1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 401408
    VAG LCD FILEPOS: 62448 mod: 1008
 SEQUENCE: #: 63 Name: GS_ATK_GIRL2 
  PATCH: #: 88 Name: pat_pbatt2 
   MAP: #: 88 Name: map_pbatt2 
    ROOT: 77 FINE: 123
   VAG: #: 46 Name: vag_pbatt2 File: pbatt2.vag
    VAG SIZE: 3696
    VAG RATE: 15624
    VAG POS: 407552
    VAG LCD FILEPOS: 67168 mod: 1632
 SEQUENCE: #: 78 Name: GS_JUMP_GIRL 
  PATCH: #: 89 Name: pat_pbjump 
   MAP: #: 89 Name: map_pbjump 
    ROOT: 77 FINE: 123
   VAG: #: 47 Name: vag_pbjump File: pbjump.vag
    VAG SIZE: 3552
    VAG RATE: 15624
    VAG POS: 411648
    VAG LCD FILEPOS: 70864 mod: 1232
 SEQUENCE: #: 86 Name: GS_GRAB_GIRL 
  PATCH: #: 90 Name: pat_pbgrab 
   MAP: #: 90 Name: map_pbgrab 
    ROOT: 77 FINE: 123
   VAG: #: 48 Name: vag_pbgrab File: pbgrab.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 415744
    VAG LCD FILEPOS: 74416 mod: 688
 SEQUENCE: #: 95 Name: GS_SLAM_GIRL 
  PATCH: #: 91 Name: pat_pbthrow 
   MAP: #: 91 Name: map_pbthrow 
    ROOT: 77 FINE: 123
   VAG: #: 49 Name: vag_pbthrow File: pbthrow.vag
    VAG SIZE: 9936
    VAG RATE: 15624
    VAG POS: 419840
    VAG LCD FILEPOS: 77696 mod: 1920
 SEQUENCE: #: 104 Name: GS_WST_GIRL1 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
 SEQUENCE: #: 105 Name: GS_WST_GIRL2 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
 SEQUENCE: #: 176 Name: ST_SONYA_RINGS 
  PATCH: #: 147 Name: pat_sonforce 
   MAP: #: 147 Name: map_sonforce 
    ROOT: 77 FINE: 123
   VAG: #: 166 Name: vag_sonforce File: sonforce.vag
    VAG SIZE: 9856
    VAG RATE: 15624
    VAG POS: 1374208
    VAG LCD FILEPOS: 87632 mod: 1616
 SEQUENCE: #: 177 Name: ST_SONYA_RINGS_HIT 
  PATCH: #: 95 Name: pat_forcehit 
   MAP: #: 95 Name: map_forcehit 
    ROOT: 77 FINE: 123
   VAG: #: 145 Name: vag_forcehit File: forcehit.vag
    VAG SIZE: 8080
    VAG RATE: 15624
    VAG POS: 1200128
    VAG LCD FILEPOS: 97488 mod: 1232
 SEQUENCE: #: 178 Name: ST_SONYA_RINGS_BLK 
  PATCH: #: 95 Name: pat_forcehit 
   MAP: #: 95 Name: map_forcehit 
    ROOT: 77 FINE: 123
   VAG: #: 145 Name: vag_forcehit File: forcehit.vag
    VAG SIZE: 8080
    VAG RATE: 15624
    VAG POS: 1200128
 SEQUENCE: #: 179 Name: ST_SONYA_BIKE 
  PATCH: #: 233 Name: pat_sonbike 
   MAP: #: 233 Name: map_sonbike 
    ROOT: 77 FINE: 123
   VAG: #: 229 Name: vag_sonbike File: sonbike.vag
    VAG SIZE: 7392
    VAG RATE: 15624
    VAG POS: 2101248
    VAG LCD FILEPOS: 105568 mod: 1120
 SEQUENCE: #: 180 Name: ST_SONYA_FLY 
  PATCH: #: 42 Name: pat_sonyafly 
   MAP: #: 42 Name: map_sonyafly 
    ROOT: 77 FINE: 123
   VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
    VAG SIZE: 8288
    VAG RATE: 15624
    VAG POS: 1144832
    VAG LCD FILEPOS: 112960 mod: 320
 SEQUENCE: #: 181 Name: ST_SONYA_BIKE_VOICE 
  PATCH: #: 4 Name: pat_pbcycle 
   MAP: #: 4 Name: map_pbcycle 
    ROOT: 77 FINE: 123
   VAG: #: 35 Name: vag_pbcycle File: pbcycle.vag
    VAG SIZE: 12192
    VAG RATE: 15624
    VAG POS: 288768
    VAG LCD FILEPOS: 121248 mod: 416
LOADLIST----TOTAL: Name: SONYA_FIGHT_SNDS File: sonya.lcd Size: 131392

LOADLIST: Name: LIA_FIGHT_SNDS File: lia.lcd
 SEQUENCE: #: 0 Name: GS_DEATH_GIRL 
  PATCH: #: 82 Name: pat_pbdeath 
   MAP: #: 82 Name: map_pbdeath 
    ROOT: 81 FINE: 105
   VAG: #: 40 Name: vag_pbdeath File: pbdeath.vag
    VAG SIZE: 19664
    VAG RATE: 12500
    VAG POS: 346112
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 9 Name: GS_SHOOK_GIRL 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
    VAG LCD FILEPOS: 21712 mod: 1232
 SEQUENCE: #: 18 Name: GS_RUN_GIRL 
  PATCH: #: 83 Name: pat_pbrun 
   MAP: #: 83 Name: map_pbrun 
    ROOT: 77 FINE: 123
   VAG: #: 41 Name: vag_pbrun File: pbrun.vag
    VAG SIZE: 17536
    VAG RATE: 15624
    VAG POS: 366592
    VAG LCD FILEPOS: 31232 mod: 512
 SEQUENCE: #: 27 Name: GS_TV_GIRL 
  PATCH: #: 84 Name: pat_pbtrip 
   MAP: #: 84 Name: map_pbtrip 
    ROOT: 77 FINE: 123
   VAG: #: 42 Name: vag_pbtrip File: pbtrip.vag
    VAG SIZE: 3552
    VAG RATE: 15624
    VAG POS: 385024
    VAG LCD FILEPOS: 48768 mod: 1664
 SEQUENCE: #: 35 Name: GS_FHV_GIRL1 
  PATCH: #: 85 Name: pat_pbface1 
   MAP: #: 85 Name: map_pbface1 
    ROOT: 77 FINE: 123
   VAG: #: 43 Name: vag_pbface1 File: pbface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 389120
    VAG LCD FILEPOS: 52320 mod: 1120
 SEQUENCE: #: 36 Name: GS_FHV_GIRL2 
  PATCH: #: 86 Name: pat_pbface2 
   MAP: #: 86 Name: map_pbface2 
    ROOT: 77 FINE: 123
   VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 395264
    VAG LCD FILEPOS: 57040 mod: 1744
 SEQUENCE: #: 37 Name: GS_FHV_GIRL3 
  PATCH: #: 86 Name: pat_pbface2 
   MAP: #: 86 Name: map_pbface2 
    ROOT: 77 FINE: 123
   VAG: #: 44 Name: vag_pbface2 File: pbface2.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 395264
 SEQUENCE: #: 62 Name: GS_ATK_GIRL1 
  PATCH: #: 87 Name: pat_pbatt1 
   MAP: #: 87 Name: map_pbatt1 
    ROOT: 77 FINE: 123
   VAG: #: 45 Name: vag_pbatt1 File: pbatt1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 401408
    VAG LCD FILEPOS: 62448 mod: 1008
 SEQUENCE: #: 63 Name: GS_ATK_GIRL2 
  PATCH: #: 88 Name: pat_pbatt2 
   MAP: #: 88 Name: map_pbatt2 
    ROOT: 77 FINE: 123
   VAG: #: 46 Name: vag_pbatt2 File: pbatt2.vag
    VAG SIZE: 3696
    VAG RATE: 15624
    VAG POS: 407552
    VAG LCD FILEPOS: 67168 mod: 1632
 SEQUENCE: #: 78 Name: GS_JUMP_GIRL 
  PATCH: #: 89 Name: pat_pbjump 
   MAP: #: 89 Name: map_pbjump 
    ROOT: 77 FINE: 123
   VAG: #: 47 Name: vag_pbjump File: pbjump.vag
    VAG SIZE: 3552
    VAG RATE: 15624
    VAG POS: 411648
    VAG LCD FILEPOS: 70864 mod: 1232
 SEQUENCE: #: 86 Name: GS_GRAB_GIRL 
  PATCH: #: 90 Name: pat_pbgrab 
   MAP: #: 90 Name: map_pbgrab 
    ROOT: 77 FINE: 123
   VAG: #: 48 Name: vag_pbgrab File: pbgrab.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 415744
    VAG LCD FILEPOS: 74416 mod: 688
 SEQUENCE: #: 95 Name: GS_SLAM_GIRL 
  PATCH: #: 91 Name: pat_pbthrow 
   MAP: #: 91 Name: map_pbthrow 
    ROOT: 77 FINE: 123
   VAG: #: 49 Name: vag_pbthrow File: pbthrow.vag
    VAG SIZE: 9936
    VAG RATE: 15624
    VAG POS: 419840
    VAG LCD FILEPOS: 77696 mod: 1920
 SEQUENCE: #: 104 Name: GS_WST_GIRL1 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
 SEQUENCE: #: 105 Name: GS_WST_GIRL2 
  PATCH: #: 92 Name: pat_pbwast1 
   MAP: #: 92 Name: map_pbwast1 
    ROOT: 77 FINE: 123
   VAG: #: 50 Name: vag_pbwast1 File: pbwast1.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 430080
 SEQUENCE: #: 231 Name: ST_LIA_HAIR 
  PATCH: #: 5 Name: pat_hairthrw 
   MAP: #: 5 Name: map_hairthrw 
    ROOT: 77 FINE: 123
   VAG: #: 36 Name: vag_hairthrw File: hairthrw.vag
    VAG SIZE: 11376
    VAG RATE: 15624
    VAG POS: 301056
    VAG LCD FILEPOS: 87632 mod: 1616
 SEQUENCE: #: 232 Name: ST_LIA_SCREAM 
  PATCH: #: 6 Name: pat_scrmtrp2 
   MAP: #: 6 Name: map_scrmtrp2 
    ROOT: 77 FINE: 123
   VAG: #: 37 Name: vag_scrmtrp2 File: scrmtrp2.vag
    VAG SIZE: 16176
    VAG RATE: 15624
    VAG POS: 313344
    VAG LCD FILEPOS: 99008 mod: 704
 SEQUENCE: #: 233 Name: ST_LIA_FIREBALL 
  PATCH: #: 26 Name: pat_mfirebal 
   MAP: #: 26 Name: map_mfirebal 
    ROOT: 77 FINE: 123
   VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
    VAG SIZE: 11296
    VAG RATE: 15624
    VAG POS: 204800
    VAG LCD FILEPOS: 115184 mod: 496
 SEQUENCE: #: 234 Name: ST_LIA_FIREBALL_HIT 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
    VAG LCD FILEPOS: 126480 mod: 1552
 SEQUENCE: #: 235 Name: ST_LIA_FIREBALL_BLOCK 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
 SEQUENCE: #: 236 Name: ST_LIA_FLY_UP 
  PATCH: #: 7 Name: pat_flying1 
   MAP: #: 7 Name: map_flying1 
    ROOT: 77 FINE: 123
   VAG: #: 39 Name: vag_flying1 File: flying1.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 335872
    VAG LCD FILEPOS: 131888 mod: 816
 SEQUENCE: #: 237 Name: ST_LIA_FLY_ACROSS 
  PATCH: #: 7 Name: pat_flying1 
   MAP: #: 7 Name: map_flying1 
    ROOT: 77 FINE: 123
   VAG: #: 39 Name: vag_flying1 File: flying1.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 335872
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 140240 mod: 976
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 143536 mod: 176
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: LIA_FIGHT_SNDS File: lia.lcd Size: 144832

LOADLIST: Name: SHEEVA_FIGHT_SNDS File: sheeva.lcd
 SEQUENCE: #: 7 Name: GS_DEATH_SG 
  PATCH: #: 240 Name: pat_amdeath 
   MAP: #: 240 Name: map_amdeath 
    ROOT: 77 FINE: 123
   VAG: #: 231 Name: vag_amdeath File: amdeath.vag
    VAG SIZE: 26272
    VAG RATE: 15624
    VAG POS: 2115584
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 16 Name: GS_SHOOK_SG 
  PATCH: #: 257 Name: pat_amwast1 
   MAP: #: 257 Name: map_amwast1 
    ROOT: 77 FINE: 123
   VAG: #: 248 Name: vag_amwast1 File: amwast1.vag
    VAG SIZE: 12256
    VAG RATE: 15624
    VAG POS: 2254848
    VAG LCD FILEPOS: 28320 mod: 1696
 SEQUENCE: #: 25 Name: GS_RUN_SG 
  PATCH: #: 241 Name: pat_amrun 
   MAP: #: 241 Name: map_amrun 
    ROOT: 77 FINE: 123
   VAG: #: 232 Name: vag_amrun File: amrun.vag
    VAG SIZE: 10720
    VAG RATE: 15624
    VAG POS: 2142208
    VAG LCD FILEPOS: 40576 mod: 1664
 SEQUENCE: #: 33 Name: GS_TV_SG 
  PATCH: #: 242 Name: pat_amtrip 
   MAP: #: 242 Name: map_amtrip 
    ROOT: 77 FINE: 123
   VAG: #: 233 Name: vag_amtrip File: amtrip.vag
    VAG SIZE: 3552
    VAG RATE: 15624
    VAG POS: 2154496
    VAG LCD FILEPOS: 51296 mod: 96
 SEQUENCE: #: 56 Name: GS_FHV_SG1 
  PATCH: #: 244 Name: pat_amface1 
   MAP: #: 244 Name: map_amface1 
    ROOT: 77 FINE: 123
   VAG: #: 235 Name: vag_amface1 File: amface1.vag
    VAG SIZE: 4176
    VAG RATE: 15624
    VAG POS: 2162688
    VAG LCD FILEPOS: 54848 mod: 1600
 SEQUENCE: #: 57 Name: GS_FHV_SG2 
  PATCH: #: 243 Name: pat_ambody2 
   MAP: #: 243 Name: map_ambody2 
    ROOT: 77 FINE: 123
   VAG: #: 234 Name: vag_ambody2 File: ambody2.vag
    VAG SIZE: 2800
    VAG RATE: 15624
    VAG POS: 2158592
    VAG LCD FILEPOS: 59024 mod: 1680
 SEQUENCE: #: 58 Name: GS_FHV_SG3 
  PATCH: #: 245 Name: pat_amface2 
   MAP: #: 245 Name: map_amface2 
    ROOT: 77 FINE: 123
   VAG: #: 236 Name: vag_amface2 File: amface2.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2168832
    VAG LCD FILEPOS: 61824 mod: 384
 SEQUENCE: #: 74 Name: GS_ATK_SG1 
  PATCH: #: 249 Name: pat_amatt1 
   MAP: #: 249 Name: map_amatt1 
    ROOT: 77 FINE: 123
   VAG: #: 240 Name: vag_amatt1 File: amatt1.vag
    VAG SIZE: 3408
    VAG RATE: 15624
    VAG POS: 2195456
    VAG LCD FILEPOS: 67296 mod: 1760
 SEQUENCE: #: 75 Name: GS_ATK_SG2 
  PATCH: #: 250 Name: pat_amatt2 
   MAP: #: 250 Name: map_amatt2 
    ROOT: 77 FINE: 123
   VAG: #: 241 Name: vag_amatt2 File: amatt2.vag
    VAG SIZE: 3616
    VAG RATE: 15624
    VAG POS: 2199552
    VAG LCD FILEPOS: 70704 mod: 1072
 SEQUENCE: #: 84 Name: GS_JUMP_SG 
  PATCH: #: 251 Name: pat_amjump 
   MAP: #: 251 Name: map_amjump 
    ROOT: 77 FINE: 123
   VAG: #: 242 Name: vag_amjump File: amjump.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 2203648
    VAG LCD FILEPOS: 74320 mod: 592
 SEQUENCE: #: 93 Name: GS_GRAB_SG 
  PATCH: #: 254 Name: pat_amgrab 
   MAP: #: 254 Name: map_amgrab 
    ROOT: 77 FINE: 123
   VAG: #: 245 Name: vag_amgrab File: amgrab.vag
    VAG SIZE: 3696
    VAG RATE: 15624
    VAG POS: 2222080
    VAG LCD FILEPOS: 77600 mod: 1824
 SEQUENCE: #: 102 Name: GS_SLAM_SG 
  PATCH: #: 255 Name: pat_amthrow 
   MAP: #: 255 Name: map_amthrow 
    ROOT: 77 FINE: 123
   VAG: #: 246 Name: vag_amthrow File: amthrow.vag
    VAG SIZE: 8688
    VAG RATE: 15624
    VAG POS: 2226176
    VAG LCD FILEPOS: 81296 mod: 1424
 SEQUENCE: #: 118 Name: GS_WST_SG1 
  PATCH: #: 257 Name: pat_amwast1 
   MAP: #: 257 Name: map_amwast1 
    ROOT: 77 FINE: 123
   VAG: #: 248 Name: vag_amwast1 File: amwast1.vag
    VAG SIZE: 12256
    VAG RATE: 15624
    VAG POS: 2254848
 SEQUENCE: #: 119 Name: GS_WST_SG2 
  PATCH: #: 258 Name: pat_amwast2 
   MAP: #: 258 Name: map_amwast2 
    ROOT: 77 FINE: 123
   VAG: #: 249 Name: vag_amwast2 File: amwast2.vag
    VAG SIZE: 11984
    VAG RATE: 15624
    VAG POS: 2267136
    VAG LCD FILEPOS: 89984 mod: 1920
 SEQUENCE: #: 297 Name: ST_SG_TELE 
  PATCH: #: 93 Name: pat_shevtele 
   MAP: #: 93 Name: map_shevtele 
    ROOT: 77 FINE: 123
   VAG: #: 143 Name: vag_shevtele File: shevtele.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 1181696
    VAG LCD FILEPOS: 101968 mod: 1616
 SEQUENCE: #: 298 Name: ST_SG_POUNCE 
  PATCH: #: 94 Name: pat_bigbslam 
   MAP: #: 94 Name: map_bigbslam 
    ROOT: 77 FINE: 123
   VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
    VAG SIZE: 8416
    VAG RATE: 15624
    VAG POS: 1189888
    VAG LCD FILEPOS: 108672 mod: 128
 SEQUENCE: #: 299 Name: ST_SG_ZAP_HIT 
  PATCH: #: 95 Name: pat_forcehit 
   MAP: #: 95 Name: map_forcehit 
    ROOT: 77 FINE: 123
   VAG: #: 145 Name: vag_forcehit File: forcehit.vag
    VAG SIZE: 8080
    VAG RATE: 15624
    VAG POS: 1200128
    VAG LCD FILEPOS: 117088 mod: 352
 SEQUENCE: #: 300 Name: ST_SG_ZAP 
  PATCH: #: 42 Name: pat_sonyafly 
   MAP: #: 42 Name: map_sonyafly 
    ROOT: 77 FINE: 123
   VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
    VAG SIZE: 8288
    VAG RATE: 15624
    VAG POS: 1144832
    VAG LCD FILEPOS: 125168 mod: 240
 SEQUENCE: #: 301 Name: ST_SG_ZAP_START 
  PATCH: #: 96 Name: pat_robtele 
   MAP: #: 96 Name: map_robtele 
    ROOT: 77 FINE: 123
   VAG: #: 116 Name: vag_robtele File: robtele.vag
    VAG SIZE: 10080
    VAG RATE: 15624
    VAG POS: 958464
    VAG LCD FILEPOS: 133456 mod: 336
LOADLIST----TOTAL: Name: SHEEVA_FIGHT_SNDS File: sheeva.lcd Size: 141488

LOADLIST: Name: KANO_FIGHT_SNDS File: kano.lcd
 SEQUENCE: #: 3 Name: GS_DEATH_EB 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 12 Name: GS_SHOOK_EB 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 21 Name: GS_RUN_EB 
  PATCH: #: 217 Name: pat_eblrun 
   MAP: #: 217 Name: map_eblrun 
    ROOT: 77 FINE: 123
   VAG: #: 76 Name: vag_eblrun File: eblrun.vag
    VAG SIZE: 11648
    VAG RATE: 15624
    VAG POS: 669696
    VAG LCD FILEPOS: 30608 mod: 1936
 SEQUENCE: #: 30 Name: GS_TV_EB 
  PATCH: #: 218 Name: pat_ebtrip 
   MAP: #: 218 Name: map_ebtrip 
    ROOT: 77 FINE: 123
   VAG: #: 77 Name: vag_ebtrip File: ebtrip.vag
    VAG SIZE: 3072
    VAG RATE: 15624
    VAG POS: 681984
    VAG LCD FILEPOS: 42256 mod: 1296
 SEQUENCE: #: 44 Name: GS_FHV_EB1 
  PATCH: #: 219 Name: pat_eblface1 
   MAP: #: 219 Name: map_eblface1 
    ROOT: 77 FINE: 123
   VAG: #: 78 Name: vag_eblface1 File: eblface1.vag
    VAG SIZE: 2464
    VAG RATE: 15624
    VAG POS: 686080
    VAG LCD FILEPOS: 45328 mod: 272
 SEQUENCE: #: 45 Name: GS_FHV_EB2 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
    VAG LCD FILEPOS: 47792 mod: 688
 SEQUENCE: #: 46 Name: GS_FHV_EB3 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
 SEQUENCE: #: 68 Name: GS_ATK_EB1 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
    VAG LCD FILEPOS: 52304 mod: 1104
 SEQUENCE: #: 69 Name: GS_ATK_EB2 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
 SEQUENCE: #: 81 Name: GS_JUMP_EB 
  PATCH: #: 222 Name: pat_ebljump 
   MAP: #: 222 Name: map_ebljump 
    ROOT: 77 FINE: 123
   VAG: #: 82 Name: vag_ebljump File: ebljump.vag
    VAG SIZE: 2320
    VAG RATE: 15624
    VAG POS: 704512
    VAG LCD FILEPOS: 55312 mod: 16
 SEQUENCE: #: 89 Name: GS_GRAB_EB 
  PATCH: #: 223 Name: pat_eblgrab 
   MAP: #: 223 Name: map_eblgrab 
    ROOT: 77 FINE: 123
   VAG: #: 83 Name: vag_eblgrab File: eblgrab.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 708608
    VAG LCD FILEPOS: 57632 mod: 288
 SEQUENCE: #: 98 Name: GS_SLAM_EB 
  PATCH: #: 224 Name: pat_eblthrow 
   MAP: #: 224 Name: map_eblthrow 
    ROOT: 77 FINE: 123
   VAG: #: 84 Name: vag_eblthrow File: eblthrow.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 712704
    VAG LCD FILEPOS: 60848 mod: 1456
 SEQUENCE: #: 110 Name: GS_WST_EB1 
  PATCH: #: 225 Name: pat_eblwast1 
   MAP: #: 225 Name: map_eblwast1 
    ROOT: 77 FINE: 123
   VAG: #: 85 Name: vag_eblwast1 File: eblwast1.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 720896
    VAG LCD FILEPOS: 68448 mod: 864
 SEQUENCE: #: 111 Name: GS_WST_EB2 
  PATCH: #: 226 Name: pat_eblwast2 
   MAP: #: 226 Name: map_eblwast2 
    ROOT: 81 FINE: 105
   VAG: #: 86 Name: vag_eblwast2 File: eblwast2.vag
    VAG SIZE: 7568
    VAG RATE: 12500
    VAG POS: 731136
    VAG LCD FILEPOS: 78576 mod: 752
 SEQUENCE: #: 167 Name: ST_KANO_AX 
  PATCH: #: 109 Name: pat_kanoax 
   MAP: #: 109 Name: map_kanoax 
    ROOT: 81 FINE: 105
   VAG: #: 150 Name: vag_kanoax File: kanoax.vag
    VAG SIZE: 6960
    VAG RATE: 12500
    VAG POS: 1241088
    VAG LCD FILEPOS: 86144 mod: 128
 SEQUENCE: #: 168 Name: ST_KANO_SKNIFE 
  PATCH: #: 206 Name: pat_kanoknif 
   MAP: #: 206 Name: map_kanoknif 
    ROOT: 77 FINE: 123
   VAG: #: 215 Name: vag_kanoknif File: kanoknif.vag
    VAG SIZE: 11568
    VAG RATE: 15624
    VAG POS: 1961984
    VAG LCD FILEPOS: 93104 mod: 944
 SEQUENCE: #: 169 Name: ST_KANO_SKNIFE_HIT 
  PATCH: #: 207 Name: pat_newstab2 
   MAP: #: 207 Name: map_newstab2 
    ROOT: 77 FINE: 123
   VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
    VAG SIZE: 4096
    VAG RATE: 15624
    VAG POS: 1974272
    VAG LCD FILEPOS: 104672 mod: 224
 SEQUENCE: #: 170 Name: ST_KANO_SKNIFE_BLK 
  PATCH: #: 207 Name: pat_newstab2 
   MAP: #: 207 Name: map_newstab2 
    ROOT: 77 FINE: 123
   VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
    VAG SIZE: 4096
    VAG RATE: 15624
    VAG POS: 1974272
 SEQUENCE: #: 171 Name: ST_KANO_BALL 
  PATCH: #: 208 Name: pat_kanoball 
   MAP: #: 208 Name: map_kanoball 
    ROOT: 77 FINE: 123
   VAG: #: 217 Name: vag_kanoball File: kanoball.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 1978368
    VAG LCD FILEPOS: 108768 mod: 224
 SEQUENCE: #: 146 Name: RSND_KLANG1 
  PATCH: #: 288 Name: pat_mhit1 
   MAP: #: 288 Name: map_mhit1 
    ROOT: 81 FINE: 105
   VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
    VAG SIZE: 3184
    VAG RATE: 12500
    VAG POS: 2527232
    VAG LCD FILEPOS: 118896 mod: 112
 SEQUENCE: #: 147 Name: RSND_KLANG2 
  PATCH: #: 289 Name: pat_mhit2 
   MAP: #: 289 Name: map_mhit2 
    ROOT: 81 FINE: 105
   VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
    VAG SIZE: 3568
    VAG RATE: 12500
    VAG POS: 2531328
    VAG LCD FILEPOS: 122080 mod: 1248
LOADLIST----TOTAL: Name: KANO_FIGHT_SNDS File: kano.lcd Size: 123600

LOADLIST: Name: NOOB_FIGHT_SNDS File: noob.lcd
 SEQUENCE: #: 4 Name: GS_DEATH_JH 
  PATCH: #: 216 Name: pat_ebldeath 
   MAP: #: 216 Name: map_ebldeath 
    ROOT: 81 FINE: 105
   VAG: #: 74 Name: vag_ebldeath File: ebldeath.vag
    VAG SIZE: 19952
    VAG RATE: 12500
    VAG POS: 634880
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 13 Name: GS_SHOOK_JH 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 22000 mod: 1520
 SEQUENCE: #: 22 Name: GS_RUN_JH 
  PATCH: #: 98 Name: pat_jhrun 
   MAP: #: 98 Name: map_jhrun 
    ROOT: 81 FINE: 105
   VAG: #: 88 Name: vag_jhrun File: jhrun.vag
    VAG SIZE: 9328
    VAG RATE: 12500
    VAG POS: 755712
    VAG LCD FILEPOS: 34560 mod: 1792
 SEQUENCE: #: 31 Name: GS_TV_JH 
  PATCH: #: 99 Name: pat_jhtrip 
   MAP: #: 99 Name: map_jhtrip 
    ROOT: 81 FINE: 105
   VAG: #: 89 Name: vag_jhtrip File: jhtrip.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 765952
    VAG LCD FILEPOS: 43888 mod: 880
 SEQUENCE: #: 47 Name: GS_FHV_JH1 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
    VAG LCD FILEPOS: 46800 mod: 1744
 SEQUENCE: #: 48 Name: GS_FHV_JH2 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 49 Name: GS_FHV_JH3 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 70 Name: GS_ATK_JH1 
  PATCH: #: 100 Name: pat_jhatt1 
   MAP: #: 100 Name: map_jhatt1 
    ROOT: 81 FINE: 105
   VAG: #: 91 Name: vag_jhatt1 File: jhatt1.vag
    VAG SIZE: 2080
    VAG RATE: 12500
    VAG POS: 776192
    VAG LCD FILEPOS: 51792 mod: 592
 SEQUENCE: #: 71 Name: GS_ATK_JH2 
  PATCH: #: 143 Name: pat_jhatt2 
   MAP: #: 143 Name: map_jhatt2 
    ROOT: 77 FINE: 123
   VAG: #: 92 Name: vag_jhatt2 File: jhatt2.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 780288
    VAG LCD FILEPOS: 53872 mod: 624
 SEQUENCE: #: 82 Name: GS_JUMP_JH 
  PATCH: #: 101 Name: pat_jhjump 
   MAP: #: 101 Name: map_jhjump 
    ROOT: 81 FINE: 105
   VAG: #: 93 Name: vag_jhjump File: jhjump.vag
    VAG SIZE: 2032
    VAG RATE: 12500
    VAG POS: 784384
    VAG LCD FILEPOS: 57152 mod: 1856
 SEQUENCE: #: 90 Name: GS_GRAB_JH 
  PATCH: #: 102 Name: pat_jhgrab 
   MAP: #: 102 Name: map_jhgrab 
    ROOT: 81 FINE: 105
   VAG: #: 94 Name: vag_jhgrab File: jhgrab.vag
    VAG SIZE: 2416
    VAG RATE: 12500
    VAG POS: 786432
    VAG LCD FILEPOS: 59184 mod: 1840
 SEQUENCE: #: 99 Name: GS_SLAM_JH 
  PATCH: #: 103 Name: pat_jhthrow 
   MAP: #: 103 Name: map_jhthrow 
    ROOT: 81 FINE: 105
   VAG: #: 95 Name: vag_jhthrow File: jhthrow.vag
    VAG SIZE: 5488
    VAG RATE: 12500
    VAG POS: 790528
    VAG LCD FILEPOS: 61600 mod: 160
 SEQUENCE: #: 112 Name: GS_WST_JH1 
  PATCH: #: 144 Name: pat_jhwast1 
   MAP: #: 144 Name: map_jhwast1 
    ROOT: 77 FINE: 123
   VAG: #: 96 Name: vag_jhwast1 File: jhwast1.vag
    VAG SIZE: 13424
    VAG RATE: 15624
    VAG POS: 796672
    VAG LCD FILEPOS: 67088 mod: 1552
 SEQUENCE: #: 113 Name: GS_WST_JH2 
  PATCH: #: 104 Name: pat_jhwast2 
   MAP: #: 104 Name: map_jhwast2 
    ROOT: 81 FINE: 105
   VAG: #: 97 Name: vag_jhwast2 File: jhwast2.vag
    VAG SIZE: 9552
    VAG RATE: 12500
    VAG POS: 811008
    VAG LCD FILEPOS: 80512 mod: 640
 SEQUENCE: #: 167 Name: ST_KANO_AX 
  PATCH: #: 109 Name: pat_kanoax 
   MAP: #: 109 Name: map_kanoax 
    ROOT: 81 FINE: 105
   VAG: #: 150 Name: vag_kanoax File: kanoax.vag
    VAG SIZE: 6960
    VAG RATE: 12500
    VAG POS: 1241088
    VAG LCD FILEPOS: 90064 mod: 2000
 SEQUENCE: #: 168 Name: ST_KANO_SKNIFE 
  PATCH: #: 206 Name: pat_kanoknif 
   MAP: #: 206 Name: map_kanoknif 
    ROOT: 77 FINE: 123
   VAG: #: 215 Name: vag_kanoknif File: kanoknif.vag
    VAG SIZE: 11568
    VAG RATE: 15624
    VAG POS: 1961984
    VAG LCD FILEPOS: 97024 mod: 768
 SEQUENCE: #: 169 Name: ST_KANO_SKNIFE_HIT 
  PATCH: #: 207 Name: pat_newstab2 
   MAP: #: 207 Name: map_newstab2 
    ROOT: 77 FINE: 123
   VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
    VAG SIZE: 4096
    VAG RATE: 15624
    VAG POS: 1974272
    VAG LCD FILEPOS: 108592 mod: 48
 SEQUENCE: #: 170 Name: ST_KANO_SKNIFE_BLK 
  PATCH: #: 207 Name: pat_newstab2 
   MAP: #: 207 Name: map_newstab2 
    ROOT: 77 FINE: 123
   VAG: #: 216 Name: vag_newstab2 File: newstab2.vag
    VAG SIZE: 4096
    VAG RATE: 15624
    VAG POS: 1974272
 SEQUENCE: #: 171 Name: ST_KANO_BALL 
  PATCH: #: 208 Name: pat_kanoball 
   MAP: #: 208 Name: map_kanoball 
    ROOT: 77 FINE: 123
   VAG: #: 217 Name: vag_kanoball File: kanoball.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 1978368
    VAG LCD FILEPOS: 112688 mod: 48
 SEQUENCE: #: 146 Name: RSND_KLANG1 
  PATCH: #: 288 Name: pat_mhit1 
   MAP: #: 288 Name: map_mhit1 
    ROOT: 81 FINE: 105
   VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
    VAG SIZE: 3184
    VAG RATE: 12500
    VAG POS: 2527232
    VAG LCD FILEPOS: 122816 mod: 1984
 SEQUENCE: #: 147 Name: RSND_KLANG2 
  PATCH: #: 289 Name: pat_mhit2 
   MAP: #: 289 Name: map_mhit2 
    ROOT: 81 FINE: 105
   VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
    VAG SIZE: 3568
    VAG RATE: 12500
    VAG POS: 2531328
    VAG LCD FILEPOS: 126000 mod: 1072
LOADLIST----TOTAL: Name: NOOB_FIGHT_SNDS File: noob.lcd Size: 127520

LOADLIST: Name: JAX_FIGHT_SNDS File: jax.lcd
 SEQUENCE: #: 3 Name: GS_DEATH_EB 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 12 Name: GS_SHOOK_EB 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 21 Name: GS_RUN_EB 
  PATCH: #: 217 Name: pat_eblrun 
   MAP: #: 217 Name: map_eblrun 
    ROOT: 77 FINE: 123
   VAG: #: 76 Name: vag_eblrun File: eblrun.vag
    VAG SIZE: 11648
    VAG RATE: 15624
    VAG POS: 669696
    VAG LCD FILEPOS: 30608 mod: 1936
 SEQUENCE: #: 30 Name: GS_TV_EB 
  PATCH: #: 218 Name: pat_ebtrip 
   MAP: #: 218 Name: map_ebtrip 
    ROOT: 77 FINE: 123
   VAG: #: 77 Name: vag_ebtrip File: ebtrip.vag
    VAG SIZE: 3072
    VAG RATE: 15624
    VAG POS: 681984
    VAG LCD FILEPOS: 42256 mod: 1296
 SEQUENCE: #: 44 Name: GS_FHV_EB1 
  PATCH: #: 219 Name: pat_eblface1 
   MAP: #: 219 Name: map_eblface1 
    ROOT: 77 FINE: 123
   VAG: #: 78 Name: vag_eblface1 File: eblface1.vag
    VAG SIZE: 2464
    VAG RATE: 15624
    VAG POS: 686080
    VAG LCD FILEPOS: 45328 mod: 272
 SEQUENCE: #: 45 Name: GS_FHV_EB2 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
    VAG LCD FILEPOS: 47792 mod: 688
 SEQUENCE: #: 46 Name: GS_FHV_EB3 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
 SEQUENCE: #: 68 Name: GS_ATK_EB1 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
    VAG LCD FILEPOS: 52304 mod: 1104
 SEQUENCE: #: 69 Name: GS_ATK_EB2 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
 SEQUENCE: #: 81 Name: GS_JUMP_EB 
  PATCH: #: 222 Name: pat_ebljump 
   MAP: #: 222 Name: map_ebljump 
    ROOT: 77 FINE: 123
   VAG: #: 82 Name: vag_ebljump File: ebljump.vag
    VAG SIZE: 2320
    VAG RATE: 15624
    VAG POS: 704512
    VAG LCD FILEPOS: 55312 mod: 16
 SEQUENCE: #: 89 Name: GS_GRAB_EB 
  PATCH: #: 223 Name: pat_eblgrab 
   MAP: #: 223 Name: map_eblgrab 
    ROOT: 77 FINE: 123
   VAG: #: 83 Name: vag_eblgrab File: eblgrab.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 708608
    VAG LCD FILEPOS: 57632 mod: 288
 SEQUENCE: #: 98 Name: GS_SLAM_EB 
  PATCH: #: 224 Name: pat_eblthrow 
   MAP: #: 224 Name: map_eblthrow 
    ROOT: 77 FINE: 123
   VAG: #: 84 Name: vag_eblthrow File: eblthrow.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 712704
    VAG LCD FILEPOS: 60848 mod: 1456
 SEQUENCE: #: 110 Name: GS_WST_EB1 
  PATCH: #: 225 Name: pat_eblwast1 
   MAP: #: 225 Name: map_eblwast1 
    ROOT: 77 FINE: 123
   VAG: #: 85 Name: vag_eblwast1 File: eblwast1.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 720896
    VAG LCD FILEPOS: 68448 mod: 864
 SEQUENCE: #: 111 Name: GS_WST_EB2 
  PATCH: #: 226 Name: pat_eblwast2 
   MAP: #: 226 Name: map_eblwast2 
    ROOT: 81 FINE: 105
   VAG: #: 86 Name: vag_eblwast2 File: eblwast2.vag
    VAG SIZE: 7568
    VAG RATE: 12500
    VAG POS: 731136
    VAG LCD FILEPOS: 78576 mod: 752
 SEQUENCE: #: 184 Name: ST_JAX_CANNON 
  PATCH: #: 145 Name: pat_jaxcann 
   MAP: #: 145 Name: map_jaxcann 
    ROOT: 77 FINE: 123
   VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
    VAG SIZE: 12880
    VAG RATE: 15624
    VAG POS: 1347584
    VAG LCD FILEPOS: 86144 mod: 128
 SEQUENCE: #: 185 Name: ST_JAX_ZAP_HIT 
  PATCH: #: 149 Name: pat_rockhit 
   MAP: #: 149 Name: map_rockhit 
    ROOT: 77 FINE: 123
   VAG: #: 168 Name: vag_rockhit File: rockhit.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 1392640
    VAG LCD FILEPOS: 99024 mod: 720
 SEQUENCE: #: 186 Name: ST_JAX_ZAP_BLK 
  PATCH: #: 149 Name: pat_rockhit 
   MAP: #: 149 Name: map_rockhit 
    ROOT: 77 FINE: 123
   VAG: #: 168 Name: vag_rockhit File: rockhit.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 1392640
 SEQUENCE: #: 187 Name: ST_JAX_BLURR 
  PATCH: #: 150 Name: pat_jaxrush 
   MAP: #: 150 Name: map_jaxrush 
    ROOT: 77 FINE: 123
   VAG: #: 169 Name: vag_jaxrush File: jaxrush.vag
    VAG SIZE: 7056
    VAG RATE: 15624
    VAG POS: 1400832
    VAG LCD FILEPOS: 106624 mod: 128
 SEQUENCE: #: 188 Name: ST_JAX_GROUND 
  PATCH: #: 151 Name: pat_jaxhitg 
   MAP: #: 151 Name: map_jaxhitg 
    ROOT: 77 FINE: 123
   VAG: #: 170 Name: vag_jaxhitg File: jaxhitg.vag
    VAG SIZE: 11920
    VAG RATE: 15624
    VAG POS: 1409024
    VAG LCD FILEPOS: 113680 mod: 1040
 SEQUENCE: #: 189 Name: ST_JAX_SLAM 
  PATCH: #: 94 Name: pat_bigbslam 
   MAP: #: 94 Name: map_bigbslam 
    ROOT: 77 FINE: 123
   VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
    VAG SIZE: 8416
    VAG RATE: 15624
    VAG POS: 1189888
    VAG LCD FILEPOS: 125600 mod: 672
 SEQUENCE: #: 190 Name: ST_JAX_GOTCHA 
  PATCH: #: 266 Name: pat_jaxgotch 
   MAP: #: 266 Name: map_jaxgotch 
    ROOT: 77 FINE: 123
   VAG: #: 257 Name: vag_jaxgotch File: jaxgotch.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 2320384
    VAG LCD FILEPOS: 134016 mod: 896
 SEQUENCE: #: 191 Name: ST_JAX_BREAK 
  PATCH: #: 110 Name: pat_crunch2 
   MAP: #: 110 Name: map_crunch2 
    ROOT: 81 FINE: 105
   VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
    VAG SIZE: 4656
    VAG RATE: 12500
    VAG POS: 1249280
    VAG LCD FILEPOS: 139968 mod: 704
 SEQUENCE: #: 192 Name: TS_KLANG1 
  PATCH: #: 288 Name: pat_mhit1 
   MAP: #: 288 Name: map_mhit1 
    ROOT: 81 FINE: 105
   VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
    VAG SIZE: 3184
    VAG RATE: 12500
    VAG POS: 2527232
    VAG LCD FILEPOS: 144624 mod: 1264
LOADLIST----TOTAL: Name: JAX_FIGHT_SNDS File: jax.lcd Size: 145760

LOADLIST: Name: SUBZERO_FIGHT_SNDS File: subzero.lcd
 SEQUENCE: #: 4 Name: GS_DEATH_JH 
  PATCH: #: 216 Name: pat_ebldeath 
   MAP: #: 216 Name: map_ebldeath 
    ROOT: 81 FINE: 105
   VAG: #: 74 Name: vag_ebldeath File: ebldeath.vag
    VAG SIZE: 19952
    VAG RATE: 12500
    VAG POS: 634880
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 13 Name: GS_SHOOK_JH 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 22000 mod: 1520
 SEQUENCE: #: 22 Name: GS_RUN_JH 
  PATCH: #: 98 Name: pat_jhrun 
   MAP: #: 98 Name: map_jhrun 
    ROOT: 81 FINE: 105
   VAG: #: 88 Name: vag_jhrun File: jhrun.vag
    VAG SIZE: 9328
    VAG RATE: 12500
    VAG POS: 755712
    VAG LCD FILEPOS: 34560 mod: 1792
 SEQUENCE: #: 31 Name: GS_TV_JH 
  PATCH: #: 99 Name: pat_jhtrip 
   MAP: #: 99 Name: map_jhtrip 
    ROOT: 81 FINE: 105
   VAG: #: 89 Name: vag_jhtrip File: jhtrip.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 765952
    VAG LCD FILEPOS: 43888 mod: 880
 SEQUENCE: #: 47 Name: GS_FHV_JH1 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
    VAG LCD FILEPOS: 46800 mod: 1744
 SEQUENCE: #: 48 Name: GS_FHV_JH2 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 49 Name: GS_FHV_JH3 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 70 Name: GS_ATK_JH1 
  PATCH: #: 100 Name: pat_jhatt1 
   MAP: #: 100 Name: map_jhatt1 
    ROOT: 81 FINE: 105
   VAG: #: 91 Name: vag_jhatt1 File: jhatt1.vag
    VAG SIZE: 2080
    VAG RATE: 12500
    VAG POS: 776192
    VAG LCD FILEPOS: 51792 mod: 592
 SEQUENCE: #: 71 Name: GS_ATK_JH2 
  PATCH: #: 143 Name: pat_jhatt2 
   MAP: #: 143 Name: map_jhatt2 
    ROOT: 77 FINE: 123
   VAG: #: 92 Name: vag_jhatt2 File: jhatt2.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 780288
    VAG LCD FILEPOS: 53872 mod: 624
 SEQUENCE: #: 82 Name: GS_JUMP_JH 
  PATCH: #: 101 Name: pat_jhjump 
   MAP: #: 101 Name: map_jhjump 
    ROOT: 81 FINE: 105
   VAG: #: 93 Name: vag_jhjump File: jhjump.vag
    VAG SIZE: 2032
    VAG RATE: 12500
    VAG POS: 784384
    VAG LCD FILEPOS: 57152 mod: 1856
 SEQUENCE: #: 90 Name: GS_GRAB_JH 
  PATCH: #: 102 Name: pat_jhgrab 
   MAP: #: 102 Name: map_jhgrab 
    ROOT: 81 FINE: 105
   VAG: #: 94 Name: vag_jhgrab File: jhgrab.vag
    VAG SIZE: 2416
    VAG RATE: 12500
    VAG POS: 786432
    VAG LCD FILEPOS: 59184 mod: 1840
 SEQUENCE: #: 99 Name: GS_SLAM_JH 
  PATCH: #: 103 Name: pat_jhthrow 
   MAP: #: 103 Name: map_jhthrow 
    ROOT: 81 FINE: 105
   VAG: #: 95 Name: vag_jhthrow File: jhthrow.vag
    VAG SIZE: 5488
    VAG RATE: 12500
    VAG POS: 790528
    VAG LCD FILEPOS: 61600 mod: 160
 SEQUENCE: #: 112 Name: GS_WST_JH1 
  PATCH: #: 144 Name: pat_jhwast1 
   MAP: #: 144 Name: map_jhwast1 
    ROOT: 77 FINE: 123
   VAG: #: 96 Name: vag_jhwast1 File: jhwast1.vag
    VAG SIZE: 13424
    VAG RATE: 15624
    VAG POS: 796672
    VAG LCD FILEPOS: 67088 mod: 1552
 SEQUENCE: #: 113 Name: GS_WST_JH2 
  PATCH: #: 104 Name: pat_jhwast2 
   MAP: #: 104 Name: map_jhwast2 
    ROOT: 81 FINE: 105
   VAG: #: 97 Name: vag_jhwast2 File: jhwast2.vag
    VAG SIZE: 9552
    VAG RATE: 12500
    VAG POS: 811008
    VAG LCD FILEPOS: 80512 mod: 640
 SEQUENCE: #: 197 Name: ST_SZ_ICE_UP 
  PATCH: #: 193 Name: pat_sziceup 
   MAP: #: 193 Name: map_sziceup 
    ROOT: 77 FINE: 123
   VAG: #: 209 Name: vag_sziceup File: sziceup.vag
    VAG SIZE: 11856
    VAG RATE: 15624
    VAG POS: 1900544
    VAG LCD FILEPOS: 90064 mod: 2000
 SEQUENCE: #: 198 Name: ST_SZ_ICE_DOWN 
  PATCH: #: 194 Name: pat_szicedwn 
   MAP: #: 194 Name: map_szicedwn 
    ROOT: 77 FINE: 123
   VAG: #: 210 Name: vag_szicedwn File: szicedwn.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 1912832
    VAG LCD FILEPOS: 101920 mod: 1568
 SEQUENCE: #: 199 Name: ST_SZ_ICE_HIT 
  PATCH: #: 195 Name: pat_icehit 
   MAP: #: 195 Name: map_icehit 
    ROOT: 77 FINE: 123
   VAG: #: 211 Name: vag_icehit File: icehit.vag
    VAG SIZE: 6368
    VAG RATE: 15624
    VAG POS: 1923072
    VAG LCD FILEPOS: 110272 mod: 1728
 SEQUENCE: #: 200 Name: ST_SZ_ICE_BLOCK 
  PATCH: #: 195 Name: pat_icehit 
   MAP: #: 195 Name: map_icehit 
    ROOT: 77 FINE: 123
   VAG: #: 211 Name: vag_icehit File: icehit.vag
    VAG SIZE: 6368
    VAG RATE: 15624
    VAG POS: 1923072
 SEQUENCE: #: 201 Name: ST_SZ_ICE_HIT2 
  PATCH: #: 196 Name: pat_subzturd 
   MAP: #: 196 Name: map_subzturd 
    ROOT: 77 FINE: 123
   VAG: #: 212 Name: vag_subzturd File: subzturd.vag
    VAG SIZE: 6656
    VAG RATE: 15624
    VAG POS: 1931264
    VAG LCD FILEPOS: 116640 mod: 1952
 SEQUENCE: #: 202 Name: ST_SZ_ICE_DECOY 
  PATCH: #: 197 Name: pat_szifrz 
   MAP: #: 197 Name: map_szifrz 
    ROOT: 77 FINE: 123
   VAG: #: 213 Name: vag_szifrz File: szifrz.vag
    VAG SIZE: 10272
    VAG RATE: 15624
    VAG POS: 1939456
    VAG LCD FILEPOS: 123296 mod: 416
 SEQUENCE: #: 203 Name: ST_SZ_SLIDE 
  PATCH: #: 198 Name: pat_szslide 
   MAP: #: 198 Name: map_szslide 
    ROOT: 77 FINE: 123
   VAG: #: 214 Name: vag_szslide File: szslide.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 1951744
    VAG LCD FILEPOS: 133568 mod: 448
 SEQUENCE: #: 386 Name: TS_DF_FROSTY 
  PATCH: #: 23 Name: pat_dfrosty 
   MAP: #: 23 Name: map_dfrosty 
    ROOT: 77 FINE: 123
   VAG: #: 18 Name: vag_dfrosty File: dfrosty.vag
    VAG SIZE: 7056
    VAG RATE: 15624
    VAG POS: 180224
    VAG LCD FILEPOS: 143632 mod: 272
LOADLIST----TOTAL: Name: SUBZERO_FIGHT_SNDS File: subzero.lcd Size: 148640

LOADLIST: Name: SWAT_FIGHT_SNDS File: swat.lcd
 SEQUENCE: #: 1 Name: GS_DEATH_VP 
  PATCH: #: 67 Name: pat_vpdeath 
   MAP: #: 67 Name: map_vpdeath 
    ROOT: 82 FINE: 5
   VAG: #: 51 Name: vag_vpdeath File: vpdeath.vag
    VAG SIZE: 22656
    VAG RATE: 11000
    VAG POS: 440320
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 10 Name: GS_SHOOK_VP 
  PATCH: #: 68 Name: pat_vpnotbut 
   MAP: #: 68 Name: map_vpnotbut 
    ROOT: 82 FINE: 5
   VAG: #: 52 Name: vag_vpnotbut File: vpnotbut.vag
    VAG SIZE: 17536
    VAG RATE: 11000
    VAG POS: 464896
    VAG LCD FILEPOS: 24704 mod: 128
 SEQUENCE: #: 19 Name: GS_RUN_VP 
  PATCH: #: 69 Name: pat_vprun 
   MAP: #: 69 Name: map_vprun 
    ROOT: 77 FINE: 123
   VAG: #: 53 Name: vag_vprun File: vprun.vag
    VAG SIZE: 11568
    VAG RATE: 15624
    VAG POS: 483328
    VAG LCD FILEPOS: 42240 mod: 1280
 SEQUENCE: #: 28 Name: GS_TV_VP 
  PATCH: #: 70 Name: pat_vpgrab 
   MAP: #: 70 Name: map_vpgrab 
    ROOT: 77 FINE: 123
   VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
    VAG SIZE: 2864
    VAG RATE: 15624
    VAG POS: 495616
    VAG LCD FILEPOS: 53808 mod: 560
 SEQUENCE: #: 38 Name: GS_FHV_VP1 
  PATCH: #: 71 Name: pat_vpface1 
   MAP: #: 71 Name: map_vpface1 
    ROOT: 77 FINE: 123
   VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 499712
    VAG LCD FILEPOS: 56672 mod: 1376
 SEQUENCE: #: 39 Name: GS_FHV_VP2 
  PATCH: #: 71 Name: pat_vpface1 
   MAP: #: 71 Name: map_vpface1 
    ROOT: 77 FINE: 123
   VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 499712
 SEQUENCE: #: 40 Name: GS_FHV_VP3 
  PATCH: #: 72 Name: pat_vpface2 
   MAP: #: 72 Name: map_vpface2 
    ROOT: 77 FINE: 123
   VAG: #: 56 Name: vag_vpface2 File: vpface2.vag
    VAG SIZE: 5200
    VAG RATE: 15624
    VAG POS: 505856
    VAG LCD FILEPOS: 61392 mod: 2000
 SEQUENCE: #: 64 Name: GS_ATK_VP1 
  PATCH: #: 73 Name: pat_vpatt1 
   MAP: #: 73 Name: map_vpatt1 
    ROOT: 77 FINE: 123
   VAG: #: 57 Name: vag_vpatt1 File: vpatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 512000
    VAG LCD FILEPOS: 66592 mod: 1056
 SEQUENCE: #: 65 Name: GS_ATK_VP2 
  PATCH: #: 74 Name: pat_vpatt2 
   MAP: #: 74 Name: map_vpatt2 
    ROOT: 77 FINE: 123
   VAG: #: 58 Name: vag_vpatt2 File: vpatt2.vag
    VAG SIZE: 3136
    VAG RATE: 15624
    VAG POS: 516096
    VAG LCD FILEPOS: 69600 mod: 2016
 SEQUENCE: #: 79 Name: GS_JUMP_VP 
  PATCH: #: 75 Name: pat_vpjump 
   MAP: #: 75 Name: map_vpjump 
    ROOT: 77 FINE: 123
   VAG: #: 59 Name: vag_vpjump File: vpjump.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 520192
    VAG LCD FILEPOS: 72736 mod: 1056
 SEQUENCE: #: 87 Name: GS_GRAB_VP 
  PATCH: #: 70 Name: pat_vpgrab 
   MAP: #: 70 Name: map_vpgrab 
    ROOT: 77 FINE: 123
   VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
    VAG SIZE: 2864
    VAG RATE: 15624
    VAG POS: 495616
 SEQUENCE: #: 96 Name: GS_SLAM_VP 
  PATCH: #: 76 Name: pat_vpthrow 
   MAP: #: 76 Name: map_vpthrow 
    ROOT: 77 FINE: 123
   VAG: #: 60 Name: vag_vpthrow File: vpthrow.vag
    VAG SIZE: 8560
    VAG RATE: 15624
    VAG POS: 524288
    VAG LCD FILEPOS: 75952 mod: 176
 SEQUENCE: #: 106 Name: GS_WST_VP1 
  PATCH: #: 77 Name: pat_vpwast2 
   MAP: #: 77 Name: map_vpwast2 
    ROOT: 81 FINE: 105
   VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
    VAG SIZE: 7728
    VAG RATE: 12500
    VAG POS: 534528
    VAG LCD FILEPOS: 84512 mod: 544
 SEQUENCE: #: 107 Name: GS_WST_VP2 
  PATCH: #: 77 Name: pat_vpwast2 
   MAP: #: 77 Name: map_vpwast2 
    ROOT: 81 FINE: 105
   VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
    VAG SIZE: 7728
    VAG RATE: 12500
    VAG POS: 534528
 SEQUENCE: #: 206 Name: ST_SWAT_BOMB_EXP 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 92240 mod: 80
 SEQUENCE: #: 207 Name: ST_SWAT_BOMB_THROW 
  PATCH: #: 231 Name: pat_strkgren 
   MAP: #: 231 Name: map_strkgren 
    ROOT: 77 FINE: 123
   VAG: #: 227 Name: vag_strkgren File: strkgren.vag
    VAG SIZE: 7264
    VAG RATE: 15624
    VAG POS: 2084864
    VAG LCD FILEPOS: 106896 mod: 400
 SEQUENCE: #: 208 Name: ST_SWAT_ZOOM 
  PATCH: #: 232 Name: pat_strklung 
   MAP: #: 232 Name: map_strklung 
    ROOT: 77 FINE: 123
   VAG: #: 228 Name: vag_strklung File: strklung.vag
    VAG SIZE: 7088
    VAG RATE: 15624
    VAG POS: 2093056
    VAG LCD FILEPOS: 114160 mod: 1520
 SEQUENCE: #: 209 Name: ST_SWAT_STICK 
  PATCH: #: 233 Name: pat_sonbike 
   MAP: #: 233 Name: map_sonbike 
    ROOT: 77 FINE: 123
   VAG: #: 229 Name: vag_sonbike File: sonbike.vag
    VAG SIZE: 7392
    VAG RATE: 15624
    VAG POS: 2101248
    VAG LCD FILEPOS: 121248 mod: 416
 SEQUENCE: #: 210 Name: ST_SWAT_ZOOM_THROW 
  PATCH: #: 234 Name: pat_strkthro 
   MAP: #: 234 Name: map_strkthro 
    ROOT: 77 FINE: 123
   VAG: #: 230 Name: vag_strkthro File: strkthro.vag
    VAG SIZE: 4640
    VAG RATE: 15624
    VAG POS: 2109440
    VAG LCD FILEPOS: 128640 mod: 1664
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 133280 mod: 160
LOADLIST----TOTAL: Name: SWAT_FIGHT_SNDS File: swat.lcd Size: 133200

LOADLIST: Name: INDIAN_FIGHT_SNDS File: indian.lcd
 SEQUENCE: #: 2 Name: GS_DEATH_DF 
  PATCH: #: 199 Name: pat_dfdeath 
   MAP: #: 199 Name: map_dfdeath 
    ROOT: 81 FINE: 105
   VAG: #: 62 Name: vag_dfdeath File: dfdeath.vag
    VAG SIZE: 13872
    VAG RATE: 12500
    VAG POS: 542720
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 11 Name: GS_SHOOK_DF 
  PATCH: #: 68 Name: pat_vpnotbut 
   MAP: #: 68 Name: map_vpnotbut 
    ROOT: 82 FINE: 5
   VAG: #: 52 Name: vag_vpnotbut File: vpnotbut.vag
    VAG SIZE: 17536
    VAG RATE: 11000
    VAG POS: 464896
    VAG LCD FILEPOS: 15920 mod: 1584
 SEQUENCE: #: 20 Name: GS_RUN_DF 
  PATCH: #: 200 Name: pat_dfrun 
   MAP: #: 200 Name: map_dfrun 
    ROOT: 77 FINE: 123
   VAG: #: 63 Name: vag_dfrun File: dfrun.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 557056
    VAG LCD FILEPOS: 33456 mod: 688
 SEQUENCE: #: 29 Name: GS_TV_DF 
  PATCH: #: 47 Name: pat_dftrip 
   MAP: #: 47 Name: map_dftrip 
    ROOT: 77 FINE: 123
   VAG: #: 64 Name: vag_dftrip File: dftrip.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 573440
    VAG LCD FILEPOS: 48112 mod: 1008
 SEQUENCE: #: 41 Name: GS_FHV_DF1 
  PATCH: #: 201 Name: pat_dfface1 
   MAP: #: 201 Name: map_dfface1 
    ROOT: 77 FINE: 123
   VAG: #: 65 Name: vag_dfface1 File: dfface1.vag
    VAG SIZE: 4304
    VAG RATE: 15624
    VAG POS: 579584
    VAG LCD FILEPOS: 52896 mod: 1696
 SEQUENCE: #: 42 Name: GS_FHV_DF2 
  PATCH: #: 201 Name: pat_dfface1 
   MAP: #: 201 Name: map_dfface1 
    ROOT: 77 FINE: 123
   VAG: #: 65 Name: vag_dfface1 File: dfface1.vag
    VAG SIZE: 4304
    VAG RATE: 15624
    VAG POS: 579584
 SEQUENCE: #: 43 Name: GS_FHV_DF3 
  PATCH: #: 202 Name: pat_dfface2 
   MAP: #: 202 Name: map_dfface2 
    ROOT: 77 FINE: 123
   VAG: #: 66 Name: vag_dfface2 File: dfface2.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 585728
    VAG LCD FILEPOS: 57200 mod: 1904
 SEQUENCE: #: 66 Name: GS_ATK_DF1 
  PATCH: #: 49 Name: pat_dfatt1 
   MAP: #: 49 Name: map_dfatt1 
    ROOT: 77 FINE: 123
   VAG: #: 67 Name: vag_dfatt1 File: dfatt1.vag
    VAG SIZE: 3968
    VAG RATE: 15624
    VAG POS: 591872
    VAG LCD FILEPOS: 62192 mod: 752
 SEQUENCE: #: 67 Name: GS_ATK_DF2 
  PATCH: #: 50 Name: pat_dfatt2 
   MAP: #: 50 Name: map_dfatt2 
    ROOT: 77 FINE: 123
   VAG: #: 68 Name: vag_dfatt2 File: dfatt2.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 595968
    VAG LCD FILEPOS: 66160 mod: 624
 SEQUENCE: #: 80 Name: GS_JUMP_DF 
  PATCH: #: 51 Name: pat_dfjump 
   MAP: #: 51 Name: map_dfjump 
    ROOT: 77 FINE: 123
   VAG: #: 69 Name: vag_dfjump File: dfjump.vag
    VAG SIZE: 2048
    VAG RATE: 15624
    VAG POS: 600064
    VAG LCD FILEPOS: 69376 mod: 1792
 SEQUENCE: #: 88 Name: GS_GRAB_DF 
  PATCH: #: 203 Name: pat_dfgrab 
   MAP: #: 203 Name: map_dfgrab 
    ROOT: 77 FINE: 123
   VAG: #: 70 Name: vag_dfgrab File: dfgrab.vag
    VAG SIZE: 2800
    VAG RATE: 15624
    VAG POS: 602112
    VAG LCD FILEPOS: 71424 mod: 1792
 SEQUENCE: #: 97 Name: GS_SLAM_DF 
  PATCH: #: 204 Name: pat_dfthrow 
   MAP: #: 204 Name: map_dfthrow 
    ROOT: 77 FINE: 123
   VAG: #: 71 Name: vag_dfthrow File: dfthrow.vag
    VAG SIZE: 7328
    VAG RATE: 15624
    VAG POS: 606208
    VAG LCD FILEPOS: 74224 mod: 496
 SEQUENCE: #: 108 Name: GS_WST_DF1 
  PATCH: #: 205 Name: pat_dfwast2 
   MAP: #: 205 Name: map_dfwast2 
    ROOT: 77 FINE: 123
   VAG: #: 72 Name: vag_dfwast2 File: dfwast2.vag
    VAG SIZE: 8816
    VAG RATE: 15624
    VAG POS: 614400
    VAG LCD FILEPOS: 81552 mod: 1680
 SEQUENCE: #: 109 Name: GS_WST_DF2 
  PATCH: #: 205 Name: pat_dfwast2 
   MAP: #: 205 Name: map_dfwast2 
    ROOT: 77 FINE: 123
   VAG: #: 72 Name: vag_dfwast2 File: dfwast2.vag
    VAG SIZE: 8816
    VAG RATE: 15624
    VAG POS: 614400
 SEQUENCE: #: 216 Name: ST_IND_ARROW_APPEAR 
  PATCH: #: 105 Name: pat_bappear 
   MAP: #: 105 Name: map_bappear 
    ROOT: 81 FINE: 105
   VAG: #: 146 Name: vag_bappear File: bappear.vag
    VAG SIZE: 5600
    VAG RATE: 12500
    VAG POS: 1208320
    VAG LCD FILEPOS: 90368 mod: 256
 SEQUENCE: #: 217 Name: ST_IND_ARROW_FIRE 
  PATCH: #: 106 Name: pat_barrow2 
   MAP: #: 106 Name: map_barrow2 
    ROOT: 81 FINE: 105
   VAG: #: 147 Name: vag_barrow2 File: barrow2.vag
    VAG SIZE: 10864
    VAG RATE: 12500
    VAG POS: 1214464
    VAG LCD FILEPOS: 95968 mod: 1760
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 106832 mod: 336
 SEQUENCE: #: 219 Name: ST_IND_ARROW_HIT 
  PATCH: #: 108 Name: pat_arrowhit 
   MAP: #: 108 Name: map_arrowhit 
    ROOT: 81 FINE: 105
   VAG: #: 149 Name: vag_arrowhit File: arrowhit.vag
    VAG SIZE: 3792
    VAG RATE: 12500
    VAG POS: 1236992
    VAG LCD FILEPOS: 116816 mod: 80
 SEQUENCE: #: 220 Name: ST_IND_ARROW_BLK 
  PATCH: #: 108 Name: pat_arrowhit 
   MAP: #: 108 Name: map_arrowhit 
    ROOT: 81 FINE: 105
   VAG: #: 149 Name: vag_arrowhit File: arrowhit.vag
    VAG SIZE: 3792
    VAG RATE: 12500
    VAG POS: 1236992
 SEQUENCE: #: 221 Name: ST_IND_AXE_UP 
  PATCH: #: 109 Name: pat_kanoax 
   MAP: #: 109 Name: map_kanoax 
    ROOT: 81 FINE: 105
   VAG: #: 150 Name: vag_kanoax File: kanoax.vag
    VAG SIZE: 6960
    VAG RATE: 12500
    VAG POS: 1241088
    VAG LCD FILEPOS: 120608 mod: 1824
 SEQUENCE: #: 222 Name: ST_IND_KNEE_BREAK 
  PATCH: #: 110 Name: pat_crunch2 
   MAP: #: 110 Name: map_crunch2 
    ROOT: 81 FINE: 105
   VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
    VAG SIZE: 4656
    VAG RATE: 12500
    VAG POS: 1249280
    VAG LCD FILEPOS: 127568 mod: 592
 SEQUENCE: #: 223 Name: ST_IND_REFLECT 
  PATCH: #: 111 Name: pat_salrflc2 
   MAP: #: 111 Name: map_salrflc2 
    ROOT: 81 FINE: 105
   VAG: #: 152 Name: vag_salrflc2 File: salrflc2.vag
    VAG SIZE: 5872
    VAG RATE: 12500
    VAG POS: 1255424
    VAG LCD FILEPOS: 132224 mod: 1152
 SEQUENCE: #: 224 Name: ST_IND_SHADOW_SHLD 
  PATCH: #: 112 Name: pat_salrush 
   MAP: #: 112 Name: map_salrush 
    ROOT: 81 FINE: 105
   VAG: #: 153 Name: vag_salrush File: salrush.vag
    VAG SIZE: 5376
    VAG RATE: 12500
    VAG POS: 1261568
    VAG LCD FILEPOS: 138096 mod: 880
 SEQUENCE: #: 225 Name: ST_IND_AXE_HIT 
  PATCH: #: 113 Name: pat_faxhit 
   MAP: #: 113 Name: map_faxhit 
    ROOT: 81 FINE: 105
   VAG: #: 154 Name: vag_faxhit File: faxhit.vag
    VAG SIZE: 8016
    VAG RATE: 12500
    VAG POS: 1267712
    VAG LCD FILEPOS: 143472 mod: 112
LOADLIST----TOTAL: Name: INDIAN_FIGHT_SNDS File: indian.lcd Size: 149440

LOADLIST: Name: ROBO1_FIGHT_SNDS File: robo1.lcd
 SEQUENCE: #: 5 Name: GS_DEATH_ROBO 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 14 Name: GS_SHOOK_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 23 Name: GS_RUN_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
 SEQUENCE: #: 50 Name: GS_FHV_ROBO1 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
    VAG LCD FILEPOS: 28112 mod: 1488
 SEQUENCE: #: 51 Name: GS_FHV_ROBO2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 52 Name: GS_FHV_ROBO3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
    VAG LCD FILEPOS: 32832 mod: 64
 SEQUENCE: #: 91 Name: GS_GRAB_ROBO 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
    VAG LCD FILEPOS: 39536 mod: 624
 SEQUENCE: #: 100 Name: GS_SLAM_ROBO 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
    VAG LCD FILEPOS: 45488 mod: 432
 SEQUENCE: #: 114 Name: GS_WST_ROBO1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 115 Name: GS_WST_ROBO2 
  PATCH: #: 54 Name: pat_robtalk4 
   MAP: #: 54 Name: map_robtalk4 
    ROOT: 77 FINE: 123
   VAG: #: 101 Name: vag_robtalk4 File: robtalk4.vag
    VAG SIZE: 8896
    VAG RATE: 15624
    VAG POS: 841728
    VAG LCD FILEPOS: 55824 mod: 528
 SEQUENCE: #: 241 Name: ST_ROBO_ARM_OUT 
  PATCH: #: 235 Name: pat_roboout 
   MAP: #: 235 Name: map_roboout 
    ROOT: 77 FINE: 123
   VAG: #: 117 Name: vag_roboout File: roboout.vag
    VAG SIZE: 3776
    VAG RATE: 15624
    VAG POS: 968704
    VAG LCD FILEPOS: 64720 mod: 1232
 SEQUENCE: #: 242 Name: ST_ROBO_ARM_THROW 
  PATCH: #: 236 Name: pat_robothrw 
   MAP: #: 236 Name: map_robothrw 
    ROOT: 77 FINE: 123
   VAG: #: 118 Name: vag_robothrw File: robothrw.vag
    VAG SIZE: 6432
    VAG RATE: 15624
    VAG POS: 972800
    VAG LCD FILEPOS: 68496 mod: 912
 SEQUENCE: #: 243 Name: ST_ROBO_ARM_IN 
  PATCH: #: 237 Name: pat_roboin 
   MAP: #: 237 Name: map_roboin 
    ROOT: 77 FINE: 123
   VAG: #: 119 Name: vag_roboin File: roboin.vag
    VAG SIZE: 2736
    VAG RATE: 15624
    VAG POS: 980992
    VAG LCD FILEPOS: 74928 mod: 1200
 SEQUENCE: #: 244 Name: ST_ROBO_FAST_ROCKET 
  PATCH: #: 238 Name: pat_robrock1 
   MAP: #: 238 Name: map_robrock1 
    ROOT: 77 FINE: 123
   VAG: #: 120 Name: vag_robrock1 File: robrock1.vag
    VAG SIZE: 12336
    VAG RATE: 15624
    VAG POS: 985088
    VAG LCD FILEPOS: 77664 mod: 1888
 SEQUENCE: #: 245 Name: ST_ROBO_CRUISE_ROCKET 
  PATCH: #: 239 Name: pat_robrock2 
   MAP: #: 239 Name: map_robrock2 
    ROOT: 77 FINE: 123
   VAG: #: 121 Name: vag_robrock2 File: robrock2.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 999424
    VAG LCD FILEPOS: 90000 mod: 1936
 SEQUENCE: #: 247 Name: ST_ROBO_ZAP_HIT 
  PATCH: #: 149 Name: pat_rockhit 
   MAP: #: 149 Name: map_rockhit 
    ROOT: 77 FINE: 123
   VAG: #: 168 Name: vag_rockhit File: rockhit.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 1392640
    VAG LCD FILEPOS: 95472 mod: 1264
 SEQUENCE: #: 248 Name: ST_ROBO_DROP 
  PATCH: #: 96 Name: pat_robtele 
   MAP: #: 96 Name: map_robtele 
    ROOT: 77 FINE: 123
   VAG: #: 116 Name: vag_robtele File: robtele.vag
    VAG SIZE: 10080
    VAG RATE: 15624
    VAG POS: 958464
    VAG LCD FILEPOS: 103072 mod: 672
 SEQUENCE: #: 251 Name: ST_ROBO_BALL_BOOM 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 113152 mod: 512
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 127808 mod: 832
 SEQUENCE: #: 253 Name: ST_ROBO_TARGET 
  PATCH: #: 60 Name: pat_robowarn 
   MAP: #: 60 Name: map_robowarn 
    ROOT: 77 FINE: 123
   VAG: #: 107 Name: vag_robowarn File: robowarn.vag
    VAG SIZE: 1904
    VAG RATE: 15624
    VAG POS: 892928
    VAG LCD FILEPOS: 132048 mod: 976
 SEQUENCE: #: 256 Name: ST_ROBO_SPARK1 
  PATCH: #: 63 Name: pat_robshk1 
   MAP: #: 63 Name: map_robshk1 
    ROOT: 77 FINE: 123
   VAG: #: 110 Name: vag_robshk1 File: robshk1.vag
    VAG SIZE: 4848
    VAG RATE: 15624
    VAG POS: 909312
    VAG LCD FILEPOS: 133952 mod: 832
 SEQUENCE: #: 257 Name: ST_ROBO_SPARK2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 258 Name: ST_ROBO_SPARK3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
 SEQUENCE: #: 259 Name: ST_ROBO_SPEECH1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 260 Name: ST_ROBO_SPEECH2 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
 SEQUENCE: #: 261 Name: ST_ROBO_SPEECH3 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 262 Name: ST_ROBO_SPEECH4 
  PATCH: #: 66 Name: pat_netblk 
   MAP: #: 66 Name: map_netblk 
    ROOT: 77 FINE: 123
   VAG: #: 113 Name: vag_netblk File: netblk.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 935936
    VAG LCD FILEPOS: 138800 mod: 1584
LOADLIST----TOTAL: Name: ROBO1_FIGHT_SNDS File: robo1.lcd Size: 142768

LOADLIST: Name: ROBO2_FIGHT_SNDS File: robo2.lcd
 SEQUENCE: #: 5 Name: GS_DEATH_ROBO 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 14 Name: GS_SHOOK_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 23 Name: GS_RUN_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
 SEQUENCE: #: 50 Name: GS_FHV_ROBO1 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
    VAG LCD FILEPOS: 28112 mod: 1488
 SEQUENCE: #: 51 Name: GS_FHV_ROBO2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 52 Name: GS_FHV_ROBO3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
    VAG LCD FILEPOS: 32832 mod: 64
 SEQUENCE: #: 91 Name: GS_GRAB_ROBO 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
    VAG LCD FILEPOS: 39536 mod: 624
 SEQUENCE: #: 100 Name: GS_SLAM_ROBO 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
    VAG LCD FILEPOS: 45488 mod: 432
 SEQUENCE: #: 114 Name: GS_WST_ROBO1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 115 Name: GS_WST_ROBO2 
  PATCH: #: 54 Name: pat_robtalk4 
   MAP: #: 54 Name: map_robtalk4 
    ROOT: 77 FINE: 123
   VAG: #: 101 Name: vag_robtalk4 File: robtalk4.vag
    VAG SIZE: 8896
    VAG RATE: 15624
    VAG POS: 841728
    VAG LCD FILEPOS: 55824 mod: 528
 SEQUENCE: #: 246 Name: ST_ROBO_NET 
  PATCH: #: 55 Name: pat_netthrow 
   MAP: #: 55 Name: map_netthrow 
    ROOT: 77 FINE: 123
   VAG: #: 102 Name: vag_netthrow File: netthrow.vag
    VAG SIZE: 9520
    VAG RATE: 15624
    VAG POS: 851968
    VAG LCD FILEPOS: 64720 mod: 1232
 SEQUENCE: #: 249 Name: ST_ROBO_CLANG1 
  PATCH: #: 56 Name: pat_robball1 
   MAP: #: 56 Name: map_robball1 
    ROOT: 77 FINE: 123
   VAG: #: 103 Name: vag_robball1 File: robball1.vag
    VAG SIZE: 3408
    VAG RATE: 15624
    VAG POS: 862208
    VAG LCD FILEPOS: 74240 mod: 512
 SEQUENCE: #: 250 Name: ST_ROBO_CLANG2 
  PATCH: #: 57 Name: pat_robball2 
   MAP: #: 57 Name: map_robball2 
    ROOT: 77 FINE: 123
   VAG: #: 104 Name: vag_robball2 File: robball2.vag
    VAG SIZE: 3344
    VAG RATE: 15624
    VAG POS: 866304
    VAG LCD FILEPOS: 77648 mod: 1872
 SEQUENCE: #: 251 Name: ST_ROBO_BALL_BOOM 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 80992 mod: 1120
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 95648 mod: 1440
 SEQUENCE: #: 254 Name: ST_ROBO_EXPLODE 
  PATCH: #: 61 Name: pat_robxpld2 
   MAP: #: 61 Name: map_robxpld2 
    ROOT: 77 FINE: 123
   VAG: #: 108 Name: vag_robxpld2 File: robxpld2.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 894976
    VAG LCD FILEPOS: 99888 mod: 1584
 SEQUENCE: #: 255 Name: ST_ROBO_IMPLODE 
  PATCH: #: 62 Name: pat_robimpld 
   MAP: #: 62 Name: map_robimpld 
    ROOT: 77 FINE: 123
   VAG: #: 109 Name: vag_robimpld File: robimpld.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 903168
    VAG LCD FILEPOS: 107616 mod: 1120
 SEQUENCE: #: 256 Name: ST_ROBO_SPARK1 
  PATCH: #: 63 Name: pat_robshk1 
   MAP: #: 63 Name: map_robshk1 
    ROOT: 77 FINE: 123
   VAG: #: 110 Name: vag_robshk1 File: robshk1.vag
    VAG SIZE: 4848
    VAG RATE: 15624
    VAG POS: 909312
    VAG LCD FILEPOS: 112128 mod: 1536
 SEQUENCE: #: 257 Name: ST_ROBO_SPARK2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 258 Name: ST_ROBO_SPARK3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
 SEQUENCE: #: 259 Name: ST_ROBO_SPEECH1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 260 Name: ST_ROBO_SPEECH2 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
 SEQUENCE: #: 261 Name: ST_ROBO_SPEECH3 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 262 Name: ST_ROBO_SPEECH4 
  PATCH: #: 66 Name: pat_netblk 
   MAP: #: 66 Name: map_netblk 
    ROOT: 77 FINE: 123
   VAG: #: 113 Name: vag_netblk File: netblk.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 935936
    VAG LCD FILEPOS: 116976 mod: 240
LOADLIST----TOTAL: Name: ROBO2_FIGHT_SNDS File: robo2.lcd Size: 120944

LOADLIST: Name: SMOKE_FIGHT_SNDS File: smoke.lcd
 SEQUENCE: #: 5 Name: GS_DEATH_ROBO 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 14 Name: GS_SHOOK_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 23 Name: GS_RUN_ROBO 
  PATCH: #: 46 Name: pat_robtalk5 
   MAP: #: 46 Name: map_robtalk5 
    ROOT: 77 FINE: 123
   VAG: #: 114 Name: vag_robtalk5 File: robtalk5.vag
    VAG SIZE: 10064
    VAG RATE: 15624
    VAG POS: 942080
 SEQUENCE: #: 50 Name: GS_FHV_ROBO1 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
    VAG LCD FILEPOS: 28112 mod: 1488
 SEQUENCE: #: 51 Name: GS_FHV_ROBO2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 52 Name: GS_FHV_ROBO3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
    VAG LCD FILEPOS: 32832 mod: 64
 SEQUENCE: #: 91 Name: GS_GRAB_ROBO 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
    VAG LCD FILEPOS: 39536 mod: 624
 SEQUENCE: #: 100 Name: GS_SLAM_ROBO 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
    VAG LCD FILEPOS: 45488 mod: 432
 SEQUENCE: #: 114 Name: GS_WST_ROBO1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 115 Name: GS_WST_ROBO2 
  PATCH: #: 54 Name: pat_robtalk4 
   MAP: #: 54 Name: map_robtalk4 
    ROOT: 77 FINE: 123
   VAG: #: 101 Name: vag_robtalk4 File: robtalk4.vag
    VAG SIZE: 8896
    VAG RATE: 15624
    VAG POS: 841728
    VAG LCD FILEPOS: 55824 mod: 528
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 64720 mod: 1232
 SEQUENCE: #: 248 Name: ST_ROBO_DROP 
  PATCH: #: 96 Name: pat_robtele 
   MAP: #: 96 Name: map_robtele 
    ROOT: 77 FINE: 123
   VAG: #: 116 Name: vag_robtele File: robtele.vag
    VAG SIZE: 10080
    VAG RATE: 15624
    VAG POS: 958464
    VAG LCD FILEPOS: 68960 mod: 1376
 SEQUENCE: #: 263 Name: ST_ROBO_SPEAR 
  PATCH: #: 265 Name: pat_bigstab2 
   MAP: #: 265 Name: map_bigstab2 
    ROOT: 81 FINE: 105
   VAG: #: 256 Name: vag_bigstab2 File: bigstab2.vag
    VAG SIZE: 6032
    VAG RATE: 12500
    VAG POS: 2314240
    VAG LCD FILEPOS: 79040 mod: 1216
 SEQUENCE: #: 264 Name: ST_ROBO_CRSH_SMOOSH2 
  PATCH: #: 273 Name: pat_scorthr1 
   MAP: #: 273 Name: map_scorthr1 
    ROOT: 77 FINE: 123
   VAG: #: 264 Name: vag_scorthr1 File: scorthr1.vag
    VAG SIZE: 15696
    VAG RATE: 15624
    VAG POS: 2383872
    VAG LCD FILEPOS: 85072 mod: 1104
 SEQUENCE: #: 256 Name: ST_ROBO_SPARK1 
  PATCH: #: 63 Name: pat_robshk1 
   MAP: #: 63 Name: map_robshk1 
    ROOT: 77 FINE: 123
   VAG: #: 110 Name: vag_robshk1 File: robshk1.vag
    VAG SIZE: 4848
    VAG RATE: 15624
    VAG POS: 909312
    VAG LCD FILEPOS: 100768 mod: 416
 SEQUENCE: #: 257 Name: ST_ROBO_SPARK2 
  PATCH: #: 48 Name: pat_robshk2 
   MAP: #: 48 Name: map_robshk2 
    ROOT: 77 FINE: 123
   VAG: #: 115 Name: vag_robshk2 File: robshk2.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 952320
 SEQUENCE: #: 258 Name: ST_ROBO_SPARK3 
  PATCH: #: 64 Name: pat_robshk3 
   MAP: #: 64 Name: map_robshk3 
    ROOT: 77 FINE: 123
   VAG: #: 111 Name: vag_robshk3 File: robshk3.vag
    VAG SIZE: 6704
    VAG RATE: 15624
    VAG POS: 915456
 SEQUENCE: #: 259 Name: ST_ROBO_SPEECH1 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 260 Name: ST_ROBO_SPEECH2 
  PATCH: #: 52 Name: pat_robtalk2 
   MAP: #: 52 Name: map_robtalk2 
    ROOT: 77 FINE: 123
   VAG: #: 99 Name: vag_robtalk2 File: robtalk2.vag
    VAG SIZE: 5952
    VAG RATE: 15624
    VAG POS: 827392
 SEQUENCE: #: 261 Name: ST_ROBO_SPEECH3 
  PATCH: #: 65 Name: pat_robtalk1 
   MAP: #: 65 Name: map_robtalk1 
    ROOT: 77 FINE: 123
   VAG: #: 112 Name: vag_robtalk1 File: robtalk1.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 923648
 SEQUENCE: #: 262 Name: ST_ROBO_SPEECH4 
  PATCH: #: 66 Name: pat_netblk 
   MAP: #: 66 Name: map_netblk 
    ROOT: 77 FINE: 123
   VAG: #: 113 Name: vag_netblk File: netblk.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 935936
    VAG LCD FILEPOS: 105616 mod: 1168
 SEQUENCE: #: 251 Name: ST_ROBO_BALL_BOOM 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 111632 mod: 1040
LOADLIST----TOTAL: Name: SMOKE_FIGHT_SNDS File: smoke.lcd Size: 124240

LOADLIST: Name: KUNGLAO_FIGHT_SNDS File: kunglao.lcd
 SEQUENCE: #: 4 Name: GS_DEATH_JH 
  PATCH: #: 216 Name: pat_ebldeath 
   MAP: #: 216 Name: map_ebldeath 
    ROOT: 81 FINE: 105
   VAG: #: 74 Name: vag_ebldeath File: ebldeath.vag
    VAG SIZE: 19952
    VAG RATE: 12500
    VAG POS: 634880
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 13 Name: GS_SHOOK_JH 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 22000 mod: 1520
 SEQUENCE: #: 22 Name: GS_RUN_JH 
  PATCH: #: 98 Name: pat_jhrun 
   MAP: #: 98 Name: map_jhrun 
    ROOT: 81 FINE: 105
   VAG: #: 88 Name: vag_jhrun File: jhrun.vag
    VAG SIZE: 9328
    VAG RATE: 12500
    VAG POS: 755712
    VAG LCD FILEPOS: 34560 mod: 1792
 SEQUENCE: #: 31 Name: GS_TV_JH 
  PATCH: #: 99 Name: pat_jhtrip 
   MAP: #: 99 Name: map_jhtrip 
    ROOT: 81 FINE: 105
   VAG: #: 89 Name: vag_jhtrip File: jhtrip.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 765952
    VAG LCD FILEPOS: 43888 mod: 880
 SEQUENCE: #: 47 Name: GS_FHV_JH1 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
    VAG LCD FILEPOS: 46800 mod: 1744
 SEQUENCE: #: 48 Name: GS_FHV_JH2 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 49 Name: GS_FHV_JH3 
  PATCH: #: 142 Name: pat_jhface1 
   MAP: #: 142 Name: map_jhface1 
    ROOT: 77 FINE: 123
   VAG: #: 90 Name: vag_jhface1 File: jhface1.vag
    VAG SIZE: 4992
    VAG RATE: 15624
    VAG POS: 770048
 SEQUENCE: #: 70 Name: GS_ATK_JH1 
  PATCH: #: 100 Name: pat_jhatt1 
   MAP: #: 100 Name: map_jhatt1 
    ROOT: 81 FINE: 105
   VAG: #: 91 Name: vag_jhatt1 File: jhatt1.vag
    VAG SIZE: 2080
    VAG RATE: 12500
    VAG POS: 776192
    VAG LCD FILEPOS: 51792 mod: 592
 SEQUENCE: #: 71 Name: GS_ATK_JH2 
  PATCH: #: 143 Name: pat_jhatt2 
   MAP: #: 143 Name: map_jhatt2 
    ROOT: 77 FINE: 123
   VAG: #: 92 Name: vag_jhatt2 File: jhatt2.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 780288
    VAG LCD FILEPOS: 53872 mod: 624
 SEQUENCE: #: 82 Name: GS_JUMP_JH 
  PATCH: #: 101 Name: pat_jhjump 
   MAP: #: 101 Name: map_jhjump 
    ROOT: 81 FINE: 105
   VAG: #: 93 Name: vag_jhjump File: jhjump.vag
    VAG SIZE: 2032
    VAG RATE: 12500
    VAG POS: 784384
    VAG LCD FILEPOS: 57152 mod: 1856
 SEQUENCE: #: 90 Name: GS_GRAB_JH 
  PATCH: #: 102 Name: pat_jhgrab 
   MAP: #: 102 Name: map_jhgrab 
    ROOT: 81 FINE: 105
   VAG: #: 94 Name: vag_jhgrab File: jhgrab.vag
    VAG SIZE: 2416
    VAG RATE: 12500
    VAG POS: 786432
    VAG LCD FILEPOS: 59184 mod: 1840
 SEQUENCE: #: 99 Name: GS_SLAM_JH 
  PATCH: #: 103 Name: pat_jhthrow 
   MAP: #: 103 Name: map_jhthrow 
    ROOT: 81 FINE: 105
   VAG: #: 95 Name: vag_jhthrow File: jhthrow.vag
    VAG SIZE: 5488
    VAG RATE: 12500
    VAG POS: 790528
    VAG LCD FILEPOS: 61600 mod: 160
 SEQUENCE: #: 112 Name: GS_WST_JH1 
  PATCH: #: 144 Name: pat_jhwast1 
   MAP: #: 144 Name: map_jhwast1 
    ROOT: 77 FINE: 123
   VAG: #: 96 Name: vag_jhwast1 File: jhwast1.vag
    VAG SIZE: 13424
    VAG RATE: 15624
    VAG POS: 796672
    VAG LCD FILEPOS: 67088 mod: 1552
 SEQUENCE: #: 113 Name: GS_WST_JH2 
  PATCH: #: 104 Name: pat_jhwast2 
   MAP: #: 104 Name: map_jhwast2 
    ROOT: 81 FINE: 105
   VAG: #: 97 Name: vag_jhwast2 File: jhwast2.vag
    VAG SIZE: 9552
    VAG RATE: 12500
    VAG POS: 811008
    VAG LCD FILEPOS: 80512 mod: 640
 SEQUENCE: #: 277 Name: ST_LAO_ANGLE 
  PATCH: #: 227 Name: pat_lkflykck 
   MAP: #: 227 Name: map_lkflykck 
    ROOT: 77 FINE: 123
   VAG: #: 136 Name: vag_lkflykck File: lkflykck.vag
    VAG SIZE: 5264
    VAG RATE: 15624
    VAG POS: 1130496
    VAG LCD FILEPOS: 90064 mod: 2000
 SEQUENCE: #: 278 Name: ST_LAO_HAT_THROW 
  PATCH: #: 228 Name: pat_kungthrw 
   MAP: #: 228 Name: map_kungthrw 
    ROOT: 77 FINE: 123
   VAG: #: 225 Name: vag_kungthrw File: kungthrw.vag
    VAG SIZE: 12368
    VAG RATE: 15624
    VAG POS: 2064384
    VAG LCD FILEPOS: 95328 mod: 1120
 SEQUENCE: #: 279 Name: ST_LAO_HAT_HIT 
  PATCH: #: 229 Name: pat_sawhit 
   MAP: #: 229 Name: map_sawhit 
    ROOT: 77 FINE: 123
   VAG: #: 226 Name: vag_sawhit File: sawhit.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 2078720
    VAG LCD FILEPOS: 107696 mod: 1200
 SEQUENCE: #: 280 Name: ST_LAO_HAT_BLK 
  PATCH: #: 229 Name: pat_sawhit 
   MAP: #: 229 Name: map_sawhit 
    ROOT: 77 FINE: 123
   VAG: #: 226 Name: vag_sawhit File: sawhit.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 2078720
 SEQUENCE: #: 281 Name: ST_LAO_TELE 
  PATCH: #: 96 Name: pat_robtele 
   MAP: #: 96 Name: map_robtele 
    ROOT: 77 FINE: 123
   VAG: #: 116 Name: vag_robtele File: robtele.vag
    VAG SIZE: 10080
    VAG RATE: 15624
    VAG POS: 958464
    VAG LCD FILEPOS: 113712 mod: 1072
 SEQUENCE: #: 282 Name: ST_LAO_THROW 
  PATCH: #: 230 Name: pat_dfklthro 
   MAP: #: 230 Name: map_dfklthro 
    ROOT: 77 FINE: 123
   VAG: #: 73 Name: vag_dfklthro File: dfklthro.vag
    VAG SIZE: 9168
    VAG RATE: 15624
    VAG POS: 624640
    VAG LCD FILEPOS: 123792 mod: 912
LOADLIST----TOTAL: Name: KUNGLAO_FIGHT_SNDS File: kunglao.lcd Size: 130912

LOADLIST: Name: TUSK_FIGHT_SNDS File: tusk.lcd
 SEQUENCE: #: 1 Name: GS_DEATH_VP 
  PATCH: #: 67 Name: pat_vpdeath 
   MAP: #: 67 Name: map_vpdeath 
    ROOT: 82 FINE: 5
   VAG: #: 51 Name: vag_vpdeath File: vpdeath.vag
    VAG SIZE: 22656
    VAG RATE: 11000
    VAG POS: 440320
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 10 Name: GS_SHOOK_VP 
  PATCH: #: 68 Name: pat_vpnotbut 
   MAP: #: 68 Name: map_vpnotbut 
    ROOT: 82 FINE: 5
   VAG: #: 52 Name: vag_vpnotbut File: vpnotbut.vag
    VAG SIZE: 17536
    VAG RATE: 11000
    VAG POS: 464896
    VAG LCD FILEPOS: 24704 mod: 128
 SEQUENCE: #: 19 Name: GS_RUN_VP 
  PATCH: #: 69 Name: pat_vprun 
   MAP: #: 69 Name: map_vprun 
    ROOT: 77 FINE: 123
   VAG: #: 53 Name: vag_vprun File: vprun.vag
    VAG SIZE: 11568
    VAG RATE: 15624
    VAG POS: 483328
    VAG LCD FILEPOS: 42240 mod: 1280
 SEQUENCE: #: 28 Name: GS_TV_VP 
  PATCH: #: 70 Name: pat_vpgrab 
   MAP: #: 70 Name: map_vpgrab 
    ROOT: 77 FINE: 123
   VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
    VAG SIZE: 2864
    VAG RATE: 15624
    VAG POS: 495616
    VAG LCD FILEPOS: 53808 mod: 560
 SEQUENCE: #: 38 Name: GS_FHV_VP1 
  PATCH: #: 71 Name: pat_vpface1 
   MAP: #: 71 Name: map_vpface1 
    ROOT: 77 FINE: 123
   VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 499712
    VAG LCD FILEPOS: 56672 mod: 1376
 SEQUENCE: #: 39 Name: GS_FHV_VP2 
  PATCH: #: 71 Name: pat_vpface1 
   MAP: #: 71 Name: map_vpface1 
    ROOT: 77 FINE: 123
   VAG: #: 55 Name: vag_vpface1 File: vpface1.vag
    VAG SIZE: 4720
    VAG RATE: 15624
    VAG POS: 499712
 SEQUENCE: #: 40 Name: GS_FHV_VP3 
  PATCH: #: 72 Name: pat_vpface2 
   MAP: #: 72 Name: map_vpface2 
    ROOT: 77 FINE: 123
   VAG: #: 56 Name: vag_vpface2 File: vpface2.vag
    VAG SIZE: 5200
    VAG RATE: 15624
    VAG POS: 505856
    VAG LCD FILEPOS: 61392 mod: 2000
 SEQUENCE: #: 64 Name: GS_ATK_VP1 
  PATCH: #: 73 Name: pat_vpatt1 
   MAP: #: 73 Name: map_vpatt1 
    ROOT: 77 FINE: 123
   VAG: #: 57 Name: vag_vpatt1 File: vpatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 512000
    VAG LCD FILEPOS: 66592 mod: 1056
 SEQUENCE: #: 65 Name: GS_ATK_VP2 
  PATCH: #: 74 Name: pat_vpatt2 
   MAP: #: 74 Name: map_vpatt2 
    ROOT: 77 FINE: 123
   VAG: #: 58 Name: vag_vpatt2 File: vpatt2.vag
    VAG SIZE: 3136
    VAG RATE: 15624
    VAG POS: 516096
    VAG LCD FILEPOS: 69600 mod: 2016
 SEQUENCE: #: 79 Name: GS_JUMP_VP 
  PATCH: #: 75 Name: pat_vpjump 
   MAP: #: 75 Name: map_vpjump 
    ROOT: 77 FINE: 123
   VAG: #: 59 Name: vag_vpjump File: vpjump.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 520192
    VAG LCD FILEPOS: 72736 mod: 1056
 SEQUENCE: #: 87 Name: GS_GRAB_VP 
  PATCH: #: 70 Name: pat_vpgrab 
   MAP: #: 70 Name: map_vpgrab 
    ROOT: 77 FINE: 123
   VAG: #: 54 Name: vag_vpgrab File: vpgrab.vag
    VAG SIZE: 2864
    VAG RATE: 15624
    VAG POS: 495616
 SEQUENCE: #: 96 Name: GS_SLAM_VP 
  PATCH: #: 76 Name: pat_vpthrow 
   MAP: #: 76 Name: map_vpthrow 
    ROOT: 77 FINE: 123
   VAG: #: 60 Name: vag_vpthrow File: vpthrow.vag
    VAG SIZE: 8560
    VAG RATE: 15624
    VAG POS: 524288
    VAG LCD FILEPOS: 75952 mod: 176
 SEQUENCE: #: 106 Name: GS_WST_VP1 
  PATCH: #: 77 Name: pat_vpwast2 
   MAP: #: 77 Name: map_vpwast2 
    ROOT: 81 FINE: 105
   VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
    VAG SIZE: 7728
    VAG RATE: 12500
    VAG POS: 534528
    VAG LCD FILEPOS: 84512 mod: 544
 SEQUENCE: #: 107 Name: GS_WST_VP2 
  PATCH: #: 77 Name: pat_vpwast2 
   MAP: #: 77 Name: map_vpwast2 
    ROOT: 81 FINE: 105
   VAG: #: 61 Name: vag_vpwast2 File: vpwast2.vag
    VAG SIZE: 7728
    VAG RATE: 12500
    VAG POS: 534528
 SEQUENCE: #: 286 Name: ST_TUSK_ZAP 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
    VAG LCD FILEPOS: 92240 mod: 80
 SEQUENCE: #: 287 Name: ST_TUSK_CANNON 
  PATCH: #: 79 Name: pat_cabalgun 
   MAP: #: 79 Name: map_cabalgun 
    ROOT: 77 FINE: 123
   VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
    VAG SIZE: 13568
    VAG RATE: 15624
    VAG POS: 1161216
    VAG LCD FILEPOS: 97648 mod: 1392
 SEQUENCE: #: 288 Name: ST_TUSK_BLURR 
  PATCH: #: 217 Name: pat_eblrun 
   MAP: #: 217 Name: map_eblrun 
    ROOT: 77 FINE: 123
   VAG: #: 76 Name: vag_eblrun File: eblrun.vag
    VAG SIZE: 11648
    VAG RATE: 15624
    VAG POS: 669696
    VAG LCD FILEPOS: 111216 mod: 624
 SEQUENCE: #: 289 Name: ST_TUSK_FLIP1 
  PATCH: #: 80 Name: pat_hicwhsh 
   MAP: #: 80 Name: map_hicwhsh 
    ROOT: 77 FINE: 123
   VAG: #: 141 Name: vag_hicwhsh File: hicwhsh.vag
    VAG SIZE: 1968
    VAG RATE: 15624
    VAG POS: 1175552
    VAG LCD FILEPOS: 122864 mod: 2032
 SEQUENCE: #: 290 Name: ST_TUSK_FLIP2 
  PATCH: #: 81 Name: pat_locwhsh 
   MAP: #: 81 Name: map_locwhsh 
    ROOT: 81 FINE: 105
   VAG: #: 142 Name: vag_locwhsh File: locwhsh.vag
    VAG SIZE: 2352
    VAG RATE: 12500
    VAG POS: 1177600
    VAG LCD FILEPOS: 124832 mod: 1952
 SEQUENCE: #: 291 Name: ST_TUSK_SAW 
  PATCH: #: 229 Name: pat_sawhit 
   MAP: #: 229 Name: map_sawhit 
    ROOT: 77 FINE: 123
   VAG: #: 226 Name: vag_sawhit File: sawhit.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 2078720
    VAG LCD FILEPOS: 127184 mod: 208
 SEQUENCE: #: 292 Name: ST_TUSK_REFLECT 
  PATCH: #: 111 Name: pat_salrflc2 
   MAP: #: 111 Name: map_salrflc2 
    ROOT: 81 FINE: 105
   VAG: #: 152 Name: vag_salrflc2 File: salrflc2.vag
    VAG SIZE: 5872
    VAG RATE: 12500
    VAG POS: 1255424
    VAG LCD FILEPOS: 133200 mod: 80
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 139072 mod: 1856
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 142368 mod: 1056
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: TUSK_FIGHT_SNDS File: tusk.lcd Size: 143664

LOADLIST: Name: SHANG_FIGHT_SNDS File: shang.lcd
 SEQUENCE: #: 3 Name: GS_DEATH_EB 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 12 Name: GS_SHOOK_EB 
  PATCH: #: 97 Name: pat_ebshook 
   MAP: #: 97 Name: map_ebshook 
    ROOT: 81 FINE: 105
   VAG: #: 75 Name: vag_ebshook File: ebshook.vag
    VAG SIZE: 12560
    VAG RATE: 12500
    VAG POS: 655360
    VAG LCD FILEPOS: 18048 mod: 1664
 SEQUENCE: #: 21 Name: GS_RUN_EB 
  PATCH: #: 217 Name: pat_eblrun 
   MAP: #: 217 Name: map_eblrun 
    ROOT: 77 FINE: 123
   VAG: #: 76 Name: vag_eblrun File: eblrun.vag
    VAG SIZE: 11648
    VAG RATE: 15624
    VAG POS: 669696
    VAG LCD FILEPOS: 30608 mod: 1936
 SEQUENCE: #: 30 Name: GS_TV_EB 
  PATCH: #: 218 Name: pat_ebtrip 
   MAP: #: 218 Name: map_ebtrip 
    ROOT: 77 FINE: 123
   VAG: #: 77 Name: vag_ebtrip File: ebtrip.vag
    VAG SIZE: 3072
    VAG RATE: 15624
    VAG POS: 681984
    VAG LCD FILEPOS: 42256 mod: 1296
 SEQUENCE: #: 44 Name: GS_FHV_EB1 
  PATCH: #: 219 Name: pat_eblface1 
   MAP: #: 219 Name: map_eblface1 
    ROOT: 77 FINE: 123
   VAG: #: 78 Name: vag_eblface1 File: eblface1.vag
    VAG SIZE: 2464
    VAG RATE: 15624
    VAG POS: 686080
    VAG LCD FILEPOS: 45328 mod: 272
 SEQUENCE: #: 45 Name: GS_FHV_EB2 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
    VAG LCD FILEPOS: 47792 mod: 688
 SEQUENCE: #: 46 Name: GS_FHV_EB3 
  PATCH: #: 192 Name: pat_eblface2 
   MAP: #: 192 Name: map_eblface2 
    ROOT: 77 FINE: 123
   VAG: #: 79 Name: vag_eblface2 File: eblface2.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 690176
 SEQUENCE: #: 68 Name: GS_ATK_EB1 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
    VAG LCD FILEPOS: 52304 mod: 1104
 SEQUENCE: #: 69 Name: GS_ATK_EB2 
  PATCH: #: 220 Name: pat_eblatt1 
   MAP: #: 220 Name: map_eblatt1 
    ROOT: 77 FINE: 123
   VAG: #: 80 Name: vag_eblatt1 File: eblatt1.vag
    VAG SIZE: 3008
    VAG RATE: 15624
    VAG POS: 696320
 SEQUENCE: #: 81 Name: GS_JUMP_EB 
  PATCH: #: 222 Name: pat_ebljump 
   MAP: #: 222 Name: map_ebljump 
    ROOT: 77 FINE: 123
   VAG: #: 82 Name: vag_ebljump File: ebljump.vag
    VAG SIZE: 2320
    VAG RATE: 15624
    VAG POS: 704512
    VAG LCD FILEPOS: 55312 mod: 16
 SEQUENCE: #: 89 Name: GS_GRAB_EB 
  PATCH: #: 223 Name: pat_eblgrab 
   MAP: #: 223 Name: map_eblgrab 
    ROOT: 77 FINE: 123
   VAG: #: 83 Name: vag_eblgrab File: eblgrab.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 708608
    VAG LCD FILEPOS: 57632 mod: 288
 SEQUENCE: #: 98 Name: GS_SLAM_EB 
  PATCH: #: 224 Name: pat_eblthrow 
   MAP: #: 224 Name: map_eblthrow 
    ROOT: 77 FINE: 123
   VAG: #: 84 Name: vag_eblthrow File: eblthrow.vag
    VAG SIZE: 7600
    VAG RATE: 15624
    VAG POS: 712704
    VAG LCD FILEPOS: 60848 mod: 1456
 SEQUENCE: #: 110 Name: GS_WST_EB1 
  PATCH: #: 225 Name: pat_eblwast1 
   MAP: #: 225 Name: map_eblwast1 
    ROOT: 77 FINE: 123
   VAG: #: 85 Name: vag_eblwast1 File: eblwast1.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 720896
    VAG LCD FILEPOS: 68448 mod: 864
 SEQUENCE: #: 111 Name: GS_WST_EB2 
  PATCH: #: 226 Name: pat_eblwast2 
   MAP: #: 226 Name: map_eblwast2 
    ROOT: 81 FINE: 105
   VAG: #: 86 Name: vag_eblwast2 File: eblwast2.vag
    VAG SIZE: 7568
    VAG RATE: 12500
    VAG POS: 731136
    VAG LCD FILEPOS: 78576 mod: 752
 SEQUENCE: #: 304 Name: ST_ST_CANNON 
  PATCH: #: 145 Name: pat_jaxcann 
   MAP: #: 145 Name: map_jaxcann 
    ROOT: 77 FINE: 123
   VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
    VAG SIZE: 12880
    VAG RATE: 15624
    VAG POS: 1347584
    VAG LCD FILEPOS: 86144 mod: 128
 SEQUENCE: #: 305 Name: ST_ST_SUMMON 
  PATCH: #: 146 Name: pat_stfireup 
   MAP: #: 146 Name: map_stfireup 
    ROOT: 77 FINE: 123
   VAG: #: 165 Name: vag_stfireup File: stfireup.vag
    VAG SIZE: 11072
    VAG RATE: 15624
    VAG POS: 1361920
    VAG LCD FILEPOS: 99024 mod: 720
 SEQUENCE: #: 306 Name: ST_ST_FIRE_HIT 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
    VAG LCD FILEPOS: 110096 mod: 1552
 SEQUENCE: #: 307 Name: ST_ST_SKULL 
  PATCH: #: 147 Name: pat_sonforce 
   MAP: #: 147 Name: map_sonforce 
    ROOT: 77 FINE: 123
   VAG: #: 166 Name: vag_sonforce File: sonforce.vag
    VAG SIZE: 9856
    VAG RATE: 15624
    VAG POS: 1374208
    VAG LCD FILEPOS: 115504 mod: 816
 SEQUENCE: #: 308 Name: ST_ST_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 125360 mod: 432
 SEQUENCE: #: 309 Name: ST_ST_FLIP 
  PATCH: #: 136 Name: pat_gudfall4 
   MAP: #: 136 Name: map_gudfall4 
    ROOT: 81 FINE: 105
   VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
    VAG SIZE: 4608
    VAG RATE: 12500
    VAG POS: 1341440
    VAG LCD FILEPOS: 132432 mod: 1360
 SEQUENCE: #: 310 Name: ST_ST_FIRE_BLK 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
LOADLIST----TOTAL: Name: SHANG_FIGHT_SNDS File: shang.lcd Size: 134992

LOADLIST: Name: KANG_FIGHT_SNDS File: liukang.lcd
 SEQUENCE: #: 6 Name: GS_DEATH_KANG 
  PATCH: #: 29 Name: pat_lkdeath 
   MAP: #: 29 Name: map_lkdeath 
    ROOT: 82 FINE: 5
   VAG: #: 122 Name: vag_lkdeath File: lkdeath.vag
    VAG SIZE: 22192
    VAG RATE: 11000
    VAG POS: 1005568
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 15 Name: GS_SHOOK_KANG 
  PATCH: #: 30 Name: pat_lkrun 
   MAP: #: 30 Name: map_lkrun 
    ROOT: 81 FINE: 105
   VAG: #: 123 Name: vag_lkrun File: lkrun.vag
    VAG SIZE: 10592
    VAG RATE: 12500
    VAG POS: 1028096
    VAG LCD FILEPOS: 24240 mod: 1712
 SEQUENCE: #: 24 Name: GS_RUN_KANG 
  PATCH: #: 30 Name: pat_lkrun 
   MAP: #: 30 Name: map_lkrun 
    ROOT: 81 FINE: 105
   VAG: #: 123 Name: vag_lkrun File: lkrun.vag
    VAG SIZE: 10592
    VAG RATE: 12500
    VAG POS: 1028096
 SEQUENCE: #: 32 Name: GS_TV_KANG 
  PATCH: #: 31 Name: pat_lktrip 
   MAP: #: 31 Name: map_lktrip 
    ROOT: 77 FINE: 123
   VAG: #: 124 Name: vag_lktrip File: lktrip.vag
    VAG SIZE: 4368
    VAG RATE: 15624
    VAG POS: 1040384
    VAG LCD FILEPOS: 34832 mod: 16
 SEQUENCE: #: 53 Name: GS_FHV_KANG1 
  PATCH: #: 32 Name: pat_lkface1 
   MAP: #: 32 Name: map_lkface1 
    ROOT: 77 FINE: 123
   VAG: #: 125 Name: vag_lkface1 File: lkface1.vag
    VAG SIZE: 4656
    VAG RATE: 15624
    VAG POS: 1046528
    VAG LCD FILEPOS: 39200 mod: 288
 SEQUENCE: #: 54 Name: GS_FHV_KANG2 
  PATCH: #: 32 Name: pat_lkface1 
   MAP: #: 32 Name: map_lkface1 
    ROOT: 77 FINE: 123
   VAG: #: 125 Name: vag_lkface1 File: lkface1.vag
    VAG SIZE: 4656
    VAG RATE: 15624
    VAG POS: 1046528
 SEQUENCE: #: 55 Name: GS_FHV_KANG3 
  PATCH: #: 33 Name: pat_lkface2 
   MAP: #: 33 Name: map_lkface2 
    ROOT: 77 FINE: 123
   VAG: #: 126 Name: vag_lkface2 File: lkface2.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 1052672
    VAG LCD FILEPOS: 43856 mod: 848
 SEQUENCE: #: 72 Name: GS_ATK_KANG1 
  PATCH: #: 34 Name: pat_lkatt1 
   MAP: #: 34 Name: map_lkatt1 
    ROOT: 77 FINE: 123
   VAG: #: 127 Name: vag_lkatt1 File: lkatt1.vag
    VAG SIZE: 2736
    VAG RATE: 15624
    VAG POS: 1056768
    VAG LCD FILEPOS: 47072 mod: 2016
 SEQUENCE: #: 73 Name: GS_ATK_KANG2 
  PATCH: #: 35 Name: pat_lkatt2 
   MAP: #: 35 Name: map_lkatt2 
    ROOT: 77 FINE: 123
   VAG: #: 128 Name: vag_lkatt2 File: lkatt2.vag
    VAG SIZE: 3824
    VAG RATE: 15624
    VAG POS: 1060864
    VAG LCD FILEPOS: 49808 mod: 656
 SEQUENCE: #: 83 Name: GS_JUMP_KANG 
  PATCH: #: 36 Name: pat_lkjump 
   MAP: #: 36 Name: map_lkjump 
    ROOT: 77 FINE: 123
   VAG: #: 129 Name: vag_lkjump File: lkjump.vag
    VAG SIZE: 3280
    VAG RATE: 15624
    VAG POS: 1064960
    VAG LCD FILEPOS: 53632 mod: 384
 SEQUENCE: #: 92 Name: GS_GRAB_KANG 
  PATCH: #: 37 Name: pat_lkgrab 
   MAP: #: 37 Name: map_lkgrab 
    ROOT: 77 FINE: 123
   VAG: #: 130 Name: vag_lkgrab File: lkgrab.vag
    VAG SIZE: 1904
    VAG RATE: 15624
    VAG POS: 1069056
    VAG LCD FILEPOS: 56912 mod: 1616
 SEQUENCE: #: 101 Name: GS_SLAM_KANG 
  PATCH: #: 38 Name: pat_lkthrow 
   MAP: #: 38 Name: map_lkthrow 
    ROOT: 77 FINE: 123
   VAG: #: 131 Name: vag_lkthrow File: lkthrow.vag
    VAG SIZE: 6640
    VAG RATE: 15624
    VAG POS: 1071104
    VAG LCD FILEPOS: 58816 mod: 1472
 SEQUENCE: #: 116 Name: GS_WST_KANG1 
  PATCH: #: 39 Name: pat_lkwast1 
   MAP: #: 39 Name: map_lkwast1 
    ROOT: 82 FINE: 5
   VAG: #: 132 Name: vag_lkwast1 File: lkwast1.vag
    VAG SIZE: 14592
    VAG RATE: 11000
    VAG POS: 1079296
    VAG LCD FILEPOS: 65456 mod: 1968
 SEQUENCE: #: 117 Name: GS_WST_KANG2 
  PATCH: #: 256 Name: pat_lkwast2 
   MAP: #: 256 Name: map_lkwast2 
    ROOT: 77 FINE: 123
   VAG: #: 247 Name: vag_lkwast2 File: lkwast2.vag
    VAG SIZE: 17328
    VAG RATE: 15624
    VAG POS: 2236416
    VAG LCD FILEPOS: 80048 mod: 176
 SEQUENCE: #: 314 Name: ST_LK_FIRE 
  PATCH: #: 40 Name: pat_lkfball 
   MAP: #: 40 Name: map_lkfball 
    ROOT: 77 FINE: 123
   VAG: #: 133 Name: vag_lkfball File: lkfball.vag
    VAG SIZE: 8768
    VAG RATE: 15624
    VAG POS: 1095680
    VAG LCD FILEPOS: 97376 mod: 1120
 SEQUENCE: #: 315 Name: ST_LK_FIRE_HIT 
  PATCH: #: 41 Name: pat_axhit 
   MAP: #: 41 Name: map_axhit 
    ROOT: 77 FINE: 123
   VAG: #: 137 Name: vag_axhit File: axhit.vag
    VAG SIZE: 7536
    VAG RATE: 15624
    VAG POS: 1136640
    VAG LCD FILEPOS: 106144 mod: 1696
 SEQUENCE: #: 316 Name: ST_LK_SUPER_KICK 
  PATCH: #: 42 Name: pat_sonyafly 
   MAP: #: 42 Name: map_sonyafly 
    ROOT: 77 FINE: 123
   VAG: #: 138 Name: vag_sonyafly File: sonyafly.vag
    VAG SIZE: 8288
    VAG RATE: 15624
    VAG POS: 1144832
    VAG LCD FILEPOS: 113680 mod: 1040
 SEQUENCE: #: 317 Name: ST_LK_FIRE_BLK 
  PATCH: #: 41 Name: pat_axhit 
   MAP: #: 41 Name: map_axhit 
    ROOT: 77 FINE: 123
   VAG: #: 137 Name: vag_axhit File: axhit.vag
    VAG SIZE: 7536
    VAG RATE: 15624
    VAG POS: 1136640
 SEQUENCE: #: 318 Name: ST_LK_BIKE 
  PATCH: #: 43 Name: pat_lkcycle 
   MAP: #: 43 Name: map_lkcycle 
    ROOT: 81 FINE: 105
   VAG: #: 134 Name: vag_lkcycle File: lkcycle.vag
    VAG SIZE: 11024
    VAG RATE: 12500
    VAG POS: 1105920
    VAG LCD FILEPOS: 121968 mod: 1136
 SEQUENCE: #: 319 Name: ST_LK_FLY_YELL 
  PATCH: #: 44 Name: pat_lkflyink 
   MAP: #: 44 Name: map_lkflyink 
    ROOT: 81 FINE: 105
   VAG: #: 135 Name: vag_lkflyink File: lkflyink.vag
    VAG SIZE: 12240
    VAG RATE: 12500
    VAG POS: 1118208
    VAG LCD FILEPOS: 132992 mod: 1920
LOADLIST----TOTAL: Name: KANG_FIGHT_SNDS File: liukang.lcd Size: 143184

LOADLIST: Name: MOTARO_FIGHT_SNDS File: motaro.lcd
 SEQUENCE: #: 323 Name: ST_MOT_EFF1 
  PATCH: #: 211 Name: pat_centaur1 
   MAP: #: 211 Name: map_centaur1 
    ROOT: 81 FINE: 105
   VAG: #: 220 Name: vag_centaur1 File: centaur1.vag
    VAG SIZE: 10640
    VAG RATE: 12500
    VAG POS: 2013184
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 324 Name: ST_MOT_EFF2 
  PATCH: #: 212 Name: pat_centaur2 
   MAP: #: 212 Name: map_centaur2 
    ROOT: 81 FINE: 105
   VAG: #: 221 Name: vag_centaur2 File: centaur2.vag
    VAG SIZE: 12784
    VAG RATE: 12500
    VAG POS: 2025472
    VAG LCD FILEPOS: 12688 mod: 400
 SEQUENCE: #: 325 Name: ST_MOT_EFF3 
  PATCH: #: 213 Name: pat_hboss1 
   MAP: #: 213 Name: map_hboss1 
    ROOT: 81 FINE: 105
   VAG: #: 222 Name: vag_hboss1 File: hboss1.vag
    VAG SIZE: 8192
    VAG RATE: 12500
    VAG POS: 2039808
    VAG LCD FILEPOS: 25472 mod: 896
 SEQUENCE: #: 326 Name: ST_MOT_HIT1 
  PATCH: #: 214 Name: pat_hhit1 
   MAP: #: 214 Name: map_hhit1 
    ROOT: 81 FINE: 105
   VAG: #: 223 Name: vag_hhit1 File: hhit1.vag
    VAG SIZE: 4672
    VAG RATE: 12500
    VAG POS: 2048000
    VAG LCD FILEPOS: 33664 mod: 896
 SEQUENCE: #: 327 Name: ST_MOT_HIT2 
  PATCH: #: 215 Name: pat_hhit2 
   MAP: #: 215 Name: map_hhit2 
    ROOT: 81 FINE: 105
   VAG: #: 224 Name: vag_hhit2 File: hhit2.vag
    VAG SIZE: 8400
    VAG RATE: 12500
    VAG POS: 2054144
    VAG LCD FILEPOS: 38336 mod: 1472
 SEQUENCE: #: 328 Name: ST_MOT_FIRE 
  PATCH: #: 79 Name: pat_cabalgun 
   MAP: #: 79 Name: map_cabalgun 
    ROOT: 77 FINE: 123
   VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
    VAG SIZE: 13568
    VAG RATE: 15624
    VAG POS: 1161216
    VAG LCD FILEPOS: 46736 mod: 1680
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 60304 mod: 912
LOADLIST----TOTAL: Name: MOTARO_FIGHT_SNDS File: motaro.lcd Size: 65328

LOADLIST: Name: SHAOKAHN_FIGHT_SNDS File: skfight.lcd
 SEQUENCE: #: 8 Name: GS_DEATH_SK 
  PATCH: #: 260 Name: pat_skwast2 
   MAP: #: 260 Name: map_skwast2 
    ROOT: 77 FINE: 123
   VAG: #: 251 Name: vag_skwast2 File: skwast2.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 2289664
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 17 Name: GS_SHOOK_SK 
  PATCH: #: 259 Name: pat_skwast1 
   MAP: #: 259 Name: map_skwast1 
    ROOT: 77 FINE: 123
   VAG: #: 250 Name: vag_skwast1 File: skwast1.vag
    VAG SIZE: 9040
    VAG RATE: 15624
    VAG POS: 2279424
    VAG LCD FILEPOS: 12176 mod: 1936
 SEQUENCE: #: 26 Name: GS_RUN_SK 
  PATCH: #: 241 Name: pat_amrun 
   MAP: #: 241 Name: map_amrun 
    ROOT: 77 FINE: 123
   VAG: #: 232 Name: vag_amrun File: amrun.vag
    VAG SIZE: 10720
    VAG RATE: 15624
    VAG POS: 2142208
    VAG LCD FILEPOS: 21216 mod: 736
 SEQUENCE: #: 34 Name: GS_TV_SK 
  PATCH: #: 246 Name: pat_skreact1 
   MAP: #: 246 Name: map_skreact1 
    ROOT: 77 FINE: 123
   VAG: #: 237 Name: vag_skreact1 File: skreact1.vag
    VAG SIZE: 4368
    VAG RATE: 15624
    VAG POS: 2174976
    VAG LCD FILEPOS: 31936 mod: 1216
 SEQUENCE: #: 59 Name: GS_FHV_SK1 
  PATCH: #: 246 Name: pat_skreact1 
   MAP: #: 246 Name: map_skreact1 
    ROOT: 77 FINE: 123
   VAG: #: 237 Name: vag_skreact1 File: skreact1.vag
    VAG SIZE: 4368
    VAG RATE: 15624
    VAG POS: 2174976
 SEQUENCE: #: 60 Name: GS_FHV_SK2 
  PATCH: #: 247 Name: pat_skreact2 
   MAP: #: 247 Name: map_skreact2 
    ROOT: 77 FINE: 123
   VAG: #: 238 Name: vag_skreact2 File: skreact2.vag
    VAG SIZE: 5136
    VAG RATE: 15624
    VAG POS: 2181120
    VAG LCD FILEPOS: 36304 mod: 1488
 SEQUENCE: #: 61 Name: GS_FHV_SK3 
  PATCH: #: 248 Name: pat_skreact3 
   MAP: #: 248 Name: map_skreact3 
    ROOT: 77 FINE: 123
   VAG: #: 239 Name: vag_skreact3 File: skreact3.vag
    VAG SIZE: 6368
    VAG RATE: 15624
    VAG POS: 2187264
    VAG LCD FILEPOS: 41440 mod: 480
 SEQUENCE: #: 76 Name: GS_ATK_SK1 
  PATCH: #: 252 Name: pat_skatt1 
   MAP: #: 252 Name: map_skatt1 
    ROOT: 77 FINE: 123
   VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
    VAG SIZE: 5744
    VAG RATE: 15624
    VAG POS: 2207744
    VAG LCD FILEPOS: 47808 mod: 704
 SEQUENCE: #: 77 Name: GS_ATK_SK2 
  PATCH: #: 253 Name: pat_skatt2 
   MAP: #: 253 Name: map_skatt2 
    ROOT: 77 FINE: 123
   VAG: #: 244 Name: vag_skatt2 File: skatt2.vag
    VAG SIZE: 6160
    VAG RATE: 15624
    VAG POS: 2213888
    VAG LCD FILEPOS: 53552 mod: 304
 SEQUENCE: #: 85 Name: GS_JUMP_SK 
  PATCH: #: 253 Name: pat_skatt2 
   MAP: #: 253 Name: map_skatt2 
    ROOT: 77 FINE: 123
   VAG: #: 244 Name: vag_skatt2 File: skatt2.vag
    VAG SIZE: 6160
    VAG RATE: 15624
    VAG POS: 2213888
 SEQUENCE: #: 94 Name: GS_GRAB_SK 
  PATCH: #: 252 Name: pat_skatt1 
   MAP: #: 252 Name: map_skatt1 
    ROOT: 77 FINE: 123
   VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
    VAG SIZE: 5744
    VAG RATE: 15624
    VAG POS: 2207744
 SEQUENCE: #: 103 Name: GS_SLAM_SK 
  PATCH: #: 252 Name: pat_skatt1 
   MAP: #: 252 Name: map_skatt1 
    ROOT: 77 FINE: 123
   VAG: #: 243 Name: vag_skatt1 File: skatt1.vag
    VAG SIZE: 5744
    VAG RATE: 15624
    VAG POS: 2207744
 SEQUENCE: #: 120 Name: GS_WST_SK1 
  PATCH: #: 259 Name: pat_skwast1 
   MAP: #: 259 Name: map_skwast1 
    ROOT: 77 FINE: 123
   VAG: #: 250 Name: vag_skwast1 File: skwast1.vag
    VAG SIZE: 9040
    VAG RATE: 15624
    VAG POS: 2279424
 SEQUENCE: #: 121 Name: GS_WST_SK2 
  PATCH: #: 260 Name: pat_skwast2 
   MAP: #: 260 Name: map_skwast2 
    ROOT: 77 FINE: 123
   VAG: #: 251 Name: vag_skwast2 File: skwast2.vag
    VAG SIZE: 10128
    VAG RATE: 15624
    VAG POS: 2289664
 SEQUENCE: #: 329 Name: ST_SK_SHOULDER 
  PATCH: #: 112 Name: pat_salrush 
   MAP: #: 112 Name: map_salrush 
    ROOT: 81 FINE: 105
   VAG: #: 153 Name: vag_salrush File: salrush.vag
    VAG SIZE: 5376
    VAG RATE: 12500
    VAG POS: 1261568
    VAG LCD FILEPOS: 59712 mod: 320
 SEQUENCE: #: 330 Name: ST_SK_PHOTON 
  PATCH: #: 79 Name: pat_cabalgun 
   MAP: #: 79 Name: map_cabalgun 
    ROOT: 77 FINE: 123
   VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
    VAG SIZE: 13568
    VAG RATE: 15624
    VAG POS: 1161216
    VAG LCD FILEPOS: 65088 mod: 1600
 SEQUENCE: #: 331 Name: ST_SK_KLANG1 
  PATCH: #: 288 Name: pat_mhit1 
   MAP: #: 288 Name: map_mhit1 
    ROOT: 81 FINE: 105
   VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
    VAG SIZE: 3184
    VAG RATE: 12500
    VAG POS: 2527232
    VAG LCD FILEPOS: 78656 mod: 832
 SEQUENCE: #: 332 Name: ST_SK_KLANG2 
  PATCH: #: 289 Name: pat_mhit2 
   MAP: #: 289 Name: map_mhit2 
    ROOT: 81 FINE: 105
   VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
    VAG SIZE: 3568
    VAG RATE: 12500
    VAG POS: 2531328
    VAG LCD FILEPOS: 81840 mod: 1968
 SEQUENCE: #: 334 Name: ST_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 85408 mod: 1440
 SEQUENCE: #: 335 Name: ST_SK_LAUGH3 
  PATCH: #: 186 Name: pat_sklaff3 
   MAP: #: 186 Name: map_sklaff3 
    ROOT: 82 FINE: 5
   VAG: #: 205 Name: vag_sklaff3 File: sklaff3.vag
    VAG SIZE: 17248
    VAG RATE: 11000
    VAG POS: 1835008
    VAG LCD FILEPOS: 99952 mod: 1648
 SEQUENCE: #: 337 Name: ST_SK_NEVER_WIN 
  PATCH: #: 190 Name: pat_sknvrwin 
   MAP: #: 190 Name: map_sknvrwin 
    ROOT: 77 FINE: 123
   VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
    VAG SIZE: 18112
    VAG RATE: 15624
    VAG POS: 1865728
    VAG LCD FILEPOS: 117200 mod: 464
 SEQUENCE: #: 427 Name: TS_DEATH_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 135312 mod: 144
LOADLIST----TOTAL: Name: SHAOKAHN_FIGHT_SNDS File: skfight.lcd Size: 144768

LOADLIST: Name: KANO_BABEFRIEND_SNDS File: bkano.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: KANO_BABEFRIEND_SNDS File: bkano.lcd Size: 40992

LOADLIST: Name: KANO_ANIMALITY_SNDS File: akano.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
LOADLIST----TOTAL: Name: KANO_ANIMALITY_SNDS File: akano.lcd Size: 65600

LOADLIST: Name: KANO_FATAL_SNDS File: fkano.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 172 Name: FT_KANO_LASER 
  PATCH: #: 209 Name: pat_storb 
   MAP: #: 209 Name: map_storb 
    ROOT: 77 FINE: 123
   VAG: #: 218 Name: vag_storb File: storb.vag
    VAG SIZE: 9184
    VAG RATE: 15624
    VAG POS: 1988608
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 173 Name: FT_KANO_LASER_BURN 
  PATCH: #: 210 Name: pat_kanlasr1 
   MAP: #: 210 Name: map_kanlasr1 
    ROOT: 77 FINE: 123
   VAG: #: 219 Name: vag_kanlasr1 File: kanlasr1.vag
    VAG SIZE: 12528
    VAG RATE: 15624
    VAG POS: 1998848
    VAG LCD FILEPOS: 38640 mod: 1776
 SEQUENCE: #: 174 Name: FT_KANO_SKELETON_RIP 
  PATCH: #: 110 Name: pat_crunch2 
   MAP: #: 110 Name: map_crunch2 
    ROOT: 81 FINE: 105
   VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
    VAG SIZE: 4656
    VAG RATE: 12500
    VAG POS: 1249280
    VAG LCD FILEPOS: 51168 mod: 2016
 SEQUENCE: #: 175 Name: FT_KANO_SKIN_FALL 
  PATCH: #: 136 Name: pat_gudfall4 
   MAP: #: 136 Name: map_gudfall4 
    ROOT: 81 FINE: 105
   VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
    VAG SIZE: 4608
    VAG RATE: 12500
    VAG POS: 1341440
    VAG LCD FILEPOS: 55824 mod: 528
LOADLIST----TOTAL: Name: KANO_FATAL_SNDS File: fkano.lcd Size: 58384

LOADLIST: Name: SONYA_BABEFRIEND_SNDS File: bsonya.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: SONYA_BABEFRIEND_SNDS File: bsonya.lcd Size: 40992

LOADLIST: Name: SONYA_ANIMALITY_SNDS File: asonya.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
LOADLIST----TOTAL: Name: SONYA_ANIMALITY_SNDS File: asonya.lcd Size: 65600

LOADLIST: Name: SONYA_FATAL_SNDS File: fsonya.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 182 Name: FT_SONYA_BLOW_KISS 
  PATCH: #: 26 Name: pat_mfirebal 
   MAP: #: 26 Name: map_mfirebal 
    ROOT: 77 FINE: 123
   VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
    VAG SIZE: 11296
    VAG RATE: 15624
    VAG POS: 204800
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 183 Name: FT_SONYA_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 40752 mod: 1840
 SEQUENCE: #: 383 Name: TS_BURNING 
  PATCH: #: 27 Name: pat_ignite 
   MAP: #: 27 Name: map_ignite 
    ROOT: 77 FINE: 123
   VAG: #: 22 Name: vag_ignite File: ignite.vag
    VAG SIZE: 23776
    VAG RATE: 15624
    VAG POS: 217088
    VAG LCD FILEPOS: 52256 mod: 1056
 SEQUENCE: #: 384 Name: TS_BONES 
  PATCH: #: 28 Name: pat_bones 
   MAP: #: 28 Name: map_bones 
    ROOT: 77 FINE: 123
   VAG: #: 23 Name: vag_bones File: bones.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 241664
    VAG LCD FILEPOS: 76032 mod: 256
LOADLIST----TOTAL: Name: SONYA_FATAL_SNDS File: fsonya.lcd Size: 78768

LOADLIST: Name: JAX_BABEFRIEND_SNDS File: bjax.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: JAX_BABEFRIEND_SNDS File: bjax.lcd Size: 40992

LOADLIST: Name: JAX_ANIMALITY_SNDS File: ajax.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
LOADLIST----TOTAL: Name: JAX_ANIMALITY_SNDS File: ajax.lcd Size: 65600

LOADLIST: Name: JAX_FATAL_SNDS File: fjax.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 193 Name: FT_JAX_ARM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 194 Name: FT_JAX_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 36528 mod: 1712
 SEQUENCE: #: 195 Name: FT_JAX_CRUNCH2 
  PATCH: #: 110 Name: pat_crunch2 
   MAP: #: 110 Name: map_crunch2 
    ROOT: 81 FINE: 105
   VAG: #: 151 Name: vag_crunch2 File: crunch2.vag
    VAG SIZE: 4656
    VAG RATE: 12500
    VAG POS: 1249280
    VAG LCD FILEPOS: 40752 mod: 1840
 SEQUENCE: #: 196 Name: FT_JAX_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 45408 mod: 352
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 56912 mod: 1616
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 60208 mod: 816
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: JAX_FATAL_SNDS File: fjax.lcd Size: 61504

LOADLIST: Name: SUBZERO_BABEFRIEND_SNDS File: bsubzero.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
 SEQUENCE: #: 197 Name: ST_SZ_ICE_UP 
  PATCH: #: 193 Name: pat_sziceup 
   MAP: #: 193 Name: map_sziceup 
    ROOT: 77 FINE: 123
   VAG: #: 209 Name: vag_sziceup File: sziceup.vag
    VAG SIZE: 11856
    VAG RATE: 15624
    VAG POS: 1900544
    VAG LCD FILEPOS: 43040 mod: 32
LOADLIST----TOTAL: Name: SUBZERO_BABEFRIEND_SNDS File: bsubzero.lcd Size: 52848

LOADLIST: Name: SUBZERO_ANIMALITY_SNDS File: asubzero.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 414 Name: TS_ANIM_ROAR 
  PATCH: #: 311 Name: pat_anmlroar 
   MAP: #: 311 Name: map_anmlroar 
    ROOT: 77 FINE: 123
   VAG: #: 302 Name: vag_anmlroar File: anmlroar.vag
    VAG SIZE: 12848
    VAG RATE: 15624
    VAG POS: 2859008
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 70800 mod: 1168
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 75024 mod: 1296
LOADLIST----TOTAL: Name: SUBZERO_ANIMALITY_SNDS File: asubzero.lcd Size: 78448

LOADLIST: Name: SUBZERO_FATAL_SNDS File: fsubzero.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 33680 mod: 912
 SEQUENCE: #: 204 Name: FT_SZ_BLOW 
  PATCH: #: 26 Name: pat_mfirebal 
   MAP: #: 26 Name: map_mfirebal 
    ROOT: 77 FINE: 123
   VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
    VAG SIZE: 11296
    VAG RATE: 15624
    VAG POS: 204800
    VAG LCD FILEPOS: 39152 mod: 240
 SEQUENCE: #: 205 Name: FT_SZ_FROZE 
  PATCH: #: 271 Name: pat_robpres2 
   MAP: #: 271 Name: map_robpres2 
    ROOT: 77 FINE: 123
   VAG: #: 262 Name: vag_robpres2 File: robpres2.vag
    VAG SIZE: 9728
    VAG RATE: 15624
    VAG POS: 2363392
    VAG LCD FILEPOS: 50448 mod: 1296
 SEQUENCE: #: 427 Name: TS_DEATH_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 60176 mod: 784
LOADLIST----TOTAL: Name: SUBZERO_FATAL_SNDS File: fsubzero.lcd Size: 69632

LOADLIST: Name: SWAT_BABEFRIEND_SNDS File: bswat.lcd
 SEQUENCE: #: 214 Name: FT_SWAT_WHISTLE 
  PATCH: #: 268 Name: pat_whistle 
   MAP: #: 268 Name: map_whistle 
    ROOT: 77 FINE: 123
   VAG: #: 259 Name: vag_whistle File: whistle.vag
    VAG SIZE: 8016
    VAG RATE: 15624
    VAG POS: 2332672
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 10064 mod: 1872
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 24720 mod: 144
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 51056 mod: 1904
LOADLIST----TOTAL: Name: SWAT_BABEFRIEND_SNDS File: bswat.lcd Size: 50976

LOADLIST: Name: SWAT_ANIMALITY_SNDS File: aswat.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 414 Name: TS_ANIM_ROAR 
  PATCH: #: 311 Name: pat_anmlroar 
   MAP: #: 311 Name: map_anmlroar 
    ROOT: 77 FINE: 123
   VAG: #: 302 Name: vag_anmlroar File: anmlroar.vag
    VAG SIZE: 12848
    VAG RATE: 15624
    VAG POS: 2859008
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 70800 mod: 1168
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 74096 mod: 368
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 77440 mod: 1664
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 79408 mod: 1584
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 83632 mod: 1712
LOADLIST----TOTAL: Name: SWAT_ANIMALITY_SNDS File: aswat.lcd Size: 87056

LOADLIST: Name: SWAT_FATAL_SNDS File: fswat.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 212 Name: FT_SWAT_TASER 
  PATCH: #: 25 Name: pat_kanlasrl 
   MAP: #: 25 Name: map_kanlasrl 
    ROOT: 77 FINE: 123
   VAG: #: 20 Name: vag_kanlasrl File: kanlasrl.vag
    VAG SIZE: 6224
    VAG RATE: 15624
    VAG POS: 196608
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 213 Name: FT_SWAT_TASER_FIRE 
  PATCH: #: 61 Name: pat_robxpld2 
   MAP: #: 61 Name: map_robxpld2 
    ROOT: 77 FINE: 123
   VAG: #: 108 Name: vag_robxpld2 File: robxpld2.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 894976
    VAG LCD FILEPOS: 35680 mod: 864
 SEQUENCE: #: 381 Name: TS_SHOCK 
  PATCH: #: 25 Name: pat_kanlasrl 
   MAP: #: 25 Name: map_kanlasrl 
    ROOT: 77 FINE: 123
   VAG: #: 20 Name: vag_kanlasrl File: kanlasrl.vag
    VAG SIZE: 6224
    VAG RATE: 15624
    VAG POS: 196608
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 43408 mod: 400
LOADLIST----TOTAL: Name: SWAT_FATAL_SNDS File: fswat.lcd Size: 43328

LOADLIST: Name: INDIAN_BABEFRIEND_SNDS File: bindian.lcd
 SEQUENCE: #: 220 Name: ST_IND_ARROW_BLK 
  PATCH: #: 108 Name: pat_arrowhit 
   MAP: #: 108 Name: map_arrowhit 
    ROOT: 81 FINE: 105
   VAG: #: 149 Name: vag_arrowhit File: arrowhit.vag
    VAG SIZE: 3792
    VAG RATE: 12500
    VAG POS: 1236992
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 308 Name: ST_ST_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 5840 mod: 1744
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 12912 mod: 624
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 27568 mod: 944
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 53904 mod: 656
LOADLIST----TOTAL: Name: INDIAN_BABEFRIEND_SNDS File: bindian.lcd Size: 61840

LOADLIST: Name: INDIAN_ANIMALITY_SNDS File: aindian.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 67648 mod: 64
LOADLIST----TOTAL: Name: INDIAN_ANIMALITY_SNDS File: aindian.lcd Size: 75584

LOADLIST: Name: INDIAN_FATAL_SNDS File: findian.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 226 Name: FT_IND_LIGHT_START 
  PATCH: #: 114 Name: pat_litestrt 
   MAP: #: 114 Name: map_litestrt 
    ROOT: 81 FINE: 105
   VAG: #: 155 Name: vag_litestrt File: litestrt.vag
    VAG SIZE: 4832
    VAG RATE: 12500
    VAG POS: 1275904
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 227 Name: FT_IND_LIGHT_LOOP 
  PATCH: #: 115 Name: pat_liteloop 
   MAP: #: 115 Name: map_liteloop 
    ROOT: 81 FINE: 105
   VAG: #: 156 Name: vag_liteloop File: liteloop.vag
    VAG SIZE: 9552
    VAG RATE: 12500
    VAG POS: 1282048
    VAG LCD FILEPOS: 34288 mod: 1520
 SEQUENCE: #: 228 Name: FT_IND_LIGHT_END 
  PATCH: #: 116 Name: pat_liteend 
   MAP: #: 116 Name: map_liteend 
    ROOT: 81 FINE: 105
   VAG: #: 157 Name: vag_liteend File: liteend.vag
    VAG SIZE: 6192
    VAG RATE: 12500
    VAG POS: 1292288
    VAG LCD FILEPOS: 43840 mod: 832
 SEQUENCE: #: 229 Name: FT_IND_LIGHT_HIT_AXE 
  PATCH: #: 117 Name: pat_strtboom 
   MAP: #: 117 Name: map_strtboom 
    ROOT: 81 FINE: 105
   VAG: #: 158 Name: vag_strtboom File: strtboom.vag
    VAG SIZE: 13488
    VAG RATE: 12500
    VAG POS: 1300480
    VAG LCD FILEPOS: 50032 mod: 880
 SEQUENCE: #: 230 Name: FT_IND_LIGHT_HIT_HIM 
  PATCH: #: 118 Name: pat_strtbum2 
   MAP: #: 118 Name: map_strtbum2 
    ROOT: 81 FINE: 105
   VAG: #: 159 Name: vag_strtbum2 File: strtbum2.vag
    VAG SIZE: 13104
    VAG RATE: 12500
    VAG POS: 1314816
    VAG LCD FILEPOS: 63520 mod: 32
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 76624 mod: 848
LOADLIST----TOTAL: Name: INDIAN_FATAL_SNDS File: findian.lcd Size: 84560

LOADLIST: Name: LIA_BABEFRIEND_SNDS File: blia.lcd
 SEQUENCE: #: 240 Name: FT_LIA_TWFUN 
  PATCH: #: 269 Name: pat_pbfun 
   MAP: #: 269 Name: map_pbfun 
    ROOT: 81 FINE: 105
   VAG: #: 260 Name: vag_pbfun File: pbfun.vag
    VAG SIZE: 11296
    VAG RATE: 12500
    VAG POS: 2340864
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 13344 mod: 1056
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 28000 mod: 1376
LOADLIST----TOTAL: Name: LIA_BABEFRIEND_SNDS File: blia.lcd Size: 52288

LOADLIST: Name: LIA_ANIMALITY_SNDS File: alia.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: LIA_ANIMALITY_SNDS File: alia.lcd Size: 62544

LOADLIST: Name: LIA_FATAL_SNDS File: flia.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 33680 mod: 912
 SEQUENCE: #: 238 Name: FT_LIA_HAIR 
  PATCH: #: 284 Name: pat_whirl1 
   MAP: #: 284 Name: map_whirl1 
    ROOT: 77 FINE: 123
   VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
    VAG SIZE: 14448
    VAG RATE: 15624
    VAG POS: 2486272
    VAG LCD FILEPOS: 39152 mod: 240
 SEQUENCE: #: 239 Name: FT_LIA_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 53600 mod: 352
 SEQUENCE: #: 232 Name: ST_LIA_SCREAM 
  PATCH: #: 6 Name: pat_scrmtrp2 
   MAP: #: 6 Name: map_scrmtrp2 
    ROOT: 77 FINE: 123
   VAG: #: 37 Name: vag_scrmtrp2 File: scrmtrp2.vag
    VAG SIZE: 16176
    VAG RATE: 15624
    VAG POS: 313344
    VAG LCD FILEPOS: 65104 mod: 1616
 SEQUENCE: #: 418 Name: TS_NASTY_GOO 
  PATCH: #: 300 Name: pat_spinerip 
   MAP: #: 300 Name: map_spinerip 
    ROOT: 77 FINE: 123
   VAG: #: 291 Name: vag_spinerip File: spinerip.vag
    VAG SIZE: 10000
    VAG RATE: 15624
    VAG POS: 2686976
    VAG LCD FILEPOS: 81280 mod: 1408
LOADLIST----TOTAL: Name: LIA_FATAL_SNDS File: flia.lcd Size: 89232

LOADLIST: Name: ROBO1_BABEFRIEND_SNDS File: brobo1.lcd
 SEQUENCE: #: 275 Name: FT_ROBO_SLIDE 
  PATCH: #: 278 Name: pat_slide 
   MAP: #: 278 Name: map_slide 
    ROOT: 77 FINE: 123
   VAG: #: 269 Name: vag_slide File: slide.vag
    VAG SIZE: 5232
    VAG RATE: 15624
    VAG POS: 2433024
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 7280 mod: 1136
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 21936 mod: 1456
LOADLIST----TOTAL: Name: ROBO1_BABEFRIEND_SNDS File: brobo1.lcd Size: 46224

LOADLIST: Name: ROBO1_ANIMALITY_SNDS File: arobo1.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
 SEQUENCE: #: 274 Name: FT_ROBO_BAT 
  PATCH: #: 277 Name: pat_bird 
   MAP: #: 277 Name: map_bird 
    ROOT: 77 FINE: 123
   VAG: #: 268 Name: vag_bird File: bird.vag
    VAG SIZE: 11024
    VAG RATE: 15624
    VAG POS: 2420736
    VAG LCD FILEPOS: 64592 mod: 1104
LOADLIST----TOTAL: Name: ROBO1_ANIMALITY_SNDS File: arobo1.lcd Size: 73568

LOADLIST: Name: ROBO1_FATAL_SNDS File: frobo1.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 382 Name: TS_FLAME 
  PATCH: #: 26 Name: pat_mfirebal 
   MAP: #: 26 Name: map_mfirebal 
    ROOT: 77 FINE: 123
   VAG: #: 21 Name: vag_mfirebal File: mfirebal.vag
    VAG SIZE: 11296
    VAG RATE: 15624
    VAG POS: 204800
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 383 Name: TS_BURNING 
  PATCH: #: 27 Name: pat_ignite 
   MAP: #: 27 Name: map_ignite 
    ROOT: 77 FINE: 123
   VAG: #: 22 Name: vag_ignite File: ignite.vag
    VAG SIZE: 23776
    VAG RATE: 15624
    VAG POS: 217088
    VAG LCD FILEPOS: 40752 mod: 1840
 SEQUENCE: #: 384 Name: TS_BONES 
  PATCH: #: 28 Name: pat_bones 
   MAP: #: 28 Name: map_bones 
    ROOT: 77 FINE: 123
   VAG: #: 23 Name: vag_bones File: bones.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 241664
    VAG LCD FILEPOS: 64528 mod: 1040
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 69312 mod: 1728
 SEQUENCE: #: 265 Name: FT_ROBO_CRSH_OUT 
  PATCH: #: 270 Name: pat_robpres1 
   MAP: #: 270 Name: map_robpres1 
    ROOT: 81 FINE: 105
   VAG: #: 261 Name: vag_robpres1 File: robpres1.vag
    VAG SIZE: 8400
    VAG RATE: 12500
    VAG POS: 2353152
    VAG LCD FILEPOS: 73552 mod: 1872
 SEQUENCE: #: 266 Name: FT_ROBO_CRSH_DOWN 
  PATCH: #: 271 Name: pat_robpres2 
   MAP: #: 271 Name: map_robpres2 
    ROOT: 77 FINE: 123
   VAG: #: 262 Name: vag_robpres2 File: robpres2.vag
    VAG SIZE: 9728
    VAG RATE: 15624
    VAG POS: 2363392
    VAG LCD FILEPOS: 81952 mod: 32
 SEQUENCE: #: 267 Name: FT_ROBO_CRSH_SMOOSH1 
  PATCH: #: 272 Name: pat_bodsmsh1 
   MAP: #: 272 Name: map_bodsmsh1 
    ROOT: 77 FINE: 123
   VAG: #: 263 Name: vag_bodsmsh1 File: bodsmsh1.vag
    VAG SIZE: 9248
    VAG RATE: 15624
    VAG POS: 2373632
    VAG LCD FILEPOS: 91680 mod: 1568
 SEQUENCE: #: 268 Name: FT_ROBO_CRSH_SMOOSH2 
  PATCH: #: 273 Name: pat_scorthr1 
   MAP: #: 273 Name: map_scorthr1 
    ROOT: 77 FINE: 123
   VAG: #: 264 Name: vag_scorthr1 File: scorthr1.vag
    VAG SIZE: 15696
    VAG RATE: 15624
    VAG POS: 2383872
    VAG LCD FILEPOS: 100928 mod: 576
LOADLIST----TOTAL: Name: ROBO1_FATAL_SNDS File: frobo1.lcd Size: 114576

LOADLIST: Name: ROBO2_BABEFRIEND_SNDS File: brobo2.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: ROBO2_BABEFRIEND_SNDS File: brobo2.lcd Size: 40992

LOADLIST: Name: ROBO2_ANIMALITY_SNDS File: arobo2.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 414 Name: TS_ANIM_ROAR 
  PATCH: #: 311 Name: pat_anmlroar 
   MAP: #: 311 Name: map_anmlroar 
    ROOT: 77 FINE: 123
   VAG: #: 302 Name: vag_anmlroar File: anmlroar.vag
    VAG SIZE: 12848
    VAG RATE: 15624
    VAG POS: 2859008
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 70800 mod: 1168
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 75024 mod: 1296
LOADLIST----TOTAL: Name: ROBO2_ANIMALITY_SNDS File: arobo2.lcd Size: 78448

LOADLIST: Name: ROBO2_FATAL_SNDS File: frobo2.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 269 Name: FT_ROBO_BEEP 
  PATCH: #: 274 Name: pat_beep1 
   MAP: #: 274 Name: map_beep1 
    ROOT: 77 FINE: 123
   VAG: #: 265 Name: vag_beep1 File: beep1.vag
    VAG SIZE: 2048
    VAG RATE: 15624
    VAG POS: 2400256
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 270 Name: FT_ROBO_SELF_DESTRUCT 
  PATCH: #: 275 Name: pat_compfx1 
   MAP: #: 275 Name: map_compfx1 
    ROOT: 77 FINE: 123
   VAG: #: 266 Name: vag_compfx1 File: compfx1.vag
    VAG SIZE: 11920
    VAG RATE: 15624
    VAG POS: 2402304
    VAG LCD FILEPOS: 31504 mod: 784
 SEQUENCE: #: 271 Name: FT_ROBO_DESTRUCT_LAFF 
  PATCH: #: 276 Name: pat_roblaff 
   MAP: #: 276 Name: map_roblaff 
    ROOT: 77 FINE: 123
   VAG: #: 267 Name: vag_roblaff File: roblaff.vag
    VAG SIZE: 5520
    VAG RATE: 15624
    VAG POS: 2414592
    VAG LCD FILEPOS: 43424 mod: 416
 SEQUENCE: #: 272 Name: FT_ROBO_WHIRL 
  PATCH: #: 284 Name: pat_whirl1 
   MAP: #: 284 Name: map_whirl1 
    ROOT: 77 FINE: 123
   VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
    VAG SIZE: 14448
    VAG RATE: 15624
    VAG POS: 2486272
    VAG LCD FILEPOS: 48944 mod: 1840
 SEQUENCE: #: 273 Name: FT_ROBO_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 63392 mod: 1952
 SEQUENCE: #: 255 Name: ST_ROBO_IMPLODE 
  PATCH: #: 62 Name: pat_robimpld 
   MAP: #: 62 Name: map_robimpld 
    ROOT: 77 FINE: 123
   VAG: #: 109 Name: vag_robimpld File: robimpld.vag
    VAG SIZE: 4512
    VAG RATE: 15624
    VAG POS: 903168
    VAG LCD FILEPOS: 74896 mod: 1168
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 79408 mod: 1584
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 82704 mod: 784
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: ROBO2_FATAL_SNDS File: frobo2.lcd Size: 84000

LOADLIST: Name: SMOKE_BABEFRIEND_SNDS File: bsmoke.lcd
 SEQUENCE: #: 276 Name: FT_ROBO_HORN 
  PATCH: #: 279 Name: pat_horn 
   MAP: #: 279 Name: map_horn 
    ROOT: 77 FINE: 123
   VAG: #: 270 Name: vag_horn File: horn.vag
    VAG SIZE: 10000
    VAG RATE: 15624
    VAG POS: 2439168
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 12048 mod: 1808
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 16288 mod: 1952
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 30944 mod: 224
LOADLIST----TOTAL: Name: SMOKE_BABEFRIEND_SNDS File: bsmoke.lcd Size: 55232

LOADLIST: Name: SMOKE_ANIMALITY_SNDS File: asmoke.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: SMOKE_ANIMALITY_SNDS File: asmoke.lcd Size: 62544

LOADLIST: Name: SMOKE_FATAL_SNDS File: fsmoke.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 252 Name: ST_ROBO_CHEST_OPEN 
  PATCH: #: 59 Name: pat_robclik 
   MAP: #: 59 Name: map_robclik 
    ROOT: 77 FINE: 123
   VAG: #: 106 Name: vag_robclik File: robclik.vag
    VAG SIZE: 4240
    VAG RATE: 15624
    VAG POS: 886784
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 241 Name: ST_ROBO_ARM_OUT 
  PATCH: #: 235 Name: pat_roboout 
   MAP: #: 235 Name: map_roboout 
    ROOT: 77 FINE: 123
   VAG: #: 117 Name: vag_roboout File: roboout.vag
    VAG SIZE: 3776
    VAG RATE: 15624
    VAG POS: 968704
    VAG LCD FILEPOS: 33696 mod: 928
 SEQUENCE: #: 242 Name: ST_ROBO_ARM_THROW 
  PATCH: #: 236 Name: pat_robothrw 
   MAP: #: 236 Name: map_robothrw 
    ROOT: 77 FINE: 123
   VAG: #: 118 Name: vag_robothrw File: robothrw.vag
    VAG SIZE: 6432
    VAG RATE: 15624
    VAG POS: 972800
    VAG LCD FILEPOS: 37472 mod: 608
 SEQUENCE: #: 243 Name: ST_ROBO_ARM_IN 
  PATCH: #: 237 Name: pat_roboin 
   MAP: #: 237 Name: map_roboin 
    ROOT: 77 FINE: 123
   VAG: #: 119 Name: vag_roboin File: roboin.vag
    VAG SIZE: 2736
    VAG RATE: 15624
    VAG POS: 980992
    VAG LCD FILEPOS: 43904 mod: 896
 SEQUENCE: #: 249 Name: ST_ROBO_CLANG1 
  PATCH: #: 56 Name: pat_robball1 
   MAP: #: 56 Name: map_robball1 
    ROOT: 77 FINE: 123
   VAG: #: 103 Name: vag_robball1 File: robball1.vag
    VAG SIZE: 3408
    VAG RATE: 15624
    VAG POS: 862208
    VAG LCD FILEPOS: 46640 mod: 1584
 SEQUENCE: #: 250 Name: ST_ROBO_CLANG2 
  PATCH: #: 57 Name: pat_robball2 
   MAP: #: 57 Name: map_robball2 
    ROOT: 77 FINE: 123
   VAG: #: 104 Name: vag_robball2 File: robball2.vag
    VAG SIZE: 3344
    VAG RATE: 15624
    VAG POS: 866304
    VAG LCD FILEPOS: 50048 mod: 896
 SEQUENCE: #: 251 Name: ST_ROBO_BALL_BOOM 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 53392 mod: 144
 SEQUENCE: #: 425 Name: TS_FINAL_DEATH 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 68048 mod: 464
LOADLIST----TOTAL: Name: SMOKE_FATAL_SNDS File: fsmoke.lcd Size: 77504

LOADLIST: Name: KUNGLAO_BABEFRIEND_SNDS File: bkunglao.lcd
 SEQUENCE: #: 428 Name: TS_DOG 
  PATCH: #: 306 Name: pat_dogyip 
   MAP: #: 306 Name: map_dogyip 
    ROOT: 77 FINE: 123
   VAG: #: 297 Name: vag_dogyip File: dogyip.vag
    VAG SIZE: 3616
    VAG RATE: 15624
    VAG POS: 2789376
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 429 Name: TS_DOG_OUCH 
  PATCH: #: 307 Name: pat_dogwhine 
   MAP: #: 307 Name: map_dogwhine 
    ROOT: 77 FINE: 123
   VAG: #: 298 Name: vag_dogwhine File: dogwhine.vag
    VAG SIZE: 13568
    VAG RATE: 15624
    VAG POS: 2793472
    VAG LCD FILEPOS: 5664 mod: 1568
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 19232 mod: 800
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 33888 mod: 1120
LOADLIST----TOTAL: Name: KUNGLAO_BABEFRIEND_SNDS File: bkunglao.lcd Size: 58176

LOADLIST: Name: KUNGLAO_ANIMALITY_SNDS File: akunglao.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
LOADLIST----TOTAL: Name: KUNGLAO_ANIMALITY_SNDS File: akunglao.lcd Size: 65600

LOADLIST: Name: KUNGLAO_FATAL_SNDS File: fkunglao.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 33680 mod: 912
 SEQUENCE: #: 278 Name: ST_LAO_HAT_THROW 
  PATCH: #: 228 Name: pat_kungthrw 
   MAP: #: 228 Name: map_kungthrw 
    ROOT: 77 FINE: 123
   VAG: #: 225 Name: vag_kungthrw File: kungthrw.vag
    VAG SIZE: 12368
    VAG RATE: 15624
    VAG POS: 2064384
    VAG LCD FILEPOS: 39152 mod: 240
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 51520 mod: 320
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 54816 mod: 1568
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
 SEQUENCE: #: 283 Name: FT_LAO_TORNADO 
  PATCH: #: 284 Name: pat_whirl1 
   MAP: #: 284 Name: map_whirl1 
    ROOT: 77 FINE: 123
   VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
    VAG SIZE: 14448
    VAG RATE: 15624
    VAG POS: 2486272
    VAG LCD FILEPOS: 58160 mod: 816
 SEQUENCE: #: 284 Name: FT_LAO_QUAKE 
  PATCH: #: 280 Name: pat_quake1 
   MAP: #: 280 Name: map_quake1 
    ROOT: 77 FINE: 123
   VAG: #: 271 Name: vag_quake1 File: quake1.vag
    VAG SIZE: 11504
    VAG RATE: 15624
    VAG POS: 2449408
    VAG LCD FILEPOS: 72608 mod: 928
 SEQUENCE: #: 285 Name: FT_LAO_TORNADO_HIT 
  PATCH: #: 229 Name: pat_sawhit 
   MAP: #: 229 Name: map_sawhit 
    ROOT: 77 FINE: 123
   VAG: #: 226 Name: vag_sawhit File: sawhit.vag
    VAG SIZE: 6016
    VAG RATE: 15624
    VAG POS: 2078720
    VAG LCD FILEPOS: 84112 mod: 144
LOADLIST----TOTAL: Name: KUNGLAO_FATAL_SNDS File: fkunglao.lcd Size: 88080

LOADLIST: Name: TUSK_BABEFRIEND_SNDS File: btusk.lcd
 SEQUENCE: #: 226 Name: FT_IND_LIGHT_START 
  PATCH: #: 114 Name: pat_litestrt 
   MAP: #: 114 Name: map_litestrt 
    ROOT: 81 FINE: 105
   VAG: #: 155 Name: vag_litestrt File: litestrt.vag
    VAG SIZE: 4832
    VAG RATE: 12500
    VAG POS: 1275904
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 296 Name: FT_TUSK_BURN 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 6880 mod: 736
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 16864 mod: 480
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 31520 mod: 800
LOADLIST----TOTAL: Name: TUSK_BABEFRIEND_SNDS File: btusk.lcd Size: 55808

LOADLIST: Name: TUSK_ANIMALITY_SNDS File: atusk.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: TUSK_ANIMALITY_SNDS File: atusk.lcd Size: 62544

LOADLIST: Name: TUSK_FATAL_SNDS File: ftusk.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 293 Name: FT_TUSK_BUBBLE 
  PATCH: #: 281 Name: pat_bubble1 
   MAP: #: 281 Name: map_bubble1 
    ROOT: 77 FINE: 123
   VAG: #: 272 Name: vag_bubble1 File: bubble1.vag
    VAG SIZE: 3216
    VAG RATE: 15624
    VAG POS: 2461696
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 294 Name: FT_TUSK_GROW 
  PATCH: #: 282 Name: pat_headgrow 
   MAP: #: 282 Name: map_headgrow 
    ROOT: 77 FINE: 123
   VAG: #: 273 Name: vag_headgrow File: headgrow.vag
    VAG SIZE: 5856
    VAG RATE: 15624
    VAG POS: 2465792
    VAG LCD FILEPOS: 32672 mod: 1952
 SEQUENCE: #: 295 Name: FT_TUSK_HEAD_SCREAM 
  PATCH: #: 283 Name: pat_cblscrm 
   MAP: #: 283 Name: map_cblscrm 
    ROOT: 77 FINE: 123
   VAG: #: 274 Name: vag_cblscrm File: cblscrm.vag
    VAG SIZE: 12736
    VAG RATE: 15624
    VAG POS: 2471936
    VAG LCD FILEPOS: 38528 mod: 1664
 SEQUENCE: #: 296 Name: FT_TUSK_BURN 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 51264 mod: 64
LOADLIST----TOTAL: Name: TUSK_FATAL_SNDS File: ftusk.lcd Size: 59200

LOADLIST: Name: SHEEVA_BABEFRIEND_SNDS File: bsheeva.lcd
 SEQUENCE: #: 303 Name: FT_SG_SPIN 
  PATCH: #: 284 Name: pat_whirl1 
   MAP: #: 284 Name: map_whirl1 
    ROOT: 77 FINE: 123
   VAG: #: 275 Name: vag_whirl1 File: whirl1.vag
    VAG SIZE: 14448
    VAG RATE: 15624
    VAG POS: 2486272
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 16496 mod: 112
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 31152 mod: 432
LOADLIST----TOTAL: Name: SHEEVA_BABEFRIEND_SNDS File: bsheeva.lcd Size: 55440

LOADLIST: Name: SHEEVA_ANIMALITY_SNDS File: asheeva.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: SHEEVA_ANIMALITY_SNDS File: asheeva.lcd Size: 62544

LOADLIST: Name: SHEEVA_FATAL_SNDS File: fsheeva.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 302 Name: FT_SG_KLANG 
  PATCH: #: 289 Name: pat_mhit2 
   MAP: #: 289 Name: map_mhit2 
    ROOT: 81 FINE: 105
   VAG: #: 280 Name: vag_mhit2 File: mhit2.vag
    VAG SIZE: 3568
    VAG RATE: 12500
    VAG POS: 2531328
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 418 Name: TS_NASTY_GOO 
  PATCH: #: 300 Name: pat_spinerip 
   MAP: #: 300 Name: map_spinerip 
    ROOT: 77 FINE: 123
   VAG: #: 291 Name: vag_spinerip File: spinerip.vag
    VAG SIZE: 10000
    VAG RATE: 15624
    VAG POS: 2686976
    VAG LCD FILEPOS: 33024 mod: 256
LOADLIST----TOTAL: Name: SHEEVA_FATAL_SNDS File: fsheeva.lcd Size: 40976

LOADLIST: Name: SHANG_BABEFRIEND_SNDS File: bshang.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: SHANG_BABEFRIEND_SNDS File: bshang.lcd Size: 40992

LOADLIST: Name: SHANG_ANIMALITY_SNDS File: ashang.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 408 Name: TS_CRUNCH1 
  PATCH: #: 267 Name: pat_crunch1 
   MAP: #: 267 Name: map_crunch1 
    ROOT: 81 FINE: 105
   VAG: #: 258 Name: vag_crunch1 File: crunch1.vag
    VAG SIZE: 4224
    VAG RATE: 12500
    VAG POS: 2326528
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 409 Name: TS_CRUNCH2 
  PATCH: #: 294 Name: pat_kneebrk 
   MAP: #: 294 Name: map_kneebrk 
    ROOT: 77 FINE: 123
   VAG: #: 285 Name: vag_kneebrk File: kneebrk.vag
    VAG SIZE: 5472
    VAG RATE: 15624
    VAG POS: 2594816
    VAG LCD FILEPOS: 62176 mod: 736
LOADLIST----TOTAL: Name: SHANG_ANIMALITY_SNDS File: ashang.lcd Size: 65600

LOADLIST: Name: SHANG_FATAL_SNDS File: fshang.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 311 Name: FT_ST_SWORD 
  PATCH: #: 285 Name: pat_swords 
   MAP: #: 285 Name: map_swords 
    ROOT: 77 FINE: 123
   VAG: #: 276 Name: vag_swords File: swords.vag
    VAG SIZE: 9792
    VAG RATE: 15624
    VAG POS: 2502656
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 312 Name: FT_ST_CLOUD 
  PATCH: #: 286 Name: pat_stcloud1 
   MAP: #: 286 Name: map_stcloud1 
    ROOT: 77 FINE: 123
   VAG: #: 277 Name: vag_stcloud1 File: stcloud1.vag
    VAG SIZE: 3888
    VAG RATE: 15624
    VAG POS: 2512896
    VAG LCD FILEPOS: 39248 mod: 336
 SEQUENCE: #: 313 Name: FT_ST_BONES 
  PATCH: #: 28 Name: pat_bones 
   MAP: #: 28 Name: map_bones 
    ROOT: 77 FINE: 123
   VAG: #: 23 Name: vag_bones File: bones.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 241664
    VAG LCD FILEPOS: 43136 mod: 128
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 47920 mod: 816
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 51216 mod: 16
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: SHANG_FATAL_SNDS File: fshang.lcd Size: 52512

LOADLIST: Name: LIUKANG_BABEFRIEND_SNDS File: bliukang.lcd
 SEQUENCE: #: 430 Name: TS_BABY_POOF 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 431 Name: TS_BABY_CRY 
  PATCH: #: 308 Name: pat_baby1 
   MAP: #: 308 Name: map_baby1 
    ROOT: 77 FINE: 123
   VAG: #: 299 Name: vag_baby1 File: baby1.vag
    VAG SIZE: 26336
    VAG RATE: 15624
    VAG POS: 2807808
    VAG LCD FILEPOS: 16704 mod: 320
LOADLIST----TOTAL: Name: LIUKANG_BABEFRIEND_SNDS File: bliukang.lcd Size: 40992

LOADLIST: Name: LIUKANG_ANIMALITY_SNDS File: aliukang.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 410 Name: TS_ANIM_SIZZLE 
  PATCH: #: 295 Name: pat_hissmix 
   MAP: #: 295 Name: map_hissmix 
    ROOT: 77 FINE: 123
   VAG: #: 286 Name: vag_hissmix File: hissmix.vag
    VAG SIZE: 13696
    VAG RATE: 15624
    VAG POS: 2600960
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 411 Name: TS_ANIM_SIZZLE1 
  PATCH: #: 296 Name: pat_maul 
   MAP: #: 296 Name: map_maul 
    ROOT: 77 FINE: 123
   VAG: #: 287 Name: vag_maul File: maul.vag
    VAG SIZE: 7728
    VAG RATE: 15624
    VAG POS: 2615296
    VAG LCD FILEPOS: 43152 mod: 144
 SEQUENCE: #: 412 Name: TS_ANIM_MORPH 
  PATCH: #: 148 Name: pat_stmorph 
   MAP: #: 148 Name: map_stmorph 
    ROOT: 77 FINE: 123
   VAG: #: 167 Name: vag_stmorph File: stmorph.vag
    VAG SIZE: 7072
    VAG RATE: 15624
    VAG POS: 1384448
    VAG LCD FILEPOS: 50880 mod: 1728
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 57952 mod: 608
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 61248 mod: 1856
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
LOADLIST----TOTAL: Name: LIUKANG_ANIMALITY_SNDS File: aliukang.lcd Size: 62544

LOADLIST: Name: LIUKANG_FATAL_SNDS File: fliukang.lcd
 SEQUENCE: #: 432 Name: TS_WINS_TONE 
  PATCH: #: 312 Name: pat_winstone 
   MAP: #: 312 Name: map_winstone 
    ROOT: 77 FINE: 123
   VAG: #: 303 Name: vag_winstone File: fatal2.vag
    VAG SIZE: 27408
    VAG RATE: 15624
    VAG POS: 2873344
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 320 Name: FT_LK_FLAME_MORPH 
  PATCH: #: 287 Name: pat_flmorph 
   MAP: #: 287 Name: map_flmorph 
    ROOT: 77 FINE: 123
   VAG: #: 278 Name: vag_flmorph File: flmorph.vag
    VAG SIZE: 9312
    VAG RATE: 15624
    VAG POS: 2516992
    VAG LCD FILEPOS: 29456 mod: 784
 SEQUENCE: #: 321 Name: FT_LK_RUN 
  PATCH: #: 30 Name: pat_lkrun 
   MAP: #: 30 Name: map_lkrun 
    ROOT: 81 FINE: 105
   VAG: #: 123 Name: vag_lkrun File: lkrun.vag
    VAG SIZE: 10592
    VAG RATE: 12500
    VAG POS: 1028096
    VAG LCD FILEPOS: 38768 mod: 1904
 SEQUENCE: #: 322 Name: FT_LK_SLAM 
  PATCH: #: 94 Name: pat_bigbslam 
   MAP: #: 94 Name: map_bigbslam 
    ROOT: 77 FINE: 123
   VAG: #: 144 Name: vag_bigbslam File: bigbslam.vag
    VAG SIZE: 8416
    VAG RATE: 15624
    VAG POS: 1189888
    VAG LCD FILEPOS: 49360 mod: 208
 SEQUENCE: #: 383 Name: TS_BURNING 
  PATCH: #: 27 Name: pat_ignite 
   MAP: #: 27 Name: map_ignite 
    ROOT: 77 FINE: 123
   VAG: #: 22 Name: vag_ignite File: ignite.vag
    VAG SIZE: 23776
    VAG RATE: 15624
    VAG POS: 217088
    VAG LCD FILEPOS: 57776 mod: 432
 SEQUENCE: #: 384 Name: TS_BONES 
  PATCH: #: 28 Name: pat_bones 
   MAP: #: 28 Name: map_bones 
    ROOT: 77 FINE: 123
   VAG: #: 23 Name: vag_bones File: bones.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 241664
    VAG LCD FILEPOS: 81552 mod: 1680
LOADLIST----TOTAL: Name: LIUKANG_FATAL_SNDS File: fliukang.lcd Size: 84288

LOADLIST: Name: SHAOKAHN_SPECIAL_SNDS File: skspcial.lcd
 SEQUENCE: #: 426 Name: TS_SK_DEATH 
  PATCH: #: 305 Name: pat_skdiemix 
   MAP: #: 305 Name: map_skdiemix 
    ROOT: 77 FINE: 123
   VAG: #: 296 Name: vag_skdiemix File: skdiemix.vag
    VAG SIZE: 37424
    VAG RATE: 15624
    VAG POS: 2750464
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 336 Name: ST_SK_PATHETIC 
  PATCH: #: 290 Name: pat_skpathet 
   MAP: #: 290 Name: map_skpathet 
    ROOT: 77 FINE: 123
   VAG: #: 281 Name: vag_skpathet File: skpathet.vag
    VAG SIZE: 20016
    VAG RATE: 15624
    VAG POS: 2535424
    VAG LCD FILEPOS: 39472 mod: 560
 SEQUENCE: #: 339 Name: ST_SK_DONT_LAUGH 
  PATCH: #: 292 Name: pat_skdntlaf 
   MAP: #: 292 Name: map_skdntlaf 
    ROOT: 77 FINE: 123
   VAG: #: 283 Name: vag_skdntlaf File: skdntlaf.vag
    VAG SIZE: 14176
    VAG RATE: 15624
    VAG POS: 2576384
    VAG LCD FILEPOS: 59488 mod: 96
 SEQUENCE: #: 338 Name: ST_SK_STILL_TRYING 
  PATCH: #: 291 Name: pat_sktrying 
   MAP: #: 291 Name: map_sktrying 
    ROOT: 77 FINE: 123
   VAG: #: 282 Name: vag_sktrying File: sktrying.vag
    VAG SIZE: 19200
    VAG RATE: 15624
    VAG POS: 2555904
    VAG LCD FILEPOS: 73664 mod: 1984
 SEQUENCE: #: 446 Name: TUNE_END_PIT 
  PATCH: #: 327 Name: pat_pitend 
   MAP: #: 327 Name: map_pitend 
    ROOT: 77 FINE: 123
   VAG: #: 318 Name: vag_pitend File: finish\pitend.vag
    VAG SIZE: 11984
    VAG RATE: 15624
    VAG POS: 3684352
    VAG LCD FILEPOS: 92864 mod: 704
LOADLIST----TOTAL: Name: SHAOKAHN_SPECIAL_SNDS File: skspcial.lcd Size: 102800

LOADLIST: Name: SHAOKAHN_BANK_SNDS File: skbank.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 440 Name: TUNE_END_BANK 
  PATCH: #: 321 Name: pat_slowend 
   MAP: #: 321 Name: map_slowend 
    ROOT: 77 FINE: 123
   VAG: #: 312 Name: vag_slowend File: finish\slowend.vag
    VAG SIZE: 16656
    VAG RATE: 15624
    VAG POS: 3586048
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_BANK_SNDS File: skbank.lcd Size: 95984

LOADLIST: Name: SHAOKAHN_ROOF_SNDS File: skroof.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 441 Name: TUNE_END_ROOF 
  PATCH: #: 322 Name: pat_coolend 
   MAP: #: 322 Name: map_coolend 
    ROOT: 77 FINE: 123
   VAG: #: 313 Name: vag_coolend File: finish\cooltend.vag
    VAG SIZE: 15344
    VAG RATE: 15624
    VAG POS: 3604480
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_ROOF_SNDS File: skroof.lcd Size: 94672

LOADLIST: Name: SHAOKAHN_SOUL_SNDS File: sksoul.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 443 Name: TUNE_END_SOUL 
  PATCH: #: 324 Name: pat_tonguend 
   MAP: #: 324 Name: map_tonguend 
    ROOT: 77 FINE: 123
   VAG: #: 315 Name: vag_tonguend File: finish\tonguend.vag
    VAG SIZE: 16144
    VAG RATE: 15624
    VAG POS: 3631104
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_SOUL_SNDS File: sksoul.lcd Size: 95472

LOADLIST: Name: SHAOKAHN_CHURCH_SNDS File: skchurch.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 444 Name: TUNE_END_CHURCH 
  PATCH: #: 325 Name: pat_chrchend 
   MAP: #: 325 Name: map_chrchend 
    ROOT: 77 FINE: 123
   VAG: #: 316 Name: vag_chrchend File: finish\chrchend.vag
    VAG SIZE: 19248
    VAG RATE: 15624
    VAG POS: 3647488
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_CHURCH_SNDS File: skchurch.lcd Size: 98576

LOADLIST: Name: SHAOKAHN_GRAVE_SNDS File: skgrave.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 445 Name: TUNE_END_GRAVE 
  PATCH: #: 326 Name: pat_gravend 
   MAP: #: 326 Name: map_gravend 
    ROOT: 77 FINE: 123
   VAG: #: 317 Name: vag_gravend File: finish\gravend.vag
    VAG SIZE: 15120
    VAG RATE: 15624
    VAG POS: 3667968
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_GRAVE_SNDS File: skgrave.lcd Size: 94448

LOADLIST: Name: SHAOKAHN_HID_SNDS File: skhid.lcd
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 16368 mod: 2032
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23856 mod: 1328
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 31488 mod: 768
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 36480 mod: 1664
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51968 mod: 768
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 66832 mod: 1296
 SEQUENCE: #: 447 Name: TUNE_END_FRENZY 
  PATCH: #: 328 Name: pat_frenzye 
   MAP: #: 328 Name: map_frenzye 
    ROOT: 77 FINE: 123
   VAG: #: 319 Name: vag_frenzye File: finish\frenzye.vag
    VAG SIZE: 15056
    VAG RATE: 15624
    VAG POS: 3696640
    VAG LCD FILEPOS: 81376 mod: 1504
LOADLIST----TOTAL: Name: SHAOKAHN_HID_SNDS File: skhid.lcd Size: 94384

LOADLIST: Name: SHAOKAHN_PIT_VOICE_SNDS File: skpit.lcd
 SEQUENCE: #: 406 Name: TS_PIT_MACH1 
  PATCH: #: 293 Name: pat_machmix 
   MAP: #: 293 Name: map_machmix 
    ROOT: 77 FINE: 123
   VAG: #: 284 Name: vag_machmix File: machmix.vag
    VAG SIZE: 3344
    VAG RATE: 15624
    VAG POS: 2590720
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 407 Name: TS_PIT_MACH2 
  PATCH: #: 272 Name: pat_bodsmsh1 
   MAP: #: 272 Name: map_bodsmsh1 
    ROOT: 77 FINE: 123
   VAG: #: 263 Name: vag_bodsmsh1 File: bodsmsh1.vag
    VAG SIZE: 9248
    VAG RATE: 15624
    VAG POS: 2373632
    VAG LCD FILEPOS: 5392 mod: 1296
 SEQUENCE: #: 342 Name: TS_SK_SUBERB 
  PATCH: #: 182 Name: pat_sksuperb 
   MAP: #: 182 Name: map_sksuperb 
    ROOT: 82 FINE: 5
   VAG: #: 201 Name: vag_sksuperb File: sksuperb.vag
    VAG SIZE: 7488
    VAG RATE: 11000
    VAG POS: 1783808
    VAG LCD FILEPOS: 14640 mod: 304
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 22128 mod: 1648
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 29760 mod: 1088
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 34752 mod: 1984
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 50240 mod: 1088
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 65104 mod: 1616
 SEQUENCE: #: 446 Name: TUNE_END_PIT 
  PATCH: #: 327 Name: pat_pitend 
   MAP: #: 327 Name: map_pitend 
    ROOT: 77 FINE: 123
   VAG: #: 318 Name: vag_pitend File: finish\pitend.vag
    VAG SIZE: 11984
    VAG RATE: 15624
    VAG POS: 3684352
    VAG LCD FILEPOS: 79648 mod: 1824
LOADLIST----TOTAL: Name: SHAOKAHN_PIT_VOICE_SNDS File: skpit.lcd Size: 89584

LOADLIST: Name: SHAOKAHN_BELL_VOICE_SNDS File: skbell.lcd
 SEQUENCE: #: 128 Name: RSND_STAB1 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 129 Name: RSND_STAB2 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
    VAG LCD FILEPOS: 5344 mod: 1248
 SEQUENCE: #: 130 Name: RSND_STAB3 
  PATCH: #: 263 Name: pat_stab1 
   MAP: #: 263 Name: map_stab1 
    ROOT: 81 FINE: 105
   VAG: #: 254 Name: vag_stab1 File: stab1.vag
    VAG SIZE: 3296
    VAG RATE: 12500
    VAG POS: 2306048
 SEQUENCE: #: 131 Name: RSND_STAB4 
  PATCH: #: 264 Name: pat_bigstab1 
   MAP: #: 264 Name: map_bigstab1 
    ROOT: 81 FINE: 105
   VAG: #: 255 Name: vag_bigstab1 File: bigstab1.vag
    VAG SIZE: 3344
    VAG RATE: 12500
    VAG POS: 2310144
 SEQUENCE: #: 341 Name: TS_CROWD_OOO 
  PATCH: #: 14 Name: pat_oooh 
   MAP: #: 14 Name: map_oooh 
    ROOT: 77 FINE: 123
   VAG: #: 8 Name: vag_oooh File: oooh.vag
    VAG SIZE: 14320
    VAG RATE: 15624
    VAG POS: 100352
    VAG LCD FILEPOS: 8688 mod: 496
 SEQUENCE: #: 343 Name: TS_SK_WELL_DONE 
  PATCH: #: 183 Name: pat_skweldon 
   MAP: #: 183 Name: map_skweldon 
    ROOT: 82 FINE: 5
   VAG: #: 202 Name: vag_skweldon File: skweldon.vag
    VAG SIZE: 7632
    VAG RATE: 11000
    VAG POS: 1792000
    VAG LCD FILEPOS: 23008 mod: 480
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 30640 mod: 1968
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 35632 mod: 816
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51120 mod: 1968
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 65984 mod: 448
 SEQUENCE: #: 441 Name: TUNE_END_ROOF 
  PATCH: #: 322 Name: pat_coolend 
   MAP: #: 322 Name: map_coolend 
    ROOT: 77 FINE: 123
   VAG: #: 313 Name: vag_coolend File: finish\cooltend.vag
    VAG SIZE: 15344
    VAG RATE: 15624
    VAG POS: 3604480
    VAG LCD FILEPOS: 80528 mod: 656
LOADLIST----TOTAL: Name: SHAOKAHN_BELL_VOICE_SNDS File: skbell.lcd Size: 93824

LOADLIST: Name: SHAOKAHN_TRAIN_VOICE_SNDS File: sktrain.lcd
 SEQUENCE: #: 192 Name: TS_KLANG1 
  PATCH: #: 288 Name: pat_mhit1 
   MAP: #: 288 Name: map_mhit1 
    ROOT: 81 FINE: 105
   VAG: #: 279 Name: vag_mhit1 File: mhit1.vag
    VAG SIZE: 3184
    VAG RATE: 12500
    VAG POS: 2527232
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 422 Name: TS_SUB_APPROACH 
  PATCH: #: 302 Name: pat_subway4 
   MAP: #: 302 Name: map_subway4 
    ROOT: 77 FINE: 123
   VAG: #: 293 Name: vag_subway4 File: subway4.vag
    VAG SIZE: 8976
    VAG RATE: 15624
    VAG POS: 2719744
    VAG LCD FILEPOS: 5232 mod: 1136
 SEQUENCE: #: 423 Name: TS_SUB_STEADY 
  PATCH: #: 303 Name: pat_subway2 
   MAP: #: 303 Name: map_subway2 
    ROOT: 77 FINE: 123
   VAG: #: 294 Name: vag_subway2 File: subway2.vag
    VAG SIZE: 8080
    VAG RATE: 15624
    VAG POS: 2729984
    VAG LCD FILEPOS: 14208 mod: 1920
 SEQUENCE: #: 424 Name: TS_SUB_GOING 
  PATCH: #: 304 Name: pat_subway3 
   MAP: #: 304 Name: map_subway3 
    ROOT: 77 FINE: 123
   VAG: #: 295 Name: vag_subway3 File: subway3.vag
    VAG SIZE: 10336
    VAG RATE: 15624
    VAG POS: 2738176
    VAG LCD FILEPOS: 22288 mod: 1808
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 32624 mod: 1904
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 37616 mod: 752
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 53104 mod: 1904
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 67968 mod: 384
 SEQUENCE: #: 438 Name: TUNE_END_TRAIN 
  PATCH: #: 319 Name: pat_traine1 
   MAP: #: 319 Name: map_traine1 
    ROOT: 77 FINE: 123
   VAG: #: 310 Name: vag_traine1 File: finish\traine1.vag
    VAG SIZE: 17824
    VAG RATE: 15624
    VAG POS: 3555328
    VAG LCD FILEPOS: 82512 mod: 592
LOADLIST----TOTAL: Name: SHAOKAHN_TRAIN_VOICE_SNDS File: sktrain.lcd Size: 98288

LOADLIST: Name: SHAOKAHN_STREET_VOICE_SNDS File: skstreet.lcd
 SEQUENCE: #: 403 Name: TS_WIND 
  PATCH: #: 11 Name: pat_windloop 
   MAP: #: 11 Name: map_windloop 
    ROOT: 81 FINE: 105
   VAG: #: 5 Name: vag_windloop File: windloop.vag
    VAG SIZE: 28496
    VAG RATE: 12500
    VAG POS: 63488
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 30544 mod: 1872
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 35536 mod: 720
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51024 mod: 1872
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 65888 mod: 352
 SEQUENCE: #: 439 Name: TUNE_END_STREET 
  PATCH: #: 320 Name: pat_oldend 
   MAP: #: 320 Name: map_oldend 
    ROOT: 77 FINE: 123
   VAG: #: 311 Name: vag_oldend File: finish\oldend.vag
    VAG SIZE: 11984
    VAG RATE: 15624
    VAG POS: 3573760
    VAG LCD FILEPOS: 80432 mod: 560
LOADLIST----TOTAL: Name: SHAOKAHN_STREET_VOICE_SNDS File: skstreet.lcd Size: 90368

LOADLIST: Name: SHAOKAHN_BRIDGE_VOICE_SNDS File: skbridge.lcd
 SEQUENCE: #: 403 Name: TS_WIND 
  PATCH: #: 11 Name: pat_windloop 
   MAP: #: 11 Name: map_windloop 
    ROOT: 81 FINE: 105
   VAG: #: 5 Name: vag_windloop File: windloop.vag
    VAG SIZE: 28496
    VAG RATE: 12500
    VAG POS: 63488
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 30544 mod: 1872
 SEQUENCE: #: 344 Name: TS_SK_FIN_HIM 
  PATCH: #: 152 Name: pat_skfinhim 
   MAP: #: 152 Name: map_skfinhim 
    ROOT: 77 FINE: 123
   VAG: #: 171 Name: vag_skfinhim File: skfinhim.vag
    VAG SIZE: 15488
    VAG RATE: 15624
    VAG POS: 1421312
    VAG LCD FILEPOS: 35536 mod: 720
 SEQUENCE: #: 345 Name: TS_SK_FIN_HER 
  PATCH: #: 153 Name: pat_skfinher 
   MAP: #: 153 Name: map_skfinher 
    ROOT: 77 FINE: 123
   VAG: #: 172 Name: vag_skfinher File: skfinher.vag
    VAG SIZE: 14864
    VAG RATE: 15624
    VAG POS: 1437696
    VAG LCD FILEPOS: 51024 mod: 1872
 SEQUENCE: #: 400 Name: TS_SK_LAUGH2 
  PATCH: #: 185 Name: pat_sklaff2 
   MAP: #: 185 Name: map_sklaff2 
    ROOT: 84 FINE: 4
   VAG: #: 204 Name: vag_sklaff2 File: sklaff2.vag
    VAG SIZE: 14544
    VAG RATE: 11000
    VAG POS: 1818624
    VAG LCD FILEPOS: 65888 mod: 352
 SEQUENCE: #: 442 Name: TUNE_END_BRIDGE 
  PATCH: #: 323 Name: pat_wackend 
   MAP: #: 323 Name: map_wackend 
    ROOT: 77 FINE: 123
   VAG: #: 314 Name: vag_wackend File: finish\wackend.vag
    VAG SIZE: 9856
    VAG RATE: 15624
    VAG POS: 3620864
    VAG LCD FILEPOS: 80432 mod: 560
LOADLIST----TOTAL: Name: SHAOKAHN_BRIDGE_VOICE_SNDS File: skbridge.lcd Size: 88240

LOADLIST: Name: RAND_SNDS File: rand.lcd
 SEQUENCE: #: 404 Name: TS_WHOOSH1 
  PATCH: #: 12 Name: pat_whoosh1 
   MAP: #: 12 Name: map_whoosh1 
    ROOT: 81 FINE: 104
   VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
    VAG SIZE: 2192
    VAG RATE: 12500
    VAG POS: 92160
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 405 Name: TS_WHOOSH3 
  PATCH: #: 13 Name: pat_whoosh3 
   MAP: #: 13 Name: map_whoosh3 
    ROOT: 81 FINE: 104
   VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
    VAG SIZE: 2688
    VAG RATE: 12500
    VAG POS: 96256
    VAG LCD FILEPOS: 4240 mod: 144
 SEQUENCE: #: 346 Name: TS_RNDHOUSE 
  PATCH: #: 8 Name: pat_rhouse 
   MAP: #: 8 Name: map_rhouse 
    ROOT: 77 FINE: 123
   VAG: #: 2 Name: vag_rhouse File: rhouse.vag
    VAG SIZE: 4784
    VAG RATE: 15624
    VAG POS: 28672
    VAG LCD FILEPOS: 6928 mod: 784
 SEQUENCE: #: 358 Name: TS_P2_PICKED 
  PATCH: #: 18 Name: pat_picked2 
   MAP: #: 18 Name: map_picked2 
    ROOT: 77 FINE: 123
   VAG: #: 13 Name: vag_picked2 File: picked2.vag
    VAG SIZE: 7936
    VAG RATE: 15624
    VAG POS: 151552
    VAG LCD FILEPOS: 11712 mod: 1472
 SEQUENCE: #: 347 Name: TS_CLOCK_TICK 
  PATCH: #: 19 Name: pat_timeout1 
   MAP: #: 19 Name: map_timeout1 
    ROOT: 77 FINE: 123
   VAG: #: 14 Name: vag_timeout1 File: timeout1.vag
    VAG SIZE: 1904
    VAG RATE: 15624
    VAG POS: 159744
    VAG LCD FILEPOS: 19648 mod: 1216
 SEQUENCE: #: 402 Name: TS_DANGER 
  PATCH: #: 309 Name: pat_danger 
   MAP: #: 309 Name: map_danger 
    ROOT: 77 FINE: 123
   VAG: #: 300 Name: vag_danger File: danger.vag
    VAG SIZE: 8768
    VAG RATE: 15624
    VAG POS: 2834432
    VAG LCD FILEPOS: 21552 mod: 1072
 SEQUENCE: #: 348 Name: TS_EXP_ST1 
  PATCH: #: 117 Name: pat_strtboom 
   MAP: #: 117 Name: map_strtboom 
    ROOT: 81 FINE: 105
   VAG: #: 158 Name: vag_strtboom File: strtboom.vag
    VAG SIZE: 13488
    VAG RATE: 12500
    VAG POS: 1300480
    VAG LCD FILEPOS: 30320 mod: 1648
 SEQUENCE: #: 349 Name: TS_EXP_ST2 
  PATCH: #: 117 Name: pat_strtboom 
   MAP: #: 117 Name: map_strtboom 
    ROOT: 81 FINE: 105
   VAG: #: 158 Name: vag_strtboom File: strtboom.vag
    VAG SIZE: 13488
    VAG RATE: 12500
    VAG POS: 1300480
 SEQUENCE: #: 132 Name: RSND_FS1 
  PATCH: #: 119 Name: pat_foot1 
   MAP: #: 119 Name: map_foot1 
    ROOT: 81 FINE: 105
   VAG: #: 24 Name: vag_foot1 File: foot1.vag
    VAG SIZE: 1536
    VAG RATE: 12500
    VAG POS: 247808
    VAG LCD FILEPOS: 43808 mod: 800
 SEQUENCE: #: 133 Name: RSND_FS2 
  PATCH: #: 120 Name: pat_foot2 
   MAP: #: 120 Name: map_foot2 
    ROOT: 81 FINE: 105
   VAG: #: 25 Name: vag_foot2 File: foot2.vag
    VAG SIZE: 1808
    VAG RATE: 12500
    VAG POS: 249856
    VAG LCD FILEPOS: 45344 mod: 288
 SEQUENCE: #: 134 Name: RSND_FS3 
  PATCH: #: 20 Name: pat_foot3 
   MAP: #: 20 Name: map_foot3 
    ROOT: 81 FINE: 105
   VAG: #: 15 Name: vag_foot3 File: foot3.vag
    VAG SIZE: 1696
    VAG RATE: 12500
    VAG POS: 161792
    VAG LCD FILEPOS: 47152 mod: 48
 SEQUENCE: #: 135 Name: RSND_FS4 
  PATCH: #: 121 Name: pat_foot4 
   MAP: #: 121 Name: map_foot4 
    ROOT: 81 FINE: 105
   VAG: #: 26 Name: vag_foot4 File: foot4.vag
    VAG SIZE: 1040
    VAG RATE: 12500
    VAG POS: 251904
    VAG LCD FILEPOS: 48848 mod: 1744
 SEQUENCE: #: 136 Name: RSND_BB1 
  PATCH: #: 122 Name: pat_gudblock 
   MAP: #: 122 Name: map_gudblock 
    ROOT: 81 FINE: 105
   VAG: #: 27 Name: vag_gudblock File: gudblock.vag
    VAG SIZE: 4336
    VAG RATE: 12500
    VAG POS: 253952
    VAG LCD FILEPOS: 49888 mod: 736
 SEQUENCE: #: 137 Name: RSND_BB2 
  PATCH: #: 122 Name: pat_gudblock 
   MAP: #: 122 Name: map_gudblock 
    ROOT: 81 FINE: 105
   VAG: #: 27 Name: vag_gudblock File: gudblock.vag
    VAG SIZE: 4336
    VAG RATE: 12500
    VAG POS: 253952
 SEQUENCE: #: 138 Name: RSND_SB1 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 54224 mod: 976
 SEQUENCE: #: 139 Name: RSND_SB2 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
 SEQUENCE: #: 140 Name: RSND_SM1 
  PATCH: #: 124 Name: pat_gudhit1 
   MAP: #: 124 Name: map_gudhit1 
    ROOT: 81 FINE: 105
   VAG: #: 29 Name: vag_gudhit1 File: gudhit1.vag
    VAG SIZE: 2256
    VAG RATE: 12500
    VAG POS: 262144
    VAG LCD FILEPOS: 56192 mod: 896
 SEQUENCE: #: 141 Name: RSND_SM2 
  PATCH: #: 124 Name: pat_gudhit1 
   MAP: #: 124 Name: map_gudhit1 
    ROOT: 81 FINE: 105
   VAG: #: 29 Name: vag_gudhit1 File: gudhit1.vag
    VAG SIZE: 2256
    VAG RATE: 12500
    VAG POS: 262144
 SEQUENCE: #: 142 Name: RSND_MSM1 
  PATCH: #: 125 Name: pat_newbig1 
   MAP: #: 125 Name: map_newbig1 
    ROOT: 81 FINE: 20
   VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
    VAG SIZE: 4384
    VAG RATE: 12500
    VAG POS: 266240
    VAG LCD FILEPOS: 58448 mod: 1104
 SEQUENCE: #: 143 Name: RSND_MSM2 
  PATCH: #: 126 Name: pat_newbig3 
   MAP: #: 126 Name: map_newbig3 
    ROOT: 81 FINE: 104
   VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
    VAG SIZE: 4384
    VAG RATE: 12500
    VAG POS: 266240
 SEQUENCE: #: 144 Name: RSND_MSM3 
  PATCH: #: 126 Name: pat_newbig3 
   MAP: #: 126 Name: map_newbig3 
    ROOT: 81 FINE: 104
   VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
    VAG SIZE: 4384
    VAG RATE: 12500
    VAG POS: 266240
 SEQUENCE: #: 145 Name: RSND_MSM4 
  PATCH: #: 127 Name: pat_newbig4 
   MAP: #: 127 Name: map_newbig4 
    ROOT: 82 FINE: 40
   VAG: #: 30 Name: vag_newbig3 File: newbig3.vag
    VAG SIZE: 4384
    VAG RATE: 12500
    VAG POS: 266240
 SEQUENCE: #: 148 Name: RSND_BSM1 
  PATCH: #: 128 Name: pat_roldbig1 
   MAP: #: 128 Name: map_roldbig1 
    ROOT: 81 FINE: 104
   VAG: #: 31 Name: vag_roldbig1 File: roldbig1.vag
    VAG SIZE: 5424
    VAG RATE: 12500
    VAG POS: 272384
    VAG LCD FILEPOS: 62832 mod: 1392
 SEQUENCE: #: 149 Name: RSND_BSM2 
  PATCH: #: 129 Name: pat_roldbig2 
   MAP: #: 129 Name: map_roldbig2 
    ROOT: 81 FINE: 20
   VAG: #: 31 Name: vag_roldbig1 File: roldbig1.vag
    VAG SIZE: 5424
    VAG RATE: 12500
    VAG POS: 272384
 SEQUENCE: #: 150 Name: RSND_ROCK1 
  PATCH: #: 130 Name: pat_rock1 
   MAP: #: 130 Name: map_rock1 
    ROOT: 81 FINE: 105
   VAG: #: 32 Name: vag_rock1 File: rock1.vag
    VAG SIZE: 2528
    VAG RATE: 12500
    VAG POS: 278528
    VAG LCD FILEPOS: 68256 mod: 672
 SEQUENCE: #: 151 Name: RSND_ROCK2 
  PATCH: #: 131 Name: pat_rock2 
   MAP: #: 131 Name: map_rock2 
    ROOT: 81 FINE: 105
   VAG: #: 33 Name: vag_rock2 File: rock2.vag
    VAG SIZE: 1808
    VAG RATE: 12500
    VAG POS: 282624
    VAG LCD FILEPOS: 70784 mod: 1152
 SEQUENCE: #: 152 Name: RSND_ROCK3 
  PATCH: #: 131 Name: pat_rock2 
   MAP: #: 131 Name: map_rock2 
    ROOT: 81 FINE: 105
   VAG: #: 33 Name: vag_rock2 File: rock2.vag
    VAG SIZE: 1808
    VAG RATE: 12500
    VAG POS: 282624
 SEQUENCE: #: 153 Name: RSND_ROCK4 
  PATCH: #: 132 Name: pat_rocks3 
   MAP: #: 132 Name: map_rocks3 
    ROOT: 81 FINE: 105
   VAG: #: 34 Name: vag_rocks3 File: rocks3.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 284672
    VAG LCD FILEPOS: 72592 mod: 912
 SEQUENCE: #: 154 Name: RSND_ROCK5 
  PATCH: #: 132 Name: pat_rocks3 
   MAP: #: 132 Name: map_rocks3 
    ROOT: 81 FINE: 105
   VAG: #: 34 Name: vag_rocks3 File: rocks3.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 284672
 SEQUENCE: #: 155 Name: RSND_BHT1 
  PATCH: #: 133 Name: pat_body1 
   MAP: #: 133 Name: map_body1 
    ROOT: 81 FINE: 105
   VAG: #: 160 Name: vag_body1 File: body1.vag
    VAG SIZE: 2912
    VAG RATE: 12500
    VAG POS: 1329152
    VAG LCD FILEPOS: 75504 mod: 1776
 SEQUENCE: #: 156 Name: RSND_BHT2 
  PATCH: #: 134 Name: pat_body2 
   MAP: #: 134 Name: map_body2 
    ROOT: 81 FINE: 105
   VAG: #: 161 Name: vag_body2 File: body2.vag
    VAG SIZE: 2800
    VAG RATE: 12500
    VAG POS: 1333248
    VAG LCD FILEPOS: 78416 mod: 592
 SEQUENCE: #: 157 Name: RSND_GND1 
  PATCH: #: 135 Name: pat_gudfall2 
   MAP: #: 135 Name: map_gudfall2 
    ROOT: 81 FINE: 105
   VAG: #: 162 Name: vag_gudfall2 File: gudfall2.vag
    VAG SIZE: 2960
    VAG RATE: 12500
    VAG POS: 1337344
    VAG LCD FILEPOS: 81216 mod: 1344
 SEQUENCE: #: 158 Name: RSND_GND2 
  PATCH: #: 135 Name: pat_gudfall2 
   MAP: #: 135 Name: map_gudfall2 
    ROOT: 81 FINE: 105
   VAG: #: 162 Name: vag_gudfall2 File: gudfall2.vag
    VAG SIZE: 2960
    VAG RATE: 12500
    VAG POS: 1337344
 SEQUENCE: #: 159 Name: RSND_GND3 
  PATCH: #: 136 Name: pat_gudfall4 
   MAP: #: 136 Name: map_gudfall4 
    ROOT: 81 FINE: 105
   VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
    VAG SIZE: 4608
    VAG RATE: 12500
    VAG POS: 1341440
    VAG LCD FILEPOS: 84176 mod: 208
 SEQUENCE: #: 160 Name: RSND_GND4 
  PATCH: #: 136 Name: pat_gudfall4 
   MAP: #: 136 Name: map_gudfall4 
    ROOT: 81 FINE: 105
   VAG: #: 163 Name: vag_gudfall4 File: gudfall4.vag
    VAG SIZE: 4608
    VAG RATE: 12500
    VAG POS: 1341440
 SEQUENCE: #: 161 Name: RSND_WHSH1 
  PATCH: #: 12 Name: pat_whoosh1 
   MAP: #: 12 Name: map_whoosh1 
    ROOT: 81 FINE: 104
   VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
    VAG SIZE: 2192
    VAG RATE: 12500
    VAG POS: 92160
 SEQUENCE: #: 162 Name: RSND_WHSH2 
  PATCH: #: 137 Name: pat_whoosh2 
   MAP: #: 137 Name: map_whoosh2 
    ROOT: 83 FINE: 50
   VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
    VAG SIZE: 2688
    VAG RATE: 12500
    VAG POS: 96256
 SEQUENCE: #: 163 Name: RSND_WHSH3 
  PATCH: #: 13 Name: pat_whoosh3 
   MAP: #: 13 Name: map_whoosh3 
    ROOT: 81 FINE: 104
   VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
    VAG SIZE: 2688
    VAG RATE: 12500
    VAG POS: 96256
 SEQUENCE: #: 164 Name: RSND_WHSH4 
  PATCH: #: 138 Name: pat_bwhoosh2 
   MAP: #: 138 Name: map_bwhoosh2 
    ROOT: 84 FINE: 80
   VAG: #: 7 Name: vag_whoosh3 File: whoosh3.vag
    VAG SIZE: 2688
    VAG RATE: 12500
    VAG POS: 96256
 SEQUENCE: #: 165 Name: RSND_BWHSH1 
  PATCH: #: 139 Name: pat_bwhoosh1 
   MAP: #: 139 Name: map_bwhoosh1 
    ROOT: 83 FINE: 50
   VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
    VAG SIZE: 2192
    VAG RATE: 12500
    VAG POS: 92160
 SEQUENCE: #: 166 Name: RSND_BWHSH2 
  PATCH: #: 140 Name: pat_bwhoosh3 
   MAP: #: 140 Name: map_bwhoosh3 
    ROOT: 85 FINE: 70
   VAG: #: 6 Name: vag_whoosh1 File: whoosh1.vag
    VAG SIZE: 2192
    VAG RATE: 12500
    VAG POS: 92160
 SEQUENCE: #: 125 Name: RSND_SPLISH1 
  PATCH: #: 261 Name: pat_splish1 
   MAP: #: 261 Name: map_splish1 
    ROOT: 81 FINE: 105
   VAG: #: 252 Name: vag_splish1 File: splish1.vag
    VAG SIZE: 2352
    VAG RATE: 12500
    VAG POS: 2299904
    VAG LCD FILEPOS: 88784 mod: 720
 SEQUENCE: #: 126 Name: RSND_SPLISH2 
  PATCH: #: 262 Name: pat_splish2 
   MAP: #: 262 Name: map_splish2 
    ROOT: 81 FINE: 105
   VAG: #: 253 Name: vag_splish2 File: splish2.vag
    VAG SIZE: 1920
    VAG RATE: 12500
    VAG POS: 2304000
    VAG LCD FILEPOS: 91136 mod: 1024
 SEQUENCE: #: 127 Name: RSND_SPLISH3 
  PATCH: #: 262 Name: pat_splish2 
   MAP: #: 262 Name: map_splish2 
    ROOT: 81 FINE: 105
   VAG: #: 253 Name: vag_splish2 File: splish2.vag
    VAG SIZE: 1920
    VAG RATE: 12500
    VAG POS: 2304000
 SEQUENCE: #: 434 Name: TS_FATAL_START 
  PATCH: #: 315 Name: pat_fstart 
   MAP: #: 315 Name: map_fstart 
    ROOT: 81 FINE: 105
   VAG: #: 306 Name: vag_fstart File: fatal12.vag
    VAG SIZE: 21344
    VAG RATE: 12500
    VAG POS: 2992128
    VAG LCD FILEPOS: 93056 mod: 896
LOADLIST----TOTAL: Name: RAND_SNDS File: rand.lcd Size: 112352

LOADLIST: Name: HIDDEN_GAME_SNDS File: hidgame.lcd
 SEQUENCE: #: 122 Name: RSND_EBOOM1 
  PATCH: #: 78 Name: pat_fballhit 
   MAP: #: 78 Name: map_fballhit 
    ROOT: 77 FINE: 123
   VAG: #: 38 Name: vag_fballhit File: fballhit.vag
    VAG SIZE: 5408
    VAG RATE: 15624
    VAG POS: 329728
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 123 Name: RSND_EBOOM2 
  PATCH: #: 58 Name: pat_robxplod 
   MAP: #: 58 Name: map_robxplod 
    ROOT: 77 FINE: 123
   VAG: #: 105 Name: vag_robxplod File: robxplod.vag
    VAG SIZE: 14656
    VAG RATE: 15624
    VAG POS: 870400
    VAG LCD FILEPOS: 7456 mod: 1312
 SEQUENCE: #: 124 Name: RSND_EBOOM3 
  PATCH: #: 145 Name: pat_jaxcann 
   MAP: #: 145 Name: map_jaxcann 
    ROOT: 77 FINE: 123
   VAG: #: 164 Name: vag_jaxcann File: jaxcann.vag
    VAG SIZE: 12880
    VAG RATE: 15624
    VAG POS: 1347584
    VAG LCD FILEPOS: 22112 mod: 1632
 SEQUENCE: #: 421 Name: TS_HGAME_FIRE 
  PATCH: #: 79 Name: pat_cabalgun 
   MAP: #: 79 Name: map_cabalgun 
    ROOT: 77 FINE: 123
   VAG: #: 140 Name: vag_cabalgun File: cabalgun.vag
    VAG SIZE: 13568
    VAG RATE: 15624
    VAG POS: 1161216
    VAG LCD FILEPOS: 34992 mod: 176
 SEQUENCE: #: 340 Name: TS_DF_TOASTY 
  PATCH: #: 22 Name: pat_dftosty 
   MAP: #: 22 Name: map_dftosty 
    ROOT: 81 FINE: 105
   VAG: #: 17 Name: vag_dftosty File: dftosty.vag
    VAG SIZE: 4992
    VAG RATE: 12500
    VAG POS: 174080
    VAG LCD FILEPOS: 48560 mod: 1456
 SEQUENCE: #: 398 Name: TS_SK_OMAW 
  PATCH: #: 181 Name: pat_skohmaw 
   MAP: #: 181 Name: map_skohmaw 
    ROOT: 77 FINE: 123
   VAG: #: 200 Name: vag_skohmaw File: skohmaw.vag
    VAG SIZE: 10816
    VAG RATE: 15624
    VAG POS: 1771520
    VAG LCD FILEPOS: 53552 mod: 304
 SEQUENCE: #: 336 Name: ST_SK_PATHETIC 
  PATCH: #: 290 Name: pat_skpathet 
   MAP: #: 290 Name: map_skpathet 
    ROOT: 77 FINE: 123
   VAG: #: 281 Name: vag_skpathet File: skpathet.vag
    VAG SIZE: 20016
    VAG RATE: 15624
    VAG POS: 2535424
    VAG LCD FILEPOS: 64368 mod: 880
LOADLIST----TOTAL: Name: HIDDEN_GAME_SNDS File: hidgame.lcd Size: 82336

LOADLIST: Name: GAMEOVER_SNDS File: gameover.lcd
 SEQUENCE: #: 132 Name: RSND_FS1 
  PATCH: #: 119 Name: pat_foot1 
   MAP: #: 119 Name: map_foot1 
    ROOT: 81 FINE: 105
   VAG: #: 24 Name: vag_foot1 File: foot1.vag
    VAG SIZE: 1536
    VAG RATE: 12500
    VAG POS: 247808
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 133 Name: RSND_FS2 
  PATCH: #: 120 Name: pat_foot2 
   MAP: #: 120 Name: map_foot2 
    ROOT: 81 FINE: 105
   VAG: #: 25 Name: vag_foot2 File: foot2.vag
    VAG SIZE: 1808
    VAG RATE: 12500
    VAG POS: 249856
    VAG LCD FILEPOS: 3584 mod: 1536
 SEQUENCE: #: 134 Name: RSND_FS3 
  PATCH: #: 20 Name: pat_foot3 
   MAP: #: 20 Name: map_foot3 
    ROOT: 81 FINE: 105
   VAG: #: 15 Name: vag_foot3 File: foot3.vag
    VAG SIZE: 1696
    VAG RATE: 12500
    VAG POS: 161792
    VAG LCD FILEPOS: 5392 mod: 1296
 SEQUENCE: #: 135 Name: RSND_FS4 
  PATCH: #: 121 Name: pat_foot4 
   MAP: #: 121 Name: map_foot4 
    ROOT: 81 FINE: 105
   VAG: #: 26 Name: vag_foot4 File: foot4.vag
    VAG SIZE: 1040
    VAG RATE: 12500
    VAG POS: 251904
    VAG LCD FILEPOS: 7088 mod: 944
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 8128 mod: 1984
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 10096 mod: 1904
 SEQUENCE: #: 352 Name: TS_CHURCH_BELL 
  PATCH: #: 3 Name: pat_bell 
   MAP: #: 3 Name: map_bell 
    ROOT: 77 FINE: 123
   VAG: #: 9 Name: vag_bell File: bell.vag
    VAG SIZE: 22480
    VAG RATE: 15624
    VAG POS: 114688
    VAG LCD FILEPOS: 20080 mod: 1648
 SEQUENCE: #: 435 Name: TUNE_GAMEOVER 
  PATCH: #: 317 Name: pat_greenl 
   MAP: #: 317 Name: map_greenl 
    ROOT: 58 FINE: 0
   VAG: #: 308 Name: vag_greenl File: greenl.vag
    VAG SIZE: 255184
    VAG RATE: 44100
    VAG POS: 3221504
    VAG LCD FILEPOS: 42560 mod: 1600
LOADLIST----TOTAL: Name: GAMEOVER_SNDS File: gameover.lcd Size: 295696

LOADLIST: Name: SELECT_SNDS File: select.lcd
 SEQUENCE: #: 352 Name: TS_CHURCH_BELL 
  PATCH: #: 3 Name: pat_bell 
   MAP: #: 3 Name: map_bell 
    ROOT: 77 FINE: 123
   VAG: #: 9 Name: vag_bell File: bell.vag
    VAG SIZE: 22480
    VAG RATE: 15624
    VAG POS: 114688
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 350 Name: TS_ROCK_MOVE 
  PATCH: #: 0 Name: pat_srock 
   MAP: #: 1 Name: map_srock 
    ROOT: 58 FINE: 0
   VAG: #: 0 Name: vag_srock File: srock.vag
    VAG SIZE: 18272
    VAG RATE: 44100
    VAG POS: 0
    VAG LCD FILEPOS: 24528 mod: 2000
 SEQUENCE: #: 351 Name: TS_DRAGON_SPIN 
  PATCH: #: 1 Name: pat_swhoosh 
   MAP: #: 2 Name: map_swhoosh 
    ROOT: 72 FINE: 0
   VAG: #: 1 Name: vag_swhoosh File: swhoosh.vag
    VAG SIZE: 8912
    VAG RATE: 22050
    VAG POS: 18432
    VAG LCD FILEPOS: 42800 mod: 1840
 SEQUENCE: #: 355 Name: TS_P1_CURS 
  PATCH: #: 15 Name: pat_ncurs1 
   MAP: #: 15 Name: map_ncurs1 
    ROOT: 77 FINE: 123
   VAG: #: 10 Name: vag_ncurs1 File: ncurs1.vag
    VAG SIZE: 1872
    VAG RATE: 15624
    VAG POS: 137216
    VAG LCD FILEPOS: 51712 mod: 512
 SEQUENCE: #: 356 Name: TS_P2_CURS 
  PATCH: #: 16 Name: pat_ncurs2 
   MAP: #: 16 Name: map_ncurs2 
    ROOT: 77 FINE: 123
   VAG: #: 11 Name: vag_ncurs2 File: ncurs2.vag
    VAG SIZE: 1872
    VAG RATE: 15624
    VAG POS: 139264
    VAG LCD FILEPOS: 53584 mod: 336
 SEQUENCE: #: 357 Name: TS_P1_PICKED 
  PATCH: #: 17 Name: pat_picked1 
   MAP: #: 17 Name: map_picked1 
    ROOT: 77 FINE: 123
   VAG: #: 12 Name: vag_picked1 File: picked1.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 141312
    VAG LCD FILEPOS: 55456 mod: 160
 SEQUENCE: #: 358 Name: TS_P2_PICKED 
  PATCH: #: 18 Name: pat_picked2 
   MAP: #: 18 Name: map_picked2 
    ROOT: 77 FINE: 123
   VAG: #: 13 Name: vag_picked2 File: picked2.vag
    VAG SIZE: 7936
    VAG RATE: 15624
    VAG POS: 151552
    VAG LCD FILEPOS: 63808 mod: 320
 SEQUENCE: #: 375 Name: TS_MAP_ZOOM 
  PATCH: #: 21 Name: pat_mrtlwhsh 
   MAP: #: 21 Name: map_mrtlwhsh 
    ROOT: 77 FINE: 123
   VAG: #: 16 Name: vag_mrtlwhsh File: mrtlwhsh.vag
    VAG SIZE: 10208
    VAG RATE: 15624
    VAG POS: 163840
    VAG LCD FILEPOS: 71744 mod: 64
 SEQUENCE: #: 211 Name: ST_SWAT_BANG 
  PATCH: #: 123 Name: pat_block1 
   MAP: #: 123 Name: map_block1 
    ROOT: 81 FINE: 105
   VAG: #: 28 Name: vag_block1 File: block1.vag
    VAG SIZE: 1968
    VAG RATE: 12500
    VAG POS: 260096
    VAG LCD FILEPOS: 81952 mod: 32
 SEQUENCE: #: 218 Name: ST_IND_BURN_AXE 
  PATCH: #: 107 Name: pat_fireloop 
   MAP: #: 107 Name: map_fireloop 
    ROOT: 81 FINE: 105
   VAG: #: 148 Name: vag_fireloop File: fireloop.vag
    VAG SIZE: 9984
    VAG RATE: 12500
    VAG POS: 1226752
    VAG LCD FILEPOS: 83920 mod: 2000
 SEQUENCE: #: 359 Name: TS_SK_KANO 
  PATCH: #: 154 Name: pat_skkano 
   MAP: #: 154 Name: map_skkano 
    ROOT: 77 FINE: 123
   VAG: #: 173 Name: vag_skkano File: skkano.vag
    VAG SIZE: 8896
    VAG RATE: 15624
    VAG POS: 1454080
    VAG LCD FILEPOS: 93904 mod: 1744
 SEQUENCE: #: 360 Name: TS_SK_SONYA 
  PATCH: #: 155 Name: pat_sksonya 
   MAP: #: 155 Name: map_sksonya 
    ROOT: 65 FINE: 123
   VAG: #: 174 Name: vag_sksonya File: sksonya.vag
    VAG SIZE: 18976
    VAG RATE: 31250
    VAG POS: 1464320
    VAG LCD FILEPOS: 102800 mod: 400
 SEQUENCE: #: 361 Name: TS_SK_JAX 
  PATCH: #: 156 Name: pat_skjax 
   MAP: #: 156 Name: map_skjax 
    ROOT: 77 FINE: 123
   VAG: #: 175 Name: vag_skjax File: skjax.vag
    VAG SIZE: 9648
    VAG RATE: 15624
    VAG POS: 1484800
    VAG LCD FILEPOS: 121776 mod: 944
 SEQUENCE: #: 362 Name: TS_SK_NIGHTWOLF 
  PATCH: #: 157 Name: pat_sknitwlf 
   MAP: #: 157 Name: map_sknitwlf 
    ROOT: 77 FINE: 123
   VAG: #: 176 Name: vag_sknitwlf File: sknitwlf.vag
    VAG SIZE: 12960
    VAG RATE: 15624
    VAG POS: 1495040
    VAG LCD FILEPOS: 131424 mod: 352
 SEQUENCE: #: 363 Name: TS_SK_SUBZERO 
  PATCH: #: 158 Name: pat_sksubz 
   MAP: #: 158 Name: map_sksubz 
    ROOT: 77 FINE: 123
   VAG: #: 177 Name: vag_sksubz File: sksubz.vag
    VAG SIZE: 10688
    VAG RATE: 15624
    VAG POS: 1509376
    VAG LCD FILEPOS: 144384 mod: 1024
 SEQUENCE: #: 364 Name: TS_SK_STRYKER 
  PATCH: #: 159 Name: pat_skstrkr2 
   MAP: #: 159 Name: map_skstrkr2 
    ROOT: 77 FINE: 123
   VAG: #: 178 Name: vag_skstrkr2 File: skstrkr2.vag
    VAG SIZE: 8688
    VAG RATE: 15624
    VAG POS: 1521664
    VAG LCD FILEPOS: 155072 mod: 1472
 SEQUENCE: #: 365 Name: TS_SK_SINDEL 
  PATCH: #: 160 Name: pat_sksindel 
   MAP: #: 160 Name: map_sksindel 
    ROOT: 77 FINE: 123
   VAG: #: 179 Name: vag_sksindel File: sksindel.vag
    VAG SIZE: 10480
    VAG RATE: 15624
    VAG POS: 1531904
    VAG LCD FILEPOS: 163760 mod: 1968
 SEQUENCE: #: 366 Name: TS_SK_SEKTOR 
  PATCH: #: 161 Name: pat_sksector 
   MAP: #: 161 Name: map_sksector 
    ROOT: 77 FINE: 123
   VAG: #: 180 Name: vag_sksector File: sksector.vag
    VAG SIZE: 10912
    VAG RATE: 15624
    VAG POS: 1544192
    VAG LCD FILEPOS: 174240 mod: 160
 SEQUENCE: #: 367 Name: TS_SK_CYRAX 
  PATCH: #: 162 Name: pat_skcyrax 
   MAP: #: 162 Name: map_skcyrax 
    ROOT: 77 FINE: 123
   VAG: #: 181 Name: vag_skcyrax File: skcyrax.vag
    VAG SIZE: 11168
    VAG RATE: 15624
    VAG POS: 1556480
    VAG LCD FILEPOS: 185152 mod: 832
 SEQUENCE: #: 368 Name: TS_SK_LAO 
  PATCH: #: 163 Name: pat_skknglao 
   MAP: #: 163 Name: map_skknglao 
    ROOT: 77 FINE: 123
   VAG: #: 182 Name: vag_skknglao File: skknglao.vag
    VAG SIZE: 10736
    VAG RATE: 15624
    VAG POS: 1568768
    VAG LCD FILEPOS: 196320 mod: 1760
 SEQUENCE: #: 369 Name: TS_SK_KABAL 
  PATCH: #: 164 Name: pat_skcabal 
   MAP: #: 164 Name: map_skcabal 
    ROOT: 77 FINE: 123
   VAG: #: 183 Name: vag_skcabal File: skcabal.vag
    VAG SIZE: 9984
    VAG RATE: 15624
    VAG POS: 1581056
    VAG LCD FILEPOS: 207056 mod: 208
 SEQUENCE: #: 370 Name: TS_SK_SHEEVA 
  PATCH: #: 165 Name: pat_sksheeva 
   MAP: #: 165 Name: map_sksheeva 
    ROOT: 77 FINE: 123
   VAG: #: 184 Name: vag_sksheeva File: sksheeva.vag
    VAG SIZE: 8560
    VAG RATE: 15624
    VAG POS: 1591296
    VAG LCD FILEPOS: 217040 mod: 2000
 SEQUENCE: #: 371 Name: TS_SK_ST 
  PATCH: #: 166 Name: pat_skshang 
   MAP: #: 166 Name: map_skshang 
    ROOT: 77 FINE: 123
   VAG: #: 185 Name: vag_skshang File: skshang.vag
    VAG SIZE: 12256
    VAG RATE: 15624
    VAG POS: 1601536
    VAG LCD FILEPOS: 225600 mod: 320
 SEQUENCE: #: 372 Name: TS_SK_LKANG 
  PATCH: #: 167 Name: pat_skliukng 
   MAP: #: 167 Name: map_skliukng 
    ROOT: 77 FINE: 123
   VAG: #: 186 Name: vag_skliukng File: skliukng.vag
    VAG SIZE: 10688
    VAG RATE: 15624
    VAG POS: 1613824
    VAG LCD FILEPOS: 237856 mod: 288
 SEQUENCE: #: 374 Name: TS_SK_SMOKE 
  PATCH: #: 169 Name: pat_sksmoke 
   MAP: #: 169 Name: map_sksmoke 
    ROOT: 77 FINE: 123
   VAG: #: 188 Name: vag_sksmoke File: sksmoke.vag
    VAG SIZE: 8288
    VAG RATE: 15624
    VAG POS: 1638400
    VAG LCD FILEPOS: 248544 mod: 736
 SEQUENCE: #: 376 Name: TS_SK_EXCELLENT 
  PATCH: #: 189 Name: pat_skexelnt 
   MAP: #: 189 Name: map_skexelnt 
    ROOT: 77 FINE: 123
   VAG: #: 206 Name: vag_skexelnt File: skexelnt.vag
    VAG SIZE: 11136
    VAG RATE: 15624
    VAG POS: 1853440
    VAG LCD FILEPOS: 256832 mod: 832
 SEQUENCE: #: 377 Name: TS_SK_NEVER_WIN 
  PATCH: #: 190 Name: pat_sknvrwin 
   MAP: #: 190 Name: map_sknvrwin 
    ROOT: 77 FINE: 123
   VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
    VAG SIZE: 18112
    VAG RATE: 15624
    VAG POS: 1865728
    VAG LCD FILEPOS: 267968 mod: 1728
 SEQUENCE: #: 197 Name: ST_SZ_ICE_UP 
  PATCH: #: 193 Name: pat_sziceup 
   MAP: #: 193 Name: map_sziceup 
    ROOT: 77 FINE: 123
   VAG: #: 209 Name: vag_sziceup File: sziceup.vag
    VAG SIZE: 11856
    VAG RATE: 15624
    VAG POS: 1900544
    VAG LCD FILEPOS: 286080 mod: 1408
 SEQUENCE: #: 436 Name: TUNE_SELECT 
  PATCH: #: 316 Name: pat_selectl 
   MAP: #: 316 Name: map_selectl 
    ROOT: 58 FINE: 0
   VAG: #: 307 Name: vag_selectl File: selectl.vag
    VAG SIZE: 205088
    VAG RATE: 44100
    VAG POS: 3014656
    VAG LCD FILEPOS: 297936 mod: 976
LOADLIST----TOTAL: Name: SELECT_SNDS File: select.lcd Size: 500976

LOADLIST: Name: LADDER_SNDS File: ladder.lcd
 SEQUENCE: #: 377 Name: TS_SK_NEVER_WIN 
  PATCH: #: 190 Name: pat_sknvrwin 
   MAP: #: 190 Name: map_sknvrwin 
    ROOT: 77 FINE: 123
   VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
    VAG SIZE: 18112
    VAG RATE: 15624
    VAG POS: 1865728
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 378 Name: TS_SK_OUTSTANDING 
  PATCH: #: 191 Name: pat_skoutstd 
   MAP: #: 191 Name: map_skoutstd 
    ROOT: 77 FINE: 123
   VAG: #: 208 Name: vag_skoutstd File: skoutstd.vag
    VAG SIZE: 14752
    VAG RATE: 15624
    VAG POS: 1884160
    VAG LCD FILEPOS: 20160 mod: 1728
 SEQUENCE: #: 376 Name: TS_SK_EXCELLENT 
  PATCH: #: 189 Name: pat_skexelnt 
   MAP: #: 189 Name: map_skexelnt 
    ROOT: 77 FINE: 123
   VAG: #: 206 Name: vag_skexelnt File: skexelnt.vag
    VAG SIZE: 11136
    VAG RATE: 15624
    VAG POS: 1853440
    VAG LCD FILEPOS: 34912 mod: 96
 SEQUENCE: #: 415 Name: TS_SK_CHOOSE 
  PATCH: #: 297 Name: pat_skdestin 
   MAP: #: 297 Name: map_skdestin 
    ROOT: 77 FINE: 123
   VAG: #: 288 Name: vag_skdestin File: skdestin.vag
    VAG SIZE: 17952
    VAG RATE: 15624
    VAG POS: 2623488
    VAG LCD FILEPOS: 46048 mod: 992
 SEQUENCE: #: 375 Name: TS_MAP_ZOOM 
  PATCH: #: 21 Name: pat_mrtlwhsh 
   MAP: #: 21 Name: map_mrtlwhsh 
    ROOT: 77 FINE: 123
   VAG: #: 16 Name: vag_mrtlwhsh File: mrtlwhsh.vag
    VAG SIZE: 10208
    VAG RATE: 15624
    VAG POS: 163840
    VAG LCD FILEPOS: 64000 mod: 512
 SEQUENCE: #: 357 Name: TS_P1_PICKED 
  PATCH: #: 17 Name: pat_picked1 
   MAP: #: 17 Name: map_picked1 
    ROOT: 77 FINE: 123
   VAG: #: 12 Name: vag_picked1 File: picked1.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 141312
    VAG LCD FILEPOS: 74208 mod: 480
 SEQUENCE: #: 355 Name: TS_P1_CURS 
  PATCH: #: 15 Name: pat_ncurs1 
   MAP: #: 15 Name: map_ncurs1 
    ROOT: 77 FINE: 123
   VAG: #: 10 Name: vag_ncurs1 File: ncurs1.vag
    VAG SIZE: 1872
    VAG RATE: 15624
    VAG POS: 137216
    VAG LCD FILEPOS: 82560 mod: 640
 SEQUENCE: #: 433 Name: TS_ONEPLAY_END 
  PATCH: #: 314 Name: pat_onep 
   MAP: #: 314 Name: map_onep 
    ROOT: 58 FINE: 0
   VAG: #: 305 Name: vag_onep File: onep.vag
    VAG SIZE: 71152
    VAG RATE: 44100
    VAG POS: 2920448
    VAG LCD FILEPOS: 84432 mod: 464
LOADLIST----TOTAL: Name: LADDER_SNDS File: ladder.lcd Size: 153536

LOADLIST: Name: VS_SNDS File: vs.lcd
 SEQUENCE: #: 375 Name: TS_MAP_ZOOM 
  PATCH: #: 21 Name: pat_mrtlwhsh 
   MAP: #: 21 Name: map_mrtlwhsh 
    ROOT: 77 FINE: 123
   VAG: #: 16 Name: vag_mrtlwhsh File: mrtlwhsh.vag
    VAG SIZE: 10208
    VAG RATE: 15624
    VAG POS: 163840
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 437 Name: TUNE_VERSUS 
  PATCH: #: 318 Name: pat_mrtlsans 
   MAP: #: 318 Name: map_mrtlsans 
    ROOT: 58 FINE: 0
   VAG: #: 309 Name: vag_mrtlsans File: mrtlsans.vag
    VAG SIZE: 76896
    VAG RATE: 44100
    VAG POS: 3477504
    VAG LCD FILEPOS: 12256 mod: 2016
LOADLIST----TOTAL: Name: VS_SNDS File: vs.lcd Size: 87104

LOADLIST: Name: BUYIN_SNDS File: buyin.lcd
 SEQUENCE: #: 353 Name: TS_BIGDOOR_CLOSE 
  PATCH: #: 9 Name: pat_doorslid 
   MAP: #: 9 Name: map_doorslid 
    ROOT: 77 FINE: 123
   VAG: #: 3 Name: vag_doorslid File: doorslid.vag
    VAG SIZE: 12336
    VAG RATE: 15624
    VAG POS: 34816
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 354 Name: TS_BIGDOOR_SLAM 
  PATCH: #: 10 Name: pat_newdoor 
   MAP: #: 10 Name: map_newdoor 
    ROOT: 77 FINE: 123
   VAG: #: 4 Name: vag_newdoor File: newdoor.vag
    VAG SIZE: 13088
    VAG RATE: 15624
    VAG POS: 49152
    VAG LCD FILEPOS: 14384 mod: 48
 SEQUENCE: #: 357 Name: TS_P1_PICKED 
  PATCH: #: 17 Name: pat_picked1 
   MAP: #: 17 Name: map_picked1 
    ROOT: 77 FINE: 123
   VAG: #: 12 Name: vag_picked1 File: picked1.vag
    VAG SIZE: 8352
    VAG RATE: 15624
    VAG POS: 141312
    VAG LCD FILEPOS: 27472 mod: 848
 SEQUENCE: #: 355 Name: TS_P1_CURS 
  PATCH: #: 15 Name: pat_ncurs1 
   MAP: #: 15 Name: map_ncurs1 
    ROOT: 77 FINE: 123
   VAG: #: 10 Name: vag_ncurs1 File: ncurs1.vag
    VAG SIZE: 1872
    VAG RATE: 15624
    VAG POS: 137216
    VAG LCD FILEPOS: 35824 mod: 1008
 SEQUENCE: #: 386 Name: TS_DF_FROSTY 
  PATCH: #: 23 Name: pat_dfrosty 
   MAP: #: 23 Name: map_dfrosty 
    ROOT: 77 FINE: 123
   VAG: #: 18 Name: vag_dfrosty File: dfrosty.vag
    VAG SIZE: 7056
    VAG RATE: 15624
    VAG POS: 180224
    VAG LCD FILEPOS: 37696 mod: 832
LOADLIST----TOTAL: Name: BUYIN_SNDS File: buyin.lcd Size: 42704

LOADLIST: Name: SMOKE_CODE_SNDS File: smokecod.lcd
 SEQUENCE: #: 385 Name: TS_SK_ITS_OFFICIAL 
  PATCH: #: 310 Name: pat_skyousuk 
   MAP: #: 310 Name: map_skyousuk 
    ROOT: 77 FINE: 123
   VAG: #: 301 Name: vag_skyousuk File: skyousuk.vag
    VAG SIZE: 12704
    VAG RATE: 15624
    VAG POS: 2844672
    VAG LCD FILEPOS: 2048 mod: 0
 SEQUENCE: #: 348 Name: TS_EXP_ST1 
  PATCH: #: 117 Name: pat_strtboom 
   MAP: #: 117 Name: map_strtboom 
    ROOT: 81 FINE: 105
   VAG: #: 158 Name: vag_strtboom File: strtboom.vag
    VAG SIZE: 13488
    VAG RATE: 12500
    VAG POS: 1300480
    VAG LCD FILEPOS: 14752 mod: 416
 SEQUENCE: #: 349 Name: TS_EXP_ST2 
  PATCH: #: 117 Name: pat_strtboom 
   MAP: #: 117 Name: map_strtboom 
    ROOT: 81 FINE: 105
   VAG: #: 158 Name: vag_strtboom File: strtboom.vag
    VAG SIZE: 13488
    VAG RATE: 12500
    VAG POS: 1300480
 SEQUENCE: #: 5 Name: GS_DEATH_ROBO 
  PATCH: #: 45 Name: pat_jhdeath 
   MAP: #: 45 Name: map_jhdeath 
    ROOT: 82 FINE: 68
   VAG: #: 87 Name: vag_jhdeath File: jhdeath.vag
    VAG SIZE: 16000
    VAG RATE: 12000
    VAG POS: 739328
    VAG LCD FILEPOS: 28240 mod: 1616
 SEQUENCE: #: 336 Name: ST_SK_PATHETIC 
  PATCH: #: 290 Name: pat_skpathet 
   MAP: #: 290 Name: map_skpathet 
    ROOT: 77 FINE: 123
   VAG: #: 281 Name: vag_skpathet File: skpathet.vag
    VAG SIZE: 20016
    VAG RATE: 15624
    VAG POS: 2535424
    VAG LCD FILEPOS: 44240 mod: 1232
 SEQUENCE: #: 337 Name: ST_SK_NEVER_WIN 
  PATCH: #: 190 Name: pat_sknvrwin 
   MAP: #: 190 Name: map_sknvrwin 
    ROOT: 77 FINE: 123
   VAG: #: 207 Name: vag_sknvrwin File: sknvrwin.vag
    VAG SIZE: 18112
    VAG RATE: 15624
    VAG POS: 1865728
    VAG LCD FILEPOS: 64256 mod: 768
 SEQUENCE: #: 338 Name: ST_SK_STILL_TRYING 
  PATCH: #: 291 Name: pat_sktrying 
   MAP: #: 291 Name: map_sktrying 
    ROOT: 77 FINE: 123
   VAG: #: 282 Name: vag_sktrying File: sktrying.vag
    VAG SIZE: 19200
    VAG RATE: 15624
    VAG POS: 2555904
    VAG LCD FILEPOS: 82368 mod: 448
 SEQUENCE: #: 339 Name: ST_SK_DONT_LAUGH 
  PATCH: #: 292 Name: pat_skdntlaf 
   MAP: #: 292 Name: map_skdntlaf 
    ROOT: 77 FINE: 123
   VAG: #: 283 Name: vag_skdntlaf File: skdntlaf.vag
    VAG SIZE: 14176
    VAG RATE: 15624
    VAG POS: 2576384
    VAG LCD FILEPOS: 101568 mod: 1216
 SEQUENCE: #: 378 Name: TS_SK_OUTSTANDING 
  PATCH: #: 191 Name: pat_skoutstd 
   MAP: #: 191 Name: map_skoutstd 
    ROOT: 77 FINE: 123
   VAG: #: 208 Name: vag_skoutstd File: skoutstd.vag
    VAG SIZE: 14752
    VAG RATE: 15624
    VAG POS: 1884160
    VAG LCD FILEPOS: 115744 mod: 1056
LOADLIST----TOTAL: Name: SMOKE_CODE_SNDS File: smokecod.lcd Size: 128448


