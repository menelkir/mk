/*
 * $PSLibId: Runtime Library Versin 3.0$
 */
/*
 *			オンメモリSTR データビューア
 *	
 *	メインメモリ上に展開された STR 動画データをプレビューする。
 *	MOVIE コンバータで作成された STR データをそのまま見ることができる。
 *	
 *
 *		Copyright (C) 1993 by Sony Corporation
 *			All rights Reserved
 *
 *	 Version	Date		Design
 *	-----------------------------------------
 *	1.00		Jul,08,1994	suzu
 */

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#define RGB24
#ifdef RGB24
#define PPW	3/2	/* １ショートワードに何ピクセルあるか(Pixel Per Word)*/
#define IPPW	2/3	/* 1/PPW */
#define MODE	1	/* 24bit モードでデコード */
#else
#define PPW	1	/* １ショートワードに何ピクセルあるか */
#define IPPW	1	/* 1/PPW */
#define MODE	0	/* 16bit モードでデコード */
#endif

/*
 *	デコード環境
 */
typedef struct {
	u_long	*vlcbuf[2];	/* VLC バッファ（ダブルバッファ） */
	int	vlcid;		/* 現在 VLC デコード中バッファの ID */
	u_short	*imgbuf;	/* デコード画像バッファ（シングル）*/	
	RECT	rect[2];	/* 転送エリア（ダブルバッファ） */
	int	rectid;		/* 現在転送中のバッファ ID */
	RECT	slice;		/* １回の DecDCTout で取り出す領域 */
	int	d_wid;		/* 表示領域の幅 */
} DECENV;

static DECENV		dec;		/* デコード環境の実体 */
static volatile int	isEndOfFlame;	/* フレームの最後になると 1 になる */

/*
 *	フォアグラウンドプロセス
 */	
u_long	vlcbuf0[256*256];	/* 大きさ適当 */
u_long	vlcbuf1[256*256];	/* 大きさ適当 */
u_short	imgbuf[16*PPW*240*2];	/* 短柵１個 */

static int movie(void);
static int strSync(int mode);
static void strCallback(void);
static void strRewind(DECENV *dec);
static u_long *strNext(void);

main()
{
	PadInit(0);
	ResetGraph(0);		/* GPU をリセット */
	SetGraphDebug(0);	/* デバッグレベル設定 */
	SetDispMask(1);		/* 表示許可 */
	
	/* フレームバッファをクリア */
	{
		RECT	clear;
		setRECT(&clear, 0, 0, 640, 480);
		ClearImage(&clear, 0, 0, 0);
	}
	
	/* フォントロード */
	FntLoad(960, 256);
	SetDumpFnt(FntOpen(16, 16, 256, 16, 1, 512));

	/* デコード構造体に値を設定 */
	dec.vlcbuf[0] = vlcbuf0;
	dec.vlcbuf[1] = vlcbuf1;
	dec.vlcid     = 0;
	dec.imgbuf    = imgbuf;
	dec.rectid    = 0;

	/* ストリーミングスタート */
	while (movie() == 0);

	/* 終了 */
	PadStop();
	exit();
}

static int movie(void)
{
	DISPENV	disp;
	DRAWENV	draw;
	
	int	id, padd;
	u_long	*next;
	
	DecDCTReset(0);			/* MDEC をリセット */
	isEndOfFlame = 0;		/* 旗を下げる */
	strRewind(&dec);		/* フレームを巻き戻す */
	DecDCToutCallback(strCallback);/* コールバックを定義する */
	
	/* まず最初の VLC を解く */
	DecDCTvlc(strNext(), dec.vlcbuf[dec.vlcid]);
	
	while (1) {

		/* VLC の完了したデータを送信 */
		DecDCTin(dec.vlcbuf[dec.vlcid], MODE);
		
		/* 最初の短柵の受信の準備をする */
		/* ２発目からは、callback() 内で行なう */
		DecDCTout(dec.imgbuf, dec.slice.w*dec.slice.h/2);

		/* ID をスワップ */
		dec.vlcid = dec.vlcid? 0: 1;

		/* 次のVLC を解く */
		if ((next = strNext()) == 0)
			return(0);

		DecDCTvlc(next, dec.vlcbuf[dec.vlcid]);

		/*		・・・			*/
		/* ここにアプリケーションのコードが入る */
		/*		・・・			*/
		if (PadRead(1) & PADk)
			return(-1);
		
		/* データが出来上がるのを待つ */
		if (strSync(0) != 0)
			return(-1);
		
		/* V-BLNK を待つ */
		VSync(0);
		
		/* 表示バッファをスワップ */
		/* 表示バッファは、転送バッファの反対側なことに注意 */
		id = dec.rectid? 0: 1;
		
		/* 描画環をスワップ */
		SetDefDispEnv(&disp, 0, id==0? 0:240, dec.d_wid, 240);
		SetDefDrawEnv(&draw, 0, id==0? 0:240, dec.d_wid, 240);
#ifdef RGB24
		disp.isrgb24 = 1;
#endif		
		PutDispEnv(&disp);
		PutDrawEnv(&draw);
		FntFlush(-1);
	}
}

static int strSync(int mode)
{
	int	cnt = WAIT_TIME;
	while (isEndOfFlame == 0) 
		 if (cnt-- == 0)
			 return(-1);
	isEndOfFlame = 0;
	return(0);
}

/*
 * バックグラウンドプロセス 
 * (DecDCTout() が終った時に呼ばれるコールバック関数)
 */
static void strCallback(void)
{
	/* デコード結果をフレームバッファに転送 */
	LoadImage(&dec.slice, (u_long *)dec.imgbuf);
	
	/* 短柵矩形領域をひとつ右に更新 */
	dec.slice.x += 16*PPW;

	/* まだ足りなければ、*/
	if (dec.slice.x < dec.rect[dec.rectid].x + dec.rect[dec.rectid].w) {
		/* 次の短柵を受信 */
		DecDCTout(dec.imgbuf, dec.slice.w*dec.slice.h/2);
	}
	/* １フレーム分終ったら、*/
	else {
		/* 終ったことを通知 */
		isEndOfFlame = 1;
		
		/* ID を更新 */
		dec.rectid  = dec.rectid? 0: 1;
		dec.slice.x = dec.rect[dec.rectid].x;
		dec.slice.y = dec.rect[dec.rectid].y;
	}
}		

/*
 *	次のストリーミングデータを読み込む（本当は CD-ROM から来る）
 */
#define STRADDR		(CDSECTOR *)0xa0200000	/* STR データアドレス */
#define USRSIZE		(512-8)		/* セクタ当たりのデータサイズ */

typedef struct {
	u_short	id;			/* always 0x0x0160 */
	u_short	type;			
	u_short	secCount;	
	u_short	nSectors;
	u_long	frameCount;
	u_long	frameSize;
	
	u_short	width;
	u_short	height;
	u_char	reserved[12];
	
	u_long	data[USRSIZE];		/* ユーザデータ */
} CDSECTOR;				/* CD-ROM STR 構造体 */

CDSECTOR	*Sector;		/* 現在のセクタの位置 */

static void strRewind(DECENV *dec)
{
	/* ポインタの巻き戻し */
	Sector = STRADDR;
		
	/* ミニヘッダに合わせてデコード環境を変更する */
	/* 描画環境 */
	if (Sector->width <= 256)	dec->d_wid = 256;
	else if (Sector->width <= 320)	dec->d_wid = 320;
	else if (Sector->width <= 512)	dec->d_wid = 512;
	else 				dec->d_wid = 640;
		
	/* 転送領域 */
	dec->rect[0].x = dec->rect[1].x = (dec->d_wid-Sector->width)/2;
	dec->rect[0].y = (240-Sector->height)/2;
	dec->rect[1].y = (240-Sector->height)/2 + 240;
	
	dec->rect[0].w = dec->rect[1].w = Sector->width*PPW;
	dec->rect[0].h = dec->rect[1].h = Sector->height;

	setRECT(&dec->slice,
		dec->rect[0].x, dec->rect[0].y, 16*PPW, Sector->height);
}

static u_long *strNext(void)
{
	static u_long data[20*USRSIZE];	/* ミニヘッダを取り除いたデータ */
	
	int	i, j, len;
	u_long	*sp, *dp;
	
#ifndef RGB24		
	FntPrint("%d: %d sect,(%d,%d)\n",
	       Sector->frameCount, Sector->nSectors,
	       Sector->width, Sector->height);
#endif	
	/* ミニヘッダを取り除いたユーザデータを作成する */
	dp  = data;
	len = Sector->nSectors;
	while (len--) {
		/* 意味のあるヘッダかを確かめる。*/
		if (Sector->id != 0x0160) 
			return(0);
		
		/* データをコピー */
		for (sp = Sector->data, i = 0; i < USRSIZE; i++) 
			*dp++ = *sp++;
		Sector++;
	}
	return((u_long *)data);
}
