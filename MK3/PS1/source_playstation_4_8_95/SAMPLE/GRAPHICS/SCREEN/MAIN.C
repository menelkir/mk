/*
 * $PSLibId: Runtime Library Versin 3.0$
 */
/*			screen: VRAM viewer
 *
 *		Copyright (C) 1993 by Sony Corporation
 *			All rights Reserved
 *
 *	 Version	Date		Design
 *	-----------------------------------------	
 *	1.00		Sep,30,1994	suzu
 */
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include "libgpu.h"

main()
{
	DISPENV	disp;
	u_long	padd;
	int	i;
	
	PadInit(0);
	ResetGraph(0);
	SetDefDispEnv(&disp, 0, 0,  640,  480);
	
	disp.screen.x = 0;
	disp.screen.y = 0;
	disp.screen.w = 255;
	disp.screen.h = 255;

	PutDispEnv(&disp);
	SetDispMask(1);
	
	while (((padd = PadRead(1))&PADk) == 0) {
		
		if (padd & PADl)	disp.screen.w -= 2;
		if (padd & PADm)	disp.screen.w += 2;
		if (padd & PADn)	disp.screen.h -= 2;
		if (padd & PADo)	disp.screen.h += 2;
		
		if (padd & PADLleft)	disp.disp.x -= 2;
		if (padd & PADLright)	disp.disp.x += 2;
		if (padd & PADLup)	disp.disp.y -= 2;
		if (padd & PADLdown)	disp.disp.y += 2;
		
		if (padd & PADRleft)	disp.screen.x -= 2;
		if (padd & PADRright)	disp.screen.x += 2;
		if (padd & PADRup)	disp.screen.y -= 2;
		if (padd & PADRdown)	disp.screen.y += 2;
		
		VSync(0);
		PutDispEnv(&disp);
		/* DumpDispEnv(&disp);*/
	}
	PadStop();		
	return(0);
}

