/*
 * $PSLibId: Runtime Library Versin 3.0$
 */
/*
 *	header for mesh handler
 */

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

typedef struct {
	SVECTOR	x3;		/* 3D point */
	SVECTOR	x2;		/* 2D point (after perspective trans.) */
	/*int	sz;		/* otz */
} MESH;
