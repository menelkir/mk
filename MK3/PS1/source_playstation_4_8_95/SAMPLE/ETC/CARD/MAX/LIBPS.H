/*
 * $PSLibId: Runtime Library Versin 3.0$
 */
#ifndef _LIBPS_H_
#define _LIBPS_H_

/*****************************************************************
 *
 * file: libps.h
 *
 * 	Copyright (C) 1994 by Sony Computer Entertainment Inc.
 *				          All Rights Reserved.
 *
 *	Sony Computer Entertainment Inc. Development Department
 *
 *****************************************************************/

#include <sys/types.h>
/*
#include <asm.h>
#include <kernel.h>
*/
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <libgs.h>

#endif /* _LIBPS_H_ */
/* DON'T ADD STUFF AFTER THIS */

