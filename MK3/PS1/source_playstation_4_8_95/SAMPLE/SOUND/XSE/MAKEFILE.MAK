all:
	ccpsx -g -Xo$80100000 -Xm main.c -omain.cpe,main.sym,main.map
load:
	pqbload simple.vh 80010000
	pqbload simple.vb 80015000

clean:
	del *.?~
	del *.??~
	del *.cpe
	del *.sym
	del *.map
